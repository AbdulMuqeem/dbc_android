package com.app.digilink.managers.retrofit;

import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.models.wrappers.WebResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by  on 09-Mar-17.
 */

public interface WebServiceProxy {


   /* @Multipart
    @POST("./")
    Call<WebResponse<Object>> webServiceRequestAPIForWebResponseAnyObject(
            @Part(WebServiceConstants.PARAMS_REQUEST_METHOD) RequestBody requestMethod,
            @Part(WebServiceConstants.PARAMS_REQUEST_DATA) RequestBody requestData
    );


    @Headers(WebServiceConstants.WS_KEY_GET_REQUESTOR)
    @GET(WebServiceConstants.WS_KEY_GET_TOKEN)
    Call<String> getToken();
*/

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_EDIT_USER)
    Call<WebResponse<Object>> editUser(
            @Field("first_name") String fullName,
            @Field("last_name") String lastName,
            @Field("email") String email,
            @Field("phone") String phone,
            @Field("address") String address,
            @Field("occupation") String occupation,
            @Field("notes") String notes,
            @Field("user_id") int user_id,
            @Field("card_id") int card_id,
            @Field("social_icon") String social_icon,
            @Field("font") String font,
            @Field("size") String size,
            @Field("name_color") String name_color,
            @Field("occupation_color") String occupation_color,
            @Field("phone_color") String phone_color,
            @Field("email_color") String email_color,
            @Field("address_color") String address_color,
            @Field("notes_color") String notes_color,
            @Field("card_order") String card_order,

            @Field("name_font") String name_font,
            @Field("occupation_font") String occupation_font,
            @Field("phone_font") String phone_font,
            @Field("email_font") String email_font,
            @Field("address_font") String address_font,
            @Field("notes_font") String notes_font,


            @Field("name_size") String name_size,
            @Field("occupation_size") String occupation_size,
            @Field("phone_size") String phone_size,
            @Field("email_size") String email_size,
            @Field("address_size") String address_size,
            @Field("notes_size") String notes_size,

            @Field("card_design") String card_design,
            @Field("p_email") String p_email,
            @Field("facebook") String facebook,
            @Field("instagram") String instagram,
            @Field("gmail") String gmail,
            @Field("twitter") String twitter,
            @Field("linkedin") String linkedin


    );


    @Multipart
    @POST(WebServiceConstants.METHOD_REGISTERATION)
    Call<WebResponse<Object>> registeration(
            @Part("first_name") RequestBody fullName,
            @Part("last_name") RequestBody lastName,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("address") RequestBody address,
            @Part("company_name") RequestBody company_name,
            @Part("occupation") RequestBody occupation,
            @Part("social") RequestBody social,
            @Part("password") RequestBody password,
            @Part("card_type") RequestBody card_type,
            @Part("card_design") RequestBody card_design,
            @Part("card_status") RequestBody card_status,
            @Part("social_id") RequestBody social_id,
            @Part("notes") RequestBody note,
            @Part("platform") RequestBody platform,
            @Part("devicetoken") RequestBody devicetoken,
            @Part("ios_device_token") RequestBody ios_device_token,
            @Part("lng") RequestBody longitute,
            @Part("lat") RequestBody latitue,
            @Part MultipartBody.Part picture,
            @Part("facebook") RequestBody facebook,
            @Part("instagram") RequestBody instagram,
            @Part("gmail") RequestBody gmail,
            @Part("twitter") RequestBody twitter,
            @Part("linkedin") RequestBody linkedin
    );


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_WALLET)
    Call<WebResponse<Object>> walletCards(
            @Field("user_id") int userid
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_ALL_CARDS)
    Call<WebResponse<Object>> allCards(
            @Field("user_id") int userid
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_SAVE_CARDS)
    Call<WebResponse<Object>> allSaveCards(
            @Field("user_id") int userid,
            @Field("card_holder_id") int card_holder_id
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_ONLINEOFFLINE_CARD)
    Call<WebResponse<Object>> getOfflineOnline(
            @Field("user_id") int userid,
            @Field("card_id") int card_id,
            @Field("status") String status
    );

    @POST(WebServiceConstants.METHOD_TUTORIAL)
    Call<WebResponse<Object>> tutorial(
    );

    @POST(WebServiceConstants.METHOD_SOCIAL_LOGINS)
    Call<WebResponse<Object>> socialLogin(
    );

    @POST(WebServiceConstants.METHOD_CARDTEMPATES)
    Call<WebResponse<Object>> getCardTemplates(
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CARDTEMPATES_USER)
    Call<WebResponse<Object>> getCardTemplatesUser(
            @Field("user_id") int userid

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_TERM_SPECIIC_USER)
    Call<WebResponse<Object>> getSpecificUser(
            @Field("user_id") int userid

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_GET_EDIT_CARD_INFO)
    Call<WebResponse<Object>> getEditCardInfo(
            @Field("user_id") int userid,
            @Field("card_id") int card_id

    );

    @Multipart
    @POST(WebServiceConstants.METHOD_UPLOADIMAGE)
    Call<WebResponse<Object>> getImage(
            @Part MultipartBody.Part Picture

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_SEND_FREQUEST)
    Call<WebResponse<Object>> sendFriendRequest(
            @Field("user_id") int userid,
            @Field("friend_id") int friend_id
    );


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_UN_FRIEND_USER)
    Call<WebResponse<Object>> deleteFriend(
            @Field("user_id") int userid,
            @Field("friend_id") int friend_id
    );

    @POST(WebServiceConstants.METHOD_FONT_INPUT)
    Call<WebResponse<Object>> getInputFont(
    );


    @Multipart
    @POST(WebServiceConstants.METHOD_ADD_USER_CARD)
    Call<WebResponse<Object>> addAnotherCard(
            @Part("user_id") RequestBody userId,
            @Part("first_name") RequestBody first_name,
            @Part("last_name") RequestBody last_name,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("address") RequestBody address,
            @Part("company_name") RequestBody company_name,
            @Part("occupation") RequestBody occupation,
            @Part("social") RequestBody social,
            @Part("password") RequestBody password,
            @Part("card_type") RequestBody card_type,
            @Part("card_design") RequestBody card_design,
            @Part("card_status") RequestBody card_status,
            @Part("social_id") RequestBody social_id,
            @Part("notes") RequestBody note,
            @Part("platform") RequestBody platform,
            @Part("devicetoken") RequestBody devicetoken,
            @Part("ios_device_token") RequestBody ios_device_token,
            @Part MultipartBody.Part picture,
            @Part("facebook") RequestBody facebook,
            @Part("instagram") RequestBody instagram,
            @Part("gmail") RequestBody gmail,
            @Part("twitter") RequestBody twitter,
            @Part("linkedin") RequestBody linkedin
    );


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_USER_CARD_ACTIVE)
    Call<WebResponse<Object>> setFlag(
            @Field("card_id") int card_id,
            @Field("card_design") String card_design,
            @Field("user_id") int user_id

    );

    @POST(WebServiceConstants.METHOD_TERM_PRIVACY)
    Call<WebResponse<Object>> getTermPolicies(
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_GET_ALL_NOTIFICATION)
    Call<WebResponse<Object>> getNotification(
            @Field("user_id") int userid

    );

    @POST(WebServiceConstants.METHOD_GET_THEME)
    Call<WebResponse<Object>> getTheme(

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CONTACT_US)
    Call<WebResponse<Object>> contactUs(
            @Field("user_id") int userid,
            @Field("name") String name,
            @Field("phone") String phone,
            @Field("message") String message,
            @Field("feedback") String feedback

    );


    @FormUrlEncoded
    @POST(WebServiceConstants.ACCEPT_FRIEND_REQUEST)
    Call<WebResponse<Object>> acceptFR(
            @Field("user_id") int userid,
            @Field("friend_id") int friend_id
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_GET_REJECT_REQUEST1)
    Call<WebResponse<Object>> rejectFR(
            @Field("user_id") int userid,
            @Field("friend_id") int friend_id
    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CHANGE_LANGUAGE)
    Call<WebResponse<Object>> changeLanguge(
            @Field("user_id") int userid,
            @Field("status") String status

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_INVITE)
    Call<WebResponse<Object>> sendInvites(
            @Field("user_id") int userid,
            @Field("email") String email

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_PROFILE_STATUS)
    Call<WebResponse<Object>> profileStatus(
            @Field("user_id") int userid,
            @Field("status") String status

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_ON_OFF_NOTIFICATION)
    Call<WebResponse<Object>> notificationOFFON(
            @Field("user_id") int userid,
            @Field("status") String status

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_ON_OFF_NOTIFI_EMAIL)
    Call<WebResponse<Object>> emailNnotificationOFFON(
            @Field("user_id") int userid,
            @Field("status") String status

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_ADD_TOKEN)
    Call<WebResponse<Object>> addToken(
            @Field("token") String token

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_DELETE_CARD)
    Call<WebResponse<Object>> deleteCard(
            @Field("user_id") int userid,
            @Field("card_id") int card_id

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CHANGE_PRI_CARD)
    Call<WebResponse<Object>> changePriCard(
            @Field("user_id") int userid,
            @Field("card_id") int card_id

    );


    @POST(WebServiceConstants.METHOD_OCCUPATION_LIST)
    Call<WebResponse<Object>> occupationLIst(

    );


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_BY_FILTER)
    Call<WebResponse<Object>> cardByOccupation(
            @Field("user_id") int userid,
            @Field("filter_by") int filter_by,
            @Field("lat") double lat,
            @Field("lng") double lng,
            @Field("occupation") String occupation

    );


    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CARD_TEMPLATE_SAVE)
    Call<WebResponse<Object>> cardTemplate(
            @Field("user_id") int userid,
            @Field("card_template") String card_template,
            @Field("card_id") int card_id

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_USER_LOGIN)
    Call<WebResponse<Object>> login(
            @Field("email") String email,
            @Field("devicetoken") String devicetoken,
            @Field("password") String password

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_CHANGE_PASSWORD)
    Call<WebResponse<Object>> password(
            @Field("user_id") int user_id,
            @Field("password") String password

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_USER_FORGOT_PASS)
    Call<WebResponse<Object>> forgotPass(
            @Field("email") String email

    );

    @FormUrlEncoded
    @POST(WebServiceConstants.METHOD_DELETE_CARD_WALLET)
    Call<WebResponse<Object>> deleteCards(
            @Field("user_id") int userid,
            @Field("card_id") int card_holder_id
    );

    @GET(WebServiceConstants.METHOD_GET_DOWNLOAD)
    Call<WebResponse<Object>> getCallGeneric(@Query("card_id") int card_id
    );

}

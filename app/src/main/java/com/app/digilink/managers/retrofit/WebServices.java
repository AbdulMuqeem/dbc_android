package com.app.digilink.managers.retrofit;

import android.app.Activity;
import android.content.Context;

import com.app.digilink.R;
import com.app.digilink.fragments.dialogs.AqProgressLoader;
import com.app.digilink.utils.AqsaProgressLoader;
import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.helperclasses.Helper;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.models.User.EditUserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


/**
 * Created by hamzakhan on 6/30/2017.
 */

public class WebServices {
    private WebServiceProxy apiService;
    private AqProgressLoader mDialog;
    private Context mContext;
    private static String bearerToken = "";



    public WebServices(Activity activity, String token, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(token);
        mContext = activity;

        mDialog = UIHelper.getProgressHUD();

            if (!((Activity) mContext).isFinishing()) {
                FragmentManager fragmentManager = ((FragmentActivity) activity).getSupportFragmentManager();
                mDialog.show(fragmentManager,null);
        }
    }


   /* public WebServices(Activity activity, String token, boolean isShowDialog, String base_url, boolean enableReferesh) {
        apiService = WebServiceFactory.getInstance(token, base_url, enableReferesh);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }
*/
    // For New Token
/*    public WebServices(Activity activity, String newToken, boolean enableReferesh, boolean isShowDialog) {
        apiService = WebServiceFactory.getInstance(newToken, enableReferesh);
        mContext = activity;
        mDialog = UIHelper.getProgressHUD(mContext);

        if (isShowDialog) {
            if (!((Activity) mContext).isFinishing())
                mDialog.show();
        }
    }*/




    private void dismissDialog() {
        mDialog.dismiss();
    }


    public interface IRequestWebResponseAnyObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);

        void onError(Object object);
    }


    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectUserID(String methodName, int userID, final IRequestWebResponseAnyObjectCallBack callBack) {
        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_WALLET)) {
            webResponseCall = apiService.walletCards(userID);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ALL_CARDS)) {
            webResponseCall = apiService.allCards(userID);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CARDTEMPATES_USER)) {
            webResponseCall = apiService.getCardTemplatesUser(userID);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_TERM_SPECIIC_USER)) {
            webResponseCall = apiService.getSpecificUser(userID);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_GET_ALL_NOTIFICATION)) {
            webResponseCall = apiService.getNotification(userID);
        }else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_GET_THEME)) {
            webResponseCall = apiService.getTheme();
        }

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyObjectWithoutParam(String methodName,
                                                                               final IRequestWebResponseAnyObjectCallBack callBack) {


        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_TUTORIAL)) {
            webResponseCall = apiService.tutorial();
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_SOCIAL_LOGINS)) {
            webResponseCall = apiService.socialLogin();
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CARDTEMPATES)) {
            webResponseCall = apiService.getCardTemplates();
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_FONT_INPUT)) {
            webResponseCall = apiService.getInputFont();
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_TERM_PRIVACY)) {
            webResponseCall = apiService.getTermPolicies();
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_OCCUPATION_LIST)) {
            webResponseCall = apiService.occupationLIst();
        }


        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    MultipartBody.Part body = null;

    public Call<WebResponse<Object>> webServiceRequestAPIAnyRegisteration(String methodName, RequestBody userid, RequestBody fname, RequestBody lname, RequestBody email,
                                                                          RequestBody phone, RequestBody address, RequestBody company_name,
                                                                          RequestBody occupation, RequestBody social, RequestBody password,
                                                                          RequestBody card_type, RequestBody card_design, RequestBody card_status,
                                                                          RequestBody social_id, RequestBody note, RequestBody platform, RequestBody devicetoken,
                                                                          RequestBody ios_device_token, RequestBody longitute, RequestBody latitude, MultipartBody.Part picture, RequestBody facebook,
                                                                          RequestBody instagram, RequestBody gmail, RequestBody twitter, RequestBody linkedin,
                                                                          final IRequestWebResponseAnyObjectCallBack callBack) {

        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_REGISTERATION)) {
            webResponseCall = /*apiService.getSpecificUser(*/
                    /* Call<WebResponse<Object>> webResponseCall =*/ apiService.registeration(
                    fname,
                    lname,
                    email,
                    phone,
                    address,
                    company_name,
                    occupation,
                    social,
                    password,
                    card_type,
                    card_design,
                    card_status,
                    social_id,
                    note,
                    platform,
                    devicetoken,
                    ios_device_token,
                    longitute,
                    latitude,
                    picture,
                    facebook,
                    instagram,
                    gmail,
                    twitter,
                    linkedin
            );
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ADD_USER_CARD)) {
            webResponseCall = apiService.addAnotherCard(
                    userid,
                    fname,
                    lname,
                    email,
                    phone,
                    address,
                    company_name,
                    occupation,
                    social,
                    password,
                    card_type,
                    card_design,
                    card_status,
                    social_id,
                    note,
                    platform,
                    devicetoken,
                    ios_device_token,
                    picture,
                    facebook,
                    instagram,
                    gmail,
                    twitter,
                    linkedin
            );
        }


        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }


                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }



    public Call<WebResponse<Object>> webServiceRequestAPIAnyObject(String methodName, SearchModel searchModel, final IRequestWebResponseAnyObjectCallBack callBack) {
        Call<WebResponse<Object>> webResponseCall = null;
        if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_TERM_SPECIIC_USER)) {
            webResponseCall = apiService.getSpecificUser(searchModel.getId());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_SEND_FREQUEST))
            webResponseCall = apiService.sendFriendRequest(searchModel.getId(), searchModel.getFriend_id());
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_UN_FRIEND_USER))
            webResponseCall = apiService.deleteFriend(searchModel.getId(), searchModel.getFriend_id());
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_SAVE_CARDS))
            webResponseCall = apiService.allSaveCards(searchModel.getUser_id(), searchModel.getCard_holder_id());
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_DELETE_CARD_WALLET))
            webResponseCall = apiService.deleteCards(searchModel.getUser_id(), searchModel.getCard_holder_id());
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ONLINEOFFLINE_CARD))
            webResponseCall = apiService.getOfflineOnline(searchModel.getUser_id(), searchModel.getCard_id(), searchModel.getStatus());
        else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_GET_EDIT_CARD_INFO)) {
            webResponseCall = apiService.getEditCardInfo(searchModel.getUser_id(), searchModel.getCard_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_USER_CARD_ACTIVE)) {
            webResponseCall = apiService.setFlag(searchModel.getCard_id(), searchModel.getCard_design(), searchModel.getUser_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CONTACT_US)) {
            webResponseCall = apiService.contactUs(searchModel.getCard_id(), searchModel.getName(), searchModel.getPhone(), searchModel.getMessage(), searchModel.getFeedback());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.ACCEPT_FRIEND_REQUEST)) {
            webResponseCall = apiService.acceptFR(searchModel.getUser_id(), searchModel.getFriend_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_GET_REJECT_REQUEST1)) {
            webResponseCall = apiService.rejectFR(searchModel.getUser_id(), searchModel.getFriend_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CHANGE_LANGUAGE)) {
            webResponseCall = apiService.changeLanguge(searchModel.getUser_id(), searchModel.getStatus());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_INVITE)) {
            webResponseCall = apiService.sendInvites(searchModel.getUser_id(), searchModel.getEmail());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ON_OFF_NOTIFICATION)) {
            webResponseCall = apiService.notificationOFFON(searchModel.getUser_id(), searchModel.getStatus());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ON_OFF_NOTIFI_EMAIL)) {
            webResponseCall = apiService.emailNnotificationOFFON(searchModel.getUser_id(), searchModel.getStatus());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_PROFILE_STATUS)) {
            webResponseCall = apiService.profileStatus(searchModel.getUser_id(), searchModel.getStatus());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_ADD_TOKEN)) {
            webResponseCall = apiService.addToken(searchModel.getToken());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_DELETE_CARD)) {
            webResponseCall = apiService.deleteCard(searchModel.getUser_id(), searchModel.getCard_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CHANGE_PRI_CARD)) {
            webResponseCall = apiService.changePriCard(searchModel.getUser_id(), searchModel.getCard_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CARD_TEMPLATE_SAVE)) {
            webResponseCall = apiService.cardTemplate(searchModel.getUser_id(), searchModel.getCard_template(), searchModel.getCard_id());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_BY_FILTER)) {
            webResponseCall = apiService.cardByOccupation(searchModel.getUser_id(), searchModel.getFilter_by(), searchModel.getLat(), searchModel.getLng(), searchModel.getOccupation());
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_USER_LOGIN)) {
            webResponseCall = apiService.login(searchModel.getEmail(), searchModel.getDevicetoken(), searchModel.getPassword()/*,"device token","iso device token empty in my case"*/);
        } else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_CHANGE_PASSWORD)) {
            webResponseCall = apiService.password(searchModel.getUser_id(), searchModel.getPassword());
        }else if (methodName.equalsIgnoreCase(WebServiceConstants.METHOD_USER_FORGOT_PASS)) {
            webResponseCall = apiService.forgotPass(searchModel.getEmail());
        }

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }

    public Call<WebResponse<Object>> webServiceRequestAPIAnyEditCard(EditUserModel model
            , final IRequestWebResponseAnyObjectCallBack callBack) {


        Call<WebResponse<Object>> webResponseCall = apiService.editUser(

                model.getFirst_name(),
                model.getLast_name(),
                model.getEmail(),
                model.getPhone(),
                model.getAddress(),
                model.getOccupation(),
                model.getNotes(),

                model.getUser_id(),
                model.getCard_id(),
                model.getSocial_icon(),

                model.getFont(),
                model.getSize(),

                model.getName_color(),
                model.getOccupation_color(),
                model.getPhone_color(),
                model.getEmail_color(),
                model.getAddress_color(),
                model.getNotes_color(),

                model.getCard_order(),

                model.getName_font(),
                model.getOccupation_font(),
                model.getPhone_font(),
                model.getEmail_font(),
                model.getAddress_font(),
                model.getNotes_font(),

                model.getName_size(),
                model.getOccupation_size(),
                model.getPhone_size(),
                model.getEmail_size(),
                model.getAddress_size(),
                model.getNotes_size(),

                model.getCard_design(),
                model.getP_email(),

                model.getFacebook(),
                model.getInstagram(),
                model.getGmail(),
                model.getTwitter(),
                model.getLinkedin()


        );

        try {
            if (Helper.isNetworkConnected(mContext, true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {
                        dismissDialog();

                        if (response.body() == null) {
                            callBack.onError("empty response");
                            return;
                        }


                        if (response.isSuccessful() && response.body().isSuccess()) {
                            if (callBack != null)
                                callBack.requestDataResponse(response.body());
                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(mContext, message);
                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(mContext, "Something went wrong, Please check your internet connection.");
                        dismissDialog();
                        callBack.onError(null);
                    }
                });
            } else {
                dismissDialog();
            }

        } catch (Exception e) {
            dismissDialog();
            e.printStackTrace();

        }

        return webResponseCall;

    }



}



package com.app.digilink.helperclasses;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;


import com.app.digilink.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.ByteArrayOutputStream;
import java.util.Map;




/**
 * Created by aqsa.sarwar on 6/28/2019.
 */

public class ImageLoaderHelper {
//    public static void loadImageWithConstantHeadersWithoutAnimation(Context context, ImageView imageView, String path) {
//        ImageLoader.getInstance().displayImage(ImageLoaderHelper.getUserImageURL(path),
//                imageView,
//                ImageLoaderHelper.getOptionsSimple(WebServiceConstants
//                        .getHeaders(SharedPreferenceManager.getInstance(context).getString(Encoder.BuilderAES()
//                                .method(AES.Method.AES_CBC_PKCS5PADDING)
//                                .message(AppConstants.KEY_TOKEN)
//                                .key("qetuoadgjlzcbm") // not necessary
//                                .keySize(AES.Key.SIZE_128) // not necessary
//                                .decrypt()))));
//    }
//
//    public static void loadImageWithConstantHeaders(Context context, ImageView imageView, String path) {
//        ImageLoader.getInstance().displayImage(ImageLoaderHelper.getUserImageURL(path),
//                imageView,
//                ImageLoaderHelper.getOptionsWithAnimation(WebServiceConstants
//                        .getHeaders(SharedPreferenceManager.getInstance(context).getString(Encoder.BuilderAES()
//                                .method(AES.Method.AES_CBC_PKCS5PADDING)
//                                .message(AppConstants.KEY_TOKEN)
//                                .key("qetuoadgjlzcbm") // not necessary
//                                .keySize(AES.Key.SIZE_128) // not necessary
//                                .decrypt()))));
//    }

    public static DisplayImageOptions getOptionsSimple() {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .build();
    }


    public static DisplayImageOptions getOptionsSimple(Map<String, String> headers) {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .extraForDownloader(headers)
                .build();
    }

    public static DisplayImageOptions getOptionsWithAnimation() {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(500)).build();
    }


    public static DisplayImageOptions getOptionsWithAnimation(Map<String, String> headers) {

        return new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .showImageForEmptyUri(R.color.material_grey700)
                .showImageOnFail(R.drawable.profile_placeholder)
                .showImageOnLoading(R.drawable.profile_placeholder)
                .extraForDownloader(headers)
                .imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(500)).build();
    }

    public static String convertToBase64(String imagePath)

    {

        Bitmap bm = BitmapFactory.decodeFile(imagePath);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] byteArrayImage = baos.toByteArray();

        String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

        return encodedImage;

    }
}

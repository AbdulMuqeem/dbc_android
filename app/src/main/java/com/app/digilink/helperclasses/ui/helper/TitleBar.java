package com.app.digilink.helperclasses.ui.helper;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.app.digilink.R;
import com.app.digilink.widget.AnyTextView;


/**
 * Created by  on 02-Mar-17.
 */

public class TitleBar extends RelativeLayout {


    ImageView imgBackBtn;
    AnyTextView txtTitle;
    ImageView imgNotificationIcon;
    LinearLayout containerTitlebar1;

    final Handler handler = new Handler();


    public TitleBar(Context context) {
        super(context);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }


    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.titlebar_main, this);
        bindViews();
    }

    private void bindViews() {
        txtTitle = findViewById(R.id.txtTitle);
        imgBackBtn = findViewById(R.id.imgBackBtn);
        imgNotificationIcon = findViewById(R.id.imgNotificationIcon);

        containerTitlebar1 = findViewById(R.id.containerTitlebar1);

    }

    public void resetViews() {
        containerTitlebar1.setVisibility(VISIBLE);
        imgBackBtn.setVisibility(INVISIBLE);
        imgNotificationIcon.setVisibility(INVISIBLE);

    }


    public void setTitle(String title) {

        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(title);
    }

    public void showBackButton(final Activity mActivity) {
        this.imgBackBtn.setVisibility(VISIBLE);
        this.imgBackBtn.setImageResource(R.drawable.back);
        imgBackBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mActivity != null) {
                    mActivity.onBackPressed();
                }

            }
        });
    }

    public void setRightButtonCross(int drawable, OnClickListener onClickListener, int color) {
        this.imgBackBtn.setVisibility(VISIBLE);
        this.imgBackBtn.setImageResource(drawable);
        this.imgBackBtn.setOnClickListener(onClickListener);
        this.imgBackBtn.setColorFilter(color);
    }


    public void setRightButton(int drawable, OnClickListener onClickListener, int color) {
        this.imgNotificationIcon.setVisibility(VISIBLE);
        this.imgNotificationIcon.setImageResource(drawable);
        this.imgNotificationIcon.setOnClickListener(onClickListener);
    }


}

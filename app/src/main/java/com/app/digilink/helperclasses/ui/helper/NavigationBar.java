package com.app.digilink.helperclasses.ui.helper;

/**
 * Created by aqsa.sarwar on 6/27/2018.
 */

public class NavigationBar  {


//    private ImageView btnHome, btnWallet, btnAdd, btnBrowser, btnProfileSetting;
//    private LinearLayout navigationBar, contHome, contWallet, contAdd, contBrowser, contProfile;
//    SharedPreferenceManager sharedPreferenceManager;
//
//    public NavigationBar(Context context) {
//        super(context);
//        initLayout(context);
//    }
//
//    public NavigationBar(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        initLayout(context);
//    }
//
//    public NavigationBar(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        initLayout(context);
//    }
//
//
//    private void initLayout(Context context) {
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        inflater.inflate(R.layout.shortcuts_navigation_barv1, this);
//        sharedPreferenceManager = SharedPreferenceManager.getInstance(getContext());
//        bindViews();
//    }
//
//    private void bindViews() {
//        btnHome = findViewById(R.id.btnHome);
//        btnWallet = findViewById(R.id.btnWallet);
//        btnAdd = findViewById(R.id.btnAdd);
//        btnBrowser = findViewById(R.id.btnBrowser);
//        btnProfileSetting = findViewById(R.id.btnProfileSetting);
//
//        contHome = findViewById(R.id.contHome);
//        contWallet = findViewById(R.id.contWallet);
//        contAdd = findViewById(R.id.contAdd);
//        contBrowser = findViewById(R.id.contBrowser);
//        contProfile = findViewById(R.id.contProfile);
//        navigationBar = findViewById(R.id.navigationBar);
//
//    }
//
//
//    public void clickBtnHome(BaseActivity activity) {
//        resetNvigationBar();
////        resetViews();
//        this.btnHome.setVisibility(VISIBLE);
//        this.btnHome.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                btnHome.setEnabled(true);
//                btnAdd.setEnabled(true);
//                btnWallet.setEnabled(true);
//                btnProfileSetting.setEnabled(true);
//                btnBrowser.setEnabled(true);
//                if (activity instanceof HomeActivity) {
////                    activity.reload();
//                    activity.popStackTill(1);
//
//                } else {
//                    activity.clearAllActivitiesExceptThis(HomeActivity.class);
//                }
//            }
//        });
//        this.btnHome.setColorFilter(getResources().getColor(R.color.base_blue));
//    }
//
//    public void clickBtnWallet(BaseActivity activity) {
//        resetNvigationBar();
//        this.btnWallet.setVisibility(VISIBLE);
//        this.btnWallet.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                activity.addDockableFragment(WalletFragment.newInstance(), false);
//                btnWallet.setEnabled(false);
//                btnAdd.setEnabled(true);
//                btnHome.setEnabled(true);
//                btnProfileSetting.setEnabled(true);
//                btnBrowser.setEnabled(true);
//            }
//        });
//        this.btnWallet.setColorFilter(getResources().getColor(R.color.base_blue));
//        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
//    }
//
//    public void clickNewCard(BaseActivity activity) {
//        resetNvigationBar();
//        this.btnAdd.setVisibility(VISIBLE);
//        this.btnAdd.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                activity.addDockableFragment(CreatCardFragment.newInstance(), false);
//                btnAdd.setEnabled(false);
//                btnWallet.setEnabled(true);
//                btnHome.setEnabled(true);
//                btnProfileSetting.setEnabled(true);
//                btnBrowser.setEnabled(true);
//            }
//        });
//        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
////        this.btnAdd.setColorFilter(getResources().getColor(R.color.base_blue));
//    }
//
//    public void clickBtbBrowse(BaseActivity activity) {
//        resetNvigationBar();
//        this.btnBrowser.setVisibility(VISIBLE);
//        this.btnBrowser.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                activity.addDockableFragment(BrowseFragment.newInstance(null), false);
//                btnAdd.setEnabled(true);
//                btnWallet.setEnabled(true);
//                btnHome.setEnabled(true);
//                btnProfileSetting.setEnabled(true);
//                btnBrowser.setEnabled(false);
////                UIHelper.showToast(getContext(), "Will be implemented in next build");
//            }
//        });
//        this.btnBrowser.setColorFilter(getResources().getColor(R.color.base_blue));
//        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
//    }
//
//    public void clickBtnProfile(BaseActivity activity) {
//        resetNvigationBar();
//        this.btnProfileSetting.setVisibility(VISIBLE);
//        this.btnProfileSetting.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                UIHelper.showToast(getContext(), "Will be implemented in next build");
//                if (sharedPreferenceManager.isGuestUser()) {
//                    activity.addDockableFragment(GuestUserFragment.newInstance(), false);
//                } else {
//                    activity.addDockableFragment(SettingFragment.newInstance(), false);
//                }
//
//
////                activity.addDockableFragment(SettingFragment.newInstance(), false);
//                btnAdd.setEnabled(true);
//        btnWallet.setEnabled(true);
//        btnHome.setEnabled(true);
//        btnBrowser.setEnabled(true);
//        btnProfileSetting.setEnabled(false);
//
//    }
//});
//        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.base_blue));
//        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
//        }
//
//public void resetViews() {
//
////        this.btnHome.setColorFilter(getResources().getColor(R.color.base_blue));
//        btnHome.setEnabled(true);
//        btnAdd.setEnabled(true);
//        btnWallet.setEnabled(true);
//        btnAdd.setEnabled(true);
//        btnBrowser.setEnabled(true);
//    }
//
//    public void resetNvigationBar() {
//       this. //  navigationBar.setVisibility(VISIBLE);
////        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
//        this.btnWallet.setColorFilter(getResources().getColor(R.color.transparent));
//        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
//        this.btnBrowser.setColorFilter(getResources().getColor(R.color.transparent));
//        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.transparent));
//
//    }
}

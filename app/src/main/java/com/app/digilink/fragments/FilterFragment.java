package com.app.digilink.fragments;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.BrowseAllAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.GooglePlaceHelper;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.libraries.location.GpsTracker;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.AllCardsModel;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.OccupationModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;

/**
 * Created by  on 08-May-17.
 */

public class FilterFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener
        , GooglePlaceHelper.GooglePlaceDataInterface {

    Unbinder unbinder;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.txtLocation)
    AnyTextView txtLocation;
    @BindView(R.id.checkNearByLocation)
    CheckBox checkNearByLocation;
    @BindView(R.id.txtProfession)
    AnyTextView txtProfession;
    @BindView(R.id.contSpinner)
    RelativeLayout contSpinner;
    @BindView(R.id.spProfession)
    Spinner spProfession;
    @BindView(R.id.btnProceed)
    AnyTextView btnProceed;


    private String spProfessionText;
    ArrayList<String> arrayOccupation;
    private AllCardsModel allCardsModel;
    private BrowseAllAdapter adapter;
    private ArrayList<GenericCardListModel> arrayList;
    private double latitude;
    private double longitude;
    SearchModel model = new SearchModel();
    private boolean isfilterOccupation, isfilterLocation, isNearByme;

    public static FilterFragment newInstance() {
        Bundle args = new Bundle();
        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayOccupation = new ArrayList<>();
        arrayList = new ArrayList<>();
        adapter = new BrowseAllAdapter(sharedPreferenceManager, getBaseActivity(), arrayList, this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_filter;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {


        spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spProfessionText = (String) adapterView.getSelectedItem();
                txtProfession.setText(spProfessionText);
//                rvGeneric.setVisibility(View.INVISIBLE);
                model.setOccupation(spProfessionText);
                if (txtProfession.getText().equals("Select Profession"))
                    isfilterOccupation = false;
                else isfilterOccupation = true;
//                model.setFilter_by(2);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkNearByLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isNearByme = true;
                    try {
                        getLocation();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    txtLocation.setEnabled(false);
                } else {
                    isNearByme = false;
                    txtLocation.setEnabled(true);
                }
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        arrayOccupation.clear();
        serviceCall();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //  navigationBar.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {

    }


    private void serviceCall() {

        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_OCCUPATION_LIST,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<OccupationModel>>() {
                                }.getType();
                                ArrayList<OccupationModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                arrayOccupation.add("Select Profession");
                                for (int i = 0; i < arrayList1.size(); i++) {
                                    arrayOccupation.add(arrayList1.get(i).getOccupation());
                                }
                                ArrayAdapter<String> adapter =
                                        new ArrayAdapter<String>(getBaseActivity(), android.R.layout.simple_spinner_dropdown_item, arrayOccupation);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spProfession.setAdapter(adapter);

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    @OnClick({R.id.contSpinner, R.id.imgCross, R.id.btnProceed, R.id.imgBackBtn, /*R.id.btnCross,*/ R.id.txtLocation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contSpinner:
                spProfession.performClick();
                break;
            case R.id.imgBackBtn:
                popBackStack();
//                getBaseActivity().replaceDockableFragment(FilterFragarrayOccupationment.newInstance(), false);
                break;
            case R.id.btnProceed:
                if (isfilterLocation || isfilterOccupation || isNearByme)
                    serviceCallCards();
                else
                    UIHelper.showLongToastInCenter(getContext(), "Please select atleast one criteria");
                break;
            case R.id.txtLocation:
                callPlaceAutocompleteActivityIntent();
                break;
            case R.id.imgCross:
                txtLocation.setText("Enter Location");
//                txtLocation.setHint("");
                break;
        }
    }

    private void serviceCallCards() {
        //1 for location, 2 for occupation, 3 for both

        model.setUser_id(getCurrentUser().getId());

        if ((isNearByme || isfilterLocation) && isfilterOccupation) {
            model.setFilter_by(3);
        } else if (isNearByme || isfilterLocation) {
            model.setFilter_by(1);
//            if (isNearByme) {
////                model.setLat(0);
////                model.setLng(0);
//            } else {
            model.setLat(latitude);
            model.setLng(longitude);
//            }
        } else model.setFilter_by(2);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_BY_FILTER, model,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                allCardsModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), AllCardsModel.class);
                                arrayList.clear();
                                arrayList.addAll(allCardsModel.getGenericCardListModel());
//                                popBackStack();
                                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(arrayList, true), false);
//                                adapter.notifyDataSetChanged();
//                                rvGeneric.setVisibility(View.VISIBLE);

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    @Override
    public void onItemClick(int position, View view, Object object) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                String locationName = place.getName();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                txtLocation.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                assert status.getStatusMessage() != null;
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


      /*

    MAP
    *
    * */

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5656;


    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getBaseActivity(), "AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
        fields.size();
        isfilterLocation = true;
//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getBaseActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(/*Location location*/double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getBaseActivity(), Locale.getDefault());
        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
            String add = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            txtLocation.setText(add);

        }
    }


    @Override
    public void onPlaceActivityResult(double longitude, double latitude, String locationName) {

    }

    @Override
    public void onError(String error) {

    }

    public void getLocation() throws IOException {
        GpsTracker gpsTracker = new GpsTracker(getBaseActivity());
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            testCurr(latitude, longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }


}

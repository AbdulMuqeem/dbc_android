//package com.structure.fragments.dialogs;
//
//
//import android.app.ProgressDialog;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//
//
//import com.structure.activities.BaseActivity;
//import com.structure.models.GenericCardListModel;
//
//import androidx.annotation.Nullable;
//import androidx.fragment.app.DialogFragment;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//
//
///**
// * Created by  on 21-Feb-17.
// */
//
//public class InfoWebPopupDialog extends DialogFragment {
//
//    private ProgressDialog progDailog;
//    Unbinder unbinder;
//    @BindView(R.id.webUrl)
//    WebView webView;
//    private BaseActivity baseActivity;
//    private GenericCardListModel genericCardListModel;
//
//    public InfoWebPopupDialog() {
//    }
//
//    public static InfoWebPopupDialog newInstance(BaseActivity baseActivity, GenericCardListModel genericCardListModel) {
//        InfoWebPopupDialog frag = new InfoWebPopupDialog();
//
//        Bundle args = new Bundle();
//        frag.setArguments(args);
//        frag.baseActivity = baseActivity;
//        frag.genericCardListModel = genericCardListModel;
//
//        return frag;
//    }
//
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//
//        View view = inflater.inflate(R.layout.popweb, container);
//        unbinder = ButterKnife.bind(this, view);
//        return view;
//    }
//
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        /*
////        webView.setWebViewClient(new MyBrowser());
////        webView.getSettings().setLoadsImagesAutomatically(true);
////        webView.getSettings().setJavaScriptEnabled(true);
////        webView.getSettings().setLoadWithOverviewMode(true);
////        webView.getSettings().setDomStorageEnabled(true);
//
////        webView.getSettings().setPluginsEnabled(true);
//        webView.getSettings().setAllowFileAccess(true);
//        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        webView.loadUrl(genericCardListModel.getCard());
//
//
////        webView.loadUrl("http:\\www.google.com");
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//
//                return true;
//            }
//        });*/
//        progDailog = ProgressDialog.show(baseActivity, "Loading","Please wait...", true);
//        progDailog.setCancelable(false);
//
//
//
//
//
//        webView.setWebViewClient(new WebViewClient(){
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                progDailog.show();
//                view.loadUrl(url);
//
//                return true;
//            }
//            @Override
//            public void onPageFinished(WebView view, final String url) {
//                progDailog.dismiss();
//            }
//        });
//
//        webView.loadUrl("https://www.google.com/");
//
//    }
//
//
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//
//    }
//
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        unbinder.unbind();
//    }
//}
//

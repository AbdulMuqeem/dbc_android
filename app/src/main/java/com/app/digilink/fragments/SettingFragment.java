package com.app.digilink.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.TermPolicyModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatDelegate;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;
import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.FILENAME_LANG;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.KEY_DARK_THEME;
import static com.app.digilink.constatnts.AppConstants.PCODE;


public class SettingFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.imgUser)
    CircleImageView imgUser;
    @BindView(R.id.txtUserName)
    AnyTextView txtUserName;
    @BindView(R.id.txtViewProfile)
    AnyTextView txtViewProfile;
    @BindView(R.id.txtMember)
    AnyTextView txtMember;
    @BindView(R.id.txtChangePassword)
    AnyTextView txtChangePassword;
    @BindView(R.id.txtLangauge)
    AnyTextView txtLangauge;
    //    @BindView(R.id.txtEnglish)
//    AnyTextView txtEnglish;
//    @BindView(R.id.txtAr)
//    AnyTextView txtAr;
//    @BindView(R.id.contLanguage)
//    LinearLayout contLanguage;
    @BindView(R.id.txtProfileStatus)
    AnyTextView txtProfileStatus;
    @BindView(R.id.btnToggleProfileStatus)
    ToggleButton btnToggleProfileStatus;
    @BindView(R.id.txtDarkMode)
    AnyTextView txtDarkMode;
    @BindView(R.id.btnNightMode)
    ToggleButton btnNightMode;
    @BindView(R.id.txtPushNotification)
    AnyTextView txtPushNotification;
    @BindView(R.id.btnToggleNotification)
    ToggleButton btnToggleNotification;
    @BindView(R.id.txtEmailNotification)
    AnyTextView txtEmailNotification;
    @BindView(R.id.btnToggleEmail)
    ToggleButton btnToggleEmail;
    @BindView(R.id.txtContactUs)
    AnyTextView txtContactUs;
    @BindView(R.id.txtAbout)
    AnyTextView txtAbout;
    @BindView(R.id.txtHelp)
    AnyTextView txtHelp;
    @BindView(R.id.txtPrivacyPolicy)
    AnyTextView txtPrivacyPolicy;
    @BindView(R.id.txtTermsAndCondition)
    AnyTextView txtTermsAndCondition;
    @BindView(R.id.txtLogout)
    AnyTextView txtLogout;
    @BindView(R.id.btnFb)
    ImageView btnFb;
    @BindView(R.id.btninsta)
    ImageView btninsta;
    @BindView(R.id.btnTwiter)
    ImageView btnTwiter;
    @BindView(R.id.btnLinkedIn)
    ImageView btnLinkedIn;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.btnInfo1)
    ImageView btnInfo1;
    @BindView(R.id.containerPersonal)
    LinearLayout containerPersonal;
    @BindView(R.id.btnInfo)
    ImageView btnInfo;
    @BindView(R.id.btnUpgrate)
    AnyTextView btnUpgrate;
    @BindView(R.id.containerCorp)
    LinearLayout containerCorp;
    @BindView(R.id.bottom_sheet_cc)
    LinearLayout bottomSheetCc;
    @BindView(R.id.btnToggleLanguage)
    ToggleButton btnToggleLanguage;
    //    private boolean isEnglish = true;
    private ArrayList<TermPolicyModel> arrayList;

    public static SettingFragment newInstance() {
        Bundle args = new Bundle();
        SettingFragment fragment = new SettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_setting;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
        titleBar.showBackButton(getBaseActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (sharedPreferenceManager.getString(KEY_DARK_THEME).equalsIgnoreCase("Dark")) {
            btnNightMode.setChecked(true);

        } else {
            btnNightMode.setChecked(false);
        }

        if (sharedPreferenceManager.getString(FILENAME_LANG).equals("en")) {
            btnToggleLanguage.setChecked(false);
        } else {
            btnToggleLanguage.setChecked(true);
        }

        if (getCurrentUser().getProfile_status() != null && getCurrentUser().getProfile_status().contains("2")) {
            btnToggleProfileStatus.setChecked(true);
        } else {
            btnToggleProfileStatus.setChecked(false);
        }

        if (getCurrentUser().getEmail_status() != null && getCurrentUser().getEmail_status().contains("1")) {
            btnToggleEmail.setChecked(true);
        } else {
            btnToggleEmail.setChecked(false);
        }
        if (getCurrentUser().getPush_status() != null && getCurrentUser().getPush_status().contains("1")) {
            btnToggleNotification.setChecked(true);
        } else {
            btnToggleNotification.setChecked(false);
        }


        txtUserName.setText(getCurrentUser().getName());
        ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser);

        if (!onCreated)
            serviceCall();

        resetNvigationBar1();
        sheetBehaviorCreateCard = BottomSheetBehavior.from(bottomSheetCc);
    }


    @Override
    public void setListeners() {
        btnNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sharedPreferenceManager.putValue(KEY_DARK_THEME, "Dark");
                    {
                        AppCompatDelegate
                                .setDefaultNightMode(
                                        AppCompatDelegate
                                                .MODE_NIGHT_YES);
                    }

                } else {
                    sharedPreferenceManager.putValue(KEY_DARK_THEME, "Light");
                    {
                        AppCompatDelegate
                                .setDefaultNightMode(
                                        AppCompatDelegate
                                                .MODE_NIGHT_NO);

                    }

                }
                getBaseActivity().refreshFragment1(SettingFragment.newInstance());
            }
        });


        btnToggleEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    serviceCallNotiEmail("1");

                } else {
                    serviceCallNotiEmail("0");

                }
            }
        });


        btnToggleNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    serviceCallNoti("1");

                } else {
                    serviceCallNoti("0");

                }
            }
        });
        btnToggleProfileStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
//                    UIHelper.showToast(getContext(), "Enable");
                    serviceCallPublic("2");

                } else {
//                    UIHelper.showToast(getContext(), "Disable");
                    // The toggle is disabled
                    serviceCallPublic("1");

                }
            }
        });

        btnToggleLanguage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    servicCallLanguage("ar");

                } else {
                    servicCallLanguage("en");

                }
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    public void resetNvigationBar1() {
        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnWallet.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnBrowser.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.base_blue));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
    }

    @Override
    public void onClick(View v) {

    }


    @OnClick({/*R.id.txtEnglish,*/ R.id.txtChangePassword, /*R.id.txtAr,*/ R.id.imgBackBtn, R.id.txtViewProfile,
            R.id.txtLogout, R.id.btnFb, R.id.btnTwiter, R.id.btninsta,
            R.id.btnLinkedIn, R.id.btnToggleNotification, R.id.btnToggleProfileStatus,
            R.id.txtContactUs, R.id.txtAbout, R.id.txtHelp, R.id.txtPrivacyPolicy, R.id.txtTermsAndCondition,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtViewProfile:
                getBaseActivity().addDockableFragment(MyProfileFragment.newInstance(), false);
                break;
            case R.id.txtLogout:
                logoutClick(this);
                break;

            case R.id.txtContactUs:
                getBaseActivity().addDockableFragment(ContactUsFragment.newInstance(), false);
                break;
            case R.id.txtChangePassword:
                getBaseActivity().addDockableFragment(ChangePasswordFragment.newInstance(), false);
                break;
            case R.id.txtAbout:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("About", arrayList.get(0).getAbout()), false);
                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.txtHelp:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Help", arrayList.get(0).getPrivacy()), false);

                break;
            case R.id.txtPrivacyPolicy:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Privacy Policy", arrayList.get(0).getPrivacy()), false);

                break;
            case R.id.txtTermsAndCondition:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Terms and Condition", arrayList.get(0).getTerms()), false);

                break;
            case R.id.btnFb:
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.facebook.com/LinkiApp-110740787514717"));
                startActivity(viewIntent);
                break;
            case R.id.btnTwiter:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://twitter.com/"));
                startActivity(viewIntent);
                break;
            case R.id.btnLinkedIn:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.linkedin.com/"));
                startActivity(viewIntent);
                break;
            case R.id.btninsta:
                viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse("https://www.instagram.com/linkiapp/"));
                startActivity(viewIntent);
                break;


            case R.id.btnAdd:
                createCard();
//                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.contHome:
                if (getBaseActivity() instanceof HomeActivity) {
//                    activity.reload();
                    getBaseActivity().popStackTill(1);

                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;
            case R.id.contWallet:
                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null, false), false);
                break;
            case R.id.contProfile:
//                getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                break;
        }
    }

    private void serviceCallNoti(String s) {
//        sharedPreferenceManager.putValue(EMAIL_ADDRESS, edEmail.getStringTrimmed());
//        sharedPreferenceManager.putValue(PCODE, edtPassword.getStringTrimmed());
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setStatus(s);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_ON_OFF_NOTIFICATION, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
//                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void serviceCallNotiEmail(String s) {

        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setStatus(s);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_ON_OFF_NOTIFI_EMAIL, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
//                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void serviceCallPublic(String s) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setStatus(s);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_PROFILE_STATUS, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
//                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void serviceCall() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_TERM_PRIVACY,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<TermPolicyModel>>() {
                                }.getType();
                                arrayList = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    public static void logoutClick(final BaseFragment baseFragment) {
        Context context = baseFragment.getContext();


        UIHelper.showAlertDialog(context.getString(R.string.areYouSureToLogout), context.getString(R.string.logout), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                SharedPreferenceManager.clearDB();
                baseFragment.sharedPreferenceManager.setGuestUser(true);
                UserModel userModel = new UserModel();
                userModel.setId(0);

                baseFragment.sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                FILENAME_LANG = "en";
                baseFragment.getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
            }
        }, context);


    }

    protected void servicCallLanguage(String status) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setStatus(status);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_CHANGE_LANGUAGE, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UIHelper.showToast(getContext(), webResponse.result.toString());
                                sharedPreferenceManager.putValue(FILENAME_LANG, status);
                                setDefaultLocale();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private BottomSheetBehavior sheetBehaviorCreateCard;

    private void createCard() {


        if (sheetBehaviorCreateCard.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        containerPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }
            }
        });

        containerCorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }

            }
        });


    }


}
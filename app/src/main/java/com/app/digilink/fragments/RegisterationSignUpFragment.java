package com.app.digilink.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.GooglePlaceHelper;
import com.app.digilink.helperclasses.ImageLoaderHelper;
import com.app.digilink.helperclasses.RunTimePermissions;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.helperclasses.validator.MobileNumberValidation;
import com.app.digilink.libraries.location.GpsTracker;
import com.app.digilink.managers.FileManager;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.OccupationModel;
import com.app.digilink.models.SocialMediaLogin;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.textfield.TextInputLayout;

import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.ImageLoader;
//import com.theartofdev.edmodo.cropper.CropImage;
//import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.PCODE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by  on 08-May-17.
 */

public class RegisterationSignUpFragment extends BaseFragment
        implements GooglePlaceHelper.GooglePlaceDataInterface {

    Unbinder unbinder;
    @BindView(R.id.edtAddress1)
    AnyEditTextView edtAddress1;
    @BindView(R.id.containerTitleAdd1)
    TextInputLayout containerTitleAdd1;
    @BindView(R.id.iimgAddMinus1)
    ImageView iimgAddMinus1;
    @BindView(R.id.contAddress1)
    LinearLayout contAddress1;
    @BindView(R.id.edtAddress2)
    AnyEditTextView edtAddress2;
    @BindView(R.id.containerTitleAdd2)
    TextInputLayout containerTitleAdd2;
    @BindView(R.id.imgAddMinus2)
    ImageView imgAddMinus2;
    @BindView(R.id.contAddress2)
    LinearLayout contAddress2;
    @BindView(R.id.edtEmail1)
    AnyEditTextView edtEmail1;
    @BindView(R.id.imgAddEmail1)
    ImageView imgAddEmail1;
    @BindView(R.id.contEailAddress1)
    LinearLayout contEailAddress1;
    @BindView(R.id.edtEmail2)
    AnyEditTextView edtEmail2;
    @BindView(R.id.imgAddEmail2)
    ImageView imgAddEmail2;
    @BindView(R.id.contEailAddress2)
    LinearLayout contEailAddress2;
    @BindView(R.id.edtPhone1)
    AnyEditTextView edtPhone1;
    @BindView(R.id.imgAddPhone1)
    ImageView imgAddPhone1;
    @BindView(R.id.contPhone1)
    LinearLayout contPhone1;
    @BindView(R.id.edtPhone2)
    AnyEditTextView edtPhone2;
    @BindView(R.id.imgAddPhone2)
    ImageView imgAddPhone2;
    @BindView(R.id.contPhone2)
    LinearLayout contPhone2;
    @BindView(R.id.edtLastName)
    AnyEditTextView edtLastName;
    @BindView(R.id.edtPassword)
    AnyEditTextView edtPassword;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;

    private String spProfessionText;
    @BindView(R.id.spProfession)
    Spinner spProfession;
    @BindView(R.id.txtProfession)
    AnyTextView txtProfession;
    ArrayList<String> arrayOccupation;
    private String firstName = "", lastName = "", email = "", note = "",
            phone = "", address = "", company_name,
            occupation, social, password,
            card_type, card_design, card_status,
            social_id, platform, devicetoken,
            ios_device_token, picture = "", facebook = "",
            instagram = "", gmail = "";
    @BindView(R.id.imgUser)
    ImageView imgUser;
    @BindView(R.id.edtFirstName)
    AnyEditTextView edtFirstName;
    @BindView(R.id.textField)
    TextInputLayout textField;
    @BindView(R.id.edtCompanyName)
    AnyEditTextView edtCompanyName;
    @BindView(R.id.txtAddress)
    AnyTextView edtAddress;
    @BindView(R.id.containerTitleAdd)
    TextInputLayout containerTitleAdd;
    @BindView(R.id.imgAddAddress)
    ImageView imgAddAddress;
    @BindView(R.id.contAddress)
    LinearLayout contAddress;
    @BindView(R.id.contDynAdd)
    LinearLayout contDynAdd;
    @BindView(R.id.edtEmail)
    AnyEditTextView edtEmail;
    @BindView(R.id.imgAddEmail)
    ImageView imgAddEmail;
    @BindView(R.id.contDynEmail)
    LinearLayout contDynEmail;
    @BindView(R.id.edtPhone)
    AnyEditTextView edtPhone;
    @BindView(R.id.imgAddPhone)
    ImageView imgAddPhone;
    @BindView(R.id.contDynPhone)
    LinearLayout contDynPhone;
    //    @BindView(R.id.edtOccupation)
//    AnyEditTextView edtOccupation;
    @BindView(R.id.edtNote)
    AnyEditTextView edtNote;
    @BindView(R.id.contSocialIds)
    LinearLayout contSocialIds;
    @BindView(R.id.contDynSocial)
    LinearLayout contDynSocial;
    @BindView(R.id.btnSignUp)
    AnyTextView btnSignUp;
    @BindView(R.id.spAddAddress)
    Spinner spAddAddress;
    private int numberOfLines = 1;
    private int numberOfLinesPhone = 1;
    private int numberOfLinesEmail = 1;
    ArrayList<SocialMediaLogin> arrayList;
    ArrayList<String> arrType;
    private String twitter = "", linkedin = "";
    private boolean isFromCorporate;
    private File fileTemporaryProfilePicture;
    private String newToken;
    private PopupWindow popup, popupPhone, popupEmail;
    private double longitude, latitude;
    private String id, namefb, pic, emailfb;
    private String add;

    public static RegisterationSignUpFragment newInstance(boolean isFromCorporate, String id, String firstName, String email, String pic) {
        Bundle args = new Bundle();
        RegisterationSignUpFragment fragment = new RegisterationSignUpFragment();
        fragment.setArguments(args);
        fragment.isFromCorporate = isFromCorporate;
        fragment.id = id;
        fragment.namefb = firstName;
        fragment.emailfb = email;
        fragment.pic = pic;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        arrType = new ArrayList<>();
        arrayOccupation = new ArrayList<>();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_reg_signup;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {

        spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spProfessionText = (String) adapterView.getSelectedItem();
                txtProfession.setText(spProfessionText);
//                rvGeneric.setVisibility(View.INVISIBLE);
//                model.setOccupation(spProfessionText);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCallOccupation();
        serviceCallSocial();
        edtFirstName.setText(namefb + "");
        edtEmail.setText(emailfb + "");
        if (pic != null && !pic.equalsIgnoreCase(""))
            ImageLoader.getInstance().displayImage(pic, imgUser);

        edtPhone.addValidator(new MobileNumberValidation());
        try {
            getLocation();
        } catch (IOException e) {


        }
        FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(new OnCompleteListener<InstallationTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<InstallationTokenResult> task) {
//                newToken = it.result!!.token;
                newToken = task.getResult().getToken();

            }
        });
        if (sharedPreferenceManager.isGuestUser()) {
        } else {
            setData(getCurrentUser());
        }

        /*
         *
         * */
//        try {
//            getLocation();
//        } catch (IOException e) {
//        }



        /*\\\*/
        LayoutInflater inflater = (LayoutInflater)
                getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.popup_menu_phone, null);
        popupPhone = new PopupWindow(view, 300, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

        LayoutInflater inflater1 = (LayoutInflater)
                getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater1.inflate(R.layout.popup_menu_address, null);
        popup = new PopupWindow(view, 300, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

        LayoutInflater inflater2 = (LayoutInflater)
                getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater2.inflate(R.layout.popup_menu_email, null);
        popupEmail = new PopupWindow(view, 300, RelativeLayout.LayoutParams.WRAP_CONTENT, true);

    }

    private void setData(UserModel userModel) {
        edtAddress.setText(userModel.getAddress());
        edtPhone.setText(userModel.getPhone());
        edtEmail.setText(userModel.getEmail());
        edtFirstName.setText(userModel.getName());
        txtProfession.setText(userModel.getOccupation());
        edtCompanyName.setText(userModel.getCompanyName());
//        txtNote.setText(userModel.getNotes());
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //   navigationBar.resetNvigationBar();
        //  navigationBar.setVisibility(VISIBLE);
        //  navigationBar.setVisibility(GONE);
//        navigationBar.clickBtnProfile(getBaseActivity());
    }

    @Override
    public void onClick(View v) {

    }


    @OnClick({R.id.contSpinner,R.id.imgBackBtn, R.id.txtAddress, R.id.imgUser, R.id.imgAddAddress, R.id.imgAddEmail, R.id.imgAddPhone, R.id.btnSignUp,
            R.id.imgAddEmail1, R.id.imgAddEmail2, R.id.imgAddPhone1, R.id.imgAddPhone2, R.id.iimgAddMinus1, R.id.imgAddMinus2})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contSpinner:
                spProfession.performClick();
                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.txtAddress:
                callPlaceAutocompleteActivityIntent();
                break;
            case R.id.imgUser:
                if (!RunTimePermissions.isAllPhonePermissionGivenCamera(getContext(), getBaseActivity(),
                        true, getResources().getText(R.string.app_name).toString())) {
                    return;
                } else cropImagePicker();
                break;
            case R.id.imgAddAddress:
                addressPopup();
                break;
            case R.id.imgAddEmail:
                emailPopup();
                break;
            case R.id.imgAddPhone:
                phonePopup();

                break;
            case R.id.iimgAddMinus1:
                contAddress1.setVisibility(GONE);
                edtAddress1.setText("");
                numberOfLines--;
                break;
            case R.id.imgAddMinus2:
//                if (numberOfLines == 2) {
                contAddress2.setVisibility(GONE);
                edtAddress2.setText("");
                numberOfLines--;
                break;

            case R.id.imgAddEmail1:
                contEailAddress1.setVisibility(GONE);
                edtEmail1.setText("");
                numberOfLinesEmail--;
                break;
            case R.id.imgAddEmail2:
//                if (numberOfLinesEmail == 2) {
                contEailAddress2.setVisibility(GONE);
                edtEmail2.setText("");
                numberOfLinesEmail--;
//                } else {
//
//                }
                break;
            case R.id.imgAddPhone1:
                contPhone1.setVisibility(GONE);
                edtPhone1.setText("");
                numberOfLinesPhone--;
                break;
            case R.id.imgAddPhone2:
                contPhone2.setVisibility(GONE);
                edtPhone2.setText("");
                numberOfLinesPhone--;
                break;
            case R.id.btnSignUp:


                if (isFromCorporate) serviceCallRestrictedEmail();
                else {
                    if (!edtEmail1.getStringTrimmed().equalsIgnoreCase("") && !edtEmail2.getStringTrimmed().equalsIgnoreCase(""))
                        email = edtEmail.getStringTrimmed() + "," + edtEmail1.getStringTrimmed() + "," + edtEmail2.getStringTrimmed();
                    else if (!edtEmail1.getStringTrimmed().equalsIgnoreCase(""))
                        email = edtEmail.getStringTrimmed() + "," + edtEmail1.getStringTrimmed();
                    else email = edtEmail.getStringTrimmed();

                }
                note = edtNote.getStringTrimmed();
                if (txtProfession.getStringTrimmed().equalsIgnoreCase("Select Occupation"))
                    occupation = "";
                else
                    occupation = txtProfession.getStringTrimmed();

//                phone = edtPhone.getStringTrimmed();
                if (!edtPhone1.getStringTrimmed().equalsIgnoreCase("") && !edtPhone2.getStringTrimmed().equalsIgnoreCase(""))
                    phone = edtPhone.getStringTrimmed() + "," + edtPhone1.getStringTrimmed() + "," + edtPhone2.getStringTrimmed();
                else if (!edtPhone1.getStringTrimmed().equalsIgnoreCase(""))
                    phone = edtPhone.getStringTrimmed() + "," + edtPhone1.getStringTrimmed();
                else phone = edtPhone.getStringTrimmed();

                if (!edtAddress1.getStringTrimmed().equalsIgnoreCase("") && !edtAddress2.getStringTrimmed().equalsIgnoreCase(""))
                    address = edtAddress.getStringTrimmed() + ":" + edtAddress1.getStringTrimmed() + ":" + edtAddress2.getStringTrimmed();
                else if (!edtAddress1.getStringTrimmed().equalsIgnoreCase(""))
                    address = edtAddress.getStringTrimmed() + ":" + edtAddress1.getStringTrimmed();
                else address = edtAddress.getStringTrimmed();


                firstName = edtFirstName.getStringTrimmed();
                lastName = edtLastName.getStringTrimmed();
                password = edtPassword.getStringTrimmed();
                company_name = edtCompanyName.getStringTrimmed();
                social = "0";
                social_id = id;
                platform = "0"; // 1 - facebook, 2 - gmail, 3 - linkedin, 4 - twitter
                card_design = "card1.html";
                card_status = "1"; // 1 - public, 2 -private
                if (isFromCorporate)
                    card_type = "2"; //1 - personal business card, 2 - corporate business card
                else card_type = "1";

                devicetoken = newToken;
//                devicetoken = GCMRegistrar.getRegistrationId(getBaseActivity());

                ios_device_token = "NA";
                if (address.equalsIgnoreCase("")) address = edtAddress.getStringTrimmed();
                if (edtFirstName.testValidity() && edtLastName.testValidity() && edtEmail.testValidity()
                        /*edtAddress.getStringTrimmed() != null && !occupation.equalsIgnoreCase("") && edtPhone.testValidity()*/)

                    serviceCall();
                else UIHelper.showToast(getContext(), "Please fill required fields");
                break;
        }
    }

    private void serviceCallRestrictedEmail() {

//        email = edtEmail.getStringTrimmed();
        if (!edtEmail1.getStringTrimmed().equalsIgnoreCase("") && !edtEmail2.getStringTrimmed().equalsIgnoreCase(""))
            email = edtEmail.getStringTrimmed() + edtEmail1.getStringTrimmed() + edtEmail2.getStringTrimmed();
        else if (!edtEmail1.getStringTrimmed().equalsIgnoreCase(""))
            email = edtEmail.getStringTrimmed() + edtEmail1.getStringTrimmed();
        else email = edtEmail.getStringTrimmed();

    }


    private void serviceCall() {

        sharedPreferenceManager.putValue(EMAIL_ADDRESS, email);
        sharedPreferenceManager.putValue(PCODE, edtPassword.getStringTrimmed());
        MultipartBody.Part body = null;

        if (fileTemporaryProfilePicture != null) {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), fileTemporaryProfilePicture);
            body = MultipartBody.Part.createFormData("picture", fileTemporaryProfilePicture.getName(), requestFile);
        }

            new WebServices(getBaseActivity(),
                    "", true)
                    .webServiceRequestAPIAnyRegisteration(WebServiceConstants.METHOD_REGISTERATION,
                            createPartFromString(""),
                            createPartFromString(firstName),
                            createPartFromString(lastName),
                            createPartFromString(email),
                            createPartFromString(phone),
                            createPartFromString(address),
                            createPartFromString(company_name),
                            createPartFromString(occupation),
                            createPartFromString(social),
                            createPartFromString(password),
                            createPartFromString(card_type),
                            createPartFromString(card_design),
                            createPartFromString(card_status),
                            createPartFromString(social_id),
                            createPartFromString(note),
                            createPartFromString(platform),
                            createPartFromString(newToken),
                            createPartFromString(ios_device_token),
                            createPartFromString(String.valueOf(longitude)),
                            createPartFromString(String.valueOf(latitude)),
                            body,
                            createPartFromString(facebook),
                            createPartFromString(instagram),
                            createPartFromString(gmail),
                            createPartFromString(twitter),
                            createPartFromString(linkedin),

                            new WebServices.IRequestWebResponseAnyObjectCallBack() {
                                @Override
                                public void requestDataResponse(WebResponse<Object> webResponse) {

                                    UserModel userModel = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                    sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                    sharedPreferenceManager.setGuestUser(false);
                                    popStackTill(1);
//                                    getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
//                                    getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(isFromCorporate, null, userModel.getCardList().get(0)), false);
//

                                }

                                @Override
                                public void onError(Object object) {
                                    if (object instanceof String) {
                                        String message = (String) object;
                                        UIHelper.showAlertDialog(message, "Failure", getContext());
                                    }
                                }
                            });
//        } else UIHelper.showToast(getContext(), "Image is required");
    }

    @NonNull
    public RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }

    private void cropImagePicker() {
        ImagePicker.with(this)
                .crop()	    			//Crop image(Optional), Check Customization for more option
                .compress(1024)			//Final image size will be less than 1 MB(Optional)
                .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                .start();
//        CropImage.activity()
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setCropShape(CropImageView.CropShape.RECTANGLE)
//                .setMinCropWindowSize(192, 192)
//                .setMinCropResultSize(192, 192)
//                .setMultiTouchEnabled(false)
//                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
//                // FIXME: 15-Jul-17 Fix Quality if required
//                .setRequestedSize(640, 640, CropImageView.RequestSizeOptions.RESIZE_FIT)
//                .setOutputCompressQuality(80)
//                .start(getContext(), this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri documentUri = data.getData();
            setImageAfterResult(documentUri.toString());
            fileTemporaryProfilePicture = new File(documentUri.getPath());
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(getBaseActivity(), ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseActivity(), "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                String locationName = place.getName().toString();
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                edtAddress.setText(locationName);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void setImageAfterResult(final String uploadFilePath) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    setAndUploadImage(uploadFilePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setAndUploadImage(String uploadFilePath) throws IOException {
        Log.d("PICTURE", FileManager.getFileSize(uploadFilePath));


        ImageLoader.getInstance().displayImage(uploadFilePath, imgUser);
        picture = uploadFilePath;


    }


    public void SocialAdded(String social) {
        View child = getLayoutInflater().inflate(R.layout.item_input_fields, null);
        contDynSocial.addView(child);

        for (int i = 0; i < contDynSocial.getChildCount(); i++) {
            AnyEditTextView text = contDynSocial.getChildAt(i).findViewById(R.id.edtDynmaicText);
            TextInputLayout title = contDynSocial.getChildAt(i).findViewById(R.id.containerTitlDynmaicText);
            ImageView imageView = contDynSocial.getChildAt(i).findViewById(R.id.imgMinus);
            imageView.setVisibility(GONE);

            title.setHint(arrayList.get(i).getSocial());
            text.setText("https://www." + arrayList.get(i).getSocial() + ".com/");
            text.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    if (text.getStringTrimmed().contains("facebook")) {
                        facebook = charSequence.toString();
                    } else if (text.getStringTrimmed().contains("twitter")) {
                        twitter = charSequence.toString();
                    } else if (text.getStringTrimmed().contains("gmail")) {
                        gmail = charSequence.toString();
                    } else if (text.getStringTrimmed().contains("linkedin")) {
                        linkedin = charSequence.toString();
                    } else instagram = charSequence.toString();

                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

        }

    }


    private void serviceCallSocial() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_SOCIAL_LOGINS,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                Type type = new TypeToken<ArrayList<SocialMediaLogin>>() {
                                }.getType();
                                arrayList = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);

                                for (int i = 0; i < arrayList.size(); i++) {
                                    SocialAdded(arrayList.get(i).getSocial());
                                }
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }


    private void addressPopup() {

        if (numberOfLines <= 2) {
            popup.showAsDropDown(imgAddAddress, -230, 0);
            popup.getContentView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (numberOfLines == 1) contAddress1.setVisibility(VISIBLE);
                    if (numberOfLines == 2) contAddress2.setVisibility(VISIBLE);
                    numberOfLines++;
                    popup.dismiss();
                }
            });
        }
    }

    private void phonePopup() {

        if (numberOfLinesPhone <= 2) {
            popupPhone.showAsDropDown(imgAddPhone, -230, 0);
            popupPhone.getContentView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (numberOfLinesPhone == 1) contPhone1.setVisibility(VISIBLE);
                    if (numberOfLinesPhone == 2) contPhone2.setVisibility(VISIBLE);
                    numberOfLinesPhone++;
                    popupPhone.dismiss();
                }
            });

        }


    }

    private void emailPopup() {

        if (numberOfLinesEmail <= 2) {
            popupEmail.showAsDropDown(imgAddEmail, -230, 0);
            popupEmail.getContentView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (numberOfLinesEmail == 1) contEailAddress1.setVisibility(VISIBLE);
                    if (numberOfLinesEmail == 2) contEailAddress2.setVisibility(VISIBLE);
                    numberOfLinesEmail++;
                    popupEmail.dismiss();
                }
            });

        }
    }


    public void getLocation() throws IOException {
        GpsTracker gpsTracker = new GpsTracker(getBaseActivity());
        if (gpsTracker.canGetLocation()) {
            latitude = gpsTracker.getLatitude();
            longitude = gpsTracker.getLongitude();
            testCurr(latitude, longitude);
        } else {
            gpsTracker.showSettingsAlert();
        }
    }




      /*

    MAP
    *
    * */

    /**
     * Override the activity's onActivityResult(), check the request code, and
     * do something with the returned place data (in this example its place name and place ID).
     */

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 5656;


    private void callPlaceAutocompleteActivityIntent() {
        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getBaseActivity(), "AIzaSyBnz1Z14WDDlz95wcl2Kd2goy3ssUEkw3E");
        }


        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

//         Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY, fields)
                .build(getBaseActivity());
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);


    }

    private void testCurr(/*Location location*/double latitude, double longitude) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getBaseActivity(), Locale.getDefault());
        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
            add = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            edtAddress.setText(add);
        }
    }


    @Override
    public void onPlaceActivityResult(double longitude, double latitude, String locationName) {

    }

    @Override
    public void onError(String error) {

    }


    private void serviceCallOccupation() {

        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_OCCUPATION_LIST,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<OccupationModel>>() {
                                }.getType();
                                ArrayList<OccupationModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                arrayOccupation.add("Select Occupation");
                                for (int i = 0; i < arrayList1.size(); i++) {
                                    arrayOccupation.add(arrayList1.get(i).getOccupation());
                                }
                                ArrayAdapter<String> adapter =
                                        new ArrayAdapter<String>(getBaseActivity(), android.R.layout.simple_spinner_dropdown_item, arrayOccupation);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spProfession.setAdapter(adapter);

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }
}

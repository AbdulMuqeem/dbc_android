package com.app.digilink.fragments.dialogs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.digilink.R;
import com.app.digilink.activities.BaseActivity;
import com.app.digilink.adapters.TutorialsPagerAdapter;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.ncorti.slidetoact.SlideToActView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by  on 21-Feb-17.
 */

public class GenericPopupDialog extends DialogFragment {

    @BindView(R.id.txtHeader)
    AnyTextView txtHeader;
    @BindView(R.id.edtDynmaicText)
    AnyEditTextView edtDynmaicText;
    @BindView(R.id.btnOk)
    SlideToActView btnOk;
    @BindView(R.id.btnCross)
    AnyTextView btnCross;
    private TutorialsPagerAdapter tutorialsPagerAdapter;

    Unbinder unbinder;
    private BaseActivity baseActivity;
    private View.OnClickListener onClickListener;
    private String title;
    private String body;

    public GenericPopupDialog() {
    }

    public static GenericPopupDialog newInstance(BaseActivity baseActivity, String title, String body, View.OnClickListener onClickListener) {
        GenericPopupDialog frag = new GenericPopupDialog();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.baseActivity = baseActivity;
        frag.title = title;
        frag.body = body;
        frag.onClickListener = onClickListener;

        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popup_generic, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtHeader.setText(title);
        edtDynmaicText.setText(body);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnCross, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCross:
            case R.id.btnOk:
                dismiss();
                break;
        }
    }
}


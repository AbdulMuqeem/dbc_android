package com.app.digilink.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.MyFriendsBusinessCardAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.WebviewPopupDialog;
import com.app.digilink.helperclasses.ui.helper.KeyboardHelper;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.VISIBLE;

public class WalletCardsFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.edtSearch)
    AnyEditTextView edtSearch;
    @BindView(R.id.rvGeneric)
    RecyclerView rvGeneric;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    private MyFriendsBusinessCardAdapter adapter;
    private ArrayList<GenericCardListModel> arrayList;

    //
    public static WalletCardsFragment newInstance(ArrayList<GenericCardListModel> arrayList) {
        Bundle args = new Bundle();
        WalletCardsFragment fragment = new WalletCardsFragment();
        fragment.setArguments(args);
        fragment.arrayList = arrayList;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_recycler;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new MyFriendsBusinessCardAdapter(sharedPreferenceManager, getBaseActivity(), arrayList, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (arrayList.size() < 0) return;
        bindView();
        navigationBar.setVisibility(View.GONE);
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle(getResources().getString(R.string.business_cards_list));
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
            }

            //
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
                if (edtSearch.getStringTrimmed().equalsIgnoreCase("")) {
//                    isSearchBarEmpty = true;
                    imgSearch.setImageResource(R.drawable.icon_search_updated);

                } else {
//                    isSearchBarEmpty = false;
                    imgSearch.setImageResource(R.drawable.icon_cross);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //   navigationBar.resetNvigationBar();
        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickBtnWallet(getBaseActivity());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvGeneric.setLayoutManager(mLayoutManager);
        rvGeneric.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, View view, Object object) {
        GenericCardListModel genericCardListModel = adapter.getItem(position);

        String fbUrl = "", twtUrl = "", googleUrl = "", linkedInurl = "";
        if (genericCardListModel.getArrSocialLogin() != null && genericCardListModel.getArrSocialLogin().size() > 0) {
            for (int i = 0; i < genericCardListModel.getArrSocialLogin().size(); i++) {
                if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                    fbUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                    twtUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                    linkedInurl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                    googleUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                }
            }
        }

        switch (view.getId()) {
            case R.id.imgInfo:
                diaLogue(genericCardListModel);

                break;
            case R.id.imgFB:
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse(fbUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgGoogle:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(googleUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgTwitter:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(twtUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgLinkedin:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(linkedInurl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.contParent:
                if (genericCardListModel.getCardList() != null && genericCardListModel.getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SpecificUserFragment.newInstance(genericCardListModel), false);
                break;
            case R.id.addminus:
                if (genericCardListModel.getRequestStatus().equalsIgnoreCase("Add friend"))
                    serviceCallAddRequest(WebServiceConstants.METHOD_SEND_FREQUEST, genericCardListModel.getId());
                else
                    serviceCallAddRequest(WebServiceConstants.METHOD_UN_FRIEND_USER, genericCardListModel.getId());
                break;
        }
    }

    private void serviceCallAddRequest(String methodName, int id) {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(getCurrentUser().getId());
        searchModel.setFriend_id(id);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(methodName, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showToast(getContext(), webResponse.result.toString());
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void diaLogue(GenericCardListModel genericCardListModel) {
        WebviewPopupDialog genericPopupDialog = WebviewPopupDialog.newInstance(getBaseActivity(),
                genericCardListModel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }
        );
        genericPopupDialog.setCancelable(true);
        genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
    }

    @OnClick({R.id.edtSearch, R.id.imgSearch, R.id.contSearch, R.id.btnInfo})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.imgSearch:
                edtSearch.setText("");
                KeyboardHelper.hideSoftKeyboard(getContext(), edtSearch);
                break;
            case R.id.contSearch:
                break;

        }
    }
}
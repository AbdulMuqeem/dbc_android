package com.app.digilink.fragments.dialogs;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.activities.BaseActivity;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.InputModel;
import com.app.digilink.widget.AnyTextView;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class WebviewPopupDialog extends DialogFragment {


    Unbinder unbinder;
    @BindView(R.id.imgUser1)
    ImageView imgUser1;
    @BindView(R.id.txtName1)
    AnyTextView txtName1;
    @BindView(R.id.txtOcup1)
    AnyTextView txtOcup1;
    @BindView(R.id.imgFB1)
    ImageView imgFB1;
    @BindView(R.id.imgTwitter1)
    ImageView imgTwitter1;
    @BindView(R.id.imgLinkedin1)
    ImageView imgLinkedin1;
    @BindView(R.id.imgGoogle1)
    ImageView imgGoogle1;
    @BindView(R.id.txtPhoneNum1)
    AnyTextView txtPhoneNum1;
    @BindView(R.id.txtEmailAddress1)
    AnyTextView txtEmailAddress1;
    @BindView(R.id.txtAddress1)
    AnyTextView txtAddress1;
    @BindView(R.id.txtNotes1)
    AnyTextView txtNotes1;
    @BindView(R.id.contCD1)
    RoundKornerLinearLayout contCD1;
    @BindView(R.id.imgDownload1)
    ImageView imgDownload1;
    @BindView(R.id.imgEdit1)
    ImageView imgEdit1;
    @BindView(R.id.imgDelete1)
    ImageView imgDelete1;
    @BindView(R.id.imgShare1)
    ImageView imgShare1;
    @BindView(R.id.imgPower1)
    CheckBox imgPower1;
    @BindView(R.id.contCD1EditCard)
    LinearLayout contCD1EditCard;
    @BindView(R.id.txtName2)
    AnyTextView txtName2;
    @BindView(R.id.txtOcup2)
    AnyTextView txtOcup2;
    @BindView(R.id.imgFB2)
    ImageView imgFB2;
    @BindView(R.id.imgTwitter2)
    ImageView imgTwitter2;
    @BindView(R.id.imgLinkedin2)
    ImageView imgLinkedin2;
    @BindView(R.id.imgGoogle2)
    ImageView imgGoogle2;
    @BindView(R.id.txtPhoneNum2)
    AnyTextView txtPhoneNum2;
    @BindView(R.id.txtEmailAddress2)
    AnyTextView txtEmailAddress2;
    @BindView(R.id.txtAddress2)
    AnyTextView txtAddress2;
    @BindView(R.id.imgUser2)
    ImageView imgUser2;
    @BindView(R.id.txtNotes2)
    AnyTextView txtNotes2;
    @BindView(R.id.contCD2)
    RoundKornerLinearLayout contCD2;
    @BindView(R.id.imgDownload2)
    ImageView imgDownload2;
    @BindView(R.id.imgEdit2)
    ImageView imgEdit2;
    @BindView(R.id.imgDelete2)
    ImageView imgDelete2;
    @BindView(R.id.imgShare2)
    ImageView imgShare2;
    @BindView(R.id.imgPower2)
    CheckBox imgPower2;
    @BindView(R.id.contCD2EditCard)
    LinearLayout contCD2EditCard;
    @BindView(R.id.txtName3)
    AnyTextView txtName3;
    @BindView(R.id.txtOcup3)
    AnyTextView txtOcup3;
    @BindView(R.id.imgFB3)
    ImageView imgFB3;
    @BindView(R.id.imgTwitter3)
    ImageView imgTwitter3;
    @BindView(R.id.imgLinkedin3)
    ImageView imgLinkedin3;
    @BindView(R.id.imgGoogle3)
    ImageView imgGoogle3;
    @BindView(R.id.imgUser3)
    CircleImageView imgUser3;
    @BindView(R.id.txtPhoneNum3)
    AnyTextView txtPhoneNum3;
    @BindView(R.id.txtEmailAddress3)
    AnyTextView txtEmailAddress3;
    @BindView(R.id.txtAddress3)
    AnyTextView txtAddress3;
    @BindView(R.id.txtNotes3)
    AnyTextView txtNotes3;
    @BindView(R.id.contCD3)
    RoundKornerLinearLayout contCD3;
    @BindView(R.id.imgDownload3)
    ImageView imgDownload3;
    @BindView(R.id.imgEdit3)
    ImageView imgEdit3;
    @BindView(R.id.imgDelete3)
    ImageView imgDelete3;
    @BindView(R.id.imgShare3)
    ImageView imgShare3;
    @BindView(R.id.imgPower3)
    CheckBox imgPower3;
    @BindView(R.id.contCD3EditCard)
    LinearLayout contCD3EditCard;
    @BindView(R.id.imgUser4)
    CircleImageView imgUser4;
    @BindView(R.id.txtName4)
    AnyTextView txtName4;
    @BindView(R.id.txtOcup4)
    AnyTextView txtOcup4;
    @BindView(R.id.imgFB4)
    ImageView imgFB4;
    @BindView(R.id.imgTwitter4)
    ImageView imgTwitter4;
    @BindView(R.id.imgLinkedin4)
    ImageView imgLinkedin4;
    @BindView(R.id.imgGoogle4)
    ImageView imgGoogle4;
    @BindView(R.id.txtPhoneNum4)
    AnyTextView txtPhoneNum4;
    @BindView(R.id.txtEmailAddress4)
    AnyTextView txtEmailAddress4;
    @BindView(R.id.txtAddress4)
    AnyTextView txtAddress4;
    @BindView(R.id.txtNotes4)
    AnyTextView txtNotes4;
    @BindView(R.id.contCD4)
    RoundKornerLinearLayout contCD4;
    @BindView(R.id.imgDownload4)
    ImageView imgDownload4;
    @BindView(R.id.imgEdit4)
    ImageView imgEdit4;
    @BindView(R.id.imgDelete4)
    ImageView imgDelete4;
    @BindView(R.id.imgShare4)
    ImageView imgShare4;
    @BindView(R.id.imgPower4)
    CheckBox imgPower4;
    @BindView(R.id.contCD4EditCard)
    LinearLayout contCD4EditCard;
    @BindView(R.id.imgUser5)
    ImageView imgUser5;
    @BindView(R.id.txtName5)
    AnyTextView txtName5;
    @BindView(R.id.txtOcup5)
    AnyTextView txtOcup5;
    @BindView(R.id.imgFB5)
    ImageView imgFB5;
    @BindView(R.id.imgTwitter5)
    ImageView imgTwitter5;
    @BindView(R.id.imgLinkedin5)
    ImageView imgLinkedin5;
    @BindView(R.id.imgGoogle5)
    ImageView imgGoogle5;
    @BindView(R.id.txtPhoneNum5)
    AnyTextView txtPhoneNum5;
    @BindView(R.id.txtEmailAddress5)
    AnyTextView txtEmailAddress5;
    @BindView(R.id.txtAddress5)
    AnyTextView txtAddress5;
    @BindView(R.id.txtNotes5)
    AnyTextView txtNotes5;
    @BindView(R.id.contCD5)
    RoundKornerLinearLayout contCD5;
    @BindView(R.id.imgDownload5)
    ImageView imgDownload5;
    @BindView(R.id.imgEdit5)
    ImageView imgEdit5;
    @BindView(R.id.imgDelete5)
    ImageView imgDelete5;
    @BindView(R.id.imgShare5)
    ImageView imgShare5;
    @BindView(R.id.imgPower5)
    CheckBox imgPower5;
    @BindView(R.id.contCD5EditCard)
    LinearLayout contCD5EditCard;
    @BindView(R.id.txtName6)
    AnyTextView txtName6;
    @BindView(R.id.txtOcup6)
    AnyTextView txtOcup6;
    @BindView(R.id.imgFB6)
    ImageView imgFB6;
    @BindView(R.id.imgTwitter6)
    ImageView imgTwitter6;
    @BindView(R.id.imgLinkedin6)
    ImageView imgLinkedin6;
    @BindView(R.id.imgGoogle6)
    ImageView imgGoogle6;
    @BindView(R.id.imgUser6)
    ImageView imgUser6;
    @BindView(R.id.txtPhoneNum6)
    AnyTextView txtPhoneNum6;
    @BindView(R.id.txtEmailAddress6)
    AnyTextView txtEmailAddress6;
    @BindView(R.id.txtAddress6)
    AnyTextView txtAddress6;
    @BindView(R.id.txtNotes6)
    AnyTextView txtNotes6;
    @BindView(R.id.contCD6)
    RoundKornerLinearLayout contCD6;
    @BindView(R.id.imgDownload6)
    ImageView imgDownload6;
    @BindView(R.id.imgEdit6)
    ImageView imgEdit6;
    @BindView(R.id.imgDelete6)
    ImageView imgDelete6;
    @BindView(R.id.imgShare6)
    ImageView imgShare6;
    @BindView(R.id.imgPower6)
    CheckBox imgPower6;
    @BindView(R.id.contCD6EditCard)
    LinearLayout contCD6EditCard;
    @BindView(R.id.txtName7)
    AnyTextView txtName7;
    @BindView(R.id.txtOcup7)
    AnyTextView txtOcup7;
    @BindView(R.id.imgFB7)
    ImageView imgFB7;
    @BindView(R.id.imgTwitter7)
    ImageView imgTwitter7;
    @BindView(R.id.imgLinkedin7)
    ImageView imgLinkedin7;
    @BindView(R.id.imgGoogle7)
    ImageView imgGoogle7;
    @BindView(R.id.imgUser7)
    CircleImageView imgUser7;
    @BindView(R.id.txtPhoneNum7)
    AnyTextView txtPhoneNum7;
    @BindView(R.id.txtEmailAddress7)
    AnyTextView txtEmailAddress7;
    @BindView(R.id.txtAddress7)
    AnyTextView txtAddress7;
    @BindView(R.id.txtNotes7)
    AnyTextView txtNotes7;
    @BindView(R.id.contCD7)
    RoundKornerLinearLayout contCD7;
    @BindView(R.id.imgDownload7)
    ImageView imgDownload7;
    @BindView(R.id.imgEdit7)
    ImageView imgEdit7;
    @BindView(R.id.imgDelete7)
    ImageView imgDelete7;
    @BindView(R.id.imgShare7)
    ImageView imgShare7;
    @BindView(R.id.imgPower7)
    CheckBox imgPower7;
    @BindView(R.id.contCD7EditCard)
    LinearLayout contCD7EditCard;
    @BindView(R.id.imgUser8)
    CircleImageView imgUser8;
    @BindView(R.id.txtName8)
    AnyTextView txtName8;
    @BindView(R.id.txtOcup8)
    AnyTextView txtOcup8;
    @BindView(R.id.imgFB8)
    ImageView imgFB8;
    @BindView(R.id.imgTwitter8)
    ImageView imgTwitter8;
    @BindView(R.id.imgLinkedin8)
    ImageView imgLinkedin8;
    @BindView(R.id.imgGoogle8)
    ImageView imgGoogle8;
    @BindView(R.id.txtPhoneNum8)
    AnyTextView txtPhoneNum8;
    @BindView(R.id.txtEmailAddress8)
    AnyTextView txtEmailAddress8;
    @BindView(R.id.txtAddress8)
    AnyTextView txtAddress8;
    @BindView(R.id.txtNotes8)
    AnyTextView txtNotes8;
    @BindView(R.id.contCD8)
    RoundKornerLinearLayout contCD8;
    @BindView(R.id.imgDownload8)
    ImageView imgDownload8;
    @BindView(R.id.imgEdit8)
    ImageView imgEdit8;
    @BindView(R.id.imgDelete8)
    ImageView imgDelete8;
    @BindView(R.id.imgShare8)
    ImageView imgShare8;
    @BindView(R.id.imgPower8)
    CheckBox imgPower8;
    @BindView(R.id.contCD8EditCard)
    LinearLayout contCD8EditCard;
    @BindView(R.id.txtName9)
    AnyTextView txtName9;
    @BindView(R.id.txtOcup9)
    AnyTextView txtOcup9;
    @BindView(R.id.imgFB9)
    ImageView imgFB9;
    @BindView(R.id.imgTwitter9)
    ImageView imgTwitter9;
    @BindView(R.id.imgLinkedin9)
    ImageView imgLinkedin9;
    @BindView(R.id.imgGoogle9)
    ImageView imgGoogle9;
    @BindView(R.id.imgUser9)
    ImageView imgUser9;
    @BindView(R.id.txtPhoneNum9)
    AnyTextView txtPhoneNum9;
    @BindView(R.id.txtEmailAddress9)
    AnyTextView txtEmailAddress9;
    @BindView(R.id.txtAddress9)
    AnyTextView txtAddress9;
    @BindView(R.id.txtNotes9)
    AnyTextView txtNotes9;
    @BindView(R.id.contCD9)
    RoundKornerLinearLayout contCD9;
    @BindView(R.id.imgDownload9)
    ImageView imgDownload9;
    @BindView(R.id.imgEdit9)
    ImageView imgEdit9;
    @BindView(R.id.imgDelete9)
    ImageView imgDelete9;
    @BindView(R.id.imgShare9)
    ImageView imgShare9;
    @BindView(R.id.imgPower9)
    CheckBox imgPower9;
    @BindView(R.id.contCD9EditCard)
    LinearLayout contCD9EditCard;
    @BindView(R.id.txtName10)
    AnyTextView txtName10;
    @BindView(R.id.txtOcup10)
    AnyTextView txtOcup10;
    @BindView(R.id.imgFB10)
    ImageView imgFB10;
    @BindView(R.id.imgTwitter10)
    ImageView imgTwitter10;
    @BindView(R.id.imgLinkedin10)
    ImageView imgLinkedin10;
    @BindView(R.id.imgGoogle10)
    ImageView imgGoogle10;
    @BindView(R.id.imgUser10)
    ImageView imgUser10;
    @BindView(R.id.txtPhoneNum10)
    AnyTextView txtPhoneNum10;
    @BindView(R.id.txtEmailAddress10)
    AnyTextView txtEmailAddress10;
    @BindView(R.id.txtAddress10)
    AnyTextView txtAddress10;
    @BindView(R.id.txtNotes10)
    AnyTextView txtNotes10;
    @BindView(R.id.contCD10)
    RoundKornerLinearLayout contCD10;
    @BindView(R.id.imgDownload10)
    ImageView imgDownload10;
    @BindView(R.id.imgEdit10)
    ImageView imgEdit10;
    @BindView(R.id.imgDelete10)
    ImageView imgDelete10;
    @BindView(R.id.imgShare10)
    ImageView imgShare10;
    @BindView(R.id.imgPower10)
    CheckBox imgPower10;
    @BindView(R.id.contCD10EditCard)
    LinearLayout contCD10EditCard;
    private GenericCardListModel genericCardListModel;
    private Typeface popin;
    private Typeface ArimaMadurai;
    private BaseActivity baseActivity;

    public WebviewPopupDialog() {
    }

    public static WebviewPopupDialog newInstance(BaseActivity baseActivity, GenericCardListModel genericCardListModel, View.OnClickListener onClickListener) {
        WebviewPopupDialog frag = new WebviewPopupDialog();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.genericCardListModel = genericCardListModel;
        frag.baseActivity = baseActivity;
        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
        popin = Typeface.createFromAsset(baseActivity.getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(baseActivity.getAssets(), "fonts/ArimaMadurai-Regular.ttf");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popweb, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        popin = Typeface.createFromAsset(baseActivity.getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(baseActivity.getAssets(), "fonts/ArimaMadurai-Regular.ttf");
        if (genericCardListModel.getCardList().size() > 0)

            setData(genericCardListModel.getCardList().get(0));
        else {
            dismiss();
        }
    }

    private void setData(CardTemplate model) {
        if (model.getCardDesign().equalsIgnoreCase("card1.html")) {
            contCD1.setVisibility(VISIBLE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB1.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter1.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin1.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle1.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card2.html")) {
            contCD2.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB2.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter2.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin2.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle2.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card3.html")) {
            contCD3.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle3.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card4.html")) {
            contCD4.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle4.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card5.html")) {
            contCD5.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle5.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card6.html")) {
            contCD6.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle6.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card7.html")) {
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(VISIBLE);
            contCD6.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle7.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card8.html")) {
            contCD8.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle8.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card9.html")) {
            contCD9.setVisibility(VISIBLE);

            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle9.setVisibility(VISIBLE);
                    }
                }
        } else if (model.getCardDesign().equalsIgnoreCase("card10.html")) {
            contCD10.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);

            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        imgFB10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        imgTwitter10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        imgLinkedin10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        imgGoogle10.setVisibility(VISIBLE);
                    }
                }
        }


        InputModel inputModel = new InputModel();
        inputModel.setFirstName(model.getFirst_name());
        inputModel.setLastName(model.getLast_name());
        inputModel.setPhone(model.getPhone());
        inputModel.setAddress(model.getAddress());
        inputModel.setOccupation(model.getOccupation());
        inputModel.setEmail(model.getEmail());

        txtPhoneNum1.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum2.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum3.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum4.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum5.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum6.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum7.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum8.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum9.setTextSize(Float.parseFloat(model.getPhone_size()));
        txtPhoneNum10.setTextSize(Float.parseFloat(model.getPhone_size()));

        txtAddress1.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress2.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress3.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress4.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress5.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress6.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress7.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress8.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress9.setTextSize(Float.parseFloat(model.getAddress_size()));
        txtAddress10.setTextSize(Float.parseFloat(model.getAddress_size()));

        txtEmailAddress1.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress2.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress3.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress4.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress5.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress6.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress7.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress8.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress9.setTextSize(Float.parseFloat(model.getEmail_size()));
        txtEmailAddress10.setTextSize(Float.parseFloat(model.getEmail_size()));


        txtOcup1.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup2.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup3.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup4.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup5.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup6.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup7.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup8.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup9.setTextSize(Float.parseFloat(model.getOccupation_size()));
        txtOcup10.setTextSize(Float.parseFloat(model.getOccupation_size()));

        txtName1.setTextSize(Float.parseFloat(model.getName_size()));
        txtName2.setTextSize(Float.parseFloat(model.getName_size()));
        txtName3.setTextSize(Float.parseFloat(model.getName_size()));
        txtName4.setTextSize(Float.parseFloat(model.getName_size()));
        txtName5.setTextSize(Float.parseFloat(model.getName_size()));
        txtName6.setTextSize(Float.parseFloat(model.getName_size()));
        txtName7.setTextSize(Float.parseFloat(model.getName_size()));
        txtName8.setTextSize(Float.parseFloat(model.getName_size()));
        txtName9.setTextSize(Float.parseFloat(model.getName_size()));
        txtName10.setTextSize(Float.parseFloat(model.getName_size()));


        txtNotes1.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes2.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes3.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes4.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes5.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes6.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes7.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes8.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes9.setTextSize(Float.parseFloat(model.getNotes_size()));
        txtNotes10.setTextSize(Float.parseFloat(model.getNotes_size()));
        
        
        
        txtAddress1.setText(model.getAddress());
        txtAddress2.setText(model.getAddress());
        txtAddress3.setText(model.getAddress());
        txtAddress4.setText(model.getAddress());
        txtAddress5.setText(model.getAddress());
        txtAddress6.setText(model.getAddress());
        txtAddress7.setText(model.getAddress());
        txtAddress8.setText(model.getAddress());
        txtAddress9.setText(model.getAddress());
        txtAddress10.setText(model.getAddress());

        txtEmailAddress1.setText(model.getEmail());
        txtEmailAddress2.setText(model.getEmail());
        txtEmailAddress3.setText(model.getEmail());
        txtEmailAddress4.setText(model.getEmail());
        txtEmailAddress5.setText(model.getEmail());
        txtEmailAddress6.setText(model.getEmail());

        txtEmailAddress7.setText(model.getEmail());
        txtEmailAddress8.setText(model.getEmail());
        txtEmailAddress9.setText(model.getEmail());
        txtEmailAddress10.setText(model.getEmail());

        txtName1.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName2.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName3.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName4.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName5.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName6.setText(model.getFirst_name() + " " + model.getLast_name());

        txtName7.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName8.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName9.setText(model.getFirst_name() + " " + model.getLast_name());
        txtName10.setText(model.getFirst_name() + " " + model.getLast_name());

        txtOcup1.setText(model.getOccupation());
        txtOcup2.setText(model.getOccupation());
        txtOcup3.setText(model.getOccupation());
        txtOcup4.setText(model.getOccupation());
        txtOcup5.setText(model.getOccupation());
        txtOcup6.setText(model.getOccupation());

        txtOcup7.setText(model.getOccupation());
        txtOcup8.setText(model.getOccupation());
        txtOcup9.setText(model.getOccupation());
        txtOcup10.setText(model.getOccupation());


        txtNotes1.setText(model.getNotes());
        txtNotes2.setText(model.getNotes());
        txtNotes3.setText(model.getNotes());
        txtNotes4.setText(model.getNotes());
        txtNotes5.setText(model.getNotes());
        txtNotes6.setText(model.getNotes());

        txtNotes7.setText(model.getNotes());
        txtNotes8.setText(model.getNotes());
        txtNotes9.setText(model.getNotes());
        txtNotes10.setText(model.getNotes());

        txtPhoneNum1.setText(model.getPhone());
        txtPhoneNum2.setText(model.getPhone());
        txtPhoneNum3.setText(model.getPhone());
        txtPhoneNum4.setText(model.getPhone());
        txtPhoneNum5.setText(model.getPhone());
        txtPhoneNum6.setText(model.getPhone());

        txtPhoneNum7.setText(model.getPhone());
        txtPhoneNum8.setText(model.getPhone());
        txtPhoneNum9.setText(model.getPhone());
        txtPhoneNum10.setText(model.getPhone());

        ImageLoader.getInstance().displayImage(model.getImage(), imgUser1);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser2);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser3);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser4);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser5);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser6);

        ImageLoader.getInstance().displayImage(model.getImage(), imgUser7);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser8);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser9);
        ImageLoader.getInstance().displayImage(model.getImage(), imgUser10);


        if (!model.getAddressColor().equalsIgnoreCase("")) {
            txtAddress1.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress2.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress3.setTextColor(Color.parseColor(model.getAddressColor()));

            txtAddress4.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress5.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress6.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress7.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress8.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress9.setTextColor(Color.parseColor(model.getAddressColor()));
            txtAddress10.setTextColor(Color.parseColor(model.getAddressColor()));
        }
        if (!model.getEmailColor().equalsIgnoreCase("")) {
            txtEmailAddress1.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress2.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress3.setTextColor(Color.parseColor(model.getEmailColor()));

            txtEmailAddress4.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress5.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress6.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress7.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress8.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress9.setTextColor(Color.parseColor(model.getEmailColor()));
            txtEmailAddress10.setTextColor(Color.parseColor(model.getEmailColor()));
        }
        if (!model.getNameColor().equalsIgnoreCase("")) {
            txtName1.setTextColor(Color.parseColor(model.getNameColor()));
            txtName2.setTextColor(Color.parseColor(model.getNameColor()));
            txtName3.setTextColor(Color.parseColor(model.getNameColor()));
            txtName4.setTextColor(Color.parseColor(model.getNameColor()));
            txtName5.setTextColor(Color.parseColor(model.getNameColor()));
            txtName6.setTextColor(Color.parseColor(model.getNameColor()));
            txtName7.setTextColor(Color.parseColor(model.getNameColor()));
            txtName8.setTextColor(Color.parseColor(model.getNameColor()));
            txtName9.setTextColor(Color.parseColor(model.getNameColor()));
            txtName10.setTextColor(Color.parseColor(model.getNameColor()));
        }
        if (!model.getOccupationColor().equalsIgnoreCase("")) {
            txtOcup1.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup2.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup3.setTextColor(Color.parseColor(model.getOccupationColor()));

            txtOcup4.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup5.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup6.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup7.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup8.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup9.setTextColor(Color.parseColor(model.getOccupationColor()));
            txtOcup10.setTextColor(Color.parseColor(model.getOccupationColor()));

        }
        if (!model.getNotesColor().equalsIgnoreCase("")) {
            txtNotes1.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes2.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes3.setTextColor(Color.parseColor(model.getNotesColor()));

            txtNotes4.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes5.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes6.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes7.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes8.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes9.setTextColor(Color.parseColor(model.getNotesColor()));
            txtNotes10.setTextColor(Color.parseColor(model.getNotesColor()));
        }
        if (!model.getPhoneColor().equalsIgnoreCase("")) {
            txtPhoneNum1.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum2.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum3.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum4.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum5.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum6.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum7.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum8.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum9.setTextColor(Color.parseColor(model.getPhoneColor()));
            txtPhoneNum10.setTextColor(Color.parseColor(model.getPhoneColor()));

        }


        if (model.getPhone_font() != null && model.getPhone_font().equalsIgnoreCase("Arima Madurai")) {


            txtPhoneNum1.setTypeface(ArimaMadurai);
            txtAddress1.setTypeface(ArimaMadurai);
            txtEmailAddress3.setTypeface(ArimaMadurai);

            txtPhoneNum3.setTypeface(ArimaMadurai);
            txtAddress3.setTypeface(ArimaMadurai);
            txtEmailAddress3.setTypeface(ArimaMadurai);
        } else {
            txtPhoneNum1.setTypeface(popin);
            txtAddress1.setTypeface(popin);
            txtEmailAddress1.setTypeface(popin);

            txtPhoneNum3.setTypeface(popin);
            txtAddress3.setTypeface(popin);
            txtEmailAddress1.setTypeface(popin);
        }


        if (model.getOccupation_font() != null && model.getOccupation_font().equalsIgnoreCase("Arima Madurai")) {


            txtOcup1.setTypeface(ArimaMadurai);
            txtOcup3.setTypeface(ArimaMadurai);
            txtName3.setTypeface(ArimaMadurai);
            txtName1.setTypeface(ArimaMadurai);


        } else {
            txtOcup1.setTypeface(popin);
            txtOcup3.setTypeface(popin);
            txtName1.setTypeface(popin);
            txtName3.setTypeface(popin);
        }


        if (model.getNotes_font() != null && model.getNotes_font().equalsIgnoreCase("Arima Madurai")) {

            txtNotes1.setTypeface(ArimaMadurai);
            txtNotes3.setTypeface(ArimaMadurai);
        } else {
            txtNotes1.setTypeface(popin);
            txtNotes3.setTypeface(popin);


        }


    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}


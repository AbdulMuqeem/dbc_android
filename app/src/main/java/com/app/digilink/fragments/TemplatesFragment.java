package com.app.digilink.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.TemplateCardImagesAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.AppConstants;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.InputModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.sendingmodels.SendingModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.reflect.TypeToken;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;

public class TemplatesFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener {

    @BindView(R.id.imgUser1)
    ImageView imgUser1;
    @BindView(R.id.txtName1)
    AnyTextView txtName1;
    @BindView(R.id.txtOcup1)
    AnyTextView txtOcup1;
    @BindView(R.id.imgFB1)
    ImageView imgFB1;
    @BindView(R.id.imgTwitter1)
    ImageView imgTwitter1;
    @BindView(R.id.imgLinkedin1)
    ImageView imgLinkedin1;
    @BindView(R.id.imgGoogle1)
    ImageView imgGoogle1;
    @BindView(R.id.txtPhoneNum1)
    AnyTextView txtPhoneNum1;
    @BindView(R.id.txtEmailAddress1)
    AnyTextView txtEmailAddress1;
    @BindView(R.id.txtAddress1)
    AnyTextView txtAddress1;
    @BindView(R.id.txtNotes1)
    AnyTextView txtNotes1;
    @BindView(R.id.contCD1)
    RoundKornerLinearLayout contCD1;
    @BindView(R.id.imgDownload1)
    ImageView imgDownload1;
    @BindView(R.id.imgEdit1)
    ImageView imgEdit1;
    @BindView(R.id.imgDelete1)
    ImageView imgDelete1;
    @BindView(R.id.imgShare1)
    ImageView imgShare1;
    @BindView(R.id.imgPower1)
    CheckBox imgPower1;
    @BindView(R.id.contCD1EditCard)
    LinearLayout contCD1EditCard;
    @BindView(R.id.txtName2)
    AnyTextView txtName2;
    @BindView(R.id.txtOcup2)
    AnyTextView txtOcup2;
    @BindView(R.id.imgFB2)
    ImageView imgFB2;
    @BindView(R.id.imgTwitter2)
    ImageView imgTwitter2;
    @BindView(R.id.imgLinkedin2)
    ImageView imgLinkedin2;
    @BindView(R.id.imgGoogle2)
    ImageView imgGoogle2;
    @BindView(R.id.txtPhoneNum2)
    AnyTextView txtPhoneNum2;
    @BindView(R.id.txtEmailAddress2)
    AnyTextView txtEmailAddress2;
    @BindView(R.id.txtAddress2)
    AnyTextView txtAddress2;
    @BindView(R.id.imgUser2)
    ImageView imgUser2;
    @BindView(R.id.txtNotes2)
    AnyTextView txtNotes2;
    @BindView(R.id.contCD2)
    RoundKornerLinearLayout contCD2;
    @BindView(R.id.imgDownload2)
    ImageView imgDownload2;
    @BindView(R.id.imgEdit2)
    ImageView imgEdit2;
    @BindView(R.id.imgDelete2)
    ImageView imgDelete2;
    @BindView(R.id.imgShare2)
    ImageView imgShare2;
    @BindView(R.id.imgPower2)
    CheckBox imgPower2;
    @BindView(R.id.contCD2EditCard)
    LinearLayout contCD2EditCard;
    @BindView(R.id.txtName3)
    AnyTextView txtName3;
    @BindView(R.id.txtOcup3)
    AnyTextView txtOcup3;
    @BindView(R.id.imgFB3)
    ImageView imgFB3;
    @BindView(R.id.imgTwitter3)
    ImageView imgTwitter3;
    @BindView(R.id.imgLinkedin3)
    ImageView imgLinkedin3;
    @BindView(R.id.imgGoogle3)
    ImageView imgGoogle3;
    @BindView(R.id.imgUser3)
    CircleImageView imgUser3;
    @BindView(R.id.txtPhoneNum3)
    AnyTextView txtPhoneNum3;
    @BindView(R.id.txtEmailAddress3)
    AnyTextView txtEmailAddress3;
    @BindView(R.id.txtAddress3)
    AnyTextView txtAddress3;
    @BindView(R.id.txtNotes3)
    AnyTextView txtNotes3;
    @BindView(R.id.contCD3)
    RoundKornerLinearLayout contCD3;
    @BindView(R.id.imgDownload3)
    ImageView imgDownload3;
    @BindView(R.id.imgEdit3)
    ImageView imgEdit3;
    @BindView(R.id.imgDelete3)
    ImageView imgDelete3;
    @BindView(R.id.imgShare3)
    ImageView imgShare3;
    @BindView(R.id.imgPower3)
    CheckBox imgPower3;
    @BindView(R.id.contCD3EditCard)
    LinearLayout contCD3EditCard;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.txtID)
    AnyTextView txtID;
    @BindView(R.id.rvTemplates)
    RecyclerView rvTemplates;
    @BindView(R.id.contCard)
    LinearLayout contCard;
    @BindView(R.id.contDesign)
    LinearLayout contDesign;
    @BindView(R.id.btnInvite)
    ImageView btnInvite;
    @BindView(R.id.btnSave)
    ImageView btnSave;
    @BindView(R.id.btnColor)
    AnyTextView btnColor;
    @BindView(R.id.btnInput)
    AnyTextView btnInput;
    @BindView(R.id.btnfont)
    AnyTextView btnfont;
    @BindView(R.id.contEditCard)
    LinearLayout contEditCard;
    @BindView(R.id.edtEmail)
    AnyEditTextView edtEmail;
    @BindView(R.id.btnSendInvites)
    AnyTextView btnSendInvites;
    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.imgUser4)
    CircleImageView imgUser4;
    @BindView(R.id.txtName4)
    AnyTextView txtName4;
    @BindView(R.id.txtOcup4)
    AnyTextView txtOcup4;
    @BindView(R.id.imgFB4)
    ImageView imgFB4;
    @BindView(R.id.imgTwitter4)
    ImageView imgTwitter4;
    @BindView(R.id.imgLinkedin4)
    ImageView imgLinkedin4;
    @BindView(R.id.imgGoogle4)
    ImageView imgGoogle4;
    @BindView(R.id.txtPhoneNum4)
    AnyTextView txtPhoneNum4;
    @BindView(R.id.txtEmailAddress4)
    AnyTextView txtEmailAddress4;
    @BindView(R.id.txtAddress4)
    AnyTextView txtAddress4;
    @BindView(R.id.txtNotes4)
    AnyTextView txtNotes4;
    @BindView(R.id.contCD4)
    RoundKornerLinearLayout contCD4;
    @BindView(R.id.imgDownload4)
    ImageView imgDownload4;
    @BindView(R.id.imgEdit4)
    ImageView imgEdit4;
    @BindView(R.id.imgDelete4)
    ImageView imgDelete4;
    @BindView(R.id.imgShare4)
    ImageView imgShare4;
    @BindView(R.id.imgPower4)
    CheckBox imgPower4;
    @BindView(R.id.contCD4EditCard)
    LinearLayout contCD4EditCard;
    @BindView(R.id.imgUser5)
    ImageView imgUser5;
    @BindView(R.id.txtName5)
    AnyTextView txtName5;
    @BindView(R.id.txtOcup5)
    AnyTextView txtOcup5;
    @BindView(R.id.imgFB5)
    ImageView imgFB5;
    @BindView(R.id.imgTwitter5)
    ImageView imgTwitter5;
    @BindView(R.id.imgLinkedin5)
    ImageView imgLinkedin5;
    @BindView(R.id.imgGoogle5)
    ImageView imgGoogle5;
    @BindView(R.id.txtPhoneNum5)
    AnyTextView txtPhoneNum5;
    @BindView(R.id.txtEmailAddress5)
    AnyTextView txtEmailAddress5;
    @BindView(R.id.txtAddress5)
    AnyTextView txtAddress5;
    @BindView(R.id.txtNotes5)
    AnyTextView txtNotes5;
    @BindView(R.id.contCD5)
    RoundKornerLinearLayout contCD5;
    @BindView(R.id.imgDownload5)
    ImageView imgDownload5;
    @BindView(R.id.imgEdit5)
    ImageView imgEdit5;
    @BindView(R.id.imgDelete5)
    ImageView imgDelete5;
    @BindView(R.id.imgShare5)
    ImageView imgShare5;
    @BindView(R.id.imgPower5)
    CheckBox imgPower5;
    @BindView(R.id.contCD5EditCard)
    LinearLayout contCD5EditCard;
    @BindView(R.id.txtName6)
    AnyTextView txtName6;
    @BindView(R.id.txtOcup6)
    AnyTextView txtOcup6;
    @BindView(R.id.imgFB6)
    ImageView imgFB6;
    @BindView(R.id.imgTwitter6)
    ImageView imgTwitter6;
    @BindView(R.id.imgLinkedin6)
    ImageView imgLinkedin6;
    @BindView(R.id.imgGoogle6)
    ImageView imgGoogle6;
    @BindView(R.id.imgUser6)
    ImageView imgUser6;
    @BindView(R.id.txtPhoneNum6)
    AnyTextView txtPhoneNum6;
    @BindView(R.id.txtEmailAddress6)
    AnyTextView txtEmailAddress6;
    @BindView(R.id.txtAddress6)
    AnyTextView txtAddress6;
    @BindView(R.id.txtNotes6)
    AnyTextView txtNotes6;
    @BindView(R.id.contCD6)
    RoundKornerLinearLayout contCD6;
    @BindView(R.id.imgDownload6)
    ImageView imgDownload6;
    @BindView(R.id.imgEdit6)
    ImageView imgEdit6;
    @BindView(R.id.imgDelete6)
    ImageView imgDelete6;
    @BindView(R.id.imgShare6)
    ImageView imgShare6;
    @BindView(R.id.imgPower6)
    CheckBox imgPower6;
    @BindView(R.id.contCD6EditCard)
    LinearLayout contCD6EditCard;
    @BindView(R.id.txtName7)
    AnyTextView txtName7;
    @BindView(R.id.txtOcup7)
    AnyTextView txtOcup7;
    @BindView(R.id.imgFB7)
    ImageView imgFB7;
    @BindView(R.id.imgTwitter7)
    ImageView imgTwitter7;
    @BindView(R.id.imgLinkedin7)
    ImageView imgLinkedin7;
    @BindView(R.id.imgGoogle7)
    ImageView imgGoogle7;
    @BindView(R.id.imgUser7)
    CircleImageView imgUser7;
    @BindView(R.id.txtPhoneNum7)
    AnyTextView txtPhoneNum7;
    @BindView(R.id.txtEmailAddress7)
    AnyTextView txtEmailAddress7;
    @BindView(R.id.txtAddress7)
    AnyTextView txtAddress7;
    @BindView(R.id.txtNotes7)
    AnyTextView txtNotes7;
    @BindView(R.id.contCD7)
    RoundKornerLinearLayout contCD7;
    @BindView(R.id.imgDownload7)
    ImageView imgDownload7;
    @BindView(R.id.imgEdit7)
    ImageView imgEdit7;
    @BindView(R.id.imgDelete7)
    ImageView imgDelete7;
    @BindView(R.id.imgShare7)
    ImageView imgShare7;
    @BindView(R.id.imgPower7)
    CheckBox imgPower7;
    @BindView(R.id.contCD7EditCard)
    LinearLayout contCD7EditCard;
    @BindView(R.id.imgUser8)
    CircleImageView imgUser8;
    @BindView(R.id.txtName8)
    AnyTextView txtName8;
    @BindView(R.id.txtOcup8)
    AnyTextView txtOcup8;
    @BindView(R.id.imgFB8)
    ImageView imgFB8;
    @BindView(R.id.imgTwitter8)
    ImageView imgTwitter8;
    @BindView(R.id.imgLinkedin8)
    ImageView imgLinkedin8;
    @BindView(R.id.imgGoogle8)
    ImageView imgGoogle8;
    @BindView(R.id.txtPhoneNum8)
    AnyTextView txtPhoneNum8;
    @BindView(R.id.txtEmailAddress8)
    AnyTextView txtEmailAddress8;
    @BindView(R.id.txtAddress8)
    AnyTextView txtAddress8;
    @BindView(R.id.txtNotes8)
    AnyTextView txtNotes8;
    @BindView(R.id.contCD8)
    RoundKornerLinearLayout contCD8;
    @BindView(R.id.imgDownload8)
    ImageView imgDownload8;
    @BindView(R.id.imgEdit8)
    ImageView imgEdit8;
    @BindView(R.id.imgDelete8)
    ImageView imgDelete8;
    @BindView(R.id.imgShare8)
    ImageView imgShare8;
    @BindView(R.id.imgPower8)
    CheckBox imgPower8;
    @BindView(R.id.contCD8EditCard)
    LinearLayout contCD8EditCard;
    @BindView(R.id.txtName9)
    AnyTextView txtName9;
    @BindView(R.id.txtOcup9)
    AnyTextView txtOcup9;
    @BindView(R.id.imgFB9)
    ImageView imgFB9;
    @BindView(R.id.imgTwitter9)
    ImageView imgTwitter9;
    @BindView(R.id.imgLinkedin9)
    ImageView imgLinkedin9;
    @BindView(R.id.imgGoogle9)
    ImageView imgGoogle9;
    @BindView(R.id.imgUser9)
    ImageView imgUser9;
    @BindView(R.id.txtPhoneNum9)
    AnyTextView txtPhoneNum9;
    @BindView(R.id.txtEmailAddress9)
    AnyTextView txtEmailAddress9;
    @BindView(R.id.txtAddress9)
    AnyTextView txtAddress9;
    @BindView(R.id.txtNotes9)
    AnyTextView txtNotes9;
    @BindView(R.id.contCD9)
    RoundKornerLinearLayout contCD9;
    @BindView(R.id.imgDownload9)
    ImageView imgDownload9;
    @BindView(R.id.imgEdit9)
    ImageView imgEdit9;
    @BindView(R.id.imgDelete9)
    ImageView imgDelete9;
    @BindView(R.id.imgShare9)
    ImageView imgShare9;
    @BindView(R.id.imgPower9)
    CheckBox imgPower9;
    @BindView(R.id.contCD9EditCard)
    LinearLayout contCD9EditCard;
    @BindView(R.id.txtName10)
    AnyTextView txtName10;
    @BindView(R.id.txtOcup10)
    AnyTextView txtOcup10;
    @BindView(R.id.imgFB10)
    ImageView imgFB10;
    @BindView(R.id.imgTwitter10)
    ImageView imgTwitter10;
    @BindView(R.id.imgLinkedin10)
    ImageView imgLinkedin10;
    @BindView(R.id.imgGoogle10)
    ImageView imgGoogle10;
    @BindView(R.id.imgUser10)
    ImageView imgUser10;
    @BindView(R.id.txtPhoneNum10)
    AnyTextView txtPhoneNum10;
    @BindView(R.id.txtEmailAddress10)
    AnyTextView txtEmailAddress10;
    @BindView(R.id.txtAddress10)
    AnyTextView txtAddress10;
    @BindView(R.id.txtNotes10)
    AnyTextView txtNotes10;
    @BindView(R.id.contCD10)
    RoundKornerLinearLayout contCD10;
    @BindView(R.id.imgDownload10)
    ImageView imgDownload10;
    @BindView(R.id.imgEdit10)
    ImageView imgEdit10;
    @BindView(R.id.imgDelete10)
    ImageView imgDelete10;
    @BindView(R.id.imgShare10)
    ImageView imgShare10;
    @BindView(R.id.imgPower10)
    CheckBox imgPower10;
    @BindView(R.id.contCD10EditCard)
    LinearLayout contCD10EditCard;
    @BindView(R.id.chkPrimaryCard)
    CheckBox chkPrimaryCard;

    private BottomSheetBehavior sheetBehavior;
    private TemplateCardImagesAdapter adapTemplateCards;
    private ArrayList<CardTemplate> arrayList;
    private boolean isFromCorporate;
    private SendingModel sendingModel;
    private CardTemplate cardListModel;
    private CardTemplate cardsItemFromAPI;
    private int cardID;
    private boolean isPrimaryCard;

    private Typeface popin;
    private Typeface ArimaMadurai;

    //
    public static TemplatesFragment newInstance(boolean isFromCorporate, SendingModel sendingModel, CardTemplate cardsItemFromAPI) {
        Bundle args = new Bundle();
        TemplatesFragment fragment = new TemplatesFragment();
        fragment.setArguments(args);
        fragment.isFromCorporate = isFromCorporate;
        fragment.cardsItemFromAPI = cardsItemFromAPI;
        fragment.sendingModel = sendingModel;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_templates;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        adapTemplateCards = new TemplateCardImagesAdapter(getBaseActivity(), false, arrayList, this);
        popin = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/ArimaMadurai-Regular.ttf");

    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bindView();
        serviceCallGetCardTempates();
        if (isFromCorporate) {
            txtTitle.setText("Corporate Business Card");
            btnInvite.setVisibility(VISIBLE);
        } else {
            btnInvite.setVisibility(GONE);
            txtTitle.setText("Personal Business Card");
        }
        if (sendingModel != null) {
            setData(cardsItemFromAPI, true);
        } else {
            setData(cardsItemFromAPI, false);

        }

        bottomSheetBind();

    }

    private void bottomSheetBind() {
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {

        chkPrimaryCard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    isPrimaryCard = true;

                } else {
                    isPrimaryCard = false;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        Unbinder unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //  navigationBar.setVisibility(GONE);
//        //   navigationBar.resetNvigationBar();
//        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickNewCard(getBaseActivity());

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvTemplates.setLayoutManager(mLayoutManager);
        rvTemplates.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvTemplates.setAdapter(adapTemplateCards);
        adapTemplateCards.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, View view, Object object) {
        cardID = position;
        cardListModel = adapTemplateCards.getItem(position);
        switch (view.getId()) {
            case R.id.imgTemplate:

                changeCardDesign(cardListModel.getCardDesign());
                cardsItemFromAPI.setCardDesign(cardListModel.getCardDesign());
                for (CardTemplate cardTemplate : arrayList) {
                    cardTemplate.setSelected(false);
                }
                adapTemplateCards.getItem(position).setSelected(true);
                adapTemplateCards.notifyDataSetChanged();
                break;
        }
    }


    private void serviceCallGetCardTempates() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_CARDTEMPATES_USER, getCurrentUser().getId(),
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                Type type = new TypeToken<ArrayList<CardTemplate>>() {
                                }.getType();
                                ArrayList<CardTemplate> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                arrayList.addAll(arrayList1);
                                adapTemplateCards.notifyDataSetChanged();

//                                webUrl.loadUrl(arrayList1.get(0).getCard());

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }


    @OnClick({R.id.btnSave, R.id.contEditCard, R.id.imgNotificationIcon, R.id.btnColor, R.id.btnInput, R.id.btnfont, R.id.imgBackBtn, R.id.btnInvite, R.id.btnSendInvites})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSave:

                if (sendingModel != null) {
                    serviceCallNewCard(sendingModel);
                } else serviceCallSaveCardTemplate();


                break;
            case R.id.contEditCard:


                break;

            case R.id.btnColor:
                getBaseActivity().addDockableFragment(EditCardFragment.newInstance(cardsItemFromAPI, "color"), false);
                break;
            case R.id.btnInput:
                getBaseActivity().addDockableFragment(EditCardFragment.newInstance(cardsItemFromAPI, "input"), false);
                break;
            case R.id.btnfont:
                getBaseActivity().addDockableFragment(EditCardFragment.newInstance(cardsItemFromAPI, "font"), false);
                break;

            case R.id.imgNotificationIcon:
                UIHelper.showLongToastInCenter(getBaseActivity(), "This Feature will be implemented in Future builds");
                break;

            case R.id.btnInvite:

                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    btnBottomSheet.setText("Expand sheet");
//                    bottomSheet.setVisibility(View.GONE);
                }
//                newDialog();
                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.btnSendInvites:
                serviceCallInvite();
                break;
        }
    }

    private void serviceCallSaveCardTemplate() {


        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_id(cardsItemFromAPI.getId());
        searchModel.setCard_template(cardsItemFromAPI.getCardDesign());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_CARD_TEMPLATE_SAVE, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, getCurrentUser());
                                if (isPrimaryCard)
                                    serviceCallPrimaryCard(cardsItemFromAPI);

                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    private void serviceCallInvite() {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(getCurrentUser().getId());
        searchModel.setEmail(edtEmail.getStringTrimmed());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_INVITE, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                UIHelper.showAlertDialog(webResponse.message, "Success", getContext());

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void serviceCallNewCard(SendingModel sendingModel) {


        SearchModel searchModel = new SearchModel();
        searchModel.setCard_id(sendingModel.getCarid());
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_design(sendingModel.getCard_design());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_CARD_ACTIVE, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
//
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    private void newDialog() {
        final Dialog mBottomSheetDialog = new Dialog(getBaseActivity(), R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(R.layout.popup_generic); // your custom view.
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();
    }


    private void setData(CardTemplate userModel, boolean isFromSecondCard) {


        if (isFromSecondCard) {
            InputModel inputModel = new InputModel();
            inputModel.setFirstName(sendingModel.getFirstName());
            inputModel.setLastName(sendingModel.getlName());
            inputModel.setPhone(sendingModel.getPhone());
            inputModel.setAddress(sendingModel.getAddress());
            inputModel.setOccupation(sendingModel.getOccupation());
            inputModel.setEmail(sendingModel.getEmail());

            txtAddress1.setText(sendingModel.getAddress());
            txtAddress2.setText(sendingModel.getAddress());
            txtAddress3.setText(sendingModel.getAddress());
            txtAddress4.setText(sendingModel.getAddress());
            txtAddress5.setText(sendingModel.getAddress());
            txtAddress6.setText(sendingModel.getAddress());
            txtAddress7.setText(sendingModel.getAddress());
            txtAddress8.setText(sendingModel.getAddress());
            txtAddress9.setText(sendingModel.getAddress());
            txtAddress10.setText(sendingModel.getAddress());
            txtEmailAddress1.setText(sendingModel.getEmail());
            txtEmailAddress2.setText(sendingModel.getEmail());
            txtEmailAddress3.setText(sendingModel.getEmail());
            txtEmailAddress4.setText(sendingModel.getEmail());
            txtEmailAddress5.setText(sendingModel.getEmail());
            txtEmailAddress6.setText(sendingModel.getEmail());

            txtEmailAddress7.setText(sendingModel.getEmail());
            txtEmailAddress8.setText(sendingModel.getEmail());
            txtEmailAddress9.setText(sendingModel.getEmail());
            txtEmailAddress10.setText(sendingModel.getEmail());
            txtName1.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName2.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName3.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName4.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName5.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName6.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName7.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName8.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName9.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtName10.setText(sendingModel.getFirstName() + " " + sendingModel.getlName());
            txtOcup1.setText(sendingModel.getOccupation());
            txtOcup2.setText(sendingModel.getOccupation());
            txtOcup3.setText(sendingModel.getOccupation());
            txtOcup4.setText(sendingModel.getOccupation());
            txtOcup5.setText(sendingModel.getOccupation());
            txtOcup6.setText(sendingModel.getOccupation());

            txtOcup7.setText(sendingModel.getOccupation());
            txtOcup8.setText(sendingModel.getOccupation());
            txtOcup9.setText(sendingModel.getOccupation());
            txtOcup10.setText(sendingModel.getOccupation());
            txtNotes1.setText(sendingModel.getNotes());
            txtNotes2.setText(sendingModel.getNotes());
            txtNotes3.setText(sendingModel.getNotes());
            txtNotes4.setText(sendingModel.getNotes());
            txtNotes5.setText(sendingModel.getNotes());
            txtNotes6.setText(sendingModel.getNotes());
            txtNotes7.setText(sendingModel.getNotes());
            txtNotes8.setText(sendingModel.getNotes());
            txtNotes9.setText(sendingModel.getNotes());
            txtNotes10.setText(sendingModel.getNotes());
            txtPhoneNum1.setText(sendingModel.getPhone());
            txtPhoneNum2.setText(sendingModel.getPhone());
            txtPhoneNum3.setText(sendingModel.getPhone());
            txtPhoneNum4.setText(sendingModel.getPhone());
            txtPhoneNum5.setText(sendingModel.getPhone());
            txtPhoneNum6.setText(sendingModel.getPhone());
            txtPhoneNum7.setText(sendingModel.getPhone());
            txtPhoneNum8.setText(sendingModel.getPhone());
            txtPhoneNum9.setText(sendingModel.getPhone());
            txtPhoneNum10.setText(sendingModel.getPhone());
            if (sendingModel.getImage() != null) {
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser1);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser2);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser3);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser4);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser5);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser6);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser7);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser8);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser9);
                ImageLoader.getInstance().displayImage(sendingModel.getImage().getAbsolutePath(), imgUser10);
            } else {
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser1);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser2);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser3);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser4);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser5);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser6);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser7);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser8);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser9);
                ImageLoader.getInstance().displayImage(getCurrentUser().getCardList().get(0).getImage(), imgUser10);
            }


            changeCardDesign(sendingModel.getCard_design());
        } else {
            InputModel inputModel = new InputModel();
            inputModel.setFirstName(userModel.getFirst_name());
            inputModel.setLastName(userModel.getLast_name());
            inputModel.setPhone(userModel.getPhone());
            inputModel.setAddress(userModel.getAddress());
            inputModel.setOccupation(userModel.getOccupation());
            inputModel.setEmail(userModel.getEmail());

            txtAddress1.setText(userModel.getAddress());
            txtAddress2.setText(userModel.getAddress());
            txtAddress3.setText(userModel.getAddress());
            txtAddress4.setText(userModel.getAddress());
            txtAddress5.setText(userModel.getAddress());
            txtAddress6.setText(userModel.getAddress());
            txtAddress7.setText(userModel.getAddress());
            txtAddress8.setText(userModel.getAddress());
            txtAddress9.setText(userModel.getAddress());
            txtAddress10.setText(userModel.getAddress());
            txtEmailAddress1.setText(userModel.getEmail());
            txtEmailAddress2.setText(userModel.getEmail());
            txtEmailAddress3.setText(userModel.getEmail());
            txtEmailAddress4.setText(userModel.getEmail());
            txtEmailAddress5.setText(userModel.getEmail());
            txtEmailAddress6.setText(userModel.getEmail());

            txtEmailAddress7.setText(userModel.getEmail());
            txtEmailAddress8.setText(userModel.getEmail());
            txtEmailAddress9.setText(userModel.getEmail());
            txtEmailAddress10.setText(userModel.getEmail());
            txtName1.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName2.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName3.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName4.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName5.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName6.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName7.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName8.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName9.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtName10.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
            txtOcup1.setText(userModel.getOccupation());
            txtOcup2.setText(userModel.getOccupation());
            txtOcup3.setText(userModel.getOccupation());
            txtOcup4.setText(userModel.getOccupation());
            txtOcup5.setText(userModel.getOccupation());
            txtOcup6.setText(userModel.getOccupation());

            txtOcup7.setText(userModel.getOccupation());
            txtOcup8.setText(userModel.getOccupation());
            txtOcup9.setText(userModel.getOccupation());
            txtOcup10.setText(userModel.getOccupation());
            txtNotes1.setText(userModel.getNotes());
            txtNotes2.setText(userModel.getNotes());
            txtNotes3.setText(userModel.getNotes());
            txtNotes4.setText(userModel.getNotes());
            txtNotes5.setText(userModel.getNotes());
            txtNotes6.setText(userModel.getNotes());
            txtNotes7.setText(userModel.getNotes());
            txtNotes8.setText(userModel.getNotes());
            txtNotes9.setText(userModel.getNotes());
            txtNotes10.setText(userModel.getNotes());
            txtPhoneNum1.setText(userModel.getPhone());
            txtPhoneNum2.setText(userModel.getPhone());
            txtPhoneNum3.setText(userModel.getPhone());
            txtPhoneNum4.setText(userModel.getPhone());
            txtPhoneNum5.setText(userModel.getPhone());
            txtPhoneNum6.setText(userModel.getPhone());
            txtPhoneNum7.setText(userModel.getPhone());
            txtPhoneNum8.setText(userModel.getPhone());
            txtPhoneNum9.setText(userModel.getPhone());
            txtPhoneNum10.setText(userModel.getPhone());
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser1);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser2);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser3);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser4);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser5);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser6);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser7);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser8);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser9);
            ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser10);

            /*
             * font family and size*/
            if (userModel.getPhone_size() != null) {
                txtPhoneNum1.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum2.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum3.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum4.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum5.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum6.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum7.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum8.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum9.setTextSize(Float.parseFloat(userModel.getPhone_size()));
                txtPhoneNum10.setTextSize(Float.parseFloat(userModel.getPhone_size()));
            }
            if (userModel.getAddress_size() != null) {
                txtAddress1.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress2.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress3.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress4.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress5.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress6.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress7.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress8.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress9.setTextSize(Float.parseFloat(userModel.getAddress_size()));
                txtAddress10.setTextSize(Float.parseFloat(userModel.getAddress_size()));
            }
            if (userModel.getEmail_size() != null) {
                txtEmailAddress1.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress2.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress3.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress4.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress5.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress6.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress7.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress8.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress9.setTextSize(Float.parseFloat(userModel.getEmail_size()));
                txtEmailAddress10.setTextSize(Float.parseFloat(userModel.getEmail_size()));
            }

            if (userModel.getOccupation_size() != null) {
                txtOcup1.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup2.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup3.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup4.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup5.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup6.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup7.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup8.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup9.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
                txtOcup10.setTextSize(Float.parseFloat(userModel.getOccupation_size()));
            }

            if (userModel.getName_size() != null) {
                txtName1.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName2.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName3.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName4.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName5.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName6.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName7.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName8.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName9.setTextSize(Float.parseFloat(userModel.getName_size()));
                txtName10.setTextSize(Float.parseFloat(userModel.getName_size()));
            }
            if (userModel.getNotes_size() != null) {
                txtNotes1.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes2.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes3.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes4.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes5.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes6.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes7.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes8.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes9.setTextSize(Float.parseFloat(userModel.getNotes_size()));
                txtNotes10.setTextSize(Float.parseFloat(userModel.getNotes_size()));
            }

        }

        /*
         *
         * */
        if (userModel.getPhone_font() != null && userModel.getPhone_font().equalsIgnoreCase("Arima Madurai")) {
            txtPhoneNum1.setTypeface(ArimaMadurai);
            txtPhoneNum2.setTypeface(ArimaMadurai);
            txtPhoneNum3.setTypeface(ArimaMadurai);
            txtPhoneNum4.setTypeface(ArimaMadurai);
            txtPhoneNum5.setTypeface(ArimaMadurai);
            txtPhoneNum6.setTypeface(ArimaMadurai);
            txtPhoneNum7.setTypeface(ArimaMadurai);
            txtPhoneNum8.setTypeface(ArimaMadurai);
            txtPhoneNum9.setTypeface(ArimaMadurai);
            txtPhoneNum10.setTypeface(ArimaMadurai);
            txtAddress1.setTypeface(ArimaMadurai);
            txtAddress2.setTypeface(ArimaMadurai);
            txtAddress3.setTypeface(ArimaMadurai);
            txtAddress4.setTypeface(ArimaMadurai);
            txtAddress5.setTypeface(ArimaMadurai);
            txtAddress6.setTypeface(ArimaMadurai);
            txtAddress7.setTypeface(ArimaMadurai);
            txtAddress8.setTypeface(ArimaMadurai);
            txtAddress9.setTypeface(ArimaMadurai);
            txtAddress10.setTypeface(ArimaMadurai);
            txtEmailAddress1.setTypeface(ArimaMadurai);
            txtEmailAddress2.setTypeface(ArimaMadurai);
            txtEmailAddress3.setTypeface(ArimaMadurai);
            txtEmailAddress4.setTypeface(ArimaMadurai);
            txtEmailAddress5.setTypeface(ArimaMadurai);
            txtEmailAddress6.setTypeface(ArimaMadurai);
            txtEmailAddress7.setTypeface(ArimaMadurai);
            txtEmailAddress8.setTypeface(ArimaMadurai);
            txtEmailAddress9.setTypeface(ArimaMadurai);
            txtEmailAddress10.setTypeface(ArimaMadurai);

        } else {
            txtPhoneNum1.setTypeface(popin);
            txtPhoneNum2.setTypeface(popin);
            txtPhoneNum3.setTypeface(popin);
            txtPhoneNum4.setTypeface(popin);
            txtPhoneNum5.setTypeface(popin);
            txtPhoneNum6.setTypeface(popin);
            txtPhoneNum7.setTypeface(popin);
            txtPhoneNum8.setTypeface(popin);
            txtPhoneNum9.setTypeface(popin);
            txtPhoneNum10.setTypeface(popin);
            txtAddress1.setTypeface(popin);
            txtAddress2.setTypeface(popin);
            txtAddress3.setTypeface(popin);
            txtAddress4.setTypeface(popin);
            txtAddress5.setTypeface(popin);
            txtAddress6.setTypeface(popin);
            txtAddress7.setTypeface(popin);
            txtAddress8.setTypeface(popin);
            txtAddress9.setTypeface(popin);
            txtAddress10.setTypeface(popin);
            txtEmailAddress1.setTypeface(popin);
            txtEmailAddress2.setTypeface(popin);
            txtEmailAddress3.setTypeface(popin);
            txtEmailAddress4.setTypeface(popin);
            txtEmailAddress5.setTypeface(popin);
            txtEmailAddress6.setTypeface(popin);
            txtEmailAddress7.setTypeface(popin);
            txtEmailAddress8.setTypeface(popin);
            txtEmailAddress9.setTypeface(popin);
            txtEmailAddress10.setTypeface(popin);

        }


        if (userModel.getOccupation_font() != null && userModel.getOccupation_font().equalsIgnoreCase("Arima Madurai")) {


            txtOcup1.setTypeface(ArimaMadurai);
            txtOcup2.setTypeface(ArimaMadurai);
            txtOcup3.setTypeface(ArimaMadurai);
            txtOcup4.setTypeface(ArimaMadurai);
            txtOcup5.setTypeface(ArimaMadurai);
            txtOcup6.setTypeface(ArimaMadurai);
            txtOcup7.setTypeface(ArimaMadurai);
            txtOcup8.setTypeface(ArimaMadurai);
            txtOcup9.setTypeface(ArimaMadurai);
            txtOcup10.setTypeface(ArimaMadurai);

            txtName1.setTypeface(ArimaMadurai);
            txtName2.setTypeface(ArimaMadurai);
            txtName3.setTypeface(ArimaMadurai);
            txtName4.setTypeface(ArimaMadurai);
            txtName5.setTypeface(ArimaMadurai);
            txtName6.setTypeface(ArimaMadurai);
            txtName7.setTypeface(ArimaMadurai);
            txtName8.setTypeface(ArimaMadurai);
            txtName9.setTypeface(ArimaMadurai);
            txtName10.setTypeface(ArimaMadurai);


        } else {
            txtOcup1.setTypeface(popin);
            txtOcup2.setTypeface(popin);
            txtOcup3.setTypeface(popin);
            txtOcup4.setTypeface(popin);
            txtOcup5.setTypeface(popin);
            txtOcup6.setTypeface(popin);
            txtOcup7.setTypeface(popin);
            txtOcup8.setTypeface(popin);
            txtOcup9.setTypeface(popin);
            txtOcup10.setTypeface(popin);

            txtName1.setTypeface(popin);
            txtName2.setTypeface(popin);
            txtName3.setTypeface(popin);
            txtName4.setTypeface(popin);
            txtName5.setTypeface(popin);
            txtName6.setTypeface(popin);
            txtName7.setTypeface(popin);
            txtName8.setTypeface(popin);
            txtName9.setTypeface(popin);
            txtName10.setTypeface(popin);
        }


        if (userModel.getNotes_font() != null && userModel.getNotes_font().equalsIgnoreCase("Arima Madurai")) {

            txtNotes1.setTypeface(ArimaMadurai);
            txtNotes2.setTypeface(ArimaMadurai);
            txtNotes3.setTypeface(ArimaMadurai);
            txtNotes4.setTypeface(ArimaMadurai);
            txtNotes5.setTypeface(ArimaMadurai);
            txtNotes6.setTypeface(ArimaMadurai);
            txtNotes7.setTypeface(ArimaMadurai);
            txtNotes8.setTypeface(ArimaMadurai);
            txtNotes9.setTypeface(ArimaMadurai);
            txtNotes10.setTypeface(ArimaMadurai);
        } else {
            txtNotes1.setTypeface(popin);
            txtNotes2.setTypeface(popin);
            txtNotes3.setTypeface(popin);
            txtNotes4.setTypeface(popin);
            txtNotes5.setTypeface(popin);
            txtNotes6.setTypeface(popin);
            txtNotes7.setTypeface(popin);
            txtNotes8.setTypeface(popin);
            txtNotes9.setTypeface(popin);
            txtNotes10.setTypeface(popin);


        }

        changeCardDesign(userModel.getCardDesign());
//        }
        if (isFromSecondCard) {

            txtAddress1.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress2.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress3.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress4.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress5.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress6.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress7.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress8.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress9.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtAddress10.setTextColor(Color.parseColor(cardsItemFromAPI.getAddressColor()));
            txtEmailAddress1.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress2.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress3.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress4.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress5.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress6.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress7.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress8.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress9.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtEmailAddress10.setTextColor(Color.parseColor(cardsItemFromAPI.getEmailColor()));
            txtName1.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName2.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName3.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName4.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName5.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName6.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName7.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName8.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName9.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            txtName10.setTextColor(Color.parseColor(cardsItemFromAPI.getNameColor()));
            if (!cardsItemFromAPI.getOccupationColor().equalsIgnoreCase("")) {
                txtOcup1.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup2.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup3.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup4.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup5.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup6.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup7.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup8.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup9.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));
                txtOcup10.setTextColor(Color.parseColor(cardsItemFromAPI.getOccupationColor()));

            }
            if (!cardsItemFromAPI.getNotesColor().equalsIgnoreCase("")) {
                txtNotes1.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes2.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes3.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));

                txtNotes4.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes5.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes6.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes7.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes8.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes9.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
                txtNotes10.setTextColor(Color.parseColor(cardsItemFromAPI.getNotesColor()));
            }
            if (!cardsItemFromAPI.getPhoneColor().equalsIgnoreCase("")) {
                txtPhoneNum1.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum2.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum3.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum4.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum5.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum6.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum7.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum8.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum9.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));
                txtPhoneNum10.setTextColor(Color.parseColor(cardsItemFromAPI.getPhoneColor()));

            }


        } else {
            if (!userModel.getAddressColor().equalsIgnoreCase("")) {
                txtAddress1.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress2.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress3.setTextColor(Color.parseColor(userModel.getAddressColor()));

                txtAddress4.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress5.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress6.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress7.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress8.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress9.setTextColor(Color.parseColor(userModel.getAddressColor()));
                txtAddress10.setTextColor(Color.parseColor(userModel.getAddressColor()));
            }
            if (!userModel.getEmailColor().equalsIgnoreCase("")) {
                txtEmailAddress1.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress2.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress3.setTextColor(Color.parseColor(userModel.getEmailColor()));

                txtEmailAddress4.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress5.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress6.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress7.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress8.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress9.setTextColor(Color.parseColor(userModel.getEmailColor()));
                txtEmailAddress10.setTextColor(Color.parseColor(userModel.getEmailColor()));
            }
            if (!userModel.getNameColor().equalsIgnoreCase("")) {
                txtName1.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName2.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName3.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName4.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName5.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName6.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName7.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName8.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName9.setTextColor(Color.parseColor(userModel.getNameColor()));
                txtName10.setTextColor(Color.parseColor(userModel.getNameColor()));
            }
            if (!userModel.getOccupationColor().equalsIgnoreCase("")) {
                txtOcup1.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup2.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup3.setTextColor(Color.parseColor(userModel.getOccupationColor()));

                txtOcup4.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup5.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup6.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup7.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup8.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup9.setTextColor(Color.parseColor(userModel.getOccupationColor()));
                txtOcup10.setTextColor(Color.parseColor(userModel.getOccupationColor()));

            }
            if (!userModel.getNotesColor().equalsIgnoreCase("")) {
                txtNotes1.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes2.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes3.setTextColor(Color.parseColor(userModel.getNotesColor()));

                txtNotes4.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes5.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes6.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes7.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes8.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes9.setTextColor(Color.parseColor(userModel.getNotesColor()));
                txtNotes10.setTextColor(Color.parseColor(userModel.getNotesColor()));
            }
            if (!userModel.getPhoneColor().equalsIgnoreCase("")) {
                txtPhoneNum1.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum2.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum3.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum4.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum5.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum6.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum7.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum8.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum9.setTextColor(Color.parseColor(userModel.getPhoneColor()));
                txtPhoneNum10.setTextColor(Color.parseColor(userModel.getPhoneColor()));

            }
        }

    }


    private void changeCardDesign(String cardDesign) {
        if (cardDesign.equalsIgnoreCase("card1.html")) {
            contCD1.setVisibility(VISIBLE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB1.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter1.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin1.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle1.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB1.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter1.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin1.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle1.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card2.html")) {
            contCD2.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB2.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter2.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin2.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle2.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB2.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter2.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin2.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle2.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card3.html")) {
            contCD3.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB3.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter3.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin3.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle3.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB3.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter3.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin3.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle3.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card4.html")) {
            contCD4.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB4.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter4.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin4.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle4.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB4.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter4.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin4.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle4.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card5.html")) {
            contCD5.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card6.html")) {
            contCD6.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card7.html")) {
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(VISIBLE);
            contCD6.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card8.html")) {
            contCD8.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB8.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter8.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin8.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle8.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB8.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter8.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin8.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle8.setVisibility(VISIBLE);
                }

            }
        } else if (cardDesign.equalsIgnoreCase("card9.html")) {
            contCD9.setVisibility(VISIBLE);

            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB9.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter9.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin9.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle9.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB9.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter9.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin9.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle9.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card10.html")) {
            contCD10.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            if (sendingModel != null) {
                if (sendingModel.getFacebook() != null && !sendingModel.getFacebook().equalsIgnoreCase("")) {
                    imgFB10.setVisibility(VISIBLE);
                }
                if (sendingModel.getTwitter() != null && !sendingModel.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter10.setVisibility(VISIBLE);
                }
                if (sendingModel.getLinkedin() != null && !sendingModel.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin10.setVisibility(VISIBLE);
                }
                if (sendingModel.getGmail() != null && !sendingModel.getGmail().equalsIgnoreCase("")) {
                    imgGoogle10.setVisibility(VISIBLE);
                }
            } else {
                if (cardsItemFromAPI.getFacebook() != null && !cardsItemFromAPI.getFacebook().equalsIgnoreCase("")) {
                    imgFB10.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getTwitter() != null && !cardsItemFromAPI.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter10.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getLinkedin() != null && !cardsItemFromAPI.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin10.setVisibility(VISIBLE);
                }
                if (cardsItemFromAPI.getGmail() != null && !cardsItemFromAPI.getGmail().equalsIgnoreCase("")) {
                    imgGoogle10.setVisibility(VISIBLE);
                }
            }
        }
    }

}
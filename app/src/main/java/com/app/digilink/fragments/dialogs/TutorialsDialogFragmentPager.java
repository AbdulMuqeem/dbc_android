package com.app.digilink.fragments.dialogs;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.app.digilink.R;
import com.app.digilink.activities.BaseActivity;
import com.app.digilink.adapters.TutorialsPagerAdapter;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.TutorialModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.CustomViewPager;
import com.google.gson.reflect.TypeToken;
import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by  on 21-Feb-17.
 */

public class TutorialsDialogFragmentPager extends DialogFragment {

    private TutorialsPagerAdapter tutorialsPagerAdapter;

    Unbinder unbinder;
    @BindView(R.id.pagerIndicator)
    CirclePageIndicator pagerIndicator;
    @BindView(R.id.viewpager)
    CustomViewPager viewpager;
    private ArrayList<TutorialModel> arrayList;
    private BaseActivity baseActivity;

    public TutorialsDialogFragmentPager() {
    }

    public static TutorialsDialogFragmentPager newInstance(BaseActivity baseActivity) {
        TutorialsDialogFragmentPager frag = new TutorialsDialogFragmentPager();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.baseActivity = baseActivity;

        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
        arrayList = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_viewpager, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        serviceCall();

    }

    private void setViewPagerAdapter(ArrayList<TutorialModel> arrayList) {
        tutorialsPagerAdapter = new TutorialsPagerAdapter(getChildFragmentManager(), arrayList, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        viewpager.setAdapter(tutorialsPagerAdapter);
        pagerIndicator.setViewPager(viewpager);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void serviceCall() {
        if (baseActivity != null)
            new WebServices(baseActivity,
                    "", false)
                    .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_TUTORIAL,
                            new WebServices.IRequestWebResponseAnyObjectCallBack() {
                                @Override
                                public void requestDataResponse(WebResponse<Object> webResponse) {

                                    Type type = new TypeToken<ArrayList<TutorialModel>>() {
                                    }.getType();
                                    arrayList = GsonFactory.getSimpleGson()
                                            .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                    , type);
                                    setViewPagerAdapter(arrayList);
                                }

                                @Override
                                public void onError(Object object) {
                                    setViewPagerAdapter(arrayList);
                                }
                            });
    }

}


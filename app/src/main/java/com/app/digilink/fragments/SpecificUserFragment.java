package com.app.digilink.fragments;


import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.GenericCarddapterCardTemplate;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.RunTimePermissions;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class SpecificUserFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.rvCards)
    RecyclerView rvCards;
    @BindView(R.id.imgUserSp)
    ImageView imgUserSp;
    @BindView(R.id.txtFullName)
    AnyTextView txtFullName;
    @BindView(R.id.txtConection)
    AnyTextView txtConection;
    @BindView(R.id.txtDesignation)
    AnyTextView txtDesignation;
    @BindView(R.id.imgFB)
    ImageView imgFB;
    @BindView(R.id.imgTwitter)
    ImageView imgTwitter;
    @BindView(R.id.imgLinkedin)
    ImageView imgLinkedin;
    @BindView(R.id.imgGoogle)
    ImageView imgGoogle;
    @BindView(R.id.btnSavetoContact)
    ImageView btnSavetoContact;
    @BindView(R.id.txtDetail)
    AnyTextView txtDetail;
    @BindView(R.id.txtPhoneSp)
    AnyTextView txtPhoneSp;
    @BindView(R.id.txtAddressSp)
    AnyTextView txtAddressSp;
    @BindView(R.id.txtEmailAddressSp)
    AnyTextView txtEmailAddressSp;
    @BindView(R.id.contAddress)
    LinearLayout contAddress;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    private GenericCardListModel genericCardListModel1;
    private Typeface popin;
    private Typeface ArimaMadurai;
    private GenericCarddapterCardTemplate adapMyCards;

    public SpecificUserFragment() {
    }

    public static SpecificUserFragment newInstance(GenericCardListModel genericCardListModel1) {
        SpecificUserFragment frag = new SpecificUserFragment();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.genericCardListModel1 = genericCardListModel1;
        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        popin = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/ArimaMadurai-Regular.ttf");
        adapMyCards = new GenericCarddapterCardTemplate(getBaseActivity(), false, genericCardListModel1.getCardList(), this);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;

    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //    navigationBar.resetViews();
        //  navigationBar.setVisibility(GONE);
//        navigationBar.clickNewCard(getBaseActivity());

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        servicCall();

        if (genericCardListModel1.getCardList().size() > 0)
            bindView();

        if (sharedPreferenceManager.isGuestUser()) {
            btnSavetoContact.setVisibility(GONE);
        } else if (genericCardListModel1.getIs_admin() == 1) {
            btnSavetoContact.setVisibility(GONE);
        } else {
            btnSavetoContact.setVisibility(VISIBLE);
        }
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
        rvCards.setLayoutManager(mLayoutManager1);
        rvCards.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvCards.setAdapter(adapMyCards);
        adapMyCards.notifyDataSetChanged();
    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_specific_profile;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.showBackButton(getBaseActivity());
        if (sharedPreferenceManager.isGuestUser()) {
            titleBar.setTitle(genericCardListModel1.getName());
        } else {
            titleBar.setTitle(genericCardListModel1.getName() + " [" + genericCardListModel1.getConnection() + "]");

        }


    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private void servicCall() {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(genericCardListModel1.getId());

        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_TERM_SPECIIC_USER, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                GenericCardListModel model = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), GenericCardListModel.class);
                                if (model.getCardList().size() > 0) {
                                    for (int i = 0; i < model.getCardList().size(); i++)
                                        if (model.getCardList().get(i).getIs_primary() == 1) {
                                            setData(model.getCardList().get(i));
                                        } else setData(model.getCardList().get(0));
                                }

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    String fbUrl = "", twtUrl = "", googleUrl = "", linkedInurl = "";

    @OnClick({/*R.id.imgFB, R.id.imgTwitter, R.id.imgLinkedin, R.id.imgGoogle,
            R.id.imgAdd, R.id.imgInfo, R.id.imgSave,*/ R.id.btnSavetoContact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.imgFB:
//                Intent viewIntent = new Intent("android.intent.action.VIEW",
//                        Uri.parse(fbUrl));
//                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
//                    startActivity(viewIntent);
//                break;
//            case R.id.imgTwitter:
//                viewIntent = new Intent("android.intent.action.VIEW",
//                        Uri.parse(twtUrl));
//                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
//                    startActivity(viewIntent);
//                break;
//            case R.id.imgLinkedin:
//                viewIntent = new Intent("android.intent.action.VIEW",
//                        Uri.parse(linkedInurl));
//                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
//                    startActivity(viewIntent);
//                break;
//            case R.id.imgGoogle:
//                viewIntent = new Intent("android.intent.action.VIEW",
//                        Uri.parse(googleUrl));
//                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
//                    startActivity(viewIntent);
//                break;
//            case R.id.imgAdd:
//                break;
//            case R.id.imgInfo:
//                break;
//            case R.id.imgSave:
//                serviceCallSaveToWallet(genericCardListModel1);
//                break;
            case R.id.btnSavetoContact:
                if (!RunTimePermissions.isAllPhonePermissionGivenPhone(getContext(), getBaseActivity(),
                        true, getResources().getText(R.string.app_name).toString())) {
                    return;
                } else saveToMobile();
                break;
        }
    }

    private void saveToMobile() {
        ArrayList<ContentProviderOperation> op_list = new ArrayList<ContentProviderOperation>();
        op_list.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                //.withValue(RawContacts.AGGREGATION_MODE, RawContacts.AGGREGATION_MODE_DEFAULT)
                .build());

        // first and last names
        op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, genericCardListModel1.getName())
//                    .withValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, "First Name")
                .build());

        op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, genericCardListModel1.getPhone())
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                .build());
        op_list.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)

                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Email.DATA, genericCardListModel1.getEmail())
                .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                .build());

        try {
            ContentProviderResult[] results = getBaseActivity().getContentResolver().applyBatch(ContactsContract.AUTHORITY, op_list);
            UIHelper.showAlertDialognew("Contact has been saved", "Success", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }, getContext());



        } catch (Exception e) {
            e.printStackTrace();
        }
//        }
    }

    ////////////*
//
//
// */
    private void setData(CardTemplate model) {
        txtAddressSp.setText(model.getAddress());
        txtPhoneSp.setText(model.getPhone());
        txtEmailAddressSp.setText(model.getEmail());
        txtFullName.setText(genericCardListModel1.getName());
        txtDesignation.setText(model.getOccupation());


        if (sharedPreferenceManager.isGuestUser()) {
//            txtConection.setTitle(genericCardListModel1.getName());
        } else {
            txtConection.setText(genericCardListModel1.getConnection());

        }


        txtDetail.setText(genericCardListModel1.getText());
        ImageLoader.getInstance().displayImage(model.getImage(), imgUserSp);

//
//        if (model.getArrSocialLogin() != null) {
//            for (int i = 0; i < model.getArrSocialLogin().size(); i++) {
//                if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
//                    imgFB.setVisibility(VISIBLE);
//                    fbUrl = model.getArrSocialLogin().get(i).getLink();
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
//                    imgTwitter.setVisibility(VISIBLE);
//                    twtUrl = model.getArrSocialLogin().get(i).getLink();
//
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
//                    imgLinkedin.setVisibility(VISIBLE);
//                    linkedInurl = model.getArrSocialLogin().get(i).getLink();
//
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
//                    imgGoogle.setVisibility(VISIBLE);
//                    googleUrl = model.getArrSocialLogin().get(i).getLink();
//
//                }
//            }
//        }
        if (model.getFacebook() != null && !model.getFacebook().equalsIgnoreCase("")) {
            imgFB.setVisibility(VISIBLE);
        }
        if (model.getTwitter() != null && !model.getTwitter().equalsIgnoreCase("")) {
            imgTwitter.setVisibility(VISIBLE);
        }
        if (model.getLinkedin() != null && !model.getLinkedin().equalsIgnoreCase("")) {
            imgLinkedin.setVisibility(VISIBLE);
        }
        if (model.getGmail() != null && !model.getGmail().equalsIgnoreCase("")) {
            imgGoogle.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onItemClick(int position, View view, Object object) {
        if (object instanceof CardTemplate) {
            CardTemplate cardsItem = adapMyCards.getItem(position);
            if (cardsItem.getFacebook() != null && !cardsItem.getFacebook().equalsIgnoreCase("")) {
                imgFB.setVisibility(VISIBLE);
            }
            if (cardsItem.getTwitter() != null && !cardsItem.getTwitter().equalsIgnoreCase("")) {
                imgTwitter.setVisibility(VISIBLE);
            }
            if (cardsItem.getTwitter() != null && !cardsItem.getLinkedin().equalsIgnoreCase("")) {
                imgLinkedin.setVisibility(VISIBLE);
            }
            if (cardsItem.getGmail() != null && !cardsItem.getGmail().equalsIgnoreCase("")) {
                imgGoogle.setVisibility(VISIBLE);
            }
            switch (view.getId()) {
                case R.id.imgDownload1:
                case R.id.imgDownload2:
                case R.id.imgDownload3:
                case R.id.imgDownload4:
                case R.id.imgDownload5:
                case R.id.imgDownload6:
                case R.id.imgDownload7:
                case R.id.imgDownload8:
                case R.id.imgDownload9:
                case R.id.imgDownload10:

//                    servicCallDownload(cardsItem, position);
                    break;
                case R.id.imgDelete1:
                case R.id.imgDelete2:
                case R.id.imgDelete3:
                case R.id.imgDelete4:
                case R.id.imgDelete5:
                case R.id.imgDelete6:
                case R.id.imgDelete7:
                case R.id.imgDelete8:
                case R.id.imgDelete9:
                case R.id.imgDelete10:
                    if (getCurrentUser().getCardList().size() <= 1) {
                        UIHelper.showToast(getBaseActivity(), "This action cannot be performed");
                    } else serviceCallDelete(cardsItem, position);
                    break;
                case R.id.imgPower1:
                case R.id.imgPower2:
                case R.id.imgPower3:
                case R.id.imgPower4:
                case R.id.imgPower5:
                case R.id.imgPower6:
                case R.id.imgPower7:
                case R.id.imgPower8:
                case R.id.imgPower9:
                case R.id.imgPower10:
                    serviceCallOnlineOfflineCard(cardsItem, position);
                    break;
                case R.id.imgEdit1:
                case R.id.imgEdit2:
                case R.id.imgEdit3:
                case R.id.imgEdit4:
                case R.id.imgEdit5:
                case R.id.imgEdit6:
                case R.id.imgEdit7:
                case R.id.imgEdit8:
                case R.id.imgEdit9:
                case R.id.imgEdit10:
                    if (cardsItem.getCardType() == 1)
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(false, null, cardsItem), false);
                    else
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(true, null, cardsItem), false);
                    break;
                case R.id.imgShare1:
                case R.id.imgShare2:
                case R.id.imgShare3:
                case R.id.imgShare4:
                case R.id.imgShare5:
                case R.id.imgShare6:
                case R.id.imgShare7:
                case R.id.imgShare8:
                case R.id.imgShare9:
                case R.id.imgShare10:
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "My Card " + getCurrentUser().getCardList().get(position).getCard();
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Card");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    break;


                case R.id.imgFB1:
                case R.id.imgFB2:
                case R.id.imgFB3:
                case R.id.imgFB4:
                case R.id.imgFB5:
                case R.id.imgFB6:
                case R.id.imgFB7:
                case R.id.imgFB8:
                case R.id.imgFB9:
                case R.id.imgFB10:
                    Intent viewIntent1 = new Intent("android.intent.action.VIEW",
                            Uri.parse(cardsItem.getFacebook()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgGoogle1:
                case R.id.imgGoogle2:
                case R.id.imgGoogle3:
                case R.id.imgGoogle4:
                case R.id.imgGoogle5:
                case R.id.imgGoogle6:
                case R.id.imgGoogle7:
                case R.id.imgGoogle8:
                case R.id.imgGoogle9:
                case R.id.imgGoogle10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(cardsItem.getGmail()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgTwitter1:
                case R.id.imgTwitter2:
                case R.id.imgTwitter3:
                case R.id.imgTwitter4:
                case R.id.imgTwitter5:
                case R.id.imgTwitter6:
                case R.id.imgTwitter7:
                case R.id.imgTwitter8:
                case R.id.imgTwitter9:
                case R.id.imgTwitter10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(cardsItem.getTwitter()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgLinkedin1:
                case R.id.imgLinkedin2:
                case R.id.imgLinkedin3:
                case R.id.imgLinkedin4:
                case R.id.imgLinkedin5:
                case R.id.imgLinkedin6:
                case R.id.imgLinkedin7:
                case R.id.imgLinkedin8:
                case R.id.imgLinkedin9:
                case R.id.imgLinkedin10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(cardsItem.getLinkedin()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");

                    break;
            }
        }

    }


}


package com.app.digilink.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.adapters.recyleradapters.BrowseAllAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.fragments.dialogs.WebviewPopupDialog;
import com.app.digilink.helperclasses.ui.helper.KeyboardHelper;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.AllCardsModel;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.VISIBLE;

public class BrowseFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.edtSearch)
    AnyEditTextView edtSearch;
    @BindView(R.id.rvGeneric)
    RecyclerView rvGeneric;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    @BindView(R.id.imgFilter)
    ImageView imgFilter;
    @BindView(R.id.contSearch)
    LinearLayout contSearch;
    @BindView(R.id.txtNoRecord)
    AnyTextView txtNoRecord;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @BindView(R.id.contSearchBar)
    LinearLayout contSearchBar;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.btnInfo1)
    ImageView btnInfo1;
    @BindView(R.id.containerPersonal)
    LinearLayout containerPersonal;
    @BindView(R.id.btnInfo)
    ImageView btnInfo;
    @BindView(R.id.btnUpgrate)
    AnyTextView btnUpgrate;
    @BindView(R.id.containerCorp)
    LinearLayout containerCorp;
    @BindView(R.id.bottom_sheet_cc)
    LinearLayout bottomSheetCc;
    private BrowseAllAdapter adapter;
    private ArrayList<GenericCardListModel> arrayList;
    private AllCardsModel allCardsModel;
    private ArrayList<GenericCardListModel> arrayList1;
    private boolean isFromFilter;

    //
    public static BrowseFragment newInstance(ArrayList<GenericCardListModel> arrayList, boolean isFromFilter) {
        Bundle args = new Bundle();
        BrowseFragment fragment = new BrowseFragment();
        fragment.setArguments(args);
        fragment.arrayList = arrayList;
        fragment.isFromFilter = isFromFilter;
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_recycler;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList1 = new ArrayList<>();
        if (arrayList != null && arrayList.size() > 0)
            adapter = new BrowseAllAdapter(sharedPreferenceManager, getBaseActivity(), arrayList, this);
        else
            adapter = new BrowseAllAdapter(sharedPreferenceManager, getBaseActivity(), arrayList1, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (arrayList != null && arrayList.size() > 0) {
            bindView();
        } else serviceCall();
        imgFilter.setVisibility(VISIBLE);
        resetNvigationBar1();
        sheetBehaviorCreateCard = BottomSheetBehavior.from(bottomSheetCc);
    }

    public void resetNvigationBar1() {
        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnWallet.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnBrowser.setColorFilter(getResources().getColor(R.color.base_blue));
        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.transparent));

    }

    private void serviceCall() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_ALL_CARDS, getCurrentUser().getId(),
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                allCardsModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), AllCardsModel.class);
                                arrayList1.clear();
                                arrayList1.addAll(allCardsModel.getGenericCardListModel());
                                adapter.notifyDataSetChanged();
                                bindView();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle(getResources().getString(R.string.browse_all));
        if (isFromFilter) {
            titleBar.setRightButtonCross(R.drawable.cross, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popBackStack();
                }
            }, getResources().getColor(R.color.colorPrimary));
        } else
            titleBar.showBackButton(getBaseActivity());


    }


    @Override
    public void setListeners() {

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
            }

            //
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence);
                if (edtSearch.getStringTrimmed().equalsIgnoreCase("")) {
//                    isSearchBarEmpty = true;
                    imgSearch.setImageResource(R.drawable.icon_search_updated);

                } else {
//                    isSearchBarEmpty = false;
                    imgSearch.setImageResource(R.drawable.icon_cross);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (edtSearch.getStringTrimmed().equalsIgnoreCase("")) {
            imgSearch.setImageResource(R.drawable.icon_search_updated);
        } else {
            imgSearch.setImageResource(R.drawable.icon_cross);
        }
    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //   navigationBar.resetNvigationBar();
        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickBtbBrowse(getBaseActivity());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvGeneric.setLayoutManager(mLayoutManager);
        rvGeneric.setAdapter(adapter);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, View view, Object object) {

        if (object instanceof GenericCardListModel) {
            GenericCardListModel genericCardListModel = adapter.getItem(position);
            String fbUrl = "", twtUrl = "", googleUrl = "", linkedInurl = "";
            if (genericCardListModel.getArrSocialLogin() != null && genericCardListModel.getArrSocialLogin().size() > 0) {
                for (int i = 0; i < genericCardListModel.getArrSocialLogin().size(); i++) {
                    if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                        fbUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                        twtUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                        linkedInurl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                        googleUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    }
                }
            }

            switch (view.getId()) {
                case R.id.imgSave:
                    serviceCallSaveToWallet(genericCardListModel);
                    break;
                case R.id.imgInfo:
                    diaLogue(genericCardListModel);

                    break;
                case R.id.imgFB:
                    Intent viewIntent = new Intent("android.intent.action.VIEW",
                            Uri.parse(fbUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgGoogle:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(googleUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgTwitter:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(twtUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgLinkedin:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(linkedInurl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.contParent:
                    if (genericCardListModel.getCardList() != null && genericCardListModel.getCardList().size() > 0)
                        getBaseActivity().addDockableFragment(SpecificUserFragment.newInstance(genericCardListModel), false);
                    break;
                case R.id.addminus:
                    if (genericCardListModel.getRequestStatus().equalsIgnoreCase("Add friend"))
                        serviceCallAddRequest(WebServiceConstants.METHOD_SEND_FREQUEST, genericCardListModel.getId());
                    else
                        serviceCallAddRequest(WebServiceConstants.METHOD_UN_FRIEND_USER, genericCardListModel.getId());
                    break;
            }
        }
    }

    private void diaLogue(GenericCardListModel genericCardListModel) {
//        AlertDialog.Builder alert = new AlertDialog.Builder(getBaseActivity());
////        alert.setTitle("");
//
//        WebView wv = new WebView(getBaseActivity());
//        if (genericCardListModel.getCardList() != null)
//            wv.loadUrl(genericCardListModel.getCardList().get(0).getCard());
//        wv.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//
//                return true;
//            }
//        });
//
//        alert.setView(wv);
//        alert.show();

        WebviewPopupDialog genericPopupDialog = WebviewPopupDialog.newInstance(getBaseActivity(),
                genericCardListModel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }
        );
        genericPopupDialog.setCancelable(true);
        genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
    }


    private void serviceCallAddRequest(String methodName, int id) {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(getCurrentUser().getId());
        searchModel.setFriend_id(id);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(methodName, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showToast(getContext(), webResponse.result.toString());
//                                adapter.notifyDataSetChanged();
                                serviceCall();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }


    @OnClick({R.id.imgSearch, R.id.imgFilter,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                edtSearch.setText("");
                KeyboardHelper.hideSoftKeyboard(getContext(), edtSearch);
                break;
            case R.id.imgFilter:
                getBaseActivity().addDockableFragment(FilterFragment.newInstance(), false);
//                UIHelper.showLongToastInCenter(getBaseActivity(), "Will be implemented in next build");
                break;
            case R.id.contHome:
                if (getBaseActivity() instanceof HomeActivity) {
//                    activity.reload();
                    getBaseActivity().popStackTill(1);

                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;
            case R.id.btnAdd:
                createCard();
//                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;

//                if (getBaseActivity() instanceof HomeActivity) {
////                    activity.reload();
//                    getBaseActivity().popStackTill(1);
//
//                } else {
//                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
//                }
//                break;
            case R.id.contWallet:
                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
//                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null), false);
                break;
            case R.id.contProfile:
                if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                else getBaseActivity().addDockableFragment(GuestUserFragment.newInstance(), false);
                break;
        }
    }
    private BottomSheetBehavior sheetBehaviorCreateCard;
    private void createCard() {


        if (sheetBehaviorCreateCard.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        containerPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }
            }
        });

        containerCorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }

            }
        });


    }
}
package com.app.digilink.fragments;

import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;
import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.PCODE;

/**
 * Created by  on 08-May-17.
 */

public class LoginFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.edtPassword)
    AnyEditTextView edtPassword;
    @BindView(R.id.btnLogin)
    AnyTextView btnLogin;
    @BindView(R.id.txtForgotPassword)
    AnyTextView txtForgotPassword;
    @BindView(R.id.chkShowPassword)
    CheckBox chkShowPassword;
    private String newToken;

    public static LoginFragment newInstance() {
        Bundle args = new Bundle();
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {
        chkShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    edtPassword.setTransformationMethod(null);
                } else {
                    edtPassword.setTransformationMethod(new PasswordTransformationMethod());
                }
                edtPassword.setSelection(edtPassword.getText().length());
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                        return;
                    }

                    // Get new FCM registration token
                    newToken = task.getResult();


                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //  navigationBar.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {

    }

    private void serviceCall() {

        sharedPreferenceManager.putValue(EMAIL_ADDRESS, edEmail.getStringTrimmed());
        sharedPreferenceManager.putValue(PCODE, edtPassword.getStringTrimmed());

        SearchModel model = new SearchModel();
        model.setEmail(edEmail.getStringTrimmed());
        model.setPassword(edtPassword.getStringTrimmed());
        model.setDevicetoken(newToken);
        Log.d("FCM -Login",newToken);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_LOGIN, model,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
                                popStackTill(1);
//                                getBaseActivity().addDockableFragment(HomeFragment.newInstance(), false);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    @OnClick({R.id.btnLogin, R.id.txtForgotPassword, R.id.imgBackBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (edEmail.testValidity() && edtPassword.testValidity())
                    serviceCall();
                else UIHelper.showToast(getContext(), "Please provide valid information");

                break;
            case R.id.txtForgotPassword:
                getBaseActivity().addDockableFragment(ForgotPassFragment.newInstance(), false);
//                UIHelper.showToast(getContext(), "Will be implemented");
                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
        }
    }
}

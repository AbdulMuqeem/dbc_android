package com.app.digilink.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.activities.MainActivity;
import com.app.digilink.adapters.recyleradapters.BrowseAllAdapter;
import com.app.digilink.adapters.recyleradapters.GenericCarddapterCardTemplate;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.fragments.dialogs.TutorialsDialogFragmentPager;
import com.app.digilink.fragments.dialogs.WebviewPopupDialog;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.AllCardsModel;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.VISIBLE;
import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.FILENAME_LANG;
import static com.app.digilink.constatnts.AppConstants.KEY_CORPORATE_CARD;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.KEY_DARK_THEME;
import static com.app.digilink.constatnts.AppConstants.PCODE;

public class HomeFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.txtID)
    AnyTextView txtID;
    @BindView(R.id.btnNotification)
    AnyTextView btnNotification;
    @BindView(R.id.rvCards)
    RecyclerView rvCards;
    @BindView(R.id.btnCreateCard)
    ImageView btnCreateCard;
    @BindView(R.id.imgSlide)
    ImageView imgSlide;
    @BindView(R.id.btnBrowseAll)
    AnyTextView btnBrowseAll;
    @BindView(R.id.rvHome)
    RecyclerView rvHome;
    @BindView(R.id.contSlide)
    LinearLayout contSlide;
    @BindView(R.id.btnInfo)
    ImageView btnInfo;
    @BindView(R.id.bottom_sheet_d)
    LinearLayout bottom_sheet_d;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.contDownloadFile)
    AnyTextView contDownloadFile;
    @BindView(R.id.contExportprintablefile)
    AnyTextView contExportprintablefile;
    @BindView(R.id.img)
    ImageView img;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.btnInfo1)
    ImageView btnInfo1;
    @BindView(R.id.containerPersonal)
    LinearLayout containerPersonal;
    @BindView(R.id.btnUpgrate)
    AnyTextView btnUpgrate;
    @BindView(R.id.containerCorp)
    LinearLayout containerCorp;
    @BindView(R.id.bottom_sheet_cc)
    LinearLayout bottom_sheet_cc;

    private ArrayList<CardTemplate> arrayListCard;
    private GenericCarddapterCardTemplate adapMyCards;
    private BrowseAllAdapter adapBrowseCard;
    private ArrayList<GenericCardListModel> arrayList;
    private AllCardsModel allCardsModel;
    private String newToken;
    private BottomSheetBehavior sheetBehaviorDownlaod;
    private BottomSheetBehavior sheetBehaviorCreateCard;
    @BindView(R.id.bottom_sheet_home)
    LinearLayout bottomSheet;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        arrayList = new ArrayList<>();
        arrayListCard = new ArrayList<>();
        adapBrowseCard = new BrowseAllAdapter(sharedPreferenceManager, getBaseActivity(), arrayList, this);
        adapMyCards = new GenericCarddapterCardTemplate(getBaseActivity(), true, arrayListCard, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        setDefaultLocaleHome();


        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bindView();
        if (sharedPreferenceManager.getString(KEY_DARK_THEME).equalsIgnoreCase("Dark")) {
            {
                AppCompatDelegate
                        .setDefaultNightMode(
                                AppCompatDelegate
                                        .MODE_NIGHT_YES);
            }
//            getBaseActivity().setTheme(R.style.DarkTheme);
        } else {
            {
                AppCompatDelegate
                        .setDefaultNightMode(
                                AppCompatDelegate
                                        .MODE_NIGHT_NO);
            }
//            getBaseActivity().setTheme(R.style.LightTheme);
        }
//        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        sheetBehaviorDownlaod = BottomSheetBehavior.from(bottom_sheet_d);
        sheetBehaviorCreateCard = BottomSheetBehavior.from(bottom_sheet_cc);


        bottomSheetBind();


        if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
            txtID.setText(getResources().getString(R.string.hello) + getCurrentUser().getName());
            rvCards.setVisibility(VISIBLE);
            btnCreateCard.setVisibility(View.GONE);
            arrayListCard.clear();
            if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                arrayListCard.addAll(getCurrentUser().getCardList());
               for (int i = 0; i < getCurrentUser().getCardList().size(); i++) {
                if (getCurrentUser().getCardList().get(i).getStatus() == 2) {
                    sharedPreferenceManager.putValue(KEY_CORPORATE_CARD, "true");
                    break;
                }else sharedPreferenceManager.putValue(KEY_CORPORATE_CARD, "false");
            }


        } else {
            txtID.setVisibility(View.GONE);
            btnNotification.setVisibility(View.INVISIBLE);
            if (!onCreated) {
                        tutorial();
            }
        }

        servicCall();
    }


    private void tutorial() {


        TutorialsDialogFragmentPager tutorialsDialogFragment = TutorialsDialogFragmentPager.newInstance(getBaseActivity());
        tutorialsDialogFragment.setCancelable(true);
        tutorialsDialogFragment.show(getBaseActivity().getSupportFragmentManager(), null);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


    }


    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvHome.setLayoutManager(mLayoutManager);
        rvHome.setAdapter(adapBrowseCard);
        adapBrowseCard.notifyDataSetChanged();


        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
        rvCards.setLayoutManager(mLayoutManager1);
        rvCards.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvCards.setAdapter(adapMyCards);
        adapMyCards.notifyDataSetChanged();


    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void servicCall() {


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                        return;
                    }

                    // Get new FCM registration token
                    String newToken = task.getResult();

//                    newToken = task.getResult().getToken();
                    Log.e("newToken", newToken);
                    SearchModel searchModel = new SearchModel();
                    searchModel.setToken(newToken);
                    if (getBaseActivity() != null)
                        new WebServices(getBaseActivity(),
                                "", false)
                                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_ADD_TOKEN, searchModel,
                                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                                            @Override
                                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                            }

                                            @Override
                                            public void onError(Object object) {
                                            }
                                        });


                });


        new WebServices(getBaseActivity(),
                "", false)
                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_ALL_CARDS, getCurrentUser().getId(),
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                allCardsModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), AllCardsModel.class);
                                arrayList.clear();
                                arrayList.addAll(allCardsModel.getGenericCardListModel());
                                adapBrowseCard.notifyDataSetChanged();

                                if (arrayList.size() <= 0) contSlide.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @Override
    public void onItemClick(int position, View view, Object object) {
        if (object instanceof GenericCardListModel) {
            GenericCardListModel genericCardListModel = adapBrowseCard.getItem(position);
            String fbUrl = "", twtUrl = "", googleUrl = "", linkedInurl = "";
            if (genericCardListModel.getArrSocialLogin() != null && genericCardListModel.getArrSocialLogin().size() > 0) {
                for (int i = 0; i < genericCardListModel.getArrSocialLogin().size(); i++) {
                    if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                        fbUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                        twtUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                        linkedInurl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    } else if (genericCardListModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                        googleUrl = genericCardListModel.getArrSocialLogin().get(i).getLink();
                    }
                }
            }
            switch (view.getId()) {
                case R.id.imgInfo:
//                    if (genericCardListModel.getCardList().size() > 0) {
//                        for (int i = 0; i < genericCardListModel.getCardList().size(); i++)
//                            if (genericCardListModel.getCardList().get(i).getIs_primary() == 1) {
//                                diaLogue(genericCardListModel.getCardList().get(i));
//                            } else diaLogue(genericCardListModel.getCardList().get(0));
//                    }
                    diaLogue(genericCardListModel);

                    break;
                case R.id.imgSave:
                    serviceCallSaveToWallet(genericCardListModel);
                    break;
                case R.id.imgFB:
                    Intent viewIntent = new Intent("android.intent.action.VIEW",
                            Uri.parse(fbUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgGoogle:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(googleUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgTwitter:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(twtUrl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgLinkedin:
                    viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(linkedInurl));
                    if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.contParent:
                    if (genericCardListModel.getCardList() != null && genericCardListModel.getCardList().size() > 0)
                        getBaseActivity().addDockableFragment(SpecificUserFragment.newInstance(genericCardListModel), false);
                    break;
                case R.id.addminus:
                    if (genericCardListModel.getRequestStatus().equalsIgnoreCase("Add friend"))
                        serviceCallAddRequest(WebServiceConstants.METHOD_SEND_FREQUEST, genericCardListModel.getId());
                    else
                        serviceCallAddRequest(WebServiceConstants.METHOD_UN_FRIEND_USER, genericCardListModel.getId());
                    break;
            }
        }
        if (object instanceof CardTemplate) {
            String fbUrl1 = "", twtUrl1 = "", googleUrl1 = "", linkedInurl1 = "";
            CardTemplate cardsItem = adapMyCards.getItem(position);
            if (cardsItem.getArrSocialLogin() != null) {
                for (int i = 0; i < cardsItem.getArrSocialLogin().size(); i++) {
                    if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                        fbUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                        twtUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                        linkedInurl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                        googleUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    }
                }
            }
            switch (view.getId()) {
                case R.id.imgDownload1:
                case R.id.imgDownload2:
                case R.id.imgDownload3:
                case R.id.imgDownload4:
                case R.id.imgDownload5:
                case R.id.imgDownload6:
                case R.id.imgDownload7:
                case R.id.imgDownload8:
                case R.id.imgDownload9:
                case R.id.imgDownload10:

                    servicCallDownload(cardsItem, position);
                    break;
                case R.id.imgDelete1:
                case R.id.imgDelete2:
                case R.id.imgDelete3:
                case R.id.imgDelete4:
                case R.id.imgDelete5:
                case R.id.imgDelete6:
                case R.id.imgDelete7:
                case R.id.imgDelete8:
                case R.id.imgDelete9:
                case R.id.imgDelete10:
                    if (getCurrentUser().getCardList().size() <= 1) {
                        UIHelper.showToast(getBaseActivity(), "This action cannot be performed");
                    } else serviceCallDelete(cardsItem, position);
                    break;
                case R.id.imgPower1:
                case R.id.imgPower2:
                case R.id.imgPower3:
                case R.id.imgPower4:
                case R.id.imgPower5:
                case R.id.imgPower6:
                case R.id.imgPower7:
                case R.id.imgPower8:
                case R.id.imgPower9:
                case R.id.imgPower10:

                    serviceCallOnlineOfflineCard(cardsItem, position);
                    break;
                case R.id.imgEdit1:
                case R.id.imgEdit2:
                case R.id.imgEdit3:
                case R.id.imgEdit4:
                case R.id.imgEdit5:
                case R.id.imgEdit6:
                case R.id.imgEdit7:
                case R.id.imgEdit8:
                case R.id.imgEdit9:
                case R.id.imgEdit10:
                    if (cardsItem.getCardType() == 1)
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(false, null, cardsItem), false);
                    else
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(true, null, cardsItem), false);
                    break;
                case R.id.imgShare1:
                case R.id.imgShare2:
                case R.id.imgShare3:
                case R.id.imgShare4:
                case R.id.imgShare5:
                case R.id.imgShare6:
                case R.id.imgShare7:
                case R.id.imgShare8:
                case R.id.imgShare9:
                case R.id.imgShare10:
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "My Card " + getCurrentUser().getCardList().get(position).getCard();
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Card");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    break;


                case R.id.imgFB1:
                case R.id.imgFB2:
                case R.id.imgFB3:
                case R.id.imgFB4:
                case R.id.imgFB5:
                case R.id.imgFB6:
                case R.id.imgFB7:
                case R.id.imgFB8:
                case R.id.imgFB9:
                case R.id.imgFB10:
                    Intent viewIntent1 = new Intent("android.intent.action.VIEW",
                            Uri.parse(fbUrl1));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgGoogle1:
                case R.id.imgGoogle2:
                case R.id.imgGoogle3:
                case R.id.imgGoogle4:
                case R.id.imgGoogle5:
                case R.id.imgGoogle6:
                case R.id.imgGoogle7:
                case R.id.imgGoogle8:
                case R.id.imgGoogle9:
                case R.id.imgGoogle10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(googleUrl1));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgTwitter1:
                case R.id.imgTwitter2:
                case R.id.imgTwitter3:
                case R.id.imgTwitter4:
                case R.id.imgTwitter5:
                case R.id.imgTwitter6:
                case R.id.imgTwitter7:
                case R.id.imgTwitter8:
                case R.id.imgTwitter9:
                case R.id.imgTwitter10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(twtUrl1));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgLinkedin1:
                case R.id.imgLinkedin2:
                case R.id.imgLinkedin3:
                case R.id.imgLinkedin4:
                case R.id.imgLinkedin5:
                case R.id.imgLinkedin6:
                case R.id.imgLinkedin7:
                case R.id.imgLinkedin8:
                case R.id.imgLinkedin9:
                case R.id.imgLinkedin10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(linkedInurl1));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");

                    break;
            }
        }

    }


    private void serviceCallAddRequest(String methodName, int id) {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(getCurrentUser().getId());
        searchModel.setFriend_id(id);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(methodName, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showToast(getContext(), webResponse.result.toString());
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void diaLogue(GenericCardListModel genericCardListModel) {
        WebviewPopupDialog genericPopupDialog = WebviewPopupDialog.newInstance(getBaseActivity(),
                genericCardListModel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }
        );
        genericPopupDialog.setCancelable(true);
        genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
    }

    @OnClick({R.id.btnCreateCard, R.id.btnBrowseAll, R.id.btnNotification, R.id.contSlide,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCreateCard:
                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.btnAdd:
                createCard();

                break;
            case R.id.btnBrowseAll:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(arrayList, false), false);
                break;
            case R.id.contSlide:

                break;
            case R.id.btnNotification:
                getBaseActivity().addDockableFragment(NotificationFragment.newInstance(), false);
                break;
            case R.id.contHome:
                break;
            case R.id.contWallet:
                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null, false), false);
                break;
            case R.id.contProfile:
                if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                else getBaseActivity().addDockableFragment(GuestUserFragment.newInstance(), false);
                break;
        }
    }


    private void bottomSheetBind() {
//        if (getCurrentUser() != null && getCurrentUser().getId() == 0) {
//            if (width == 720 && (height > 1350 && height < 1440)) {
//                sheetBehavior.setPeekHeight(1000);
//            } else {
//                sheetBehavior.setPeekHeight(1200);
//            }
//        } else if (width == 720 && (height > 1350 && height < 1440)) {
//            sheetBehavior.setPeekHeight(800);
//        } else
//            sheetBehavior.setPeekHeight(1100);


        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        final int maxHeight = displayMetrics.heightPixels;

        if (layout != null)
            layout.post(() -> {

                int height1 = layout.getHeight();
                BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                int h = (int) ((maxHeight - height1 - img.getHeight()) - (height1 * 2.5));
                behavior.setPeekHeight(h);
            });


        BottomSheetBehavior sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//                    if (getCurrentUser() != null && getCurrentUser().getId() == 0) {
//
//                        if (width == 720 && (height > 1350 && height < 1440)) {
//                            sheetBehavior.setPeekHeight(800);
//                        } else {
//                            sheetBehavior.setPeekHeight(1000);
//                        }
//                    } else if (width == 720 && (height > 1350 && height < 1440)) {
//                        sheetBehavior.setPeekHeight(600);
//                    } else
                    sheetBehavior.setPeekHeight(800);
//                    int height = layout.getHeight();
//                    BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
//                    int h = (int) ((maxHeight - height - img.getHeight()) - (height * 2.5));
//                    sheetBehavior.setPeekHeight(h);
                    imgSlide.setColorFilter(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                imgSlide.setColorFilter(getResources().getColor(R.color.c_white));
            }
        });
        sheetBehaviorDownlaod.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        sheetBehaviorCreateCard.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void servicCallDownload(CardTemplate genericCardListModel, int position) {


        if (sheetBehaviorDownlaod.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorDownlaod.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorDownlaod.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        contDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceCallDownload(genericCardListModel, position);
            }
        });

        contExportprintablefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                UIHelper.showAlertDialog(getBaseActivity().getString(R.string.printcard), "Alert",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String url = genericCardListModel.getCard();
                                Intent ii = new Intent(Intent.ACTION_VIEW);
                                ii.setData(Uri.parse(url));
                                startActivity(ii);
                            }
                        }, getBaseActivity());


            }
        });


    }

    private void createCard() {


        if (sheetBehaviorCreateCard.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        containerPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }
            }
        });

        containerCorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }

            }
        });


    }

    private void autoLogin() {


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                        return;
                    }

                    // Get new FCM registration token
                    newToken = task.getResult();


                });
        SearchModel model = new SearchModel();
        model.setEmail(sharedPreferenceManager.getString(EMAIL_ADDRESS));
        model.setPassword(sharedPreferenceManager.getString(PCODE));
        model.setDevicetoken(newToken);
        Log.d("FCM home", newToken);
        new WebServices(getBaseActivity(),
                "", false)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_LOGIN, model,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }


}
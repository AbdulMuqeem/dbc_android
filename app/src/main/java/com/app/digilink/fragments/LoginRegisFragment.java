package com.app.digilink.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.widget.AnyTextView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;

/**
 * Created by  on 08-May-17.
 */

public class LoginRegisFragment extends BaseFragment {


    private static final int RC_SIGN_IN = 001;
    private static final int FACEBOOK = 002;

    Unbinder unbinder;

    String profile_pic;
    GoogleSignInClient mGoogleSignInClient;
    private static final String EMAIL = "email";
    @BindView(R.id.sign_in_button)
    SignInButton signInButton;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.btnfacebook)
    AnyTextView btnfacebook;
    @BindView(R.id.contFacebook)
    LinearLayout contFacebook;
    @BindView(R.id.btnGoogle)
    AnyTextView btnGoogle;
    @BindView(R.id.contGoogle)
    LinearLayout contGoogle;
    @BindView(R.id.contTwitter)
    LinearLayout contTwitter;
    @BindView(R.id.contEmail)
    LinearLayout contEmail;
    @BindView(R.id.txtSignUp)
    AnyTextView txtSignUp;
    private CallbackManager callbackManager;

    public static LoginRegisFragment newInstance() {
        Bundle args = new Bundle();
        LoginRegisFragment fragment = new LoginRegisFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getBaseActivity(), gso);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_login_home;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {
        loginButton.setFragment(this);
        loginButton.setReadPermissions("email");

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().logOut();
                loginWithFacebook();
            }
        });
//        signInButton.setOnClickListener(this);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
                UIHelper.showToast(getBaseActivity(), "hiii g+ clicked");
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Check for existing Google Sign In account, if the user is already signed in
// the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getBaseActivity());
//        updateUI(account);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
       //  navigationBar.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {

    }


    private void loginWithFacebook() {
        loginButton.setReadPermissions("email");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                AccessToken accessToken = loginResult.getAccessToken();
//
                Log.e("fb", accessToken.getUserId());
                Log.e("accessToken", "accessToken " + accessToken.getToken());
                fetchUserInfo(accessToken);
                Log.e("fb", accessToken.getUserId());

                AccessToken token = AccessToken.getCurrentAccessToken();
                if (token != null) {
                    //Log.e("accessToken2", "accessToken2 "+token);

                }
//                prefHelper.setLoginStatus(true);
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    private void fetchUserInfo(AccessToken accessToken) {
        if (accessToken != null) {
            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                    onSocialInfoFetched(jsonObject);
                    Log.e("FbUserData", jsonObject.toString());


                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();


        }

    }

    public void onSocialInfoFetched(JSONObject data) {
//        isGoogleLogin = false;
        final Bundle bundle = new Bundle();


        try {

            if (data.has("id")) {
                bundle.putString("id", data.getString("id"));
            }
            if (data.has("name")) {
                bundle.putString("name", data.getString("name"));
            }

//            try {
            profile_pic = new String("https://graph.facebook.com/" + data.getString("id") + "/picture?type=large");
            Log.i("profile_pic", profile_pic + "");
            bundle.putString("profile_pic", profile_pic.toString());
            Log.e("****", "onCurrentProfileChanged: " + profile_pic);

            if (profile_pic != null) {
//         File f =     File("image/*", new File(profile_pic));
//                File picture = MediaType("image/*", (new File(profile_pic)));
//                picture = convertFile(new File(profile_pic));
            }

            if (data.has("email")) {
                bundle.putString("email", data.getString("email"));
            }
            try {
                if (data.has("picture")) {
                    String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
//                    Bitmap profilePic = BitmapFactory.decodeStream(profilePicUrl.openConnection().getInputStream());
                    // set profilePic bitmap to imageview
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            UIHelper.showAlertDialog(data.getString("id")+data.getString("name")+data.getString("email"), profile_pic,getBaseActivity());
            getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, data.getString("id"), data.getString("name"), data.getString("email"), profile_pic), false);
//            serviceCallSocialMedia(data.getString("id"), data.getString("name"), data.getString("email"), bundle, profile_pic);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        if (requestCode == FACEBOOK) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, account.getId(), account.getDisplayName(), account.getEmail(), account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : null), false);

            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    @OnClick({R.id.imgBackBtn, R.id.contGoogle, R.id.contTwitter, R.id.contEmail, R.id.contFacebook, R.id.txtSignUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.contTwitter:
                UIHelper.showLongToastInCenter(getBaseActivity(), "This Feature will be implemented in Future builds");

                break;
            case R.id.contEmail:
                getBaseActivity().addDockableFragment(LoginFragment.newInstance(), false);
//                getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                break;

            case R.id.contFacebook:
                LoginManager.getInstance().logOut();
                loginButton.performClick();
                break;
            case R.id.contGoogle:
                mGoogleSignInClient.signOut();
                signIn();
                break;
            case R.id.txtSignUp:
                getBaseActivity().addDockableFragment(RegisterationFragment.newInstance(), false);
                break;
        }
    }
}

package com.app.digilink.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;

/**
 * Created by  on 08-May-17.
 */

public class ForgotPassFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.imgLogo)
    ImageView imgLogo;
    @BindView(R.id.edEmail)
    AnyEditTextView edEmail;
    @BindView(R.id.btnSend)
    AnyTextView btnSend;
    @BindView(R.id.txtForgotPassword)
    AnyTextView txtForgotPassword;

    public static ForgotPassFragment newInstance() {
        Bundle args = new Bundle();
        ForgotPassFragment fragment = new ForgotPassFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_forgotp;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //  navigationBar.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {

    }

    private void serviceCall() {


        SearchModel model = new SearchModel();
        model.setEmail(edEmail.getStringTrimmed());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_FORGOT_PASS, model,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UIHelper.showAlertDialog(webResponse.message,"Alert",getBaseActivity());
                                UIHelper.showAlertDialog(webResponse.message, "Alert",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                popBackStack();
                                            }
                                        }, getBaseActivity());


                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    @OnClick({R.id.btnSend, R.id.imgBackBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                if (edEmail.testValidity())
                    serviceCall();
                else UIHelper.showToast(getContext(), "Please provide valid information");

                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
        }
    }
}

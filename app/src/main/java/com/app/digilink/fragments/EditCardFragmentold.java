//package com.structure.fragments;
//
//import android.annotation.SuppressLint;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.webkit.WebView;
//import android.widget.AdapterView;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//
//
//import com.structure.adapters.recyleradapters.EditColorFieldAdapter;
//import com.structure.adapters.recyleradapters.EditFontFieldAdapter;
//import com.structure.adapters.recyleradapters.EditInputFieldAdapter;
//import com.structure.adapters.recyleradapters.TemplateCardImagesAdapter;
//import com.structure.callbacks.OnItemClickListener;
//import com.structure.constatnts.WebServiceConstants;
//import com.structure.fragments.abstracts.BaseFragment;
//import com.structure.helperclasses.ui.helper.NavigationBar;
//import com.structure.helperclasses.ui.helper.SimpleGestureFilter;
//import com.structure.helperclasses.ui.helper.TitleBar;
//import com.structure.helperclasses.ui.helper.UIHelper;
//import com.structure.libraries.swap.SimpleItemTouchHelperCallback;
//import com.structure.managers.retrofit.GsonFactory;
//import com.structure.managers.retrofit.WebServices;
//import com.structure.models.CardTemplate;
//import com.structure.models.EditUserDataModel;
//import com.structure.models.FontModel;
//import com.structure.models.InputModel;
//import com.structure.models.User.UserModel;
//import com.structure.models.sendingmodels.SearchModel;
//import com.structure.models.wrappers.WebResponse;
//import com.structure.widget.AnyTextView;
//
//import java.util.ArrayList;
//
//import androidx.recyclerview.widget.ItemTouchHelper;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.OnClick;
//import butterknife.Unbinder;
//import io.reactivex.annotations.Nullable;
//
//import static android.view.View.GONE;
//import static android.view.View.VISIBLE;
//import static com.structure.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
//
//public class EditCardFragmentold extends BaseFragment
//        implements View.OnClickListener, OnItemClickListener, SimpleGestureFilter.SimpleGestureListener {
//    @BindView(R.id.imgBackBtn)
//    ImageView imgBackBtn;
//    @BindView(R.id.txtTitle)
//    AnyTextView txtTitle;
//    @BindView(R.id.imgNotificationIcon)
//    ImageView imgNotificationIcon;
//    @BindView(R.id.containerTitlebar1)
//    LinearLayout containerTitlebar1;
//    @BindView(R.id.contCard)
//    LinearLayout contCard;
//    @BindView(R.id.webUrl)
//    WebView webUrl;
//    @BindView(R.id.btnColor)
//    AnyTextView btnColor;
//    @BindView(R.id.btnInput)
//    AnyTextView btnInput;
//    @BindView(R.id.btnFont)
//    AnyTextView btnFont;
//    @BindView(R.id.rvInput)
//    RecyclerView rvInput;
//    @BindView(R.id.rvFont)
//    RecyclerView rvFont;
//    @BindView(R.id.rvColor)
//    RecyclerView rvColor;
//    @BindView(R.id.btnSave)
//    ImageView btnSave;
//
//
//    @BindView(R.id.rvTemplates)
//    RecyclerView rvTemplates;
//    private Unbinder unbinder;
//
//
//    private EditInputFieldAdapter adapInput;
//    private EditFontFieldAdapter adapFonts;
//    private ArrayList<InputModel> arrInput;
//    private ArrayList<FontModel> arrFonts;
//    private ArrayList<InputModel> arrColor;
//    private EditUserDataModel editUserDataModel;
//    private EditColorFieldAdapter adapColor;
//    private TemplateCardImagesAdapter adapTemplateCards;
//    private ArrayList<CardTemplate> arrayList;
//    private CardTemplate cardTemplate;
//    private String setCard_design;
//    private String fontFamily;
//
//    public static EditCardFragmentold newInstance(CardTemplate cardTemplate) {
//        Bundle args = new Bundle();
//        EditCardFragmentold fragment = new EditCardFragmentold();
//        fragment.cardTemplate = cardTemplate;
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    protected int getFragmentLayout() {
//        return R.layout.fragment_editcard;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        arrInput = new ArrayList<>();
//        arrFonts = new ArrayList<>();
//        arrColor = new ArrayList<>();
//        arrayList = new ArrayList<>();
//        adapInput = new EditInputFieldAdapter(getCurrentUser(), getBaseActivity(), arrInput, this);
//        adapFonts = new EditFontFieldAdapter(getBaseActivity(), arrFonts, this);
//        adapColor = new EditColorFieldAdapter(getBaseActivity(), arrColor, this);
//        adapTemplateCards = new TemplateCardImagesAdapter(getBaseActivity(), false, arrayList, this);
//
//        // Detect touched area
//        detector = new SimpleGestureFilter(getBaseActivity(), this);
//    }
//
//    @SuppressLint("SetJavaScriptEnabled")
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        bindView();
//        serviceCall();
//        setData(getCurrentUser());
//        setCard_design = cardTemplate.getCardDesign();
//        adapInput.editUserModelSending.setCard_design(setCard_design);
//
//        webUrl.setWebViewClient(new MyBrowser());
//        webUrl.getSettings().setLoadsImagesAutomatically(true);
//        webUrl.getSettings().setJavaScriptEnabled(true);
//        webUrl.getSettings().setLoadWithOverviewMode(true);
//        webUrl.getSettings().setDomStorageEnabled(true);
//        webUrl.loadUrl(getCurrentUser().getCard());
//        adapInput.notifyDataSetChanged();
//    }
//
//
//    @Override
//    public void setTitlebar(TitleBar titleBar) {
//        titleBar.resetViews();
//        titleBar.setVisibility(GONE);
//    }
//
//
//    @Override
//    public void setListeners() {
//
//    }
//
//    @Override
//    public void onClick(View v) {
//
//
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        // TODO: inflate a fragment view
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);
//        unbinder = ButterKnife.bind(this, rootView);
//        return rootView;
//    }
//
//    @Override
//    public void setNavigationBar(NavigationBar navigationBar) {
//
//        //   navigationBar.resetNvigationBar();
//        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickBtnProfile(getBaseActivity());
//
//    }
//
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//    }
//
//
//    private void bindView() {
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
//        rvInput.setLayoutManager(mLayoutManager);
//        rvInput.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        rvInput.setAdapter(adapInput);
//        adapInput.notifyDataSetChanged();
//
//
//        /*
//        *
//        *
//        *
//        * */
//        ItemTouchHelper.Callback callback =
//                new SimpleItemTouchHelperCallback(adapInput);
//        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
//        touchHelper.attachToRecyclerView(rvInput);
//
//
//        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
//        rvFont.setLayoutManager(mLayoutManager1);
//        rvFont.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        rvFont.setAdapter(adapFonts);
//        adapFonts.notifyDataSetChanged();
//
//
//        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getBaseActivity());
//        rvColor.setLayoutManager(mLayoutManager3);
//        rvColor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
//        rvColor.setAdapter(adapColor);
//        adapColor.notifyDataSetChanged();
//
//        RecyclerView.LayoutManager mLayoutManagerCard = new LinearLayoutManager(getBaseActivity());
//        rvTemplates.setLayoutManager(mLayoutManagerCard);
//        rvTemplates.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
//        rvTemplates.setAdapter(adapTemplateCards);
//        adapTemplateCards.notifyDataSetChanged();
//    }
//
//    @Override
//    public void onItemClick(int position, View view, Object object) {
//        if (object instanceof CardTemplate) {
//            CardTemplate cardListModel = adapTemplateCards.getItem(position);
//            switch (view.getId()) {
//                case R.id.imgTemplate:
//                    webUrl.loadUrl(cardListModel.getCard());
//                    setCard_design = cardListModel.getCardDesign();
//                    break;
//
//            }
//        }
//
//        if (object instanceof FontModel) {
//            FontModel fontModel = adapFonts.getItem(position);
//            switch (view.getId()) {
//                case R.id.txtTitle:
//                    fontFamily = fontModel.getFontFamily();
//                    break;
//
//            }
//        }
//    }
//
//    @OnClick({R.id.btnColor, R.id.btnInput, R.id.btnFont, R.id.btnSave, R.id.imgBackBtn, R.id.imgNotificationIcon})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.btnColor:
//                arrColor.clear();
//
//                arrColor.add(new InputModel("COLOR 1 - TITLE", "color"));
//                arrColor.add(new InputModel("COLOR 2 - INFO", "color"));
//                arrColor.add(new InputModel("COLOR 3 - BANNER", "color"));
//
//
//                rvFont.setVisibility(GONE);
//                rvInput.setVisibility(GONE);
//                rvColor.setVisibility(VISIBLE);
//                adapColor.notifyDataSetChanged();
//
//                break;
//            case R.id.btnInput:
//                arrInput.clear();
//                arrInput.addAll(editUserDataModel.getInputs());
//                rvFont.setVisibility(GONE);
//                rvColor.setVisibility(GONE);
//                rvInput.setVisibility(VISIBLE);
//                adapInput.notifyDataSetChanged();
//                break;
//            case R.id.btnFont:
//                rvFont.setVisibility(VISIBLE);
//                rvInput.setVisibility(GONE);
//                rvColor.setVisibility(GONE);
//                arrFonts.clear();
//                arrFonts.addAll(editUserDataModel.getFonts());
//                adapFonts.notifyDataSetChanged();
//                break;
//            case R.id.btnSave:
//                if (adapInput.editUserModelSending.getName() != null && adapInput.editUserModelSending.getName().equalsIgnoreCase(""))
//                    UIHelper.showToast(getBaseActivity(), "Please Edit before saving");
//                else serviceCallEdit();
//                break;
//            case R.id.imgBackBtn:
//                popBackStack();
//                break;
//            case R.id.imgNotificationIcon:
//                UIHelper.showToast(getBaseActivity(), "Will be implemented in next build");
//                break;
//        }
//    }
//
//    private void serviceCall() {
//        new WebServices(getBaseActivity(),
//                "", true)
//                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_FONT_INPUT,
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                editUserDataModel = GsonFactory.getSimpleGson()
//                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result),
//                                                EditUserDataModel.class);
//
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                            }
//                        });
//
//
//        SearchModel searchModel = new SearchModel();
//        searchModel.setUser_id(getCurrentUser().getId());
//        searchModel.setCard_id(getCurrentUser().getCardList().get(0).getId());
//        new WebServices(getBaseActivity(),
//                "", true)
//                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_GET_EDIT_CARD_INFO, searchModel,
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UserModel userModel = GsonFactory.getSimpleGson()
//                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);
//                                setData(userModel);
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                            }
//                        });
//
//
////        new WebServices(getBaseActivity(),
////                "", true)
////                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_CARDTEMPATES_USER, getCurrentUser().getId(),
////                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
////                            @Override
////                            public void requestDataResponse(WebResponse<Object> webResponse) {
////
////                                Type type = new TypeToken<ArrayList<CardTemplate>>() {
////                                }.getType();
////                                ArrayList<CardTemplate> arrayList1 = GsonFactory.getSimpleGson()
////                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
////                                                , type);
////                                arrayList.addAll(arrayList1);
////                                adapTemplateCards.notifyDataSetChanged();
////
////                                webUrl.loadUrl(arrayList1.get(0).getCard());
////
////                            }
////
////                            @Override
////                            public void onError(Object object) {
////                            }
////                        });
//    }
//
//    private void setData(UserModel userModel) {
//
//        InputModel inputModel = new InputModel();
//        inputModel.setPname(userModel.getName());
//        inputModel.setPhone(userModel.getPhone());
//        inputModel.setAddress(userModel.getAddress());
//        inputModel.setOccupation(userModel.getOccupation());
//        inputModel.setEmail(userModel.getEmail());
//
//    }
//
//    private void serviceCallEdit() {
//        adapInput.editUserModelSending.setUser_id(getCurrentUser().getId());
//
//        adapInput.editUserModelSending.setCard_id(cardTemplate.getId());
//        String currentString = getCurrentUser().getCard();
//        String[] separated = currentString.split("http://zairabbas.net/abiha/cardapplication/public/images/user_cards/");
//        String test = separated[0]; // this will contain "Fruit"
//        String card = separated[1]; // this will contain " they taste good"
//
//
//        adapInput.editUserModelSending.setCard_name(card);
//        if (setCard_design == null) {
//            setCard_design = "card1.html";
//        }
//        adapInput.editUserModelSending.setCard_design(setCard_design);
//        adapInput.editUserModelSending.setP_email(getCurrentUser().getEmail());
//
//        adapInput.editUserModelSending.setEmail(adapInput.editUserModelSending.getEmail());
//        adapInput.editUserModelSending.setEmail_color(adapColor.editUserModelSending.getEmail_color());
//
//        adapInput.editUserModelSending.setOccupation(adapInput.editUserModelSending.getOccupation());
//        adapInput.editUserModelSending.setOccupation_color(adapColor.editUserModelSending.getOccupation_color());
//        adapInput.editUserModelSending.setName_color(adapColor.editUserModelSending.getName_color());
//        adapInput.editUserModelSending.setNotes_color(adapColor.editUserModelSending.getNotes_color());
//        adapInput.editUserModelSending.setPhone_color(adapColor.editUserModelSending.getPhone_color());
//
//        adapInput.editUserModelSending.setName(adapInput.editUserModelSending.getName());
//        adapInput.editUserModelSending.setPhone(adapInput.editUserModelSending.getPhone());
//        adapInput.editUserModelSending.setAddress(adapInput.editUserModelSending.getAddress());
//        adapInput.editUserModelSending.setAddress_color(adapColor.editUserModelSending.getAddress_color());
//        adapInput.editUserModelSending.setCompany_name(adapInput.editUserModelSending.getCompany_name());
//        adapInput.editUserModelSending.setFacebook(adapInput.editUserModelSending.getFacebook());
//        adapInput.editUserModelSending.setInstagram(adapInput.editUserModelSending.getInstagram());
//        adapInput.editUserModelSending.setGmail(adapInput.editUserModelSending.getGmail());
//        adapInput.editUserModelSending.setTwitter(adapInput.editUserModelSending.getTwitter());
//        adapInput.editUserModelSending.setLinkedin(adapInput.editUserModelSending.getLinkedin());
//
//        adapInput.editUserModelSending.setFont(fontFamily);
//        adapInput.editUserModelSending.setSize(12);
//        adapInput.editUserModelSending.setSocial_icon("gmail.com"); // Need to discuss // multiple email and phone how
//
//        adapInput.editUserModelSending.setOne(0);
//        adapInput.editUserModelSending.setTwo(0);
//        adapInput.editUserModelSending.setThree(0);
//        adapInput.editUserModelSending.setFour(1);
//        adapInput.editUserModelSending.setFive(0);
//        adapInput.editUserModelSending.setSix(0);
//
//        new WebServices(getBaseActivity(),
//                "", true)
//                .webServiceRequestAPIAnyEditCard(adapInput.editUserModelSending,
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UIHelper.showToast(getContext(), "Success");
//
//                                UserModel userModel = GsonFactory.getSimpleGson()
//                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);
//
//                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
//                                sharedPreferenceManager.setGuestUser(false);
//                                popBackStack();
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                                UIHelper.showToast(getContext(), "error");
////                                popBackStack();
//                            }
//                        });
//    }
//
//    private SimpleGestureFilter detector;
//
//    @Override
//    public void onSwipe(int direction) {
//        String str = "";
//
//        switch (direction) {
//
//            case SimpleGestureFilter.SWIPE_RIGHT:
//                str = "Swipe Right";
//                break;
//            case SimpleGestureFilter.SWIPE_LEFT:
//                str = "Swipe Left";
//                break;
//            case SimpleGestureFilter.SWIPE_DOWN:
//                str = "Swipe Down";
//                break;
//            case SimpleGestureFilter.SWIPE_UP:
//                str = "Swipe Up";
//                break;
//
//        }
////        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onDoubleTap() {
////        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void onTouchEvent(int motionEvent) {
//
//    }
//
//
//}
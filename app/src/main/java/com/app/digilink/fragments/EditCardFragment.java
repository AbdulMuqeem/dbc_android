package com.app.digilink.fragments;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.EditColorFieldAdapter;
import com.app.digilink.adapters.recyleradapters.EditFontFieldAdapter;
import com.app.digilink.adapters.recyleradapters.EditInputFieldAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.AppConstants;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.libraries.swap.SimpleItemTouchHelperCallback;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.ColorModel;
import com.app.digilink.models.EditUserDataModel;
import com.app.digilink.models.FontModel;
import com.app.digilink.models.InputModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class EditCardFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener {


    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.contCard)
    LinearLayout contCard;
    @BindView(R.id.imgUser1)
    ImageView imgUser1;
    @BindView(R.id.txtName1)
    AnyTextView txtName1;
    @BindView(R.id.txtOcup1)
    AnyTextView txtOcup1;
    @BindView(R.id.imgFB1)
    ImageView imgFB1;
    @BindView(R.id.imgTwitter1)
    ImageView imgTwitter1;
    @BindView(R.id.imgLinkedin1)
    ImageView imgLinkedin1;
    @BindView(R.id.imgGoogle1)
    ImageView imgGoogle1;
    @BindView(R.id.txtPhoneNum1)
    AnyTextView txtPhoneNum1;
    @BindView(R.id.txtEmailAddress1)
    AnyTextView txtEmailAddress1;
    @BindView(R.id.txtAddress1)
    AnyTextView txtAddress1;
    @BindView(R.id.txtNotes1)
    AnyTextView txtNotes1;
    @BindView(R.id.contCD1)
    RoundKornerLinearLayout contCD1;
    @BindView(R.id.imgDownload1)
    ImageView imgDownload1;
    @BindView(R.id.imgEdit1)
    ImageView imgEdit1;
    @BindView(R.id.imgDelete1)
    ImageView imgDelete1;
    @BindView(R.id.imgShare1)
    ImageView imgShare1;
    @BindView(R.id.imgPower1)
    CheckBox imgPower1;
    @BindView(R.id.contCD1EditCard)
    LinearLayout contCD1EditCard;
    @BindView(R.id.txtName2)
    AnyTextView txtName2;
    @BindView(R.id.txtOcup2)
    AnyTextView txtOcup2;
    @BindView(R.id.imgFB2)
    ImageView imgFB2;
    @BindView(R.id.imgTwitter2)
    ImageView imgTwitter2;
    @BindView(R.id.imgLinkedin2)
    ImageView imgLinkedin2;
    @BindView(R.id.imgGoogle2)
    ImageView imgGoogle2;
    @BindView(R.id.txtPhoneNum2)
    AnyTextView txtPhoneNum2;
    @BindView(R.id.txtEmailAddress2)
    AnyTextView txtEmailAddress2;
    @BindView(R.id.txtAddress2)
    AnyTextView txtAddress2;
    @BindView(R.id.imgUser2)
    ImageView imgUser2;
    @BindView(R.id.txtNotes2)
    AnyTextView txtNotes2;
    @BindView(R.id.contCD2)
    RoundKornerLinearLayout contCD2;
    @BindView(R.id.imgDownload2)
    ImageView imgDownload2;
    @BindView(R.id.imgEdit2)
    ImageView imgEdit2;
    @BindView(R.id.imgDelete2)
    ImageView imgDelete2;
    @BindView(R.id.imgShare2)
    ImageView imgShare2;
    @BindView(R.id.imgPower2)
    CheckBox imgPower2;
    @BindView(R.id.contCD2EditCard)
    LinearLayout contCD2EditCard;
    @BindView(R.id.txtName3)
    AnyTextView txtName3;
    @BindView(R.id.txtOcup3)
    AnyTextView txtOcup3;
    @BindView(R.id.imgFB3)
    ImageView imgFB3;
    @BindView(R.id.imgTwitter3)
    ImageView imgTwitter3;
    @BindView(R.id.imgLinkedin3)
    ImageView imgLinkedin3;
    @BindView(R.id.imgGoogle3)
    ImageView imgGoogle3;
    @BindView(R.id.imgUser3)
    CircleImageView imgUser3;
    @BindView(R.id.txtPhoneNum3)
    AnyTextView txtPhoneNum3;
    @BindView(R.id.txtEmailAddress3)
    AnyTextView txtEmailAddress3;
    @BindView(R.id.txtAddress3)
    AnyTextView txtAddress3;
    @BindView(R.id.txtNotes3)
    AnyTextView txtNotes3;
    @BindView(R.id.contCD3)
    RoundKornerLinearLayout contCD3;
    @BindView(R.id.imgDownload3)
    ImageView imgDownload3;
    @BindView(R.id.imgEdit3)
    ImageView imgEdit3;
    @BindView(R.id.imgDelete3)
    ImageView imgDelete3;
    @BindView(R.id.imgShare3)
    ImageView imgShare3;
    @BindView(R.id.imgPower3)
    CheckBox imgPower3;
    @BindView(R.id.contCD3EditCard)
    LinearLayout contCD3EditCard;
    @BindView(R.id.contDesign)
    LinearLayout contDesign;
    @BindView(R.id.btnColor)
    AnyTextView btnColor;
    @BindView(R.id.btnInput)
    AnyTextView btnInput;
    @BindView(R.id.btnFont)
    AnyTextView btnFont;
    @BindView(R.id.rvColor)
    RecyclerView rvColor;
    @BindView(R.id.rvInput)
    RecyclerView rvInput;
    @BindView(R.id.rvFont)
    RecyclerView rvFont;
    @BindView(R.id.btnAdd)
    ImageView btnAdd;
    @BindView(R.id.btnSave)
    ImageView btnSave;
    @BindView(R.id.imgUser4)
    CircleImageView imgUser4;
    @BindView(R.id.txtName4)
    AnyTextView txtName4;
    @BindView(R.id.txtOcup4)
    AnyTextView txtOcup4;
    @BindView(R.id.imgFB4)
    ImageView imgFB4;
    @BindView(R.id.imgTwitter4)
    ImageView imgTwitter4;
    @BindView(R.id.imgLinkedin4)
    ImageView imgLinkedin4;
    @BindView(R.id.imgGoogle4)
    ImageView imgGoogle4;
    @BindView(R.id.txtPhoneNum4)
    AnyTextView txtPhoneNum4;
    @BindView(R.id.txtEmailAddress4)
    AnyTextView txtEmailAddress4;
    @BindView(R.id.txtAddress4)
    AnyTextView txtAddress4;
    @BindView(R.id.txtNotes4)
    AnyTextView txtNotes4;
    @BindView(R.id.contCD4)
    RoundKornerLinearLayout contCD4;
    @BindView(R.id.imgDownload4)
    ImageView imgDownload4;
    @BindView(R.id.imgEdit4)
    ImageView imgEdit4;
    @BindView(R.id.imgDelete4)
    ImageView imgDelete4;
    @BindView(R.id.imgShare4)
    ImageView imgShare4;
    @BindView(R.id.imgPower4)
    CheckBox imgPower4;
    @BindView(R.id.contCD4EditCard)
    LinearLayout contCD4EditCard;
    @BindView(R.id.imgUser5)
    ImageView imgUser5;
    @BindView(R.id.txtName5)
    AnyTextView txtName5;
    @BindView(R.id.txtOcup5)
    AnyTextView txtOcup5;
    @BindView(R.id.imgFB5)
    ImageView imgFB5;
    @BindView(R.id.imgTwitter5)
    ImageView imgTwitter5;
    @BindView(R.id.imgLinkedin5)
    ImageView imgLinkedin5;
    @BindView(R.id.imgGoogle5)
    ImageView imgGoogle5;
    @BindView(R.id.txtPhoneNum5)
    AnyTextView txtPhoneNum5;
    @BindView(R.id.txtEmailAddress5)
    AnyTextView txtEmailAddress5;
    @BindView(R.id.txtAddress5)
    AnyTextView txtAddress5;
    @BindView(R.id.txtNotes5)
    AnyTextView txtNotes5;
    @BindView(R.id.contCD5)
    RoundKornerLinearLayout contCD5;
    @BindView(R.id.imgDownload5)
    ImageView imgDownload5;
    @BindView(R.id.imgEdit5)
    ImageView imgEdit5;
    @BindView(R.id.imgDelete5)
    ImageView imgDelete5;
    @BindView(R.id.imgShare5)
    ImageView imgShare5;
    @BindView(R.id.imgPower5)
    CheckBox imgPower5;
    @BindView(R.id.contCD5EditCard)
    LinearLayout contCD5EditCard;
    @BindView(R.id.txtName6)
    AnyTextView txtName6;
    @BindView(R.id.txtOcup6)
    AnyTextView txtOcup6;
    @BindView(R.id.imgFB6)
    ImageView imgFB6;
    @BindView(R.id.imgTwitter6)
    ImageView imgTwitter6;
    @BindView(R.id.imgLinkedin6)
    ImageView imgLinkedin6;
    @BindView(R.id.imgGoogle6)
    ImageView imgGoogle6;
    @BindView(R.id.imgUser6)
    ImageView imgUser6;
    @BindView(R.id.txtPhoneNum6)
    AnyTextView txtPhoneNum6;
    @BindView(R.id.txtEmailAddress6)
    AnyTextView txtEmailAddress6;
    @BindView(R.id.txtAddress6)
    AnyTextView txtAddress6;
    @BindView(R.id.txtNotes6)
    AnyTextView txtNotes6;
    @BindView(R.id.contCD6)
    RoundKornerLinearLayout contCD6;
    @BindView(R.id.imgDownload6)
    ImageView imgDownload6;
    @BindView(R.id.imgEdit6)
    ImageView imgEdit6;
    @BindView(R.id.imgDelete6)
    ImageView imgDelete6;
    @BindView(R.id.imgShare6)
    ImageView imgShare6;
    @BindView(R.id.imgPower6)
    CheckBox imgPower6;
    @BindView(R.id.contCD6EditCard)
    LinearLayout contCD6EditCard;
    @BindView(R.id.txtName7)
    AnyTextView txtName7;
    @BindView(R.id.txtOcup7)
    AnyTextView txtOcup7;
    @BindView(R.id.imgFB7)
    ImageView imgFB7;
    @BindView(R.id.imgTwitter7)
    ImageView imgTwitter7;
    @BindView(R.id.imgLinkedin7)
    ImageView imgLinkedin7;
    @BindView(R.id.imgGoogle7)
    ImageView imgGoogle7;
    @BindView(R.id.imgUser7)
    CircleImageView imgUser7;
    @BindView(R.id.txtPhoneNum7)
    AnyTextView txtPhoneNum7;
    @BindView(R.id.txtEmailAddress7)
    AnyTextView txtEmailAddress7;
    @BindView(R.id.txtAddress7)
    AnyTextView txtAddress7;
    @BindView(R.id.txtNotes7)
    AnyTextView txtNotes7;
    @BindView(R.id.contCD7)
    RoundKornerLinearLayout contCD7;
    @BindView(R.id.imgDownload7)
    ImageView imgDownload7;
    @BindView(R.id.imgEdit7)
    ImageView imgEdit7;
    @BindView(R.id.imgDelete7)
    ImageView imgDelete7;
    @BindView(R.id.imgShare7)
    ImageView imgShare7;
    @BindView(R.id.imgPower7)
    CheckBox imgPower7;
    @BindView(R.id.contCD7EditCard)
    LinearLayout contCD7EditCard;
    @BindView(R.id.imgUser8)
    CircleImageView imgUser8;
    @BindView(R.id.txtName8)
    AnyTextView txtName8;
    @BindView(R.id.txtOcup8)
    AnyTextView txtOcup8;
    @BindView(R.id.imgFB8)
    ImageView imgFB8;
    @BindView(R.id.imgTwitter8)
    ImageView imgTwitter8;
    @BindView(R.id.imgLinkedin8)
    ImageView imgLinkedin8;
    @BindView(R.id.imgGoogle8)
    ImageView imgGoogle8;
    @BindView(R.id.txtPhoneNum8)
    AnyTextView txtPhoneNum8;
    @BindView(R.id.txtEmailAddress8)
    AnyTextView txtEmailAddress8;
    @BindView(R.id.txtAddress8)
    AnyTextView txtAddress8;
    @BindView(R.id.txtNotes8)
    AnyTextView txtNotes8;
    @BindView(R.id.contCD8)
    RoundKornerLinearLayout contCD8;
    @BindView(R.id.imgDownload8)
    ImageView imgDownload8;
    @BindView(R.id.imgEdit8)
    ImageView imgEdit8;
    @BindView(R.id.imgDelete8)
    ImageView imgDelete8;
    @BindView(R.id.imgShare8)
    ImageView imgShare8;
    @BindView(R.id.imgPower8)
    CheckBox imgPower8;
    @BindView(R.id.contCD8EditCard)
    LinearLayout contCD8EditCard;
    @BindView(R.id.txtName9)
    AnyTextView txtName9;
    @BindView(R.id.txtOcup9)
    AnyTextView txtOcup9;
    @BindView(R.id.imgFB9)
    ImageView imgFB9;
    @BindView(R.id.imgTwitter9)
    ImageView imgTwitter9;
    @BindView(R.id.imgLinkedin9)
    ImageView imgLinkedin9;
    @BindView(R.id.imgGoogle9)
    ImageView imgGoogle9;
    @BindView(R.id.imgUser9)
    ImageView imgUser9;
    @BindView(R.id.txtPhoneNum9)
    AnyTextView txtPhoneNum9;
    @BindView(R.id.txtEmailAddress9)
    AnyTextView txtEmailAddress9;
    @BindView(R.id.txtAddress9)
    AnyTextView txtAddress9;
    @BindView(R.id.txtNotes9)
    AnyTextView txtNotes9;
    @BindView(R.id.contCD9)
    RoundKornerLinearLayout contCD9;
    @BindView(R.id.imgDownload9)
    ImageView imgDownload9;
    @BindView(R.id.imgEdit9)
    ImageView imgEdit9;
    @BindView(R.id.imgDelete9)
    ImageView imgDelete9;
    @BindView(R.id.imgShare9)
    ImageView imgShare9;
    @BindView(R.id.imgPower9)
    CheckBox imgPower9;
    @BindView(R.id.contCD9EditCard)
    LinearLayout contCD9EditCard;
    @BindView(R.id.txtName10)
    AnyTextView txtName10;
    @BindView(R.id.txtOcup10)
    AnyTextView txtOcup10;
    @BindView(R.id.imgFB10)
    ImageView imgFB10;
    @BindView(R.id.imgTwitter10)
    ImageView imgTwitter10;
    @BindView(R.id.imgLinkedin10)
    ImageView imgLinkedin10;
    @BindView(R.id.imgGoogle10)
    ImageView imgGoogle10;
    @BindView(R.id.imgUser10)
    ImageView imgUser10;
    @BindView(R.id.txtPhoneNum10)
    AnyTextView txtPhoneNum10;
    @BindView(R.id.txtEmailAddress10)
    AnyTextView txtEmailAddress10;
    @BindView(R.id.txtAddress10)
    AnyTextView txtAddress10;
    @BindView(R.id.txtNotes10)
    AnyTextView txtNotes10;
    @BindView(R.id.contCD10)
    RoundKornerLinearLayout contCD10;
    @BindView(R.id.imgDownload10)
    ImageView imgDownload10;
    @BindView(R.id.imgEdit10)
    ImageView imgEdit10;
    @BindView(R.id.imgDelete10)
    ImageView imgDelete10;
    @BindView(R.id.imgShare10)
    ImageView imgShare10;
    @BindView(R.id.imgPower10)
    CheckBox imgPower10;
    @BindView(R.id.contCD10EditCard)
    LinearLayout contCD10EditCard;
    @BindView(R.id.contHeading)
    LinearLayout contHeading;
    private Unbinder unbinder;

    private Typeface popin;
    private Typeface ArimaMadurai;
    private EditInputFieldAdapter adapInput;
    private EditFontFieldAdapter adapFonts;
    private ArrayList<InputModel> arrInput;
    private ArrayList<FontModel> arrFonts;
    private ArrayList<ColorModel> arrColor;
    private EditUserDataModel editUserDataModel;
    private EditColorFieldAdapter adapColor;
    private ArrayList<CardTemplate> arrayList;
    private CardTemplate cardTemplate;
    private String setCard_design;
    private String fontFamily;
    private ArrayList<UserModel> arrayListCard;
    private String type;

    public static EditCardFragment newInstance(CardTemplate cardTemplate,/*, CardTemplate userModel*/String type) {
        Bundle args = new Bundle();
        EditCardFragment fragment = new EditCardFragment();
        fragment.cardTemplate = cardTemplate;
        fragment.type = type;
//        fragment.userModel = userModel;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_editcard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrInput = new ArrayList<>();
        arrFonts = new ArrayList<>();
        arrColor = new ArrayList<>();
        arrayList = new ArrayList<>();
        adapInput = new EditInputFieldAdapter(getCurrentUser(), getBaseActivity(), arrInput, this);
        adapFonts = new EditFontFieldAdapter(getBaseActivity(), arrFonts, this);
        adapColor = new EditColorFieldAdapter(getBaseActivity(), arrColor, this);
//        adapMyCards = new GenericCarddapter(getBaseActivity(), true, arrayListCard, this);
        popin = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(getBaseActivity().getAssets(), "fonts/ArimaMadurai-Regular.ttf");

    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindView();
        serviceCall();
        setData(cardTemplate);

        imgNotificationIcon.setVisibility(GONE);
        btnAdd.setVisibility(GONE);
    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(GONE);
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {

//        //   navigationBar.resetNvigationBar();
//        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickNewCard(getBaseActivity());
        //  navigationBar.setVisibility(GONE);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvInput.setLayoutManager(mLayoutManager);
        rvInput.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvInput.setAdapter(adapInput);
        adapInput.notifyDataSetChanged();


        /*
         *
         *
         *
         * */
        ItemTouchHelper.Callback callback =
                new SimpleItemTouchHelperCallback(adapInput);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rvInput);


        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
        rvFont.setLayoutManager(mLayoutManager1);
        rvFont.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvFont.setAdapter(adapFonts);
        adapFonts.notifyDataSetChanged();


        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getBaseActivity());
        rvColor.setLayoutManager(mLayoutManager3);
        rvColor.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvColor.setAdapter(adapColor);
        adapColor.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(int position, View view, Object object) {
        if (object instanceof InputModel) {
            InputModel inputModel1 = adapInput.getItem(position);
            switch (view.getId()) {
                case R.id.imgTick: {

                    if (inputModel1.getName().equalsIgnoreCase("Phone")) {
                        txtPhoneNum1.setText(adapInput.editUserModelSending.getPhone());
                        txtPhoneNum3.setText(adapInput.editUserModelSending.getPhone());
                    } else if (inputModel1.getName().equalsIgnoreCase("address")) {
                        txtAddress1.setText(adapInput.editUserModelSending.getAddress());
                        txtAddress3.setText(adapInput.editUserModelSending.getAddress());
                    } else if (inputModel1.getName().equalsIgnoreCase("occupation")) {
                        txtOcup1.setText(adapInput.editUserModelSending.getOccupation());
                        txtOcup3.setText(adapInput.editUserModelSending.getOccupation());
                    } else if (inputModel1.getName().equalsIgnoreCase("name")) {
                        txtName1.setText(adapInput.editUserModelSending.getName());
                        txtName3.setText(adapInput.editUserModelSending.getName());
                    } else if (inputModel1.getName().equalsIgnoreCase("email")) {
                        txtEmailAddress1.setText(adapInput.editUserModelSending.getEmail());
                        txtEmailAddress3.setText(adapInput.editUserModelSending.getEmail());
                    } else if (inputModel1.getName().equalsIgnoreCase("social icon")) {
//                        .setText(adapColor.editUserModelSending.getFacebook());
                    }
                }

                break;


            }
        }

        if (object instanceof FontModel) {
            FontModel fontModel = adapFonts.getItem(position);
            switch (view.getId()) {
                case R.id.txtTitle:
                    fontFamily = fontModel.getFontFamily();
                    break;

            }
        }

        if (object instanceof ColorModel) {
            ColorModel colorModel = adapColor.getItem(position);
            switch (view.getId()) {
                case R.id.imgTickC: {

                    if (colorModel.getType().equalsIgnoreCase("COLOR 2 - INFO")) {
                        txtPhoneNum1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getPhone_color()));
                        txtAddress1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getAddress_color()));
                        txtEmailAddress1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getEmail_color()));

                        txtPhoneNum3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getPhone_color()));
                        txtAddress3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getAddress_color()));
                        txtEmailAddress3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getEmail_color()));
                    }
                    if (colorModel.getType().equalsIgnoreCase("COLOR 1 - TITLE")) {

                        txtOcup1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getOccupation_color()));
                        txtOcup3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getOccupation_color()));
                        txtName1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getName_color()));
                        txtName3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getName_color()));

                    }
                    if (colorModel.getType().equalsIgnoreCase("COLOR 3 - BANNER")) {
                        txtNotes1.setTextColor(Color.parseColor(adapColor.editUserModelSending.getNotes_color()));
                        txtNotes3.setTextColor(Color.parseColor(adapColor.editUserModelSending.getNotes_color()));
                    }
                }
                break;

            }
        }


        if (object instanceof FontModel) {
            FontModel fontModel = adapFonts.getItem(position);
            switch (view.getId()) {
                case R.id.imgTick: {

                    if (fontModel.getValue().equalsIgnoreCase("FONT 2 - INFO")) {
                        if (adapFonts.editUserModelSending.getPhone_font()!=null &&adapFonts.editUserModelSending.getPhone_font().equalsIgnoreCase("popin")) {
                            txtPhoneNum1.setTypeface(popin);
                            txtAddress1.setTypeface(popin);
                            txtEmailAddress1.setTypeface(popin);

                            txtPhoneNum3.setTypeface(popin);
                            txtAddress3.setTypeface(popin);
                            txtEmailAddress1.setTypeface(popin);
                        } else {
                            txtPhoneNum1.setTypeface(ArimaMadurai);
                            txtAddress1.setTypeface(ArimaMadurai);
                            txtEmailAddress3.setTypeface(ArimaMadurai);

                            txtPhoneNum3.setTypeface(ArimaMadurai);
                            txtAddress3.setTypeface(ArimaMadurai);
                            txtEmailAddress3.setTypeface(ArimaMadurai);
                        }

                    }
                    if (fontModel.getValue().equalsIgnoreCase("FONT 1 - TITLE")) {
                        if (adapFonts.editUserModelSending.getName_font() != null &&
                                adapFonts.editUserModelSending.getName_font().equalsIgnoreCase("popin")) {
                            txtOcup1.setTypeface(popin);
                            txtOcup3.setTypeface(popin);
                            txtName1.setTypeface(popin);
                            txtName3.setTypeface(popin);
                        } else {
                            txtOcup1.setTypeface(ArimaMadurai);
                            txtOcup3.setTypeface(ArimaMadurai);
                            txtName3.setTypeface(ArimaMadurai);
                            txtName1.setTypeface(ArimaMadurai);
                        }

                    }

                    if (fontModel.getValue().equalsIgnoreCase("FONT 3 - BANNER")) {

                        if (adapFonts.editUserModelSending.getNotes_font() != null && adapFonts.editUserModelSending.getNotes_font().equalsIgnoreCase("popin")) {
                            txtNotes1.setTypeface(popin);
                            txtNotes3.setTypeface(popin);
                        } else {
                            txtNotes1.setTypeface(ArimaMadurai);
                            txtNotes3.setTypeface(ArimaMadurai);
                        }


                    }
                }
                break;

            }
        }
    }

    @OnClick({R.id.btnColor, R.id.btnAdd, R.id.btnInput, R.id.btnFont, R.id.btnSave, R.id.imgBackBtn, R.id.imgNotificationIcon})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnColor:

                arrColor.clear();
                arrColor.add(new ColorModel("COLOR 1 - TITLE", "color"));
                arrColor.add(new ColorModel("COLOR 2 - INFO", "color"));
                arrColor.add(new ColorModel("COLOR 3 - BANNER", "color"));
                rvFont.setVisibility(GONE);
                rvInput.setVisibility(GONE);
                btnAdd.setVisibility(INVISIBLE);
                rvColor.setVisibility(VISIBLE);

                btnColor.setBackgroundDrawable(getResources().getDrawable(R.drawable.input_colored));
                btnColor.setTextColor(getResources().getColor(R.color.c_white));
                btnInput.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnInput.setTextColor(getResources().getColor(R.color.base_blue));
                btnFont.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnFont.setTextColor(getResources().getColor(R.color.base_blue));

                break;
            case R.id.btnInput:
                if (editUserDataModel.getInputs() == null || editUserDataModel.getInputs().size() <= 0)
                    return;
                arrInput.clear();
                arrInput.addAll(editUserDataModel.getInputs());
                rvFont.setVisibility(GONE);
                rvColor.setVisibility(GONE);
                rvInput.setVisibility(VISIBLE);
                btnInput.setBackgroundDrawable(getResources().getDrawable(R.drawable.input_colored));
                btnInput.setTextColor(getResources().getColor(R.color.c_white));

                btnFont.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnFont.setTextColor(getResources().getColor(R.color.base_blue));
                btnColor.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnColor.setTextColor(getResources().getColor(R.color.base_blue));

                break;
            case R.id.btnFont:

                if (editUserDataModel.getFonts() == null || editUserDataModel.getFonts().size() <= 0)
                    return;
                arrFonts.clear();
                arrFonts.add(new FontModel("FONT 1 - TITLE", "font", editUserDataModel.getFonts()));
                arrFonts.add(new FontModel("FONT 2 - INFO", "font", editUserDataModel.getFonts()));
                arrFonts.add(new FontModel("FONT 3 - BANNER", "font", editUserDataModel.getFonts()));

                if (editUserDataModel.getFonts().size() <= 0) return;
                rvFont.setVisibility(VISIBLE);
                rvInput.setVisibility(GONE);
                rvColor.setVisibility(GONE);
                btnAdd.setVisibility(INVISIBLE);
//                adapFonts.notifyDataSetChanged();
                btnFont.setBackgroundDrawable(getResources().getDrawable(R.drawable.input_colored));
                btnFont.setTextColor(getResources().getColor(R.color.c_white));
                btnColor.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnColor.setTextColor(getResources().getColor(R.color.base_blue));
                btnInput.setBackgroundDrawable(getResources().getDrawable(R.drawable.input));
                btnInput.setTextColor(getResources().getColor(R.color.base_blue));

                if (editUserDataModel.getInputs() == null || editUserDataModel.getInputs().size() <= 0)
                    return;
                arrInput.clear();
                arrInput.addAll(editUserDataModel.getInputs());
                rvFont.setVisibility(VISIBLE);
                rvColor.setVisibility(GONE);
                rvInput.setVisibility(GONE);

                break;
            case R.id.btnSave:
                if (adapInput.editUserModelSending.getName() != null && adapInput.editUserModelSending.getName().equalsIgnoreCase(""))
                    UIHelper.showToast(getBaseActivity(), "Please Edit before saving");
                else serviceCallEdit();
                break;
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.imgNotificationIcon:
            case R.id.btnAdd:

//                UIHelper.showToast(getBaseActivity(), "Will be implemented in next build");
                break;
        }
    }

    private void serviceCall() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_FONT_INPUT,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                editUserDataModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result),
                                                EditUserDataModel.class);
                                if (type.equalsIgnoreCase("input"))
                                    btnInput.performClick();
                                else if (type.equalsIgnoreCase("font"))
                                    btnFont.performClick();
                                else btnColor.performClick();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

//
    }


    private void serviceCallEdit() {
        adapInput.editUserModelSending.setUser_id(getCurrentUser().getId());
        adapInput.editUserModelSending.setCard_id(cardTemplate.getId());

        if (cardTemplate.getCardDesign() == null) {
            setCard_design = "card1.html";
        } else setCard_design = cardTemplate.getCardDesign();
        adapInput.editUserModelSending.setCard_design(setCard_design);


        adapInput.editUserModelSending.setFirst_name(getCurrentUser().getFirst_name());
        adapInput.editUserModelSending.setLast_name(getCurrentUser().getLast_name());
        if (adapInput.editUserModelSending.getPhone() == null) {
            adapInput.editUserModelSending.setPhone(getCurrentUser().getPhone());
        } else {
            adapInput.editUserModelSending.setPhone(adapInput.editUserModelSending.getPhone());
        }
        if (adapInput.editUserModelSending.getAddress() == null) {
            adapInput.editUserModelSending.setAddress(getCurrentUser().getAddress());
        } else {
            adapInput.editUserModelSending.setAddress(adapInput.editUserModelSending.getAddress());
        }

        adapInput.editUserModelSending.setP_email(getCurrentUser().getEmail());
        if (adapInput.editUserModelSending.getEmail() == null) {
            adapInput.editUserModelSending.setEmail(getCurrentUser().getEmail());
        } else {
            adapInput.editUserModelSending.setEmail(adapInput.editUserModelSending.getEmail());
        }

        if (adapInput.editUserModelSending.getOccupation() == null) {
            adapInput.editUserModelSending.setOccupation(getCurrentUser().getOccupation());
        } else {
            adapInput.editUserModelSending.setOccupation(adapInput.editUserModelSending.getOccupation());
        }
        /*
         * set font colors */
        if (adapColor.editUserModelSending.getEmail_color() == null)
            adapInput.editUserModelSending.setEmail_color(cardTemplate.getEmailColor());
        else
            adapInput.editUserModelSending.setEmail_color(adapColor.editUserModelSending.getEmail_color());

        if (adapColor.editUserModelSending.getOccupation_color() == null)
            adapInput.editUserModelSending.setOccupation_color(cardTemplate.getOccupationColor());
        else
            adapInput.editUserModelSending.setOccupation_color(adapColor.editUserModelSending.getOccupation_color());

        if (adapColor.editUserModelSending.getName_color() == null)
            adapInput.editUserModelSending.setName_color(cardTemplate.getNameColor());
        else
            adapInput.editUserModelSending.setName_color(adapColor.editUserModelSending.getName_color());

        if (adapColor.editUserModelSending.getNotes_color() == null)
            adapInput.editUserModelSending.setNotes_color(cardTemplate.getNotesColor());
        else
            adapInput.editUserModelSending.setNotes_color(adapColor.editUserModelSending.getNotes_color());

        if (adapColor.editUserModelSending.getPhone_color() == null)
            adapInput.editUserModelSending.setPhone_color(cardTemplate.getPhoneColor());
        else
            adapInput.editUserModelSending.setPhone_color(adapColor.editUserModelSending.getPhone_color());

        if (adapColor.editUserModelSending.getAddress_color() == null)
            adapInput.editUserModelSending.setAddress_color(cardTemplate.getAddressColor());
        else
            adapInput.editUserModelSending.setAddress_color(adapColor.editUserModelSending.getAddress_color());
        /*
         * set font end colors */


        adapInput.editUserModelSending.setCompany_name(adapInput.editUserModelSending.getCompany_name());
        adapInput.editUserModelSending.setFacebook(cardTemplate.getFacebook());
        adapInput.editUserModelSending.setInstagram(cardTemplate.getInstagram());
        adapInput.editUserModelSending.setGmail(cardTemplate.getGmail());
        adapInput.editUserModelSending.setTwitter(cardTemplate.getTwitter());
        adapInput.editUserModelSending.setLinkedin(cardTemplate.getLinkedin());
        adapInput.editUserModelSending.setCard_order(adapInput.editUserModelSending.getCard_order());
        if (fontFamily != null)
            adapInput.editUserModelSending.setFont(fontFamily);
        else adapInput.editUserModelSending.setFont("Popin");
//        if (adapFonts.editUserModelSending.getSize())
        adapInput.editUserModelSending.setSize(adapFonts.editUserModelSending.getSize());


        if (adapFonts.editUserModelSending.getAddress_font() == null)
            adapInput.editUserModelSending.setAddress_font(cardTemplate.getAddress_font());
        else
            adapInput.editUserModelSending.setAddress_font(adapFonts.editUserModelSending.getAddress_font());

        if (adapFonts.editUserModelSending.getNotes_font() == null)
            adapInput.editUserModelSending.setNotes_font(cardTemplate.getNotes_font());
        else
            adapInput.editUserModelSending.setNotes_font(adapFonts.editUserModelSending.getNotes_font());

        if (adapFonts.editUserModelSending.getOccupation_font() == null)
            adapInput.editUserModelSending.setOccupation_font(cardTemplate.getOccupation_font());
        else
            adapInput.editUserModelSending.setOccupation_font(adapFonts.editUserModelSending.getOccupation_font());

        if (adapFonts.editUserModelSending.getEmail_font() == null)
            adapInput.editUserModelSending.setEmail_font(cardTemplate.getEmail_font());
        else
            adapInput.editUserModelSending.setEmail_font(adapFonts.editUserModelSending.getEmail_font());

        if (adapFonts.editUserModelSending.getName_font() == null)
            adapInput.editUserModelSending.setName_font(cardTemplate.getName_font());
        else
            adapInput.editUserModelSending.setName_font(adapFonts.editUserModelSending.getName_font());

        if (adapFonts.editUserModelSending.getPhone_font() == null)
            adapInput.editUserModelSending.setPhone_font(cardTemplate.getPhone_font());
        else
            adapInput.editUserModelSending.setPhone_font(adapFonts.editUserModelSending.getPhone_font());

//        adapInput.editUserModelSending.setNotes_font(adapFonts.editUserModelSending.getNotes_font());
//        adapInput.editUserModelSending.setName_font(adapFonts.editUserModelSending.getName_font());
//        adapInput.editUserModelSending.setPhone_font(adapFonts.editUserModelSending.getPhone_font());
//        adapInput.editUserModelSending.setAddress_font(adapFonts.editUserModelSending.getAddress_font());
//        adapInput.editUserModelSending.setOccupation_font(adapFonts.editUserModelSending.getOccupation_font());
//        adapInput.editUserModelSending.setEmail_font(adapFonts.editUserModelSending.getEmail_font());

        adapInput.editUserModelSending.setNotes(cardTemplate.getNotes());
        if (adapInput.editUserModelSending.getSocial_icon() != null)
            adapInput.editUserModelSending.setSocial_icon(adapInput.editUserModelSending.getSocial_icon()); // Need to discuss // multiple email and phone how
        else {
            adapInput.editUserModelSending.setSocial_icon("https://www.facebook.com/"); // Need to discuss // multiple email and phone how

        }

        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyEditCard(adapInput.editUserModelSending,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(AppConstants.KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
                                popStackTill(1);
//
                            }

                            @Override
                            public void onError(Object object) {
                                UIHelper.showToast(getContext(), "error");
//                                popBackStack();
                            }
                        });
    }

    private void setData(CardTemplate userModel) {

        InputModel inputModel = new InputModel();
        inputModel.setFirstName(userModel.getFirst_name());
        inputModel.setLastName(userModel.getLast_name());
        inputModel.setPhone(userModel.getPhone());
        inputModel.setAddress(userModel.getAddress());
        inputModel.setOccupation(userModel.getOccupation());
        inputModel.setEmail(userModel.getEmail());

        txtAddress1.setText(userModel.getAddress());
        txtAddress2.setText(userModel.getAddress());
        txtAddress3.setText(userModel.getAddress());
        txtAddress4.setText(userModel.getAddress());
        txtAddress5.setText(userModel.getAddress());
        txtAddress6.setText(userModel.getAddress());
        txtAddress7.setText(userModel.getAddress());
        txtAddress8.setText(userModel.getAddress());
        txtAddress9.setText(userModel.getAddress());
        txtAddress10.setText(userModel.getAddress());
        txtEmailAddress1.setText(userModel.getEmail());
        txtEmailAddress2.setText(userModel.getEmail());
        txtEmailAddress3.setText(userModel.getEmail());
        txtEmailAddress4.setText(userModel.getEmail());
        txtEmailAddress5.setText(userModel.getEmail());
        txtEmailAddress6.setText(userModel.getEmail());

        txtEmailAddress7.setText(userModel.getEmail());
        txtEmailAddress8.setText(userModel.getEmail());
        txtEmailAddress9.setText(userModel.getEmail());
        txtEmailAddress10.setText(userModel.getEmail());
        txtName1.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName2.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName3.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName4.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName5.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName6.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName7.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName8.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName9.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtName10.setText(userModel.getFirst_name() + " " + userModel.getLast_name());
        txtOcup1.setText(userModel.getOccupation());
        txtOcup2.setText(userModel.getOccupation());
        txtOcup3.setText(userModel.getOccupation());
        txtOcup4.setText(userModel.getOccupation());
        txtOcup5.setText(userModel.getOccupation());
        txtOcup6.setText(userModel.getOccupation());

        txtOcup7.setText(userModel.getOccupation());
        txtOcup8.setText(userModel.getOccupation());
        txtOcup9.setText(userModel.getOccupation());
        txtOcup10.setText(userModel.getOccupation());
        txtNotes1.setText(userModel.getNotes());
        txtNotes2.setText(userModel.getNotes());
        txtNotes3.setText(userModel.getNotes());
        txtNotes4.setText(userModel.getNotes());
        txtNotes5.setText(userModel.getNotes());
        txtNotes6.setText(userModel.getNotes());
        txtNotes7.setText(userModel.getNotes());
        txtNotes8.setText(userModel.getNotes());
        txtNotes9.setText(userModel.getNotes());
        txtNotes10.setText(userModel.getNotes());
        txtPhoneNum1.setText(userModel.getPhone());
        txtPhoneNum2.setText(userModel.getPhone());
        txtPhoneNum3.setText(userModel.getPhone());
        txtPhoneNum4.setText(userModel.getPhone());
        txtPhoneNum5.setText(userModel.getPhone());
        txtPhoneNum6.setText(userModel.getPhone());
        txtPhoneNum7.setText(userModel.getPhone());
        txtPhoneNum8.setText(userModel.getPhone());
        txtPhoneNum9.setText(userModel.getPhone());
        txtPhoneNum10.setText(userModel.getPhone());
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser1);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser2);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser3);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser4);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser5);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser6);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser7);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser8);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser9);
        ImageLoader.getInstance().displayImage(userModel.getImage(), imgUser10);

        changeCardDesign(userModel.getCardDesign());


        if (!userModel.getAddressColor().equalsIgnoreCase("")) {
            txtAddress1.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress2.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress3.setTextColor(Color.parseColor(userModel.getAddressColor()));

            txtAddress4.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress5.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress6.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress7.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress8.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress9.setTextColor(Color.parseColor(userModel.getAddressColor()));
            txtAddress10.setTextColor(Color.parseColor(userModel.getAddressColor()));
        }
        if (!userModel.getEmailColor().equalsIgnoreCase("")) {
            txtEmailAddress1.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress2.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress3.setTextColor(Color.parseColor(userModel.getEmailColor()));

            txtEmailAddress4.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress5.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress6.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress7.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress8.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress9.setTextColor(Color.parseColor(userModel.getEmailColor()));
            txtEmailAddress10.setTextColor(Color.parseColor(userModel.getEmailColor()));
        }
        if (!userModel.getNameColor().equalsIgnoreCase("")) {
            txtName1.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName2.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName3.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName4.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName5.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName6.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName7.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName8.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName9.setTextColor(Color.parseColor(userModel.getNameColor()));
            txtName10.setTextColor(Color.parseColor(userModel.getNameColor()));
        }
        if (!userModel.getOccupationColor().equalsIgnoreCase("")) {
            txtOcup1.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup2.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup3.setTextColor(Color.parseColor(userModel.getOccupationColor()));

            txtOcup4.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup5.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup6.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup7.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup8.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup9.setTextColor(Color.parseColor(userModel.getOccupationColor()));
            txtOcup10.setTextColor(Color.parseColor(userModel.getOccupationColor()));

        }
        if (!userModel.getNotesColor().equalsIgnoreCase("")) {
            txtNotes1.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes2.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes3.setTextColor(Color.parseColor(userModel.getNotesColor()));

            txtNotes4.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes5.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes6.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes7.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes8.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes9.setTextColor(Color.parseColor(userModel.getNotesColor()));
            txtNotes10.setTextColor(Color.parseColor(userModel.getNotesColor()));
        }
        if (!userModel.getPhoneColor().equalsIgnoreCase("")) {
            txtPhoneNum1.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum2.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum3.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum4.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum5.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum6.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum7.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum8.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum9.setTextColor(Color.parseColor(userModel.getPhoneColor()));
            txtPhoneNum10.setTextColor(Color.parseColor(userModel.getPhoneColor()));

        }
    }


    private void changeCardDesign(String cardDesign) {
        if (cardDesign.equalsIgnoreCase("card1.html")) {
            contCD1.setVisibility(VISIBLE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB1.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter1.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin1.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle1.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card2.html")) {
            contCD2.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB2.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter2.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin2.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle2.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card3.html")) {
            contCD3.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB3.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter3.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin3.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle3.setVisibility(VISIBLE);
                }
            }
        } else if (cardDesign.equalsIgnoreCase("card4.html")) {
            contCD4.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB4.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter4.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin4.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle4.setVisibility(VISIBLE);
                }
                imgGoogle4.setVisibility(VISIBLE);

            }
        } else if (cardDesign.equalsIgnoreCase("card5.html")) {
            contCD5.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD6.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);
                }

            }
        } else if (cardDesign.equalsIgnoreCase("card6.html")) {
            contCD6.setVisibility(VISIBLE);
            contCD1.setVisibility(GONE);
            contCD2.setVisibility(GONE);
            contCD3.setVisibility(GONE);
            contCD4.setVisibility(GONE);
            contCD5.setVisibility(GONE);
            contCD7.setVisibility(GONE);
            contCD8.setVisibility(GONE);
            contCD9.setVisibility(GONE);
            contCD10.setVisibility(GONE);
            if (this.cardTemplate != null) {
                if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                    imgFB5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                    imgTwitter5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                    imgLinkedin5.setVisibility(VISIBLE);
                }
                if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                    imgGoogle5.setVisibility(VISIBLE);

                }
            } else if (cardDesign.equalsIgnoreCase("card7.html")) {
                contCD1.setVisibility(GONE);
                contCD2.setVisibility(GONE);
                contCD3.setVisibility(GONE);
                contCD4.setVisibility(GONE);
                contCD5.setVisibility(GONE);
                contCD7.setVisibility(VISIBLE);
                contCD6.setVisibility(GONE);
                contCD8.setVisibility(GONE);
                contCD9.setVisibility(GONE);
                contCD10.setVisibility(GONE);
                if (this.cardTemplate != null) {
                    if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                        imgFB5.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                        imgTwitter5.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                        imgLinkedin5.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                        imgGoogle5.setVisibility(VISIBLE);
                    }

                }
            } else if (cardDesign.equalsIgnoreCase("card8.html")) {
                contCD8.setVisibility(VISIBLE);
                contCD1.setVisibility(GONE);
                contCD2.setVisibility(GONE);
                contCD3.setVisibility(GONE);
                contCD4.setVisibility(GONE);
                contCD5.setVisibility(GONE);
                contCD7.setVisibility(GONE);
                contCD6.setVisibility(GONE);
                contCD9.setVisibility(GONE);
                contCD10.setVisibility(GONE);
                if (this.cardTemplate != null) {
                    if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                        imgFB8.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                        imgTwitter8.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                        imgLinkedin8.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                        imgGoogle8.setVisibility(VISIBLE);
                    }
                }
            } else if (cardDesign.equalsIgnoreCase("card9.html")) {
                contCD9.setVisibility(VISIBLE);

                contCD1.setVisibility(GONE);
                contCD2.setVisibility(GONE);
                contCD3.setVisibility(GONE);
                contCD4.setVisibility(GONE);
                contCD5.setVisibility(GONE);
                contCD7.setVisibility(GONE);
                contCD6.setVisibility(GONE);
                contCD8.setVisibility(GONE);
                contCD10.setVisibility(GONE);
                if (this.cardTemplate != null) {
                    if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                        imgFB9.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                        imgTwitter9.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                        imgLinkedin9.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                        imgGoogle9.setVisibility(VISIBLE);
                    }
                }
            } else if (cardDesign.equalsIgnoreCase("card10.html")) {
                contCD10.setVisibility(VISIBLE);
                contCD1.setVisibility(GONE);
                contCD2.setVisibility(GONE);
                contCD3.setVisibility(GONE);
                contCD4.setVisibility(GONE);
                contCD5.setVisibility(GONE);
                contCD6.setVisibility(GONE);
                contCD7.setVisibility(GONE);
                contCD8.setVisibility(GONE);
                contCD9.setVisibility(GONE);
                if (this.cardTemplate != null) {
                    if (this.cardTemplate.getFacebook() != null && !this.cardTemplate.getFacebook().equalsIgnoreCase("")) {
                        imgFB10.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getTwitter() != null && !this.cardTemplate.getTwitter().equalsIgnoreCase("")) {
                        imgTwitter10.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getLinkedin() != null && !this.cardTemplate.getLinkedin().equalsIgnoreCase("")) {
                        imgLinkedin10.setVisibility(VISIBLE);
                    }
                    if (this.cardTemplate.getGmail() != null && !this.cardTemplate.getGmail().equalsIgnoreCase("")) {
                        imgGoogle10.setVisibility(VISIBLE);
                    }
                }
            }
        }

    }
}
package com.app.digilink.fragments.abstracts;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;


import com.app.digilink.BaseApplication;
import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.constatnts.AppConstants;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.helperclasses.ui.helper.KeyboardHelper;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.gdacciaro.iOSDialog.iOSDialogBuilder;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


import com.app.digilink.activities.BaseActivity;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.Locale;

import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.Disposable;

import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.PCODE;

public abstract class BaseFragment extends Fragment implements
        View.OnClickListener, AdapterView.OnItemClickListener/*, OnNewPacketReceivedListener */ {

    protected View view;
    public SharedPreferenceManager sharedPreferenceManager;
    public String TAG = "Logging Tag";
    public boolean onCreated = false;
    Disposable subscription;
    private BottomSheetBehavior sheetBehaviorDownlaod;


    /**
     * This is an abstract class, we should inherit our fragment from this class
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(getContext());
        onCreated = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);
        return view;
    }

    public abstract void setNavigationBar(NavigationBar navigationBar);

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBaseActivity().getTitleBar().resetViews();
//        getBaseActivity().getNavigationbar().resetNvigationBar();
    }

    public UserModel getCurrentUser() {
        return sharedPreferenceManager.getCurrentUser();
    }


    // Use  UIHelper.showSpinnerDialog
    @Deprecated
    public void setSpinner(ArrayAdapter adaptSpinner, final TextView textView, final Spinner spinner) {
        if (adaptSpinner == null || spinner == null)
            return;
        //selected item will look like a spinner set from XML
//        simple_list_item_single_choice
        adaptSpinner.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adaptSpinner);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = spinner.getItemAtPosition(position).toString();
                if (textView != null)
                    textView.setText(str);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    protected abstract int getFragmentLayout();

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    public void emptyBackStack() {
        androidx.fragment.app.FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popBackStack() {
        if (getFragmentManager() == null) {
            return;
        } else {
            getFragmentManager().popBackStack();
        }
    }

    public void popStackTill(int stackNumber) {
        androidx.fragment.app.FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = stackNumber; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popStackTillReverse(int stackNumber) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < stackNumber; i++) {
            fm.popBackStack();
        }
    }

    public abstract void setTitlebar(TitleBar titleBar);


    public abstract void setListeners();

    @Override
    public void onResume() {
        super.onResume();
        onCreated = true;
        setListeners();

        if (getBaseActivity() != null) {
            setTitlebar(getBaseActivity().getTitleBar());
//            setNavigationBar(getBaseActivity().getNavigationbar());

        }
//
        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
    }

    @Override
    public void onPause() {

        if (getBaseActivity() != null && getBaseActivity().getWindow().getDecorView() != null) {
            KeyboardHelper.hideSoftKeyboard(getBaseActivity(), getBaseActivity().getWindow().getDecorView());
        }
        super.onPause();
    }


    public void notifyToAll(int event, Object data) {
        BaseApplication.getPublishSubject().onNext(new Pair<>(event, data));
    }

//    protected void subscribeToNewPacket(final OnNewPacketReceivedListener newPacketReceivedListener) {
//        subscription = BaseApplication.getPublishSubject()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Consumer<Pair>() {
//                    @Override
//                    public void accept(@NonNull Pair pair) throws Exception {
//                        Log.e("abc", "on accept");
//                        newPacketReceivedListener.onNewPacket((int) pair.first, pair.second);
//                    }
//                });
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("abc", "onDestroyView");
        if (subscription != null)
            subscription.dispose();
    }


    public void showNextBuildToast() {
        UIHelper.showToast(getContext(), "This feature is in progress");
    }

//    public static void logoutClick(final BaseFragment baseFragment) {
//        Context context = baseFragment.getContext();
//
//        new iOSDialogBuilder(context)
//                .setTitle(context.getString(R.string.logout))
//                .setSubtitle(context.getString(R.string.areYouSureToLogout))
//                .setBoldPositiveLabel(false)
//                .setCancelable(false)
//                .setPositiveListener(context.getString(R.string.yes), dialog -> {
//                    dialog.dismiss();
////                    baseFragment.sharedPreferenceManager.clearDB();
//                    baseFragment.getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
//                })
//                .setNegativeListener(context.getString(R.string.no), dialog -> dialog.dismiss())
//                .build().show();
//
//
//    }


    public void putOneTimeToken(String token) {
        sharedPreferenceManager.putValue(AppConstants.KEY_ONE_TIME_TOKEN, token);
    }

    public class MyBrowser extends WebViewClient {
        @SuppressLint("JavascriptInterface")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Context context;
            view.loadUrl(url);
            //this addjavasript is sync html logout button with webview
            view.addJavascriptInterface(new Object() {
                @JavascriptInterface
                public void performClick() throws Exception {
                    Intent i;

                }
            }, "ok");
            return true;

        }

    }

    public boolean setDefaultLocale() {
        Resources resources = getBaseActivity().getResources();
        Resources resourcesApp = getBaseActivity().getApplicationContext().getResources();
        String localLanguage = resources.getConfiguration().locale.getLanguage();
        boolean isLanguageChanged = !sharedPreferenceManager.getString(AppConstants.FILENAME_LANG).equalsIgnoreCase(localLanguage);
        if (isLanguageChanged) {
            Locale locale = new Locale(sharedPreferenceManager.getString(AppConstants.FILENAME_LANG));
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            resources.updateConfiguration(config, resources.getDisplayMetrics());
            resourcesApp.updateConfiguration(config, resources.getDisplayMetrics());

            //for API 25
            Configuration configuration = resources.getConfiguration();
            configuration.setLocale(locale);
            getBaseActivity().getApplicationContext().createConfigurationContext(configuration);
            getBaseActivity().createConfigurationContext(configuration);

            getActivity().recreate();
            ((Activity) getBaseActivity()).getWindow().getDecorView().setLayoutDirection(Locale.getDefault().getLanguage().equalsIgnoreCase("ar")
                    ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
        }
        return isLanguageChanged;
    }

    public boolean setDefaultLocaleHome() {
//        Locale.getDefault().getDisplayLanguage();
        Locale.getDefault().getISO3Language();
        Resources resources = getBaseActivity().getResources();
        Resources resourcesApp = getBaseActivity().getApplicationContext().getResources();
        String localLanguage = resources.getConfiguration().locale.getLanguage();
        boolean isLanguageChanged = !sharedPreferenceManager.getString(AppConstants.FILENAME_LANG).equalsIgnoreCase(localLanguage);
        if (isLanguageChanged) {


//            Locale locale = new Locale(sharedPreferenceManager.getString(AppConstants.FILENAME_LANG));
            Locale locale = Locale.forLanguageTag(Locale.getDefault().getISO3Language());
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            resources.updateConfiguration(config, resources.getDisplayMetrics());
            resourcesApp.updateConfiguration(config, resources.getDisplayMetrics());

            //for API 25
            Configuration configuration = resources.getConfiguration();
            configuration.setLocale(locale);
            getBaseActivity().getApplicationContext().createConfigurationContext(configuration);
            getBaseActivity().createConfigurationContext(configuration);
        }
//            getActivity().recreate();
//            ((Activity) getBaseActivity()).getWindow().getDecorView().setLayoutDirection(Locale.getDefault().getLanguage().equalsIgnoreCase("ara")
//                    ? View.LAYOUT_DIRECTION_RTL : View.LAYOUT_DIRECTION_LTR);
//        }
        return isLanguageChanged;
    }

    protected void serviceCallOnlineOfflineCard(CardTemplate cardTemplate, int position) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_id(cardTemplate.getId());
        searchModel.setStatus(String.valueOf(cardTemplate.getStatus()));
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_ONLINEOFFLINE_CARD, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showAlertDialognew(webResponse.result.toString(), "Success", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        autoLogin();
                                    }
                                }, getContext());


                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    protected void serviceCallDelete(CardTemplate genericCardListModel, int position) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_id(genericCardListModel.getId());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_DELETE_CARD, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showAlertDialognew(webResponse.result.toString(), "Success", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        autoLogin();
                                    }
                                }, getContext());


                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }

    protected void serviceCallSaveToWallet(GenericCardListModel genericCardListModel) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_holder_id(genericCardListModel.getId());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_SAVE_CARDS, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UIHelper.showAlertDialognew(webResponse.result.toString(), "Success", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        autoLogin();
                                    }
                                }, getContext());





                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    protected void serviceCallDeleteToWallet(GenericCardListModel genericCardListModel) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_holder_id(genericCardListModel.getId());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_DELETE_CARD_WALLET, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                UIHelper.showAlertDialognew(webResponse.result.toString(), "Success", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        autoLogin();
                                    }
                                }, getContext());



                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    protected void serviceCallDownload(CardTemplate cardTemplate, int position) {


        UIHelper.showAlertDialog(getBaseActivity().getString(R.string.printcard), "Alert",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String str = WebServiceConstants.BASE_URL + WebServiceConstants.METHOD_GET_DOWNLOAD + cardTemplate.getId();
                        String url = str;
                        Intent ii = new Intent(Intent.ACTION_VIEW);
                        ii.setData(Uri.parse(url));
                        startActivity(ii);
                    }
                }, getBaseActivity());


    }


    protected void serviceCallPrimaryCard(/*CardTemplate genericCardListModel, int position*/CardTemplate cardTemplate) {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_id(cardTemplate.getId());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_CHANGE_PRI_CARD, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showAlertDialognew(webResponse.result.toString(), "Success", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                    }
                                }, getContext());


//                                autoLogin();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }
    String newToken;
    private void autoLogin() {


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                        return;
                    }

                    // Get new FCM registration token
                     newToken = task.getResult();





                });
        SearchModel model = new SearchModel();
        model.setEmail(sharedPreferenceManager.getString(EMAIL_ADDRESS));
        model.setPassword(sharedPreferenceManager.getString(PCODE));
        model.setDevicetoken(newToken);
//        Log.d("FCM Bf",newToken);
        new WebServices(getBaseActivity(),
                "", false)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_LOGIN, model,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);

                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
                                sharedPreferenceManager.setGuestUser(false);
                                if (getActivity() instanceof HomeActivity) {
                                    getBaseActivity().reload();
                                } else {
                                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                                }
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }


}


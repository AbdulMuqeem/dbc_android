package com.app.digilink.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.app.digilink.R;
import com.app.digilink.models.GenericCardListModel;
import com.google.android.material.textfield.TextInputLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.app.digilink.adapters.recyleradapters.GenericCarddapterCardTemplate;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.VISIBLE;

public class MyProfileFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.rvCards)
    RecyclerView rvCards;
    @BindView(R.id.imgUser)
    ImageView imgUser;
    @BindView(R.id.txtFullName)
    AnyTextView txtFullName;
    @BindView(R.id.txtDesignation)
    AnyTextView txtDesignation;
    @BindView(R.id.imgFB)
    ImageView imgFB;
    @BindView(R.id.imgTwitter)
    ImageView imgTwitter;
    @BindView(R.id.imgLinkedin)
    ImageView imgLinkedin;
    @BindView(R.id.imgGoogle)
    ImageView imgGoogle;
    @BindView(R.id.txtDetail)
    AnyTextView txtDetail;
    @BindView(R.id.txtAddress)
    AnyEditTextView txtAddress;
    @BindView(R.id.containerTitleAdd)
    TextInputLayout containerTitleAdd;
    @BindView(R.id.contAddress)
    LinearLayout contAddress;
    @BindView(R.id.txtEmail)
    AnyEditTextView txtEmail;
    @BindView(R.id.txtPhone)
    AnyEditTextView txtPhone;
    @BindView(R.id.txtOccupation)
    AnyEditTextView txtOccupation;
    @BindView(R.id.txtNote)
    AnyEditTextView txtNote;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    String fbUrl = "", twtUrl = "", googleUrl = "", linkedInurl = "";
    private ArrayList<CardTemplate> arrayListCard;
    private GenericCarddapterCardTemplate adapMyCards;

    //
    public static MyProfileFragment newInstance() {
        Bundle args = new Bundle();
        MyProfileFragment fragment = new MyProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_my_profile;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayListCard = new ArrayList<>();
        adapMyCards = new GenericCarddapterCardTemplate(getBaseActivity(), true, arrayListCard, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCall();

    }

    private void serviceCall() {

        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setCard_id(getCurrentUser().getCardList().get(0).getId());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_GET_EDIT_CARD_INFO, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UserModel userModel = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);
                                setData(userModel);

                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle(getResources().getString(R.string.profile));
        titleBar.showBackButton(getBaseActivity());

        titleBar.setRightButton(R.drawable.icon_setting, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
//                UIHelper.showLongToastInCenter(getBaseActivity(), "Will be implemented");
            }
        }, getResources().getColor(R.color.colorPrimary));
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
//        //  navigationBar.setVisibility(VISIBLE);
//        //   navigationBar.resetNvigationBar();
//
//        navigationBar.clickBtnProfile(getBaseActivity());

    }

    private void setData(UserModel userModel) {


        txtAddress.setEnabled(false);
        txtPhone.setEnabled(false);
        txtEmail.setEnabled(false);
        txtFullName.setEnabled(false);
        txtDesignation.setEnabled(false);
        txtDetail.setEnabled(false);
        txtOccupation.setEnabled(false);


        if (userModel.getCardList() != null && userModel.getCardList().size() > 0)
            arrayListCard.addAll(userModel.getCardList());


        txtFullName.setText(userModel.getName());
        txtDesignation.setText(userModel.getCompanyName());
        txtDetail.setText(userModel.getCreatedAt());
//        txtOccupation.setText(userModel.getOccupation());
        for (int i = 0; i < userModel.getCardList().size(); i++) {
            if (userModel.getCardList().get(i).getIs_primary() == 1) {
                txtNote.setText(userModel.getCardList().get(i).getNotes());
                txtOccupation.setText(userModel.getCardList().get(i).getOccupation());
                ImageLoader.getInstance().displayImage(userModel.getCardList().get(i).getImage(), imgUser);
                txtAddress.setText(userModel.getCardList().get(i).getAddress());
                txtPhone.setText(userModel.getCardList().get(i).getPhone());
                txtEmail.setText(userModel.getCardList().get(i).getEmail());
                break;
            }
        }


        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
        rvCards.setLayoutManager(mLayoutManager1);
        rvCards.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvCards.setAdapter(adapMyCards);
        adapMyCards.notifyDataSetChanged();


        if (userModel.getArrSocialLogin() != null) {
            for (int i = 0; i < userModel.getArrSocialLogin().size(); i++) {
                if (userModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                    fbUrl = userModel.getArrSocialLogin().get(i).getLink();
                } else if (userModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                    twtUrl = userModel.getArrSocialLogin().get(i).getLink();
                } else if (userModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                    linkedInurl = userModel.getArrSocialLogin().get(i).getLink();
                } else if (userModel.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                    googleUrl = userModel.getArrSocialLogin().get(i).getLink();
                }
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onItemClick(int position, View view, Object object) {
        if (object instanceof CardTemplate) {
            String fbUrl1 = "", twtUrl1 = "", googleUrl1 = "", linkedInurl1 = "";
            CardTemplate cardsItem = adapMyCards.getItem(position);
            if (cardsItem.getArrSocialLogin() != null) {
                for (int i = 0; i < cardsItem.getArrSocialLogin().size(); i++) {
                    if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
                        fbUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
                        twtUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
                        linkedInurl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    } else if (cardsItem.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
                        googleUrl1 = cardsItem.getArrSocialLogin().get(i).getLink();
                    }
                }

                switch (view.getId()) {
                    case R.id.imgDownload1:
                    case R.id.imgDownload2:
                    case R.id.imgDownload3:
                    case R.id.imgDownload4:
                    case R.id.imgDownload5:
                    case R.id.imgDownload6:
                    case R.id.imgDownload7:
                    case R.id.imgDownload8:
                    case R.id.imgDownload9:
                    case R.id.imgDownload10:
                        serviceCallDownload(cardsItem, position);
                        break;
                    case R.id.imgDelete1:
                    case R.id.imgDelete2:
                    case R.id.imgDelete3:
                    case R.id.imgDelete4:
                    case R.id.imgDelete5:
                    case R.id.imgDelete6:
                    case R.id.imgDelete7:
                    case R.id.imgDelete8:
                    case R.id.imgDelete9:
                    case R.id.imgDelete10:
                        if (getCurrentUser().getCardList().size() <= 1) {
                            UIHelper.showToast(getBaseActivity(), "This action cannot be performed");
                        } else serviceCallDelete(cardsItem, position);
                        break;
                    case R.id.imgPower1:
                    case R.id.imgPower2:
                    case R.id.imgPower3:
                    case R.id.imgPower4:
                    case R.id.imgPower5:
                    case R.id.imgPower6:
                    case R.id.imgPower7:
                    case R.id.imgPower8:
                    case R.id.imgPower9:
                    case R.id.imgPower10:
                        serviceCallOnlineOfflineCard(cardsItem, position);
                        break;
                    case R.id.imgEdit1:
                    case R.id.imgEdit2:
                    case R.id.imgEdit3:
                    case R.id.imgEdit4:
                    case R.id.imgEdit5:
                    case R.id.imgEdit6:
                    case R.id.imgEdit7:
                    case R.id.imgEdit8:
                    case R.id.imgEdit9:
                    case R.id.imgEdit10:
                        if (getCurrentUser().getCardList().get(position).getCardType() == 1)
                            getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(false, null, getCurrentUser().getCardList().get(position)), false);
                        else
                            getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(true, null, getCurrentUser().getCardList().get(position)), false);
                        break;
                    case R.id.imgShare1:
                    case R.id.imgShare2:
                    case R.id.imgShare3:
                    case R.id.imgShare4:
                    case R.id.imgShare5:
                    case R.id.imgShare6:
                    case R.id.imgShare7:
                    case R.id.imgShare8:
                    case R.id.imgShare9:
                    case R.id.imgShare10:
                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        String shareBody = "My Card " + getCurrentUser().getCardList().get(position).getCard();
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Card");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                        break;


                    case R.id.imgFB1:
                    case R.id.imgFB2:
                    case R.id.imgFB3:
                    case R.id.imgFB4:
                    case R.id.imgFB5:
                    case R.id.imgFB6:
                    case R.id.imgFB7:
                    case R.id.imgFB8:
                    case R.id.imgFB9:
                    case R.id.imgFB10:
                        Intent viewIntent1 = new Intent("android.intent.action.VIEW",
                                Uri.parse(fbUrl1));
                        if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                            startActivity(viewIntent1);
                        else
                            UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                        break;
                    case R.id.imgGoogle1:
                    case R.id.imgGoogle2:
                    case R.id.imgGoogle3:
                    case R.id.imgGoogle4:
                    case R.id.imgGoogle5:
                    case R.id.imgGoogle6:
                    case R.id.imgGoogle7:
                    case R.id.imgGoogle8:
                    case R.id.imgGoogle9:
                    case R.id.imgGoogle10:
                        viewIntent1 =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse(googleUrl1));
                        if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                            startActivity(viewIntent1);
                        else
                            UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                        break;
                    case R.id.imgTwitter1:
                    case R.id.imgTwitter2:
                    case R.id.imgTwitter3:
                    case R.id.imgTwitter4:
                    case R.id.imgTwitter5:
                    case R.id.imgTwitter6:
                    case R.id.imgTwitter7:
                    case R.id.imgTwitter8:
                    case R.id.imgTwitter9:
                    case R.id.imgTwitter10:
                        viewIntent1 =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse(twtUrl1));
                        if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                            startActivity(viewIntent1);
                        else
                            UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                        break;
                    case R.id.imgLinkedin1:
                    case R.id.imgLinkedin2:
                    case R.id.imgLinkedin3:
                    case R.id.imgLinkedin4:
                    case R.id.imgLinkedin5:
                    case R.id.imgLinkedin6:
                    case R.id.imgLinkedin7:
                    case R.id.imgLinkedin8:
                    case R.id.imgLinkedin9:
                    case R.id.imgLinkedin10:
                        viewIntent1 =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse(linkedInurl1));
                        if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                            startActivity(viewIntent1);
                        else
                            UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");

                        break;
                }
            }
        }
    }


    @OnClick({R.id.imgFB, R.id.imgTwitter, R.id.imgLinkedin, R.id.imgGoogle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFB:
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse(fbUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgGoogle:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(googleUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgTwitter:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(twtUrl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
            case R.id.imgLinkedin:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(linkedInurl));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                break;
        }
    }
}
package com.app.digilink.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;


import com.app.digilink.R;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.models.TutorialModel;
import com.app.digilink.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.View.GONE;


public class TutorialsFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.txtHeader)
    AnyTextView txtHeader;
    @BindView(R.id.txtSkipIntro)
    AnyTextView txtSkipIntro;

    private String Title;
    private View.OnClickListener onCanceButtonClick;
    private SharedPreferenceManager sharedPreferenceManager;
    private View.OnClickListener onSaveClick;
    private boolean isFromAllergy;
    private View.OnClickListener onClickListener;
    private ArrayList<TutorialModel> arrayList;
    private String text;

    public TutorialsFragment() {
    }

    public static TutorialsFragment newInstance(View.OnClickListener onClickListener,/*, ArrayList<TutorialModel> arrayList*/String text) {
        TutorialsFragment frag = new TutorialsFragment();

        Bundle args = new Bundle();
        frag.setArguments(args);
        frag.onClickListener = onClickListener;
        frag.text = text;
//        frag.arrayList = arrayList;

        return frag;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;

    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
//       //  navigationBar.setVisibility(GONE);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        serviceCall();
//        for (int i = 0; i < arrayList.size(); i++)
            txtHeader.setText(text);

    }


    @Override
    protected int getFragmentLayout() {
        return R.layout.popup_splash;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {

    }

    @Override
    public void setListeners() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @OnClick(R.id.txtSkipIntro)
    public void onViewClicked() {
        onClickListener.onClick(txtSkipIntro);
    }


}


package com.app.digilink.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;


public class ContactUsFragment extends BaseFragment {

    Unbinder unbinder;

    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.edtFirstName)
    AnyEditTextView edtFirstName;
    @BindView(R.id.textField)
    TextInputLayout textField;
    @BindView(R.id.edtPhone)
    AnyEditTextView edtPhone;
    @BindView(R.id.txtFeedback)
    AnyTextView txtFeedback;
    @BindView(R.id.imgFeedbacl)
    ImageView imgFeedbacl;
    @BindView(R.id.spDownArrow)
    Spinner spDownArrow;
    @BindView(R.id.contFeedabck)
    RelativeLayout contFeedabck;
    @BindView(R.id.edText)
    AnyEditTextView edText;
    @BindView(R.id.btnSend)
    Button btnSend;
    ArrayList<String> arrayFeedback;

    public static ContactUsFragment newInstance() {
        Bundle args = new Bundle();
        ContactUsFragment fragment = new ContactUsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_contact_us;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
    }


    @Override
    public void setListeners() {

        spDownArrow.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String spVaccinatorText = (String) adapterView.getSelectedItem();
                txtFeedback.setText(spVaccinatorText);
//                btnProceed.performClick();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayFeedback = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        edtFirstName.setText(getCurrentUser().getFirst_name() + " " + getCurrentUser().getLast_name());
        edtPhone.setText(getCurrentUser().getPhone());
        arrayFeedback.add("Feedback Type");
        arrayFeedback.add("Support");
        arrayFeedback.add("Help");
        arrayFeedback.add("Service");
        arrayFeedback.add("Quality");
        arrayFeedback.add("Improvement");
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getBaseActivity(), android.R.layout.simple_spinner_dropdown_item, arrayFeedback);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDownArrow.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //  navigationBar.setVisibility(GONE);

    }

    @Override
    public void onClick(View v) {

    }


    private void serviceCall() {
        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setName(edtFirstName.getStringTrimmed());
        searchModel.setPhone(edtPhone.getStringTrimmed());
        searchModel.setMessage(edText.getStringTrimmed());
        searchModel.setFeedback(txtFeedback.getStringTrimmed());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_CONTACT_US, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showToast(getContext(), webResponse.result.toString());
                                popBackStack();
//
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    @OnClick({R.id.contFeedabck, R.id.btnSend, R.id.imgBackBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contFeedabck:
                spDownArrow.performClick();
                break;  case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.btnSend:
                if (edText.testValidity() && edtPhone.testValidity() && edtFirstName.testValidity() &&
                        !txtFeedback.getStringTrimmed().equalsIgnoreCase("feedback type"))
                    serviceCall();
                else UIHelper.showToast(getContext(), "Please Select Feedback Type");
                break;
        }
    }
}

package com.app.digilink.fragments.dialogs;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.digilink.R;
import com.app.digilink.activities.BaseActivity;
import com.app.digilink.fragments.CreateAnotherCard1Fragment;
import com.app.digilink.fragments.CreateCardSignUpFragment;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.widget.AnyTextView;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ncorti.slidetoact.SlideToActView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.facebook.GraphRequest.TAG;


/**
 * Created by  on 21-Feb-17.
 */

public class SocialMediaDialogFragment extends DialogFragment {


    private static final int RC_SIGN_IN = 001;
    private static final int FACEBOOK = 002;
    Unbinder unbinder;
    @BindView(R.id.txtHeader)
    AnyTextView txtHeader;
    @BindView(R.id.btnCross)
    AnyTextView btnCross;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.btnfb)
    SlideToActView btnfb;
    @BindView(R.id.btnGoogle)
    SlideToActView btnGoogle;
    //    @BindView(R.id.btnLinkedIn)
//    SlideToActView btnLinkedIn;
    @BindView(R.id.sign_in_button)
    SignInButton googleSignIn;
    //    @BindView(R.id.btnEmail)
//    SlideToActView btnEmail;
    private View.OnClickListener btnGooglePlus;
    private View.OnClickListener btnFacebook;
    private View.OnClickListener btnLinkedIn;
    private View.OnClickListener btnEmail;
    private BaseActivity baseActivity;
    private UserModel currentUser;
    private String data1;
    private AccessToken accessToken;
    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private String profile_pic;
    GoogleSignInClient mGoogleSignInClient;

    public SocialMediaDialogFragment() {
    }

    public static SocialMediaDialogFragment newInstance(UserModel currentUser, BaseActivity baseActivity, View.OnClickListener btnFacebook,
                                                        View.OnClickListener btnLinkedIn,
                                                        View.OnClickListener btnGooglePlus,
                                                        View.OnClickListener btnEmail) {
        SocialMediaDialogFragment frag = new SocialMediaDialogFragment();


        Bundle args = new Bundle();
        frag.btnFacebook = btnFacebook;
        frag.btnLinkedIn = btnLinkedIn;
        frag.currentUser = currentUser;
        frag.btnGooglePlus = btnGooglePlus;
        frag.btnEmail = btnEmail;
        frag.baseActivity = baseActivity;

        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogTheme);
        callbackManager = CallbackManager.Factory.create();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getActivity(), gso);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.popup_signup, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    private static final String EMAIL = "email";

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        loginButton.setFragment(this);
        loginButton.setReadPermissions("email");

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getActivity());
//        updateUI(account);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
//        KeyboardHelper.showSoftKeyboardForcefully(getContext(), txtPinCode);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWithFacebook();
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.btnLinkedIn, R.id.btnfb, R.id.btnGoogle, R.id.btnEmail, R.id.login_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLinkedIn:
                dismiss();
                break;
            case R.id.btnGoogle:
                mGoogleSignInClient.signOut();
                signIn();
                break;
            case R.id.btnfb:
                LoginManager.getInstance().logOut();
                loginButton.performClick();
                break;
            case R.id.btnEmail:

                if (currentUser != null && currentUser.getId() != 0) {
                    baseActivity.addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    baseActivity.addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }


                dismiss();
                break;
            case R.id.login_button:
                loginWithFacebook();
                break;
        }
    }


    @OnClick(R.id.btnCross)
    public void onViewClicked() {
        dismiss();
    }

    private void loginWithFacebook() {
        loginButton.setReadPermissions("email");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization

        // Callback registration
        this.loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                AccessToken accessToken = loginResult.getAccessToken();
//
                Log.e("fb", accessToken.getUserId());
                Log.e("accessToken", "accessToken " + accessToken.getToken());
                fetchUserInfo(accessToken);
                Log.e("fb", accessToken.getUserId());

                AccessToken token = AccessToken.getCurrentAccessToken();
                if (token != null) {
                    //Log.e("accessToken2", "accessToken2 "+token);

                }
//                prefHelper.setLoginStatus(true);
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    private void fetchUserInfo(AccessToken accessToken) {
        if (accessToken != null) {
            GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                @Override
                public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                    onSocialInfoFetched(jsonObject);
                    Log.e("FbUserData", jsonObject.toString());


                }
            });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email");
            request.setParameters(parameters);
            request.executeAsync();


        }

    }

    public void onSocialInfoFetched(JSONObject data) {
//        isGoogleLogin = false;
        final Bundle bundle = new Bundle();


        try {

            if (data.has("id")) {
                bundle.putString("id", data.getString("id"));
            }
            if (data.has("name")) {
                bundle.putString("name", data.getString("name"));
            }

//            try {
            profile_pic = new String("https://graph.facebook.com/" + data.getString("id") + "/picture?width=200&height=150");
            Log.i("profile_pic", profile_pic + "");
            bundle.putString("profile_pic", profile_pic.toString());
            Log.e("****", "onCurrentProfileChanged: " + profile_pic);

            if (profile_pic != null) {
//         File f =     File("image/*", new File(profile_pic));
//                File picture = MediaType("image/*", (new File(profile_pic)));
//                picture = convertFile(new File(profile_pic));
            }

            if (data.has("email")) {
                bundle.putString("email", data.getString("email"));
            }
            try {
                if (data.has("picture")) {
                    String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
//                    Bitmap profilePic = BitmapFactory.decodeStream(profilePicUrl.openConnection().getInputStream());
                    // set profilePic bitmap to imageview
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            dismiss();
            baseActivity.addDockableFragment(CreateCardSignUpFragment.newInstance(false, data.getString("id"), data.getString("name"), data.getString("email"), profile_pic), false);

//            serviceCallSocialMedia(data.getString("id"), data.getString("name"), data.getString("email"), bundle, profile_pic);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }



    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
        if (requestCode == FACEBOOK) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            dismiss();
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            baseActivity.addDockableFragment(CreateCardSignUpFragment.newInstance(false, account.getId(), account.getDisplayName(), account.getEmail(), account.getPhotoUrl() != null ? account.getPhotoUrl().toString() : null), false);

            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }
}


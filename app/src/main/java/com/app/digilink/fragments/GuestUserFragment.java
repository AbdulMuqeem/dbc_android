package com.app.digilink.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.TermPolicyModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatDelegate;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.annotations.Nullable;

import static com.app.digilink.constatnts.AppConstants.FILENAME_LANG;
import static com.app.digilink.constatnts.AppConstants.KEY_DARK_THEME;

/**
 * Created by  on 08-May-17.
 */

public class GuestUserFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgBackBtn)
    ImageView imgBackBtn;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.imgNotificationIcon)
    ImageView imgNotificationIcon;
    @BindView(R.id.containerTitlebar1)
    LinearLayout containerTitlebar1;
    @BindView(R.id.imgUser)
    CircleImageView imgUser;
    @BindView(R.id.txtGuestUser)
    AnyTextView txtGuestUser;
    @BindView(R.id.txtRegi_login)
    AnyTextView txtRegiLogin;
    @BindView(R.id.txtPushNotification)
    AnyTextView txtPushNotification;
    @BindView(R.id.btnNightMode)
    ToggleButton btnNightMode;
    @BindView(R.id.txtSetting)
    AnyTextView txtSetting;
    @BindView(R.id.txtHelp)
    AnyTextView txtHelp;
    @BindView(R.id.txtTermsAndCondition)
    AnyTextView txtTermsAndCondition;
    @BindView(R.id.txtPrivacyPolicy)
    AnyTextView txtPrivacyPolicy;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.btnInfo1)
    ImageView btnInfo1;
    @BindView(R.id.containerPersonal)
    LinearLayout containerPersonal;
    @BindView(R.id.btnInfo)
    ImageView btnInfo;
    @BindView(R.id.btnUpgrate)
    AnyTextView btnUpgrate;
    @BindView(R.id.containerCorp)
    LinearLayout containerCorp;
    @BindView(R.id.bottom_sheet_cc)
    LinearLayout bottomSheetCc;
    @BindView(R.id.txtLangauge)
    AnyTextView txtLangauge;
    @BindView(R.id.btnToggleLanguage)
    ToggleButton btnToggleLanguage;
    private ArrayList<TermPolicyModel> arrayList;

    public static GuestUserFragment newInstance() {
        Bundle args = new Bundle();
        GuestUserFragment fragment = new GuestUserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_guest_user_setting;
    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.GONE);
        titleBar.setTitle(getResources().getString(R.string.profile));
        titleBar.showBackButton(getBaseActivity());

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
    }

    @Override
    public void setListeners() {
        btnToggleLanguage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    servicCallLanguage("ar");

                } else {
                    servicCallLanguage("en");

                }
            }
        });
        btnNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    sharedPreferenceManager.putValue(KEY_DARK_THEME, "Dark");
                    {
                        AppCompatDelegate
                                .setDefaultNightMode(
                                        AppCompatDelegate
                                                .MODE_NIGHT_YES);
                    }

                } else {
                    sharedPreferenceManager.putValue(KEY_DARK_THEME, "Light");
//                    getBaseActivity().setTheme(R.style.LightTheme);
                    {
                        AppCompatDelegate
                                .setDefaultNightMode(
                                        AppCompatDelegate
                                                .MODE_NIGHT_NO);
                    }

                }
                getBaseActivity().refreshFragment1(GuestUserFragment.newInstance());
            }
        });

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (sharedPreferenceManager.getString(KEY_DARK_THEME).equalsIgnoreCase("Dark")) {
            btnNightMode.setChecked(true);

        } else {
            btnNightMode.setChecked(false);
        }
        if (!onCreated)
            serviceCall();
        if (sharedPreferenceManager.getString(FILENAME_LANG).equals("en")) {
            btnToggleLanguage.setChecked(false);
        } else {
            btnToggleLanguage.setChecked(true);
        }
        resetNvigationBar1();
        sheetBehaviorCreateCard = BottomSheetBehavior.from(bottomSheetCc);
    }

    public void resetNvigationBar1() {
        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnWallet.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnBrowser.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.base_blue));

    }

    private void serviceCall() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectWithoutParam(WebServiceConstants.METHOD_TERM_PRIVACY,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<TermPolicyModel>>() {
                                }.getType();
                                arrayList = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
        //   navigationBar.resetNvigationBar();
        //    navigationBar.resetViews();
        //  navigationBar.setVisibility(VISIBLE);
//        navigationBar.clickBtnProfile(getBaseActivity());

    }

    @Override
    public void onClick(View v) {

    }

    private boolean isEnglish = true;

    @OnClick({R.id.imgBackBtn, R.id.txtRegi_login, /*R.id.btnNightMode,*/ R.id.txtSetting, R.id.txtHelp,
            R.id.txtTermsAndCondition, R.id.txtPrivacyPolicy,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBackBtn:
                popBackStack();
                break;
            case R.id.txtRegi_login:
                getBaseActivity().addDockableFragment(LoginRegisFragment.newInstance(), false);
                break;
            case R.id.txtSetting:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("About", arrayList.get(0).getAbout()), false);

//                getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                break;
            case R.id.txtHelp:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Help", arrayList.get(0).getPrivacy()), false);

                break;
            case R.id.txtTermsAndCondition:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Terms and Condition", arrayList.get(0).getTerms()), false);

                break;
            case R.id.txtPrivacyPolicy:
                getBaseActivity().addDockableFragment(AboutFragment.newInstance("Privacy Policy", arrayList.get(0).getPrivacy()), false);

                break;

            case R.id.btnAdd:
                createCard();
//                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.contHome:
                if (getBaseActivity() instanceof HomeActivity) {
//                    activity.reload();
                    getBaseActivity().popStackTill(1);

                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;
            case R.id.contWallet:
                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null, false), false);
                break;
            case R.id.contProfile:
//                getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                break;
        }
    }


    protected void servicCallLanguage(String status) {
//        SearchModel searchModel = new SearchModel();
//        searchModel.setUser_id(getCurrentUser().getId());
//        searchModel.setStatus(status);
//        new WebServices(getBaseActivity(),
//                "", true)
//                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_CHANGE_LANGUAGE, searchModel,
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UIHelper.showToast(getContext(), webResponse.result.toString());
        sharedPreferenceManager.putValue(FILENAME_LANG, status);
        setDefaultLocale();
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                            }
//                        });


    }

    private BottomSheetBehavior sheetBehaviorCreateCard;

    private void createCard() {


        if (sheetBehaviorCreateCard.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        containerPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }
            }
        });

        containerCorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }

            }
        });


    }
}

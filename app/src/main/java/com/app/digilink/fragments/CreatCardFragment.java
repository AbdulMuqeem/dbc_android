package com.app.digilink.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.fragments.dialogs.SocialMediaDialogFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.OccupationModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

public class CreatCardFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.txtTitle)
    AnyTextView txtTitle;
    @BindView(R.id.txtPersonal)
    AnyTextView txtPersonal;
    @BindView(R.id.imgQuest)
    AnyTextView imgQuest;
    @BindView(R.id.txtPersonalDet)
    AnyTextView txtPersonalDet;
    @BindView(R.id.imgCount)
    AnyTextView imgCount;
    @BindView(R.id.contPersonal)
    LinearLayout contPersonal;
    @BindView(R.id.txtCoop)
    AnyTextView txtCoop;
    @BindView(R.id.imgQuest1)
    AnyTextView imgQuest1;
    @BindView(R.id.txtCoopDet)
    AnyTextView txtCoopDet;
    @BindView(R.id.imgUpgra)
    AnyTextView imgUpgra;
    @BindView(R.id.contCoorp)
    LinearLayout contCoorp;
    @BindView(R.id.tagLine)
    AnyTextView tagLine;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;

    //
    public static CreatCardFragment newInstance() {
        Bundle args = new Bundle();
        CreatCardFragment fragment = new CreatCardFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_createcard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
            contCoorp.setAlpha(1.0f);
//            contCoorp.setBackgroundTintList(getResources().getColorStateList(R.color.transparent));
            contCoorp.setBackgroundTintList(null);
//            contCoorp.setColorFilter(null)


        }

        resetNvigationBar1();
    }

    public void resetNvigationBar1() {
        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnWallet.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnBrowser.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.transparent));

    }

    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
        titleBar.setTitle(getResources().getString(R.string.Create_Business_Card));
        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {




    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.contPersonal, R.id.contCoorp,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.contPersonal:
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    SocialMediaDialogFragment socialMediaDialogFragment = SocialMediaDialogFragment.newInstance(getCurrentUser(), getBaseActivity(), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    }, view12 -> {

                    }, view1 -> {

                    }, view13 -> {

                    });
                    socialMediaDialogFragment.setCancelable(true);
                    socialMediaDialogFragment.show(getBaseActivity().getSupportFragmentManager(), null);
                }
                break;
            case R.id.contCoorp:
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }
                break;


            case R.id.btnAdd:
//                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.contHome:
                if (getBaseActivity() instanceof HomeActivity) {
//                    activity.reload();
                    getBaseActivity().popStackTill(1);

                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;
            case R.id.contWallet:
                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null, false), false);
                break;
            case R.id.contProfile:
                if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                else getBaseActivity().addDockableFragment(GuestUserFragment.newInstance(), false);                break;
        }
    }

}
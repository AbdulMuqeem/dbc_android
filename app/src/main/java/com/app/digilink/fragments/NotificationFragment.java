package com.app.digilink.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.digilink.R;
import com.app.digilink.adapters.recyleradapters.NotificationAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.NotificationModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.GONE;

public class NotificationFragment extends BaseFragment implements View.OnClickListener, OnItemClickListener {


    Unbinder unbinder;
    @BindView(R.id.edtSearch)
    AnyEditTextView edtSearch;
    @BindView(R.id.rvGeneric)
    RecyclerView rvGeneric;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout; @BindView(R.id.contSearchBar)
    LinearLayout contSearchBar;
    @BindView(R.id.txtNoRecord)
    AnyTextView txtNoRecord;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    private ArrayList<NotificationModel> arrayList;
    private NotificationAdapter adapNotification;

    //
    public static NotificationFragment newInstance() {
        Bundle args = new Bundle();
        NotificationFragment fragment = new NotificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_generic_recycler;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        adapNotification = new NotificationAdapter(getBaseActivity(), arrayList, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        serviceCallGetCardTempates();
        contSearchBar.setVisibility(GONE);

        navigationBar.setVisibility(GONE);

    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(View.VISIBLE);
//        titleBar.setTitle("Notification");
        titleBar.setTitle(getResources().getString(R.string.Notifications));

        titleBar.showBackButton(getBaseActivity());

    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }


    @Override
    public void setNavigationBar(NavigationBar navigationBar) {
//         navigationBar.setVisibility(GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void serviceCallGetCardTempates() {
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_GET_ALL_NOTIFICATION, getCurrentUser().getId(),
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {

                                Type type = new TypeToken<ArrayList<NotificationModel>>() {
                                }.getType();
                                ArrayList<NotificationModel> arrayList1 = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);
                                arrayList.addAll(arrayList1);
                                if (arrayList1.size() > 0) {
                                    bindView();
                                    txtNoRecord.setVisibility(GONE);
                                } else txtNoRecord.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });
    }

    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvGeneric.setLayoutManager(mLayoutManager);
        rvGeneric.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvGeneric.setAdapter(adapNotification);
        adapNotification.notifyDataSetChanged();


    }

    @Override
    public void onItemClick(int position, View view, Object object) {
        if (object instanceof NotificationModel) {
            NotificationModel notificationModel = adapNotification.getItem(position);
            switch (view.getId()) {
                case R.id.btnAccept:
                    UIHelper.showToast(getContext(), "Accpted");
                    serviceCall(WebServiceConstants.ACCEPT_FRIEND_REQUEST, notificationModel);

                    break;
                case R.id.btnReject:
                    serviceCall(WebServiceConstants.METHOD_GET_REJECT_REQUEST1, notificationModel);
                    UIHelper.showToast(getContext(), "Rejected");
                    break;
            }
        }
    }

    private void serviceCall(String methodName, NotificationModel notificationModel) {

        SearchModel searchModel = new SearchModel();
        searchModel.setUser_id(getCurrentUser().getId());
        searchModel.setFriend_id(notificationModel.getFriend_id());
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(methodName, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                getBaseActivity().refreshFragment(NotificationFragment.newInstance());
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }
}
package com.app.digilink.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.app.digilink.R;
import com.app.digilink.activities.HomeActivity;
import com.app.digilink.adapters.recyleradapters.GenericCarddapterCardTemplate;
import com.app.digilink.adapters.recyleradapters.MyFriendsBusinessCardAdapter;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.dialogs.GenericPopupDialog;
import com.app.digilink.fragments.dialogs.WebviewPopupDialog;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.app.digilink.widget.AnyTextView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.Nullable;

import static android.view.View.VISIBLE;

public class WalletFragment extends BaseFragment
        implements View.OnClickListener, OnItemClickListener {
    @BindView(R.id.rvWalletCards)
    RecyclerView rvWalletCards;
    @BindView(R.id.rvBusinessCards)
    RecyclerView rvBusinessCards;
    //    @BindView(R.id.contParentLayout)
//    LinearLayout contParentLayout;
    @BindView(R.id.btnViewAll)
    AnyTextView btnViewAll;
    @BindView(R.id.btnCreateCard)
    ImageView btnCreateCard;
    @BindView(R.id.txtCount)
    AnyTextView txtCount;
    @BindView(R.id.btnAdd)
    ImageButton btnAdd;
    @BindView(R.id.btnHome)
    ImageButton btnHome;
    @BindView(R.id.contHome)
    LinearLayout contHome;
    @BindView(R.id.btnWallet)
    ImageButton btnWallet;
    @BindView(R.id.contWallet)
    LinearLayout contWallet;
    @BindView(R.id.btnBrowser)
    ImageButton btnBrowser;
    @BindView(R.id.contBrowser)
    LinearLayout contBrowser;
    @BindView(R.id.btnProfileSetting)
    ImageButton btnProfileSetting;
    @BindView(R.id.contProfile)
    LinearLayout contProfile;
    @BindView(R.id.navigationBar)
    LinearLayout navigationBar;
    @BindView(R.id.btnInfo)
    ImageView btnInfo;
    @BindView(R.id.contExportprintablefile)
    AnyTextView contExportprintablefile;
    @BindView(R.id.contDownloadFile)
    AnyTextView contDownloadFile;
    @BindView(R.id.bottom_sheet_d)
    LinearLayout bottomSheetD;
    @BindView(R.id.btnInfo1)
    ImageView btnInfo1;
    @BindView(R.id.containerPersonal)
    LinearLayout containerPersonal;
    @BindView(R.id.btnUpgrate)
    AnyTextView btnUpgrate;
    @BindView(R.id.containerCorp)
    LinearLayout containerCorp;
    @BindView(R.id.bottom_sheet_cc)
    LinearLayout bottomSheetCc;
    private Unbinder unbinder;

    private BottomSheetBehavior sheetBehaviorDownlaod;
    private MyFriendsBusinessCardAdapter adapterMyFriendsSaveCards;
    private GenericCarddapterCardTemplate adapCardList;
    private ArrayList<GenericCardListModel> arrayList;
    private ArrayList<CardTemplate> arrayListCard;

    //
    public static WalletFragment newInstance() {
        Bundle args = new Bundle();
        WalletFragment fragment = new WalletFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_wallet;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        arrayList = new ArrayList<>();
        arrayListCard = new ArrayList<>();
        adapterMyFriendsSaveCards = new MyFriendsBusinessCardAdapter(sharedPreferenceManager, getBaseActivity(), arrayList, this);
        adapCardList = new GenericCarddapterCardTemplate(getBaseActivity(), true, arrayListCard, this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        servicCall();
        arrayListCard.clear();
        if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
            rvWalletCards.setVisibility(VISIBLE);
            btnCreateCard.setVisibility(View.GONE);
            if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                arrayListCard.addAll(getCurrentUser().getCardList());
        } else {
            rvWalletCards.setVisibility(View.GONE);
            btnCreateCard.setVisibility(VISIBLE);
        }
        resetNvigationBar1();
//        txtCount.setText("Business Cards List (" + arrayList.size() + ")");
        bindView();
        sheetBehaviorDownlaod = BottomSheetBehavior.from(bottomSheetD);
        sheetBehaviorCreateCard = BottomSheetBehavior.from(bottomSheetCc);
        sheetView();
    }

    private void sheetView() {
        sheetBehaviorDownlaod.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_DRAGGING:
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                    }
                    break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public void resetNvigationBar1() {
        this.btnHome.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnWallet.setColorFilter(getResources().getColor(R.color.base_blue));
        this.btnAdd.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnBrowser.setColorFilter(getResources().getColor(R.color.transparent));
        this.btnProfileSetting.setColorFilter(getResources().getColor(R.color.transparent));

    }

    private void servicCall() {


        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObjectUserID(WebServiceConstants.METHOD_WALLET, getCurrentUser().getId(),
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                Type type = new TypeToken<ArrayList<GenericCardListModel>>() {
                                }.getType();
                                ArrayList<GenericCardListModel> arrayListC = GsonFactory.getSimpleGson()
                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result)
                                                , type);


                                arrayList.clear();
                                if (sharedPreferenceManager.isGuestUser()) {
                                    txtCount.setText("Business Cards List (1) ");
                                    arrayList.addAll(arrayListC);
                                } else {
                                    arrayList.addAll(arrayListC);
                                    txtCount.setText("Business Cards List (" + arrayList.size() + ")");
                                }
                                adapterMyFriendsSaveCards.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });


    }


    @Override
    public void setTitlebar(TitleBar titleBar) {
        titleBar.resetViews();
        titleBar.setVisibility(VISIBLE);
        titleBar.setTitle(getResources().getString(R.string.personal_wallet));
        titleBar.showBackButton(getBaseActivity());
    }


    @Override
    public void setListeners() {

    }

    @Override
    public void onClick(View v) {


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void setNavigationBar(NavigationBar navigationBar) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void bindView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseActivity());
        rvBusinessCards.setLayoutManager(mLayoutManager);
        rvBusinessCards.setAdapter(adapterMyFriendsSaveCards);
        adapterMyFriendsSaveCards.notifyDataSetChanged();

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getBaseActivity());
        rvWalletCards.setLayoutManager(mLayoutManager1);
        rvWalletCards.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvWalletCards.setAdapter(adapCardList);
        adapCardList.notifyDataSetChanged();

    }


    @Override
    public void onItemClick(int position, View view, Object object) {
        GenericCardListModel genericCardListModel = adapterMyFriendsSaveCards.getItem(position);
        switch (view.getId()) {
            case R.id.imgInfo:
                diaLogue(genericCardListModel);

                break;
            case R.id.imgSave:
                serviceCallDeleteToWallet(genericCardListModel);
                break;
            case R.id.imgFB:
                Intent viewIntent = new Intent("android.intent.action.VIEW",
                        Uri.parse(genericCardListModel.getFacebook()));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                break;
            case R.id.imgGoogle:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(genericCardListModel.getGmail()));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                break;
            case R.id.imgTwitter:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(genericCardListModel.getTwitter()));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                break;
            case R.id.imgLinkedin:
                viewIntent =
                        new Intent("android.intent.action.VIEW",
                                Uri.parse(genericCardListModel.getLinkedin()));
                if (viewIntent.resolveActivity(getBaseActivity().getPackageManager()) != null)
                    startActivity(viewIntent);
                else UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                break;
            case R.id.contParent:
                if (genericCardListModel.getCardList() != null && genericCardListModel.getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SpecificUserFragment.newInstance(genericCardListModel), false);
                break;
            case R.id.addminus:
                if (genericCardListModel.getRequestStatus().equalsIgnoreCase("Add friend"))
                    serviceCallAddRequest(WebServiceConstants.METHOD_SEND_FREQUEST, genericCardListModel.getId());
                else
                    serviceCallAddRequest(WebServiceConstants.METHOD_UN_FRIEND_USER, genericCardListModel.getId());
                break;
        }
//            }
//        }
        if (object instanceof CardTemplate) {
            CardTemplate cardsItem = adapCardList.getItem(position);
            switch (view.getId()) {
                case R.id.imgDownload1:
                case R.id.imgDownload2:
                case R.id.imgDownload3:
                case R.id.imgDownload4:
                case R.id.imgDownload5:
                case R.id.imgDownload6:
                case R.id.imgDownload7:
                case R.id.imgDownload8:
                case R.id.imgDownload9:
                case R.id.imgDownload10:
                    servicCallDownload(cardsItem, position);
                    break;
                case R.id.imgDelete1:
                case R.id.imgDelete2:
                case R.id.imgDelete3:
                case R.id.imgDelete4:
                case R.id.imgDelete5:
                case R.id.imgDelete6:
                case R.id.imgDelete7:
                case R.id.imgDelete8:
                case R.id.imgDelete9:
                case R.id.imgDelete10:
                    if (genericCardListModel.getCardList().size() <= 1) {
                        UIHelper.showToast(getBaseActivity(), "This action cannot be performed");
                    } else serviceCallDelete(cardsItem, position);
                    break;
                case R.id.imgPower1:
                case R.id.imgPower2:
                case R.id.imgPower3:
                case R.id.imgPower4:
                case R.id.imgPower5:
                case R.id.imgPower6:
                case R.id.imgPower7:
                case R.id.imgPower8:
                case R.id.imgPower9:
                case R.id.imgPower10:
                    serviceCallOnlineOfflineCard(cardsItem, position);
                    break;
                case R.id.imgEdit1:
                case R.id.imgEdit2:
                case R.id.imgEdit3:
                case R.id.imgEdit4:
                case R.id.imgEdit5:
                case R.id.imgEdit6:
                case R.id.imgEdit7:
                case R.id.imgEdit8:
                case R.id.imgEdit9:
                case R.id.imgEdit10:
                    if (getCurrentUser().getCardList().get(position).getCardType() == 1)
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(false, null, getCurrentUser().getCardList().get(position)), false);
                    else
                        getBaseActivity().addDockableFragment(TemplatesFragment.newInstance(true, null, getCurrentUser().getCardList().get(position)), false);
                    break;
                case R.id.imgShare1:
                case R.id.imgShare2:
                case R.id.imgShare3:
                case R.id.imgShare4:
                case R.id.imgShare5:
                case R.id.imgShare6:
                case R.id.imgShare7:
                case R.id.imgShare8:
                case R.id.imgShare9:
                case R.id.imgShare10:
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = "My Card " + getCurrentUser().getCardList().get(position).getCard();
                    sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "My Card");
                    sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                    startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    break;


                case R.id.imgFB1:
                case R.id.imgFB2:
                case R.id.imgFB3:
                case R.id.imgFB4:
                case R.id.imgFB5:
                case R.id.imgFB6:
                case R.id.imgFB7:
                case R.id.imgFB8:
                case R.id.imgFB9:
                case R.id.imgFB10:
                    Intent viewIntent1 = new Intent("android.intent.action.VIEW",
                            Uri.parse(genericCardListModel.getFacebook()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else
                        UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgGoogle1:
                case R.id.imgGoogle2:
                case R.id.imgGoogle3:
                case R.id.imgGoogle4:
                case R.id.imgGoogle5:
                case R.id.imgGoogle6:
                case R.id.imgGoogle7:
                case R.id.imgGoogle8:
                case R.id.imgGoogle9:
                case R.id.imgGoogle10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(genericCardListModel.getGmail()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else
                        UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgTwitter1:
                case R.id.imgTwitter2:
                case R.id.imgTwitter3:
                case R.id.imgTwitter4:
                case R.id.imgTwitter5:
                case R.id.imgTwitter6:
                case R.id.imgTwitter7:
                case R.id.imgTwitter8:
                case R.id.imgTwitter9:
                case R.id.imgTwitter10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(genericCardListModel.getTwitter()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else
                        UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");
                    break;
                case R.id.imgLinkedin1:
                case R.id.imgLinkedin2:
                case R.id.imgLinkedin3:
                case R.id.imgLinkedin4:
                case R.id.imgLinkedin5:
                case R.id.imgLinkedin6:
                case R.id.imgLinkedin7:
                case R.id.imgLinkedin8:
                case R.id.imgLinkedin9:
                case R.id.imgLinkedin10:
                    viewIntent1 =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse(genericCardListModel.getLinkedin()));
                    if (viewIntent1.resolveActivity(getBaseActivity().getPackageManager()) != null)
                        startActivity(viewIntent1);
                    else
                        UIHelper.showLongToastInCenter(getBaseActivity(), "Invalid URL");

                    break;
            }
        }

    }

    @OnClick({R.id.btnCreateCard, R.id.btnViewAll,
            R.id.btnAdd, R.id.contHome, R.id.contWallet, R.id.contBrowser, R.id.contProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCreateCard:
                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.btnViewAll:
                getBaseActivity().addDockableFragment(WalletCardsFragment.newInstance(arrayList), false);
                break;

            case R.id.btnAdd:
                createCard();
//                getBaseActivity().addDockableFragment(CreatCardFragment.newInstance(), false);
                break;
            case R.id.contHome:
                if (getBaseActivity() instanceof HomeActivity) {
//                    activity.reload();
                    getBaseActivity().popStackTill(1);

                } else {
                    getBaseActivity().clearAllActivitiesExceptThis(HomeActivity.class);
                }
                break;
            case R.id.contWallet:
//                getBaseActivity().addDockableFragment(WalletFragment.newInstance(), false);
                break;
            case R.id.contBrowser:
                getBaseActivity().addDockableFragment(BrowseFragment.newInstance(null, false), false);
                break;
            case R.id.contProfile:
                if (getCurrentUser().getCardList() != null && getCurrentUser().getCardList().size() > 0)
                    getBaseActivity().addDockableFragment(SettingFragment.newInstance(), false);
                else getBaseActivity().addDockableFragment(GuestUserFragment.newInstance(), false);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private void diaLogue(GenericCardListModel genericCardListModel) {

        WebviewPopupDialog genericPopupDialog = WebviewPopupDialog.newInstance(getBaseActivity(),
                genericCardListModel, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                }
        );
        genericPopupDialog.setCancelable(true);
        genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
    }

    private void serviceCallAddRequest(String methodName, int id) {
        SearchModel searchModel = new SearchModel();
        searchModel.setId(getCurrentUser().getId());
        searchModel.setFriend_id(id);
        new WebServices(getBaseActivity(),
                "", true)
                .webServiceRequestAPIAnyObject(methodName, searchModel,
                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
                            @Override
                            public void requestDataResponse(WebResponse<Object> webResponse) {
                                UIHelper.showToast(getContext(), webResponse.result.toString());
                            }

                            @Override
                            public void onError(Object object) {
                            }
                        });

    }

    private void servicCallDownload(CardTemplate genericCardListModel, int position) {


        if (sheetBehaviorDownlaod.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorDownlaod.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorDownlaod.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        contDownloadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceCallDownload(genericCardListModel, position);
            }
        });

        contExportprintablefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = genericCardListModel.getCard();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });


    }

    private BottomSheetBehavior sheetBehaviorCreateCard;

    private void createCard() {


        if (sheetBehaviorCreateCard.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            sheetBehaviorCreateCard.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        containerPersonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(false, "", "", "", ""), false);
                } else {
                    getBaseActivity().addDockableFragment(CreateCardSignUpFragment.newInstance(false, "", "", "", ""), false);
                }
            }
        });

        containerCorp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getCurrentUser() != null && getCurrentUser().getId() != 0) {
                    getBaseActivity().addDockableFragment(CreateAnotherCard1Fragment.newInstance(true, "", "", "", ""), false);

                } else {
                    GenericPopupDialog genericPopupDialog = GenericPopupDialog.newInstance(getBaseActivity(), "UNLOCK FULL VERSION", "Create Personal Business Card first!", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                                        tutorialsDialogFragment.dismiss();
                        }
                    });
                    genericPopupDialog.setCancelable(true);
                    genericPopupDialog.show(getBaseActivity().getSupportFragmentManager(), null);
                }

            }
        });


    }

}
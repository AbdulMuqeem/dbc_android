package com.app.digilink.fragments.abstracts;

/**
 * Created by  on 18-Feb-17.
 */

public interface GenericClickableInterface {
    void click();
}

package com.app.digilink.models.User;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EditUserModel implements Serializable {
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("user_id")
    @Expose
    int user_id;
    @SerializedName("card_id")
    @Expose
    int card_id;
    @SerializedName("card_name")
    @Expose
    String card_name;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("phone")
    @Expose
    String phone;
    @SerializedName("address")
    @Expose
    String address;
    @SerializedName("company_name")
    @Expose
    String company_name;
    @SerializedName("occupation")
    @Expose
    String occupation;
    @SerializedName("facebook")
    @Expose
    String facebook;
    @SerializedName("instagram")
    @Expose
    String instagram;
    @SerializedName("gmail")
    @Expose
    String gmail;
    @SerializedName("twitter")
    @Expose
    String twitter;
    @SerializedName("linkedin")
    @Expose
    String linkedin;
    @SerializedName("phone_order")
    @Expose
    int phone_order;
    @SerializedName("email_order")
    @Expose
    int email_order;
    @SerializedName("address_order")
    @Expose
    int address_order;
    @SerializedName("notes")
    @Expose
    String notes;


    @SerializedName("card_order")
    @Expose
    String card_order;
    @SerializedName("card_design")
    @Expose
    String card_design;
    @SerializedName("p_email")
    @Expose
    String p_email;
    @SerializedName("email_color")
    @Expose
    String email_color;
    @SerializedName("occupation_color")
    @Expose
    String occupation_color;

    @SerializedName("address_color")
    @Expose
    String address_color;
    @SerializedName("phone_color")
    @Expose
    String phone_color;

    @SerializedName("notes_color")
    @Expose
    String notes_color;

    @SerializedName("name_color")
    @Expose
    String name_color;


    @SerializedName("social_icon")
    @Expose
    String social_icon;


    @SerializedName("size")
    @Expose
    String size;
    @SerializedName("font")
    @Expose
    String font;


    @SerializedName("email_font")
    @Expose
    String email_font;
    @SerializedName("occupation_font")
    @Expose
    String occupation_font;

    @SerializedName("address_font")
    @Expose
    String address_font;
    @SerializedName("phone_font")
    @Expose
    String phone_font;

    @SerializedName("notes_font")
    @Expose
    String notes_font;

    @SerializedName("name_font")
    @Expose
    String name_font;


    @SerializedName("email_size")
    @Expose
    String email_size;

    @SerializedName("occupation_size")
    @Expose
    String occupation_size;

    @SerializedName("address_size")
    @Expose
    String address_size;
    @SerializedName("phone_size")
    @Expose
    String phone_size;

    @SerializedName("notes_size")
    @Expose
    String notes_size;

    @SerializedName("name_size")
    @Expose
    String name_size;



    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCard_id() {
        return card_id;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public int getPhone_order() {
        return phone_order;
    }

    public void setPhone_order(int phone_order) {
        this.phone_order = phone_order;
    }

    public int getEmail_order() {
        return email_order;
    }

    public void setEmail_order(int email_order) {
        this.email_order = email_order;
    }

    public int getAddress_order() {
        return address_order;
    }

    public void setAddress_order(int address_order) {
        this.address_order = address_order;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public String getCard_design() {
        return card_design;
    }

    public void setCard_design(String card_design) {
        this.card_design = card_design;
    }

    public String getP_email() {
        return p_email;
    }

    public void setP_email(String p_email) {
        this.p_email = p_email;
    }

    public String getEmail_color() {
        return email_color;
    }

    public void setEmail_color(String email_color) {
        this.email_color = email_color;
    }

    public String getOccupation_color() {
        return occupation_color;
    }

    public void setOccupation_color(String occupation_color) {
        this.occupation_color = occupation_color;
    }

    public String getAddress_color() {
        return address_color;
    }

    public void setAddress_color(String address_color) {
        this.address_color = address_color;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getPhone_color() {
        return phone_color;
    }

    public void setPhone_color(String phone_color) {
        this.phone_color = phone_color;
    }

    public String getNotes_color() {
        return notes_color;
    }

    public void setNotes_color(String notes_color) {
        this.notes_color = notes_color;
    }

    public String getName_color() {
        return name_color;
    }

    public void setName_color(String name_color) {
        this.name_color = name_color;
    }

    public String getSocial_icon() {
        return social_icon;
    }

    public void setSocial_icon(String social_icon) {
        this.social_icon = social_icon;
    }


    public String getCard_order() {
        return card_order;
    }

    public void setCard_order(String card_order) {
        this.card_order = card_order;
    }


    public String getEmail_font() {
        return email_font;
    }

    public void setEmail_font(String email_font) {
        this.email_font = email_font;
    }

    public String getOccupation_font() {
        return occupation_font;
    }

    public void setOccupation_font(String occupation_font) {
        this.occupation_font = occupation_font;
    }

    public String getAddress_font() {
        return address_font;
    }

    public void setAddress_font(String address_font) {
        this.address_font = address_font;
    }

    public String getPhone_font() {
        return phone_font;
    }

    public void setPhone_font(String phone_font) {
        this.phone_font = phone_font;
    }

    public String getNotes_font() {
        return notes_font;
    }

    public void setNotes_font(String notes_font) {
        this.notes_font = notes_font;
    }

    public String getName_font() {
        return name_font;
    }

    public void setName_font(String name_font) {
        this.name_font = name_font;
    }

    public String getOccupation_size() {
        return occupation_size;
    }

    public void setOccupation_size(String occupation_size) {
        this.occupation_size = occupation_size;
    }

    public String getAddress_size() {
        return address_size;
    }

    public void setAddress_size(String address_size) {
        this.address_size = address_size;
    }

    public String getPhone_size() {
        return phone_size;
    }

    public void setPhone_size(String phone_size) {
        this.phone_size = phone_size;
    }

    public String getNotes_size() {
        return notes_size;
    }

    public void setNotes_size(String notes_size) {
        this.notes_size = notes_size;
    }

    public String getName_size() {
        return name_size;
    }

    public void setName_size(String name_size) {
        this.name_size = name_size;
    }

    public String  getEmail_size() {
        return email_size;
    }

    public void setEmail_size(String email_size) {
        this.email_size = email_size;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}
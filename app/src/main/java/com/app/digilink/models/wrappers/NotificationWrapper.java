package com.app.digilink.models.wrappers;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.app.digilink.models.NotificationModel;

/**
 * Created by  on 11-Mar-17.
 */

public class NotificationWrapper {
    @SerializedName("Notifications")
    public ArrayList<NotificationModel> notifications;

}

package com.app.digilink.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import okhttp3.RequestBody;

public class CardTemplate implements Serializable {

    boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        this.isSelected = selected;
    }

    @SerializedName("is_primary")
    @Expose
    private int is_primary;
    @SerializedName("is_default")
    @Expose
    private int isDefault;
    @SerializedName("id")
    @Expose
    private int id;
    //    @SerializedName("name")
//    @Expose
//    private String name;
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("card_type")
    @Expose
    private int cardType;
    @SerializedName("card_design")
    @Expose
    private String cardDesign;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("card")
    @Expose
    private String card;
    @SerializedName("card_order")
    @Expose
    private String cardOrder;
    @SerializedName("name_color")
    @Expose
    private String nameColor;
    @SerializedName("occupation_color")
    @Expose
    private String occupationColor;
    @SerializedName("phone_color")
    @Expose
    private String phoneColor;
    @SerializedName("email_color")
    @Expose
    private String emailColor;
    @SerializedName("address_color")
    @Expose
    private String addressColor;
    @SerializedName("notes_color")
    @Expose
    private String notesColor;
    @SerializedName("font_family")
    @Expose
    private String fontFamily;
    @SerializedName("font_size")
    @Expose
    private String fontSize;

    @SerializedName("card_img")
    @Expose
    private String cardImg;
    @SerializedName("notes")
    @Expose
    private String notes;

    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("instagram")
    @Expose
    private String instagram;
    @SerializedName("gmail")
    @Expose
    private String gmail;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("linkedin")
    @Expose
    private String linkedin;


    @SerializedName("social_login")
    @Expose
    private ArrayList<SocialMediaLogin> arrSocialLogin = null;


    @SerializedName("email_font")
    @Expose
    String email_font;
    @SerializedName("occupation_font")
    @Expose
    String occupation_font;

    @SerializedName("address_font")
    @Expose
    String address_font;
    @SerializedName("phone_font")
    @Expose
    String phone_font;

    @SerializedName("notes_font")
    @Expose
    String notes_font;

    @SerializedName("name_font")
    @Expose
    String name_font;
    @SerializedName("email_size")
    @Expose
    String email_size;

    @SerializedName("occupation_size")
    @Expose
    String occupation_size;

    @SerializedName("address_size")
    @Expose
    String address_size;
    @SerializedName("phone_size")
    @Expose
    String phone_size;

    @SerializedName("notes_size")
    @Expose
    String notes_size;

    @SerializedName("name_size")
    @Expose
    String name_size;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCardType() {
        return cardType;
    }

    public void setCardType(int cardType) {
        this.cardType = cardType;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }


    public String getCardDesign() {
        return cardDesign;
    }

    public void setCardDesign(String cardDesign) {
        this.cardDesign = cardDesign;
    }

    public String getCardImg() {
        return cardImg;
    }

    public void setCardImg(String cardImg) {
        this.cardImg = cardImg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCardOrder() {
        return cardOrder;
    }

    public void setCardOrder(String cardOrder) {
        this.cardOrder = cardOrder;
    }

    public String getNameColor() {
        return nameColor;
    }

    public void setNameColor(String nameColor) {
        this.nameColor = nameColor;
    }

    public String getOccupationColor() {
        return occupationColor;
    }

    public void setOccupationColor(String occupationColor) {
        this.occupationColor = occupationColor;
    }

    public String getPhoneColor() {
        return phoneColor;
    }

    public void setPhoneColor(String phoneColor) {
        this.phoneColor = phoneColor;
    }

    public String getEmailColor() {
        return emailColor;
    }

    public void setEmailColor(String emailColor) {
        this.emailColor = emailColor;
    }

    public String getAddressColor() {
        return addressColor;
    }

    public void setAddressColor(String addressColor) {
        this.addressColor = addressColor;
    }

    public String getNotesColor() {
        return notesColor;
    }

    public void setNotesColor(String notesColor) {
        this.notesColor = notesColor;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIs_primary() {
        return is_primary;
    }

    public void setIs_primary(int is_primary) {
        this.is_primary = is_primary;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<SocialMediaLogin> getArrSocialLogin() {
        return arrSocialLogin;
    }

    public void setArrSocialLogin(ArrayList<SocialMediaLogin> arrSocialLogin) {
        this.arrSocialLogin = arrSocialLogin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEmail_font() {
        return email_font;
    }

    public void setEmail_font(String email_font) {
        this.email_font = email_font;
    }

    public String getOccupation_font() {
        return occupation_font;
    }

    public void setOccupation_font(String occupation_font) {
        this.occupation_font = occupation_font;
    }

    public String getAddress_font() {
        return address_font;
    }

    public void setAddress_font(String address_font) {
        this.address_font = address_font;
    }

    public String getPhone_font() {
        return phone_font;
    }

    public void setPhone_font(String phone_font) {
        this.phone_font = phone_font;
    }

    public String getNotes_font() {
        return notes_font;
    }

    public void setNotes_font(String notes_font) {
        this.notes_font = notes_font;
    }

    public String getName_font() {
        return name_font;
    }

    public void setName_font(String name_font) {
        this.name_font = name_font;
    }


    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }


    public String getEmail_size() {
        return email_size;
    }

    public void setEmail_size(String email_size) {
        this.email_size = email_size;
    }

    public String getOccupation_size() {
        return occupation_size;
    }

    public void setOccupation_size(String occupation_size) {
        this.occupation_size = occupation_size;
    }

    public String getAddress_size() {
        return address_size;
    }

    public void setAddress_size(String address_size) {
        this.address_size = address_size;
    }

    public String getPhone_size() {
        return phone_size;
    }

    public void setPhone_size(String phone_size) {
        this.phone_size = phone_size;
    }

    public String getNotes_size() {
        return notes_size;
    }

    public void setNotes_size(String notes_size) {
        this.notes_size = notes_size;
    }

    public String getName_size() {
        return name_size;
    }

    public void setName_size(String name_size) {
        this.name_size = name_size;
    }
}
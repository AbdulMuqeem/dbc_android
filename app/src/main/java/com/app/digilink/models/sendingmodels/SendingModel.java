package com.app.digilink.models.sendingmodels;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.models.SocialMediaLogin;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class SendingModel implements Serializable {


    private int id;
    private String firstName;
    private String lName;
    private File image;
    private String address;
    private String companyName;
    private String occupation;
    private String card;
    private String card_design;
    private String platform;
    private String deviceToken;
    private String iosDeviceToken;
    private String socialLogins;
    private String createdAt;
    private String email;
    private String phone;
    private String notes;
    private String card_status;
    private String card_type;

    private String facebook;

    private String instagram;

    private String gmail;

    private String twitter;

    private String linkedin;


    private ArrayList<SocialMediaLogin> arrSocialLogin = null;
    private int carid;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getIosDeviceToken() {
        return iosDeviceToken;
    }

    public void setIosDeviceToken(String iosDeviceToken) {
        this.iosDeviceToken = iosDeviceToken;
    }

    public String getSocialLogins() {
        return socialLogins;
    }

    public void setSocialLogins(String socialLogins) {
        this.socialLogins = socialLogins;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    public ArrayList<SocialMediaLogin> getArrSocialLogin() {
        return arrSocialLogin;
    }

    public void setArrSocialLogin(ArrayList<SocialMediaLogin> arrSocialLogin) {
        this.arrSocialLogin = arrSocialLogin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCard_design() {
        return card_design;
    }

    public void setCard_design(String card_design) {
        this.card_design = card_design;
    }

    public String getCard_status() {
        return card_status;
    }

    public void setCard_status(String card_status) {
        this.card_status = card_status;
    }

    public String getCard_type() {
        return card_type;
    }

    public void setCard_type(String card_type) {
        this.card_type = card_type;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }

    public void setCardID(int cardid) {
        this.carid = cardid;
    }

    public int getCarid() {
        return carid;
    }

}
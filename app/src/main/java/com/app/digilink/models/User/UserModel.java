package com.app.digilink.models.User;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.models.SocialMediaLogin;

import java.io.Serializable;
import java.util.ArrayList;

public class UserModel implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("profile_status")
    @Expose
    private String profile_status;

    @SerializedName("allow_email")
    @Expose
    private String email_status;


    @SerializedName("allow_push")
    @Expose
    private String push_status;


    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("profile_type")
    @Expose
    private String profile_type;
    @SerializedName("first_name")
    @Expose
    private String first_name;
    @SerializedName("last_name")
    @Expose
    private String last_name;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("card")
    @Expose
    private String card;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("ios_device_token")
    @Expose
    private String iosDeviceToken;
    @SerializedName("social_logins")
    @Expose
    private String socialLogins;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("card_list")
    @Expose
    private ArrayList<CardTemplate> cardList = null;
    @SerializedName("notes")
    @Expose
    String notes;

    @SerializedName("social_login")
    @Expose
    private ArrayList<SocialMediaLogin> arrSocialLogin = null;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    private final static long serialVersionUID = -6746251718868041916L;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getIosDeviceToken() {
        return iosDeviceToken;
    }

    public void setIosDeviceToken(String iosDeviceToken) {
        this.iosDeviceToken = iosDeviceToken;
    }

    public String getSocialLogins() {
        return socialLogins;
    }

    public void setSocialLogins(String socialLogins) {
        this.socialLogins = socialLogins;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public ArrayList<CardTemplate> getCardList() {
        return cardList;
    }

    public void setCardList(ArrayList<CardTemplate> cardList) {
        this.cardList = cardList;
    }

    public ArrayList<SocialMediaLogin> getArrSocialLogin() {
        return arrSocialLogin;
    }

    public void setArrSocialLogin(ArrayList<SocialMediaLogin> arrSocialLogin) {
        this.arrSocialLogin = arrSocialLogin;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfile_status() {
        return profile_status;
    }

    public void setProfile_status(String profile_status) {
        this.profile_status = profile_status;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getProfile_type() {
        return profile_type;
    }

    public void setProfile_type(String profile_type) {
        this.profile_type = profile_type;
    }

    public String getEmail_status() {
        return email_status;
    }

    public void setEmail_status(String email_status) {
        this.email_status = email_status;
    }

    public String getPush_status() {
        return push_status;
    }

    public void setPush_status(String push_status) {
        this.push_status = push_status;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }
}
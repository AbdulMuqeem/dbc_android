package com.app.digilink.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllCardsModel implements Serializable {

    @SerializedName("result")
    @Expose
    private ArrayList<GenericCardListModel> genericCardListModel = null;

    @SerializedName("total")
    @Expose
    private int total;

    public ArrayList<GenericCardListModel> getGenericCardListModel() {
        return genericCardListModel;
    }

    public void setGenericCardListModel(ArrayList<GenericCardListModel> genericCardListModel) {
        this.genericCardListModel = genericCardListModel;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }


    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }

}
package com.app.digilink.models.sendingmodels;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchModel {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("lng")
    @Expose
    private double lng;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("devicetoken")
    @Expose
    private String devicetoken;
    @SerializedName("feedback")
    @Expose
    private String feedback;
    @SerializedName("user_id")
    @Expose
    private int user_id;
    @SerializedName("card_id")
    @Expose
    private int card_id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("friend_id")
    @Expose
    private int friend_id;
    @SerializedName("card_holder_id")
    @Expose
    private int card_holder_id;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("card_template")
    @Expose
    private String card_design;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("filter_by")
    @Expose
    private int filter_by;
    @SerializedName("card_template")
    @Expose
    private String card_template;

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getCard_design() {
        return card_design;
    }

    public void setCard_design(String card_design) {
        this.card_design = card_design;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFriend_id() {
        return friend_id;
    }

    public void setFriend_id(int friend_id) {
        this.friend_id = friend_id;
    }

    public int getCard_holder_id() {
        return card_holder_id;
    }

    public void setCard_holder_id(int card_holder_id) {
        this.card_holder_id = card_holder_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCard_id() {
        return card_id;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFilter_by() {
        return filter_by;
    }

    public void setFilter_by(int filter_by) {
        this.filter_by = filter_by;
    }

    public String getCard_template() {
        return card_template;
    }

    public void setCard_template(String card_template) {
        this.card_template = card_template;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getDevicetoken() {
        return devicetoken;
    }

    public void setDevicetoken(String devicetoken) {
        this.devicetoken = devicetoken;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }

}

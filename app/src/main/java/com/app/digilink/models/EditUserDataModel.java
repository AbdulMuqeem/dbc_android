package com.app.digilink.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.app.digilink.managers.retrofit.GsonFactory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditUserDataModel implements Serializable {

    @SerializedName("fonts")
    @Expose
    private ArrayList<FontModel2> fonts = null;
    @SerializedName("inputs")
    @Expose
    private ArrayList<InputModel> inputs = null;
    private final static long serialVersionUID = -8958506477575864925L;

    public ArrayList<FontModel2> getFonts() {
        return fonts;
    }

    public void setFonts(ArrayList<FontModel2> fonts) {
        this.fonts = fonts;
    }

    public ArrayList<InputModel> getInputs() {
        return inputs;
    }

    public void setInputs(ArrayList<InputModel> inputs) {
        this.inputs = inputs;
    }
    @Override
    public String toString() {
        return GsonFactory.getSimpleGson().toJson(this);
    }

}
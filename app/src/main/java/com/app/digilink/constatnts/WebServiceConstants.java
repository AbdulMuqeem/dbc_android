package com.app.digilink.constatnts;


public class WebServiceConstants {

//    public static String BASE_URL = "http://zairabbas.net/abiha/cardapplication/json/";
    public static String BASE_URL = "https://www.linkiapp.com/cardapplication/json/";
    public static final String METHOD_REGISTERATION = "userRegister";
    public static final String METHOD_WALLET = "userWallet";

    public static final String METHOD_ALL_CARDS = "getAllCards";
    public static final String METHOD_SAVE_CARDS = "saveContact";
    public static final String METHOD_TUTORIAL = "getTutorial";
    public static final String METHOD_SOCIAL_LOGINS = "getSocialLogins";
    public static final String METHOD_CARDTEMPATES = "getCardTemplates";
    public static final String METHOD_CARDTEMPATES_USER = "chooseCardTemplate";
    public static final String METHOD_UPLOADIMAGE = "uploadImage";

    public static final String METHOD_SEND_FREQUEST = "sendFriendRequest";
    public static final String METHOD_UN_FRIEND_USER = "unFriendUser";
    public static final String METHOD_TERM_SPECIIC_USER = "getSpecificUser";
    public static final String METHOD_EDIT_USER = "editUserCard";
    public static final String METHOD_FONT_INPUT = "getCardFontAndInput";
    public static final String METHOD_ONLINEOFFLINE_CARD = "onlineOfflineCard";
    public static final String METHOD_ADD_USER_CARD = "addUserCard";
    public static final String METHOD_GET_EDIT_CARD_INFO = "getEditCardInfo";
    public static final String METHOD_USER_CARD_ACTIVE = "userCardActive";
    public static final String METHOD_TERM_PRIVACY = "getAboutTermsPrivacy";

    public static final String METHOD_PROFILE_STATUS = "changeProfileStatus";
    public static final String METHOD_ON_OFF_NOTIFICATION = "onNoffPush";
    public static final String METHOD_ON_OFF_NOTIFI_EMAIL = "onNoffEmail";
    public static final String METHOD_CHANGE_LANGUAGE = "changeLanguage";
    public static final String METHOD_CONTACT_US = "contactUs";
    public static final String METHOD_INVITE = "invite";
    public static final String METHOD_GET_ALL_NOTIFICATION = "getAllNotifications";
    public static final String METHOD_GET_THEME = "getTheme";
    public static final String METHOD_GET_REJECT_REQUEST1 = "rejectFriendRequest";
    public static final String ACCEPT_FRIEND_REQUEST = "acceptFriendRequest";
    public static final String METHOD_ADD_TOKEN = "add_tokens";
    public static final String METHOD_GET_ALL_FRIENDS = "getAllFriends";
//    public static final String METHOD_GET_ALL_SAVE_CARDS = "saveCardInContacts";
    public static final String METHOD_DELETE_CARD = "deleteCard";
    public static final String METHOD_CHANGE_PRI_CARD = "changePrimaryCard";

    public static final String METHOD_USER_LOGIN = "user_login";
    public static final String METHOD_USER_FORGOT_PASS = "forgetPassword";

    public static final String METHOD_CARD_TEMPLATE_SAVE = "updateCardTemplate";
    public static final String METHOD_BY_FILTER = "getCardsByFilter";
    public static final String METHOD_OCCUPATION_LIST = "getOccupationList";
    public static final String METHOD_DELETE_CARD_WALLET  = "deleteCardFromWallet";
    public static final String METHOD_GET_DOWNLOAD  = "download_pdf.php?card_id=";
    public static final String METHOD_DOWNLOAD_CARD  = "download_card";

    public static final String METHOD_TERM_RESTRICTED_DOMAIN = "restrictedDomains";

//    public static final String METHOD_TERM_SAVE_CONTACT = "saveCardInContacts";
    public static final String METHOD_CHANGE_PASSWORD= "changePassowrd";



}

package com.app.digilink.constatnts;

import android.os.Environment;

import com.app.digilink.BaseApplication;


/**
 * Created by  on 4/20/2017.
 */

public class AppConstants {

    public static final String INPUT_DATE_FORMAT = "yyyy-dd-MM hh:mm:ss";
    public static final String INPUT_DATE_FORMAT_AM_PM = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_DATE_FORMAT = "EEEE dd,yyyy";
    public static final String INPUT_TIME_FORMAT = "yyyy-dd-MM hh:mm:ss a";
    public static final String OUTPUT_TIME_FORMAT = "hh:mm a";
    public static final String ROOT_MEDIA_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/" + BaseApplication.getApplicationName() + "/Media";
    public static final String USER_PROFILE_PICTURE = ROOT_MEDIA_PATH + "/" + BaseApplication.getApplicationName() + " Profile";

    /*******************Preferences KEYS******************/
    public static final String USER_DATA = "userData";
    public static final String USER_NOTIFICATION_DATA = "USER_NOTIFICATION_DATA";

    public static String GUEST_USER = "guest_user";
    public static String FORCED_RESTART = "forced_restart";
    public static String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    public static String PCODE = "PCODE";

    //    API SERVICES
    public static final String KEY_ONE_TIME_TOKEN = "";


    /*******************Preferences KEYS******************/
    public static final String KEY_CURRENT_USER_MODEL = "KEY_CURRENT_USER_MODEL";
    public static String FILENAME_LANG = "en";
    public static final String FILENAME_NOTIFICATION_OFF = "on";
    public static final String FILENAME_PUBLICPRIVATE = "pub";
    public static final String GCM_DATA_OBJECT = "gcmDataObject";

    public static final String KEY_REGISTERED_DEVICE = "A7hb7mVVIbNzjbAiRkDG5lWI0NZ84dBXXiZsZ1lTb7w=\n";
    public static final String KEY_INSERT_REGISTERED_DEVICE = "A7hb7mVVIbNzjbAiRkDG5lWI0NZ84dBXXiZsZ1lTb7w=\n";
    public static final String KEY_FIREBASE_TOKEN_UPDATED = "vbesxYXN7RL5oFkKT47yU2TzTYoRhoU5YfmAXv7+UC4=\n";
    public static String DEVICE_OS_ANDROID = "ANDROID";
    public static String KEY_DARK_THEME = "KEY_DARK_THEME";
    public static String KEY_CORPORATE_CARD = "KEY_CORPORATE_CARD";











}

package com.app.digilink.libraries.searchdialog.core;

public interface Searchable {
    String getTitle();

    String getContent();

    String getSearchTime();
}

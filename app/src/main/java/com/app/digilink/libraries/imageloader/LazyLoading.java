package com.app.digilink.libraries.imageloader;


import com.app.digilink.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;




/**
 * Created by  on 08-Mar-17.
 */

public class LazyLoading {

    public static DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
            .showImageForEmptyUri(R.color.material_grey100)
            .showImageOnFail(R.drawable.profile_placeholder)
            .showImageOnLoading(R.drawable.profile_placeholder).build();
}

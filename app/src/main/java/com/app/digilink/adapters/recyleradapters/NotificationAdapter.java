package com.app.digilink.adapters.recyleradapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.models.NotificationModel;
import com.app.digilink.widget.AnyTextView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;


    private Activity activity;
    private ArrayList<NotificationModel> arrayList;

    public NotificationAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;

    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_notification, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final NotificationModel model = arrayList.get(holder.getAdapterPosition());
        if (model.getType() == 1) {
            holder.contFriendReq.setVisibility(View.VISIBLE);

        } else {
            holder.contFriendReq.setVisibility(View.GONE);
        }
        holder.txtNotiDate.setText(model.getCreatedDate());
        holder.txtNotification.setText(model.getMessage());
        setListener(holder, model);


    }

    private void setListener(ViewHolder holder, NotificationModel model) {
        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
//        return 3;
    }

    public NotificationModel getItem(int position) {
        return arrayList.get(position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btnAccept)
        AnyTextView btnAccept;
        @BindView(R.id.btnReject)
        AnyTextView btnReject;
        @BindView(R.id.contFriendReq)
        LinearLayout contFriendReq;
        @BindView(R.id.txtNotification)
        AnyTextView txtNotification;
        @BindView(R.id.txtNotiDate)
        AnyTextView txtNotiDate;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

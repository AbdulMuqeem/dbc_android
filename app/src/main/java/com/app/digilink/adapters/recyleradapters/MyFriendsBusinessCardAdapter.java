package com.app.digilink.adapters.recyleradapters;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.managers.SharedPreferenceManager;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.app.digilink.models.GenericCardListModel;
import com.app.digilink.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;


public class MyFriendsBusinessCardAdapter extends RecyclerView.Adapter<MyFriendsBusinessCardAdapter.ViewHolder> {

    private Filter mFilter = new MyFriendsBusinessCardAdapter.ItemFilter();
    private final OnItemClickListener onItemClick;
    private final SharedPreferenceManager sharedPreferenceManager;
    private ArrayList<GenericCardListModel> filteredData = new ArrayList<>();
    private Activity activity;
    private ArrayList<GenericCardListModel> arrayList;

    public MyFriendsBusinessCardAdapter(SharedPreferenceManager sharedPreferenceManager, Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;
        this.sharedPreferenceManager = sharedPreferenceManager;
        this.filteredData = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_user_bcard, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

//        final GenericCardListModel model = arrayList.get(holder.getAdapterPosition());
        final GenericCardListModel model = filteredData.get(holder.getAdapterPosition());
        holder.txtName.setText(model.getName());
        holder.txtEmailAddress.setText(model.getEmail());


        if (model.getImage() != null)
            ImageLoader.getInstance().displayImage(model.getImage().toString(), holder.imgUser);

        if (model.getImage() != null)
            ImageLoader.getInstance().displayImage(model.getImage(), holder.imgUser);
        if (model.getFacebook() != null && !model.getFacebook().equalsIgnoreCase("")) {
            holder.imgFB.setVisibility(VISIBLE);
        }
        if (model.getTwitter() != null && !model.getTwitter().equalsIgnoreCase("")) {
            holder.imgTwitter.setVisibility(VISIBLE);
        }
        if (model.getLinkedin() != null && !model.getLinkedin().equalsIgnoreCase("")) {
            holder.imgLinkedin.setVisibility(VISIBLE);
        }
        if (model.getGmail() != null && !model.getGmail().equalsIgnoreCase("")) {
            holder.imgGoogle.setVisibility(VISIBLE);
        }
//        if (model.getArrSocialLogin() != null) {
//            for (int i = 0; i < model.getArrSocialLogin().size(); i++) {
//                if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("facebook")) {
//                    holder.imgFB.setVisibility(View.VISIBLE);
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("twitter")) {
//                    holder.imgTwitter.setVisibility(View.VISIBLE);
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("linkedin")) {
//                    holder.imgLinkedin.setVisibility(View.VISIBLE);
//                } else if (model.getArrSocialLogin().get(i).getName().equalsIgnoreCase("google")) {
//                    holder.imgGoogle.setVisibility(View.VISIBLE);
//                }
//            }

//        }
       /* for (int i = 0; i < model.getCardList().size(); i++) {

            if (model.getStatus() == 1) {
                holder.addminus.setVisibility(View.VISIBLE);
            } else holder.addminus.setVisibility(View.GONE);
        }*/


//        if (model.getRequestStatus().equalsIgnoreCase("friend")) {
        holder.imgSave.setImageDrawable(activity.getDrawable(R.drawable.delete_new));
//        } else {
//            holder.imgSave.setImageDrawable(activity.getDrawable(R.drawable.saveicon));
//        }
        if (model.getEmail().equalsIgnoreCase("admin@dbc.com")) {
            holder.imgSave.setVisibility(View.INVISIBLE);
        }else holder.imgSave.setVisibility(VISIBLE);

        if (sharedPreferenceManager.isGuestUser()) {
            holder.imgSave.setVisibility(View.GONE);
            holder.addminus.setVisibility(View.GONE);
        } else {
            holder.imgSave.setVisibility(View.VISIBLE);
            holder.addminus.setVisibility(View.GONE);
        }
        setListener(holder, model);
    }

    private void setListener(ViewHolder holder, GenericCardListModel model) {
        holder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), model);
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.contParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), model);
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.addminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), model);
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });



    }




//    public GenericCardListModel getItem(int position) {
//        return arrayList.get(position);
//    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgUser)
        ImageView imgUser;
        @BindView(R.id.txtName)
        AnyTextView txtName;
        @BindView(R.id.imgSave)
        ImageView imgSave;
        @BindView(R.id.imgInfo)
        ImageView imgInfo;
        @BindView(R.id.addminus)
        ImageView addminus;
        @BindView(R.id.imgFB)
        ImageView imgFB;
        @BindView(R.id.imgTwitter)
        ImageView imgTwitter;
        @BindView(R.id.imgLinkedin)
        ImageView imgLinkedin;
        @BindView(R.id.imgGoogle)
        ImageView imgGoogle;
        @BindView(R.id.txtEmailAddress)
        AnyTextView txtEmailAddress;
        @BindView(R.id.contParent)
        RoundKornerLinearLayout contParent;
        ;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public Filter getFilter() {

        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<GenericCardListModel> list = arrayList;

            int count = list.size();

//            final ArrayList<String> nlist = new ArrayList<String>(count);
            final ArrayList<GenericCardListModel> filterData = new ArrayList<GenericCardListModel>();

            String filterableString1;
            String filterableString2;
            String filterableString3;

            for (int i = 0; i < count; i++) {
                filterableString1 = list.get(i).getName();
                filterableString2 = list.get(i).getCompanyName();
                filterableString3 = list.get(i).getEmail();

                if (filterableString1 == null) filterableString1 = "";
                if (filterableString2 == null) filterableString2 = "";
                if (filterableString3 == null) filterableString3 = "";


                if (filterableString1.toLowerCase().contains(filterString)
                        || filterableString2.toLowerCase().contains(filterString)
                        || filterableString3.toLowerCase().contains(filterString)
                ) {
//                    nlist.add(filterableString);
                    filterData.add(list.get(i));
                }
            }

            results.values = filterData;
            results.count = filterData.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<GenericCardListModel>) results.values;
            notifyDataSetChanged();
        }

    }

    @Override
    public int getItemCount() {
        return getCount();
    }

    public int getCount() {
        if (filteredData == null) {
            return 0;
        }
        return filteredData.size();
    }

    public GenericCardListModel getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

}

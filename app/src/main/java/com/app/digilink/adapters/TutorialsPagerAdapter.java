package com.app.digilink.adapters;

import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;


import com.app.digilink.fragments.TutorialsFragment;
import com.app.digilink.models.TutorialModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class TutorialsPagerAdapter extends FragmentStatePagerAdapter {


    //    private final BaseActivity context;
    private final View.OnClickListener onClickListener;
    private final ArrayList<TutorialModel> arrayList;
    private int value;


    public TutorialsPagerAdapter(FragmentManager fm, ArrayList<TutorialModel> arrayList, View.OnClickListener onClickListener) {
        super(fm);
        this.arrayList = arrayList;
        this.onClickListener = onClickListener;


    }

    // CURRENT FRAGMENT

    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    @NotNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);

        return fragment;
    }


    @NotNull
    @Override
    public Fragment getItem(int position) {

        if (arrayList.size() == 2) {
            switch (position) {
                case 1:
                    return TutorialsFragment.newInstance(onClickListener, arrayList.get(1).getText());
//                case 2: return TutorialsFragment.newInstance(onClickListener, arrayList.get(2).getText());
                default:
                    return TutorialsFragment.newInstance(onClickListener, arrayList.get(0).getText());
            }
        } else if (arrayList.size() == 3) {

            switch (position) {
                case 1:
                    return TutorialsFragment.newInstance(onClickListener, arrayList.get(1).getText());
                case 2:
                    return TutorialsFragment.newInstance(onClickListener, arrayList.get(2).getText());
                default:
                    return TutorialsFragment.newInstance(onClickListener, arrayList.get(0).getText());
            }
        } else
            return TutorialsFragment.newInstance(onClickListener, arrayList.get(0).getText());

    }


    @Override
    public int getCount() {
        if (arrayList.size() > 0)
            return arrayList.size();
        else
            return 0;
    }
}
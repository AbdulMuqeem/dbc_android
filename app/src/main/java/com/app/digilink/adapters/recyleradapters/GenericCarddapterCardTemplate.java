package com.app.digilink.adapters.recyleradapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.digilink.R;
import com.app.digilink.activities.MainActivity;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.models.CardTemplate;
import com.app.digilink.widget.AnyTextView;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class GenericCarddapterCardTemplate extends RecyclerView.Adapter<GenericCarddapterCardTemplate.ViewHolder> {


    private final OnItemClickListener onItemClick;
    private final boolean isShowCardOpt;


    private Typeface popin;
    private Typeface ArimaMadurai;
    private Activity activity;
    private ArrayList<CardTemplate> arrayList;
    private int screenWidth;

    public GenericCarddapterCardTemplate(Activity activity, boolean isShowCardOpt, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.isShowCardOpt = isShowCardOpt;
        this.onItemClick = onItemClickListener;
        popin = Typeface.createFromAsset(activity.getAssets(), "fonts/Poppins Regular 400.ttf");
        ArimaMadurai = Typeface.createFromAsset(activity.getAssets(), "fonts/ArimaMadurai-Regular.ttf");
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.card_design2, parent, false);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenWidth = displayMetrics.widthPixels;
        return new ViewHolder(view);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final CardTemplate model = arrayList.get(holder.getAdapterPosition());
        if (arrayList.size() <= 1) {
        } else {
            try {
                int itemWidth = (int) (screenWidth / 1.1);
                ViewGroup.LayoutParams lp = holder.contAbbu.getLayoutParams();
                lp.width = itemWidth;
                holder.contAbbu.setLayoutParams(lp);
            } catch (Exception e) {
                //MyFunctions.log("ERROR (Adapter.onBindViewHolder): " + e.getMessage());
            }
        }


        if (model.getFacebook() != null && !model.getFacebook().equalsIgnoreCase("")) {
            holder.imgFB1.setVisibility(VISIBLE);
            holder.imgFB2.setVisibility(VISIBLE);
            holder.imgFB3.setVisibility(VISIBLE);
            holder.imgFB4.setVisibility(VISIBLE);
            holder.imgFB5.setVisibility(VISIBLE);
            holder.imgFB6.setVisibility(VISIBLE);
            holder.imgFB7.setVisibility(VISIBLE);
            holder.imgFB8.setVisibility(VISIBLE);
            holder.imgFB9.setVisibility(VISIBLE);
            holder.imgFB10.setVisibility(VISIBLE);
        }
        if (model.getTwitter() != null && !model.getTwitter().equalsIgnoreCase("")) {
            holder.imgTwitter1.setVisibility(VISIBLE);
            holder.imgTwitter2.setVisibility(VISIBLE);
            holder.imgTwitter3.setVisibility(VISIBLE);
            holder.imgTwitter4.setVisibility(VISIBLE);
            holder.imgTwitter5.setVisibility(VISIBLE);
            holder.imgTwitter6.setVisibility(VISIBLE);
            holder.imgTwitter7.setVisibility(VISIBLE);
            holder.imgTwitter8.setVisibility(VISIBLE);
            holder.imgTwitter9.setVisibility(VISIBLE);
            holder.imgTwitter10.setVisibility(VISIBLE);
        }
        if (model.getLinkedin() != null && !model.getLinkedin().equalsIgnoreCase("")) {
            holder.imgLinkedin1.setVisibility(VISIBLE);
            holder.imgLinkedin2.setVisibility(VISIBLE);
            holder.imgLinkedin3.setVisibility(VISIBLE);
            holder.imgLinkedin4.setVisibility(VISIBLE);
            holder.imgLinkedin5.setVisibility(VISIBLE);
            holder.imgLinkedin6.setVisibility(VISIBLE);
            holder.imgLinkedin7.setVisibility(VISIBLE);
            holder.imgLinkedin8.setVisibility(VISIBLE);
            holder.imgLinkedin9.setVisibility(VISIBLE);
            holder.imgLinkedin10.setVisibility(VISIBLE);
        }
        if (model.getGmail() != null && !model.getGmail().equalsIgnoreCase("")) {
            holder.imgGoogle1.setVisibility(VISIBLE);
            holder.imgGoogle2.setVisibility(VISIBLE);
            holder.imgGoogle3.setVisibility(VISIBLE);
            holder.imgGoogle4.setVisibility(VISIBLE);
            holder.imgGoogle5.setVisibility(VISIBLE);
            holder.imgGoogle6.setVisibility(VISIBLE);
            holder.imgGoogle7.setVisibility(VISIBLE);
            holder.imgGoogle8.setVisibility(VISIBLE);
            holder.imgGoogle9.setVisibility(VISIBLE);
            holder.imgGoogle10.setVisibility(VISIBLE);
        }
        setListener(holder, model);
        setData(holder, model);
        if (model.getCardDesign().equalsIgnoreCase("card1.html")) {
          /*  try
            {
                int itemWidth = (int) (screenWidth / 1.5);
                ViewGroup.LayoutParams lp = holder.contCD1EditCard.getLayoutParams();
                lp.width = itemWidth;
                holder.contCD1EditCard.setLayoutParams(lp);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            holder.contCD1.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD1EditCard.setVisibility(VISIBLE);
            else holder.contCD1EditCard.setVisibility(View.INVISIBLE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);

            if (model.getStatus() == 1) {
                holder.imgPower1.setChecked(true);
            } else holder.imgPower1.setChecked(false);

        } else if (model.getCardDesign().equalsIgnoreCase("card2.html")) {
            holder.contCD2.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD2EditCard.setVisibility(VISIBLE);
            else holder.contCD2EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower2.setChecked(true);
            } else holder.imgPower2.setChecked(false);

        } else if (model.getCardDesign().equalsIgnoreCase("card3.html")) {
         /*   try
            {
                int itemWidth = (int) (screenWidth / 1.5);
                ViewGroup.LayoutParams lp = holder.contCD3EditCard.getLayoutParams();
                lp.width = itemWidth;
                holder.contCD3EditCard.setLayoutParams(lp);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            holder.contCD3.setVisibility(VISIBLE);
            if (isShowCardOpt) holder.contCD3EditCard.setVisibility(VISIBLE);
            else holder.contCD3EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower3.setChecked(true);
            } else holder.imgPower3.setChecked(false);

        /*    if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin3.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        holder.imgGoogle3.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card4.html")) {
            holder.contCD4.setVisibility(VISIBLE);
            if (isShowCardOpt) holder.contCD4EditCard.setVisibility(VISIBLE);
            else holder.contCD4EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower4.setChecked(true);
            } else holder.imgPower4.setChecked(false);

/*            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin4.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("gmail")) {
                        holder.imgGoogle4.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card5.html")) {
            holder.contCD5.setVisibility(VISIBLE);
            if (isShowCardOpt) holder.contCD5EditCard.setVisibility(VISIBLE);
            else holder.contCD5EditCard.setVisibility(View.INVISIBLE);

            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower5.setChecked(true);
            } else holder.imgPower5.setChecked(false);

/*            if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin5.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("gmail")) {
                        holder.imgGoogle5.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card6.html")) {
            holder.contCD6.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD6EditCard.setVisibility(VISIBLE);
            else holder.contCD6EditCard.setVisibility(View.INVISIBLE);
            if (model.getStatus() == 1) {
                holder.imgPower6.setChecked(true);
            } else holder.imgPower6.setChecked(false);

            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
     /*       if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin6.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("gmail")) {
                        holder.imgGoogle6.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card7.html")) {
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD7EditCard.setVisibility(VISIBLE);
            else
                holder.contCD7EditCard.setVisibility(View.INVISIBLE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower7.setChecked(true);
            } else holder.imgPower7.setChecked(false);

          /*  if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin7.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        holder.imgGoogle7.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card8.html")) {
            holder.contCD8.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD8EditCard.setVisibility(VISIBLE);
            else holder.contCD8EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower8.setChecked(true);
            } else holder.imgPower8.setChecked(false);

        /*    if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin8.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        holder.imgGoogle8.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card9.html")) {
            holder.contCD9.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD9EditCard.setVisibility(VISIBLE);
            else holder.contCD9EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD10.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower9.setChecked(true);
            } else holder.imgPower9.setChecked(false);

           /* if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin9.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        holder.imgGoogle9.setVisibility(VISIBLE);
                    }
                }*/
        } else if (model.getCardDesign().equalsIgnoreCase("card10.html")) {
            holder.contCD10.setVisibility(VISIBLE);
            if (isShowCardOpt)
                holder.contCD10EditCard.setVisibility(VISIBLE);
            else holder.contCD10EditCard.setVisibility(View.INVISIBLE);
            holder.contCD1.setVisibility(GONE);
            holder.contCD2.setVisibility(GONE);
            holder.contCD3.setVisibility(GONE);
            holder.contCD4.setVisibility(GONE);
            holder.contCD5.setVisibility(GONE);
            holder.contCD6.setVisibility(GONE);
            holder.contCD7.setVisibility(GONE);
            holder.contCD8.setVisibility(GONE);
            holder.contCD9.setVisibility(GONE);
            if (model.getStatus() == 1) {
                holder.imgPower10.setChecked(true);
            } else holder.imgPower10.setChecked(false);

          /*  if (model.getArrSocialLogin() != null)
                for (int j = 0; j < model.getArrSocialLogin().size(); j++) {
                    if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("facebook")) {
                        holder.imgFB10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("twitter")) {
                        holder.imgTwitter10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("linkedin")) {
                        holder.imgLinkedin10.setVisibility(VISIBLE);
                    } else if (model.getArrSocialLogin().get(j).getName().equalsIgnoreCase("google")) {
                        holder.imgGoogle10.setVisibility(VISIBLE);
                    }
                }*/
        }

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public CardTemplate getItem(int position) {
        return arrayList.get(position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgUser1)
        ImageView imgUser1;
        @BindView(R.id.txtName1)
        AnyTextView txtName1;
        @BindView(R.id.txtOcup1)
        AnyTextView txtOcup1;
        @BindView(R.id.imgFB1)
        ImageView imgFB1;
        @BindView(R.id.imgTwitter1)
        ImageView imgTwitter1;
        @BindView(R.id.imgLinkedin1)
        ImageView imgLinkedin1;
        @BindView(R.id.imgGoogle1)
        ImageView imgGoogle1;
        @BindView(R.id.txtPhoneNum1)
        AnyTextView txtPhoneNum1;
        @BindView(R.id.txtEmailAddress1)
        AnyTextView txtEmailAddress1;
        @BindView(R.id.txtAddress1)
        AnyTextView txtAddress1;
        @BindView(R.id.txtNotes1)
        AnyTextView txtNotes1;
        @BindView(R.id.contCD1)
        RoundKornerLinearLayout contCD1;
        @BindView(R.id.imgDownload1)
        ImageView imgDownload1;
        @BindView(R.id.imgEdit1)
        ImageView imgEdit1;
        @BindView(R.id.imgDelete1)
        ImageView imgDelete1;
        @BindView(R.id.imgShare1)
        ImageView imgShare1;
        @BindView(R.id.imgPower1)
        CheckBox imgPower1;
        @BindView(R.id.contCD1EditCard)
        LinearLayout contCD1EditCard;
        @BindView(R.id.txtName2)
        AnyTextView txtName2;
        @BindView(R.id.txtOcup2)
        AnyTextView txtOcup2;
        @BindView(R.id.imgFB2)
        ImageView imgFB2;
        @BindView(R.id.imgTwitter2)
        ImageView imgTwitter2;
        @BindView(R.id.imgLinkedin2)
        ImageView imgLinkedin2;
        @BindView(R.id.imgGoogle2)
        ImageView imgGoogle2;
        @BindView(R.id.txtPhoneNum2)
        AnyTextView txtPhoneNum2;
        @BindView(R.id.txtEmailAddress2)
        AnyTextView txtEmailAddress2;
        @BindView(R.id.txtAddress2)
        AnyTextView txtAddress2;
        @BindView(R.id.imgUser2)
        ImageView imgUser2;
        @BindView(R.id.txtNotes2)
        AnyTextView txtNotes2;
        @BindView(R.id.contCD2)
        RoundKornerLinearLayout contCD2;
        @BindView(R.id.imgDownload2)
        ImageView imgDownload2;
        @BindView(R.id.imgEdit2)
        ImageView imgEdit2;
        @BindView(R.id.imgDelete2)
        ImageView imgDelete2;
        @BindView(R.id.imgShare2)
        ImageView imgShare2;
        @BindView(R.id.imgPower2)
        CheckBox imgPower2;
        @BindView(R.id.contCD2EditCard)
        LinearLayout contCD2EditCard;
        @BindView(R.id.txtName3)
        AnyTextView txtName3;
        @BindView(R.id.txtOcup3)
        AnyTextView txtOcup3;
        @BindView(R.id.imgFB3)
        ImageView imgFB3;
        @BindView(R.id.imgTwitter3)
        ImageView imgTwitter3;
        @BindView(R.id.imgLinkedin3)
        ImageView imgLinkedin3;
        @BindView(R.id.imgGoogle3)
        ImageView imgGoogle3;
        @BindView(R.id.imgUser3)
        CircleImageView imgUser3;
        @BindView(R.id.txtPhoneNum3)
        AnyTextView txtPhoneNum3;
        @BindView(R.id.txtEmailAddress3)
        AnyTextView txtEmailAddress3;
        @BindView(R.id.txtAddress3)
        AnyTextView txtAddress3;
        @BindView(R.id.txtNotes3)
        AnyTextView txtNotes3;
        @BindView(R.id.contCD3)
        RoundKornerLinearLayout contCD3;
        @BindView(R.id.imgDownload3)
        ImageView imgDownload3;
        @BindView(R.id.imgEdit3)
        ImageView imgEdit3;
        @BindView(R.id.imgDelete3)
        ImageView imgDelete3;
        @BindView(R.id.imgShare3)
        ImageView imgShare3;
        @BindView(R.id.imgPower3)
        CheckBox imgPower3;
        @BindView(R.id.contCD3EditCard)
        LinearLayout contCD3EditCard;
        @BindView(R.id.imgUser4)
        CircleImageView imgUser4;
        @BindView(R.id.txtName4)
        AnyTextView txtName4;
        @BindView(R.id.txtOcup4)
        AnyTextView txtOcup4;
        @BindView(R.id.imgFB4)
        ImageView imgFB4;
        @BindView(R.id.imgTwitter4)
        ImageView imgTwitter4;
        @BindView(R.id.imgLinkedin4)
        ImageView imgLinkedin4;
        @BindView(R.id.imgGoogle4)
        ImageView imgGoogle4;
        @BindView(R.id.txtPhoneNum4)
        AnyTextView txtPhoneNum4;
        @BindView(R.id.txtEmailAddress4)
        AnyTextView txtEmailAddress4;
        @BindView(R.id.txtAddress4)
        AnyTextView txtAddress4;
        @BindView(R.id.txtNotes4)
        AnyTextView txtNotes4;
        @BindView(R.id.contCD4)
        RoundKornerLinearLayout contCD4;
        @BindView(R.id.imgDownload4)
        ImageView imgDownload4;
        @BindView(R.id.imgEdit4)
        ImageView imgEdit4;
        @BindView(R.id.imgDelete4)
        ImageView imgDelete4;
        @BindView(R.id.imgShare4)
        ImageView imgShare4;
        @BindView(R.id.imgPower4)
        CheckBox imgPower4;
        @BindView(R.id.contCD4EditCard)
        LinearLayout contCD4EditCard;
        @BindView(R.id.imgUser5)
        ImageView imgUser5;
        @BindView(R.id.txtName5)
        AnyTextView txtName5;
        @BindView(R.id.txtOcup5)
        AnyTextView txtOcup5;
        @BindView(R.id.imgFB5)
        ImageView imgFB5;
        @BindView(R.id.imgTwitter5)
        ImageView imgTwitter5;
        @BindView(R.id.imgLinkedin5)
        ImageView imgLinkedin5;
        @BindView(R.id.imgGoogle5)
        ImageView imgGoogle5;
        @BindView(R.id.txtPhoneNum5)
        AnyTextView txtPhoneNum5;
        @BindView(R.id.txtEmailAddress5)
        AnyTextView txtEmailAddress5;
        @BindView(R.id.txtAddress5)
        AnyTextView txtAddress5;
        @BindView(R.id.txtNotes5)
        AnyTextView txtNotes5;
        @BindView(R.id.contCD5)
        RoundKornerLinearLayout contCD5;
        @BindView(R.id.imgDownload5)
        ImageView imgDownload5;
        @BindView(R.id.imgEdit5)
        ImageView imgEdit5;
        @BindView(R.id.imgDelete5)
        ImageView imgDelete5;
        @BindView(R.id.imgShare5)
        ImageView imgShare5;
        @BindView(R.id.imgPower5)
        CheckBox imgPower5;
        @BindView(R.id.contCD5EditCard)
        LinearLayout contCD5EditCard;
        @BindView(R.id.txtName6)
        AnyTextView txtName6;
        @BindView(R.id.txtOcup6)
        AnyTextView txtOcup6;
        @BindView(R.id.imgFB6)
        ImageView imgFB6;
        @BindView(R.id.imgTwitter6)
        ImageView imgTwitter6;
        @BindView(R.id.imgLinkedin6)
        ImageView imgLinkedin6;
        @BindView(R.id.imgGoogle6)
        ImageView imgGoogle6;
        @BindView(R.id.imgUser6)
        ImageView imgUser6;
        @BindView(R.id.txtPhoneNum6)
        AnyTextView txtPhoneNum6;
        @BindView(R.id.txtEmailAddress6)
        AnyTextView txtEmailAddress6;
        @BindView(R.id.txtAddress6)
        AnyTextView txtAddress6;
        @BindView(R.id.txtNotes6)
        AnyTextView txtNotes6;
        @BindView(R.id.contCD6)
        RoundKornerLinearLayout contCD6;
        @BindView(R.id.imgDownload6)
        ImageView imgDownload6;
        @BindView(R.id.imgEdit6)
        ImageView imgEdit6;
        @BindView(R.id.imgDelete6)
        ImageView imgDelete6;
        @BindView(R.id.imgShare6)
        ImageView imgShare6;
        @BindView(R.id.imgPower6)
        CheckBox imgPower6;
        @BindView(R.id.contCD6EditCard)
        LinearLayout contCD6EditCard;
        @BindView(R.id.txtName7)
        AnyTextView txtName7;
        @BindView(R.id.txtOcup7)
        AnyTextView txtOcup7;
        @BindView(R.id.imgFB7)
        ImageView imgFB7;
        @BindView(R.id.imgTwitter7)
        ImageView imgTwitter7;
        @BindView(R.id.imgLinkedin7)
        ImageView imgLinkedin7;
        @BindView(R.id.imgGoogle7)
        ImageView imgGoogle7;
        @BindView(R.id.imgUser7)
        CircleImageView imgUser7;
        @BindView(R.id.txtPhoneNum7)
        AnyTextView txtPhoneNum7;
        @BindView(R.id.txtEmailAddress7)
        AnyTextView txtEmailAddress7;
        @BindView(R.id.txtAddress7)
        AnyTextView txtAddress7;
        @BindView(R.id.txtNotes7)
        AnyTextView txtNotes7;
        @BindView(R.id.contCD7)
        RoundKornerLinearLayout contCD7;
        @BindView(R.id.imgDownload7)
        ImageView imgDownload7;
        @BindView(R.id.imgEdit7)
        ImageView imgEdit7;
        @BindView(R.id.imgDelete7)
        ImageView imgDelete7;
        @BindView(R.id.imgShare7)
        ImageView imgShare7;
        @BindView(R.id.imgPower7)
        CheckBox imgPower7;
        @BindView(R.id.contCD7EditCard)
        LinearLayout contCD7EditCard;
        @BindView(R.id.imgUser8)
        CircleImageView imgUser8;
        @BindView(R.id.txtName8)
        AnyTextView txtName8;
        @BindView(R.id.txtOcup8)
        AnyTextView txtOcup8;
        @BindView(R.id.imgFB8)
        ImageView imgFB8;
        @BindView(R.id.imgTwitter8)
        ImageView imgTwitter8;
        @BindView(R.id.imgLinkedin8)
        ImageView imgLinkedin8;
        @BindView(R.id.imgGoogle8)
        ImageView imgGoogle8;
        @BindView(R.id.txtPhoneNum8)
        AnyTextView txtPhoneNum8;
        @BindView(R.id.txtEmailAddress8)
        AnyTextView txtEmailAddress8;
        @BindView(R.id.txtAddress8)
        AnyTextView txtAddress8;
        @BindView(R.id.txtNotes8)
        AnyTextView txtNotes8;
        @BindView(R.id.contCD8)
        RoundKornerLinearLayout contCD8;
        @BindView(R.id.imgDownload8)
        ImageView imgDownload8;
        @BindView(R.id.imgEdit8)
        ImageView imgEdit8;
        @BindView(R.id.imgDelete8)
        ImageView imgDelete8;
        @BindView(R.id.imgShare8)
        ImageView imgShare8;
        @BindView(R.id.imgPower8)
        CheckBox imgPower8;
        @BindView(R.id.contCD8EditCard)
        LinearLayout contCD8EditCard;
        @BindView(R.id.txtName9)
        AnyTextView txtName9;
        @BindView(R.id.txtOcup9)
        AnyTextView txtOcup9;
        @BindView(R.id.imgFB9)
        ImageView imgFB9;
        @BindView(R.id.imgTwitter9)
        ImageView imgTwitter9;
        @BindView(R.id.imgLinkedin9)
        ImageView imgLinkedin9;
        @BindView(R.id.imgGoogle9)
        ImageView imgGoogle9;
        @BindView(R.id.imgUser9)
        ImageView imgUser9;
        @BindView(R.id.txtPhoneNum9)
        AnyTextView txtPhoneNum9;
        @BindView(R.id.txtEmailAddress9)
        AnyTextView txtEmailAddress9;
        @BindView(R.id.txtAddress9)
        AnyTextView txtAddress9;
        @BindView(R.id.txtNotes9)
        AnyTextView txtNotes9;
        @BindView(R.id.contCD9)
        RoundKornerLinearLayout contCD9;
        @BindView(R.id.imgDownload9)
        ImageView imgDownload9;
        @BindView(R.id.imgEdit9)
        ImageView imgEdit9;
        @BindView(R.id.imgDelete9)
        ImageView imgDelete9;
        @BindView(R.id.imgShare9)
        ImageView imgShare9;
        @BindView(R.id.imgPower9)
        CheckBox imgPower9;
        @BindView(R.id.contCD9EditCard)
        LinearLayout contCD9EditCard;
        @BindView(R.id.txtName10)
        AnyTextView txtName10;
        @BindView(R.id.txtOcup10)
        AnyTextView txtOcup10;
        @BindView(R.id.imgFB10)
        ImageView imgFB10;
        @BindView(R.id.imgTwitter10)
        ImageView imgTwitter10;
        @BindView(R.id.imgLinkedin10)
        ImageView imgLinkedin10;
        @BindView(R.id.imgGoogle10)
        ImageView imgGoogle10;
        @BindView(R.id.imgUser10)
        ImageView imgUser10;
        @BindView(R.id.txtPhoneNum10)
        AnyTextView txtPhoneNum10;
        @BindView(R.id.txtEmailAddress10)
        AnyTextView txtEmailAddress10;
        @BindView(R.id.txtAddress10)
        AnyTextView txtAddress10;
        @BindView(R.id.txtNotes10)
        AnyTextView txtNotes10;
        @BindView(R.id.contCD10)
        RoundKornerLinearLayout contCD10;
        @BindView(R.id.imgDownload10)
        ImageView imgDownload10;
        @BindView(R.id.imgEdit10)
        ImageView imgEdit10;
        @BindView(R.id.imgDelete10)
        ImageView imgDelete10;
        @BindView(R.id.imgShare10)
        ImageView imgShare10;
        @BindView(R.id.imgPower10)
        CheckBox imgPower10;
        @BindView(R.id.contCD10EditCard)
        LinearLayout contCD10EditCard;
        @BindView(R.id.contAbbu)
        LinearLayout contAbbu;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    private void setListener(ViewHolder holder, CardTemplate model) {
        holder.imgDelete1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDelete10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgDownload1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgDownload10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgEdit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower1, model);
            }
        });
        holder.imgPower2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower2, model);
            }
        });
        holder.imgPower3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower3, model);
            }
        });
        holder.imgPower4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower4, model);
            }
        });
        holder.imgPower5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower5, model);
            }
        });
        holder.imgPower6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower6, model);
            }
        });
        holder.imgPower7.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower7, model);
            }
        });
        holder.imgPower8.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower8, model);
            }
        });
        holder.imgPower9.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower9, model);
            }
        });
        holder.imgPower10.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    model.setStatus(1);
                else model.setStatus(2);

//                onItemClick.onItemClick(holder.getAdapterPosition(), holder.imgPower10, model);
            }
        });
        holder.imgPower1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgPower10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgShare1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgShare10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgFB1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgFB10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgGoogle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgGoogle10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgTwitter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgTwitter10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });

        holder.imgLinkedin1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgLinkedin10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
    }

    private void setData(ViewHolder holder, CardTemplate cardTemplate) {
        /*
         *
         * Card 1*/
        holder.txtAddress1.setText(cardTemplate.getAddress());
        holder.txtEmailAddress1.setText(cardTemplate.getEmail());
        holder.txtName1.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup1.setText(cardTemplate.getOccupation());
        holder.txtNotes1.setText(cardTemplate.getNotes());
        holder.txtPhoneNum1.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress1, holder.txtEmailAddress1, holder.txtPhoneNum1);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress1.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress1.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName1.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup1.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes1.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum1.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser1);
        /*
         *
         *card no 2
         *
         * */
        holder.txtAddress2.setText(cardTemplate.getAddress());
        holder.txtEmailAddress2.setText(cardTemplate.getEmail());
        holder.txtName2.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup2.setText(cardTemplate.getOccupation());
        holder.txtNotes2.setText(cardTemplate.getNotes());
        holder.txtPhoneNum2.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress2, holder.txtEmailAddress2, holder.txtPhoneNum2);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress2.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress2.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName2.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup2.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes2.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum2.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser2);

//        /*
//        card 3
//
//        */

        holder.txtName3.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup3.setText(cardTemplate.getOccupation());
        holder.txtNotes3.setText(cardTemplate.getNotes());
        holder.txtAddress3.setText(cardTemplate.getAddress());
        holder.txtEmailAddress3.setText(cardTemplate.getEmail());
        holder.txtPhoneNum3.setText(cardTemplate.getPhone());
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress3.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress3.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName3.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup3.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes3.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum3.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser3);
        setOrders(cardTemplate, holder.txtAddress3, holder.txtEmailAddress3, holder.txtPhoneNum3);
        /*
         *
         * card 4
         *
         * */
        holder.txtAddress4.setText(cardTemplate.getAddress());
        holder.txtEmailAddress4.setText(cardTemplate.getEmail());
        holder.txtName4.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup4.setText(cardTemplate.getOccupation());
        holder.txtNotes4.setText(cardTemplate.getNotes());
        holder.txtPhoneNum4.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress4, holder.txtEmailAddress4, holder.txtPhoneNum4);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress4.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress4.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName4.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup4.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes4.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum4.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser4);

//        /*
//        card 5
//        */
        holder.txtAddress5.setText(cardTemplate.getAddress());
        holder.txtEmailAddress5.setText(cardTemplate.getEmail());
        holder.txtName5.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup5.setText(cardTemplate.getOccupation());
        holder.txtNotes5.setText(cardTemplate.getNotes());
        holder.txtPhoneNum5.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress5, holder.txtEmailAddress5, holder.txtPhoneNum5);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress5.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress5.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName5.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup5.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes5.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum5.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser5);

/*///
card no 6
*/
        holder.txtAddress6.setText(cardTemplate.getAddress());
        holder.txtEmailAddress6.setText(cardTemplate.getEmail());
        holder.txtName6.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup6.setText(cardTemplate.getOccupation());
        holder.txtNotes6.setText(cardTemplate.getNotes());
        holder.txtPhoneNum6.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress6, holder.txtEmailAddress6, holder.txtPhoneNum6);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress6.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress6.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName6.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup6.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes6.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum6.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser6);






        /*///
card no 8
*/
        holder.txtAddress8.setText(cardTemplate.getAddress());
        holder.txtEmailAddress8.setText(cardTemplate.getEmail());
        holder.txtName8.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup8.setText(cardTemplate.getOccupation());
        holder.txtNotes8.setText(cardTemplate.getNotes());
        holder.txtPhoneNum8.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress8, holder.txtEmailAddress8, holder.txtPhoneNum8);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress8.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress8.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName8.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup8.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes8.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum8.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser8);




        /*///
card no 7
*/
        holder.txtAddress7.setText(cardTemplate.getAddress());
        holder.txtEmailAddress7.setText(cardTemplate.getEmail());
        holder.txtName7.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup7.setText(cardTemplate.getOccupation());
        holder.txtNotes7.setText(cardTemplate.getNotes());
        holder.txtPhoneNum7.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress7, holder.txtEmailAddress7, holder.txtPhoneNum7);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress7.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress7.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName7.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup7.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes7.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum7.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser7);



        /*///
card no 9
*/
        holder.txtAddress9.setText(cardTemplate.getAddress());
        holder.txtEmailAddress9.setText(cardTemplate.getEmail());
        holder.txtName9.setText(cardTemplate.getFirst_name() + cardTemplate.getLast_name());
        holder.txtOcup9.setText(cardTemplate.getOccupation());
        holder.txtNotes9.setText(cardTemplate.getNotes());
        holder.txtPhoneNum9.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress9, holder.txtEmailAddress9, holder.txtPhoneNum9);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress9.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress9.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName9.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup9.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes9.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum9.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser9);

        /*///
card no 10
*/
        holder.txtAddress10.setText(cardTemplate.getAddress());
        holder.txtEmailAddress10.setText(cardTemplate.getEmail());
        holder.txtName10.setText(cardTemplate.getFirst_name() + " " + cardTemplate.getLast_name());
        holder.txtOcup10.setText(cardTemplate.getOccupation());
        holder.txtNotes10.setText(cardTemplate.getNotes());
        holder.txtPhoneNum10.setText(cardTemplate.getPhone());
        setOrders(cardTemplate, holder.txtAddress10, holder.txtEmailAddress10, holder.txtPhoneNum10);
        if (!cardTemplate.getAddressColor().equalsIgnoreCase(""))
            holder.txtAddress10.setTextColor(Color.parseColor(cardTemplate.getAddressColor()));
        if (!cardTemplate.getEmailColor().equalsIgnoreCase(""))
            holder.txtEmailAddress10.setTextColor(Color.parseColor(cardTemplate.getEmailColor()));
        if (!cardTemplate.getNameColor().equalsIgnoreCase(""))
            holder.txtName10.setTextColor(Color.parseColor(cardTemplate.getNameColor()));
        if (!cardTemplate.getOccupationColor().equalsIgnoreCase(""))
            holder.txtOcup10.setTextColor(Color.parseColor(cardTemplate.getOccupationColor()));
        if (!cardTemplate.getNotesColor().equalsIgnoreCase(""))
            holder.txtNotes10.setTextColor(Color.parseColor(cardTemplate.getNotesColor()));
        if (!cardTemplate.getPhoneColor().equalsIgnoreCase(""))
            holder.txtPhoneNum10.setTextColor(Color.parseColor(cardTemplate.getPhoneColor()));
        if (cardTemplate.getImage() != null)
            ImageLoader.getInstance().displayImage(cardTemplate.getImage(), holder.imgUser10);

        /*///*/
        if (cardTemplate.getPhone_font() != null && cardTemplate.getPhone_font().equalsIgnoreCase("Arima Madurai")) {
            holder.txtPhoneNum1.setTypeface(ArimaMadurai);
            holder.txtPhoneNum2.setTypeface(ArimaMadurai);
            holder.txtPhoneNum3.setTypeface(ArimaMadurai);
            holder.txtPhoneNum4.setTypeface(ArimaMadurai);
            holder.txtPhoneNum5.setTypeface(ArimaMadurai);
            holder.txtPhoneNum6.setTypeface(ArimaMadurai);
            holder.txtPhoneNum7.setTypeface(ArimaMadurai);
            holder.txtPhoneNum8.setTypeface(ArimaMadurai);
            holder.txtPhoneNum9.setTypeface(ArimaMadurai);
            holder.txtPhoneNum10.setTypeface(ArimaMadurai);
            holder.txtAddress1.setTypeface(ArimaMadurai);
            holder.txtAddress2.setTypeface(ArimaMadurai);
            holder.txtAddress3.setTypeface(ArimaMadurai);
            holder.txtAddress4.setTypeface(ArimaMadurai);
            holder.txtAddress5.setTypeface(ArimaMadurai);
            holder.txtAddress6.setTypeface(ArimaMadurai);
            holder.txtAddress7.setTypeface(ArimaMadurai);
            holder.txtAddress8.setTypeface(ArimaMadurai);
            holder.txtAddress9.setTypeface(ArimaMadurai);
            holder.txtAddress10.setTypeface(ArimaMadurai);
            holder.txtEmailAddress1.setTypeface(ArimaMadurai);
            holder.txtEmailAddress2.setTypeface(ArimaMadurai);
            holder.txtEmailAddress3.setTypeface(ArimaMadurai);
            holder.txtEmailAddress4.setTypeface(ArimaMadurai);
            holder.txtEmailAddress5.setTypeface(ArimaMadurai);
            holder.txtEmailAddress6.setTypeface(ArimaMadurai);
            holder.txtEmailAddress7.setTypeface(ArimaMadurai);
            holder.txtEmailAddress8.setTypeface(ArimaMadurai);
            holder.txtEmailAddress9.setTypeface(ArimaMadurai);
            holder.txtEmailAddress10.setTypeface(ArimaMadurai);

        } else {
            holder.txtPhoneNum1.setTypeface(popin);
            holder.txtPhoneNum2.setTypeface(popin);
            holder.txtPhoneNum3.setTypeface(popin);
            holder.txtPhoneNum4.setTypeface(popin);
            holder.txtPhoneNum5.setTypeface(popin);
            holder.txtPhoneNum6.setTypeface(popin);
            holder.txtPhoneNum7.setTypeface(popin);
            holder.txtPhoneNum8.setTypeface(popin);
            holder.txtPhoneNum9.setTypeface(popin);
            holder.txtPhoneNum10.setTypeface(popin);
            holder.txtAddress1.setTypeface(popin);
            holder.txtAddress2.setTypeface(popin);
            holder.txtAddress3.setTypeface(popin);
            holder.txtAddress4.setTypeface(popin);
            holder.txtAddress5.setTypeface(popin);
            holder.txtAddress6.setTypeface(popin);
            holder.txtAddress7.setTypeface(popin);
            holder.txtAddress8.setTypeface(popin);
            holder.txtAddress9.setTypeface(popin);
            holder.txtAddress10.setTypeface(popin);
            holder.txtEmailAddress1.setTypeface(popin);
            holder.txtEmailAddress2.setTypeface(popin);
            holder.txtEmailAddress3.setTypeface(popin);
            holder.txtEmailAddress4.setTypeface(popin);
            holder.txtEmailAddress5.setTypeface(popin);
            holder.txtEmailAddress6.setTypeface(popin);
            holder.txtEmailAddress7.setTypeface(popin);
            holder.txtEmailAddress8.setTypeface(popin);
            holder.txtEmailAddress9.setTypeface(popin);
            holder.txtEmailAddress10.setTypeface(popin);

        }


        if (cardTemplate.getOccupation_font() != null && cardTemplate.getOccupation_font().equalsIgnoreCase("Arima Madurai")) {


            holder.txtOcup1.setTypeface(ArimaMadurai);
            holder.txtOcup2.setTypeface(ArimaMadurai);
            holder.txtOcup3.setTypeface(ArimaMadurai);
            holder.txtOcup4.setTypeface(ArimaMadurai);
            holder.txtOcup5.setTypeface(ArimaMadurai);
            holder.txtOcup6.setTypeface(ArimaMadurai);
            holder.txtOcup7.setTypeface(ArimaMadurai);
            holder.txtOcup8.setTypeface(ArimaMadurai);
            holder.txtOcup9.setTypeface(ArimaMadurai);
            holder.txtOcup10.setTypeface(ArimaMadurai);

            holder.txtName1.setTypeface(ArimaMadurai);
            holder.txtName2.setTypeface(ArimaMadurai);
            holder.txtName3.setTypeface(ArimaMadurai);
            holder.txtName4.setTypeface(ArimaMadurai);
            holder.txtName5.setTypeface(ArimaMadurai);
            holder.txtName6.setTypeface(ArimaMadurai);
            holder.txtName7.setTypeface(ArimaMadurai);
            holder.txtName8.setTypeface(ArimaMadurai);
            holder.txtName9.setTypeface(ArimaMadurai);
            holder.txtName10.setTypeface(ArimaMadurai);


        } else {
            holder.txtOcup1.setTypeface(popin);
            holder.txtOcup2.setTypeface(popin);
            holder.txtOcup3.setTypeface(popin);
            holder.txtOcup4.setTypeface(popin);
            holder.txtOcup5.setTypeface(popin);
            holder.txtOcup6.setTypeface(popin);
            holder.txtOcup7.setTypeface(popin);
            holder.txtOcup8.setTypeface(popin);
            holder.txtOcup9.setTypeface(popin);
            holder.txtOcup10.setTypeface(popin);

            holder.txtName1.setTypeface(popin);
            holder.txtName2.setTypeface(popin);
            holder.txtName3.setTypeface(popin);
            holder.txtName4.setTypeface(popin);
            holder.txtName5.setTypeface(popin);
            holder.txtName6.setTypeface(popin);
            holder.txtName7.setTypeface(popin);
            holder.txtName8.setTypeface(popin);
            holder.txtName9.setTypeface(popin);
            holder.txtName10.setTypeface(popin);
        }


        if (cardTemplate.getNotes_font() != null && cardTemplate.getNotes_font().equalsIgnoreCase("Arima Madurai")) {

            holder.txtNotes1.setTypeface(ArimaMadurai);
            holder.txtNotes2.setTypeface(ArimaMadurai);
            holder.txtNotes3.setTypeface(ArimaMadurai);
            holder.txtNotes4.setTypeface(ArimaMadurai);
            holder.txtNotes5.setTypeface(ArimaMadurai);
            holder.txtNotes6.setTypeface(ArimaMadurai);
            holder.txtNotes7.setTypeface(ArimaMadurai);
            holder.txtNotes8.setTypeface(ArimaMadurai);
            holder.txtNotes9.setTypeface(ArimaMadurai);
            holder.txtNotes10.setTypeface(ArimaMadurai);
        } else {
            holder.txtNotes1.setTypeface(popin);
            holder.txtNotes2.setTypeface(popin);
            holder.txtNotes3.setTypeface(popin);
            holder.txtNotes4.setTypeface(popin);
            holder.txtNotes5.setTypeface(popin);
            holder.txtNotes6.setTypeface(popin);
            holder.txtNotes7.setTypeface(popin);
            holder.txtNotes8.setTypeface(popin);
            holder.txtNotes9.setTypeface(popin);
            holder.txtNotes10.setTypeface(popin);

        }

        /*
         *
         * */
        if (cardTemplate.getPhone_size() != null) {
            holder.txtPhoneNum1.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum2.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum3.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum4.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum5.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum6.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum7.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum8.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum9.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
            holder.txtPhoneNum10.setTextSize(Float.parseFloat(cardTemplate.getPhone_size()));
        }
        if (cardTemplate.getAddress_size() != null) {
            holder.txtAddress1.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress2.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress3.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress4.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress5.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress6.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress7.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress8.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress9.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
            holder.txtAddress10.setTextSize(Float.parseFloat(cardTemplate.getAddress_size()));
        }

        if (cardTemplate.getEmail_size() != null) {
            holder.txtEmailAddress1.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress2.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress3.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress4.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress5.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress6.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress7.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress8.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress9.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
            holder.txtEmailAddress10.setTextSize(Float.parseFloat(cardTemplate.getEmail_size()));
        }

        if (cardTemplate.getOccupation_size() != null) {

            holder.txtOcup1.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup2.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup3.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup4.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup5.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup6.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup7.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup8.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup9.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
            holder.txtOcup10.setTextSize(Float.parseFloat(cardTemplate.getOccupation_size()));
        }

        if (cardTemplate.getName_size() != null) {
            holder.txtName1.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName2.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName3.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName4.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName5.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName6.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName7.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName8.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName9.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
            holder.txtName10.setTextSize(Float.parseFloat(cardTemplate.getName_size()));
        }

        if (cardTemplate.getNotes_size() != null) {
        holder.txtNotes1.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes2.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes3.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes4.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes5.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes6.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes7.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes8.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes9.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));
        holder.txtNotes10.setTextSize(Float.parseFloat(cardTemplate.getNotes_size()));}
    }

    private void setOrders(CardTemplate cardTemplate, AnyTextView txtAddress, AnyTextView txtEmailAddress, AnyTextView txtPhoneNum) {
        if (cardTemplate.getCardOrder() != null) {
            if (cardTemplate.getCardOrder().equalsIgnoreCase("1,2,3")) {
                // phone/ email / address
                txtPhoneNum.setText(cardTemplate.getPhone());
                txtEmailAddress.setText(cardTemplate.getEmail());
                txtAddress.setText(cardTemplate.getAddress());


            } else if (cardTemplate.getCardOrder().equalsIgnoreCase("2,1,3")) {
                // 1 phone/ 2 email / 3 address
                txtPhoneNum.setText(cardTemplate.getEmail());
                txtEmailAddress.setText(cardTemplate.getPhone());
                txtAddress.setText(cardTemplate.getAddress());

            } else if (cardTemplate.getCardOrder().equalsIgnoreCase("2,3,1")) {

                txtPhoneNum.setText(cardTemplate.getEmail());
                txtEmailAddress.setText(cardTemplate.getAddress());
                txtAddress.setText(cardTemplate.getPhone());

            } else if (cardTemplate.getCardOrder().equalsIgnoreCase("1,3,2")) {

                txtPhoneNum.setText(cardTemplate.getPhone());
                txtEmailAddress.setText(cardTemplate.getAddress());
                txtAddress.setText(cardTemplate.getEmail());

            } else if (cardTemplate.getCardOrder().equalsIgnoreCase("3,2,1")) {

                txtPhoneNum.setText(cardTemplate.getAddress());
                txtEmailAddress.setText(cardTemplate.getEmail());
                txtAddress.setText(cardTemplate.getPhone());

            } else if (cardTemplate.getCardOrder().equalsIgnoreCase("3,1,2")) {

                txtPhoneNum.setText(cardTemplate.getAddress());
                txtEmailAddress.setText(cardTemplate.getPhone());
                txtAddress.setText(cardTemplate.getEmail());

            }

        }

    }

}

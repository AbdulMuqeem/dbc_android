package com.app.digilink.adapters.recyleradapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.rtugeek.android.colorseekbar.ColorSeekBar;
import com.app.digilink.models.ColorModel;
import com.app.digilink.models.User.EditUserModel;
import com.app.digilink.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class EditColorFieldAdapter extends RecyclerView.Adapter<EditColorFieldAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;


    private Activity activity;
    private ArrayList<ColorModel> arrayList;
    public EditUserModel editUserModelSending = new EditUserModel();
    private String colorValue;

    public EditColorFieldAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;
        colorValue = "FFFFFF";

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_edit_color_fields, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ColorModel model = arrayList.get(holder.getAdapterPosition());
        holder.txtColorTitle.setText(model.getType());
        holder.txtColorTitleOpened.setText(model.getType());

        setListener(holder, model);

    }

    private void setListener(ViewHolder holder, ColorModel model) {


        holder.imgEditColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.colorSlider.setVisibility(View.VISIBLE);
                holder.contOpened.setVisibility(View.VISIBLE);
                holder.contClosed.setVisibility(View.GONE);


                if (model.getType().equalsIgnoreCase("COLOR 2 - INFO")) {
                    holder.colorSlider.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
                        @Override
                        public void onColorChangeListener(int colorBarPosition, int alphaBarPosition, int color) {
                            colorValue = Integer.toHexString(color).substring(2);
                            editUserModelSending.setPhone_color("#" + colorValue);
                            editUserModelSending.setAddress_color("#" + colorValue);
                            editUserModelSending.setEmail_color("#" + colorValue);
                        }
                    });

                } else if (model.getType().equalsIgnoreCase("COLOR 1 - TITLE")) {
                    holder.colorSlider.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
                        @Override
                        public void onColorChangeListener(int colorBarPosition, int alphaBarPosition, int color) {
                            colorValue = Integer.toHexString(color).substring(2);
                            editUserModelSending.setOccupation_color("#" + colorValue);
                            editUserModelSending.setName_color("#" + colorValue);
                        }
                    });

                } else if (model.getType().equalsIgnoreCase("COLOR 3 - BANNER")) {

                    holder.colorSlider.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
                        @Override
                        public void onColorChangeListener(int colorBarPosition, int alphaBarPosition, int color) {
                            colorValue = Integer.toHexString(color).substring(2);
                            editUserModelSending.setNotes_color("#" + colorValue);
                        }
                    });
                }

            }
        });
        holder.imgTick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
                holder.contOpened.setVisibility(View.GONE);
                holder.contClosed.setVisibility(View.VISIBLE);
                holder.colorSlider.setVisibility(View.GONE);
            }
        });
        holder.imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
                holder.contOpened.setVisibility(View.GONE);
                holder.contClosed.setVisibility(View.VISIBLE);
                holder.colorSlider.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public ColorModel getItem(int position) {
        return arrayList.get(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtColorTitleOpened)
        AnyTextView txtColorTitleOpened;
        @BindView(R.id.imgTickC)
        ImageView imgTick;
        @BindView(R.id.imgcross)
        ImageView imgcross;
        @BindView(R.id.contOpened)
        LinearLayout contOpened;
        @BindView(R.id.txtColorTitle)
        AnyTextView txtColorTitle;
        @BindView(R.id.imgEditColor)
        ImageView imgEditColor;
        @BindView(R.id.contClosed)
        LinearLayout contClosed;
        @BindView(R.id.colorSlider)
        ColorSeekBar colorSlider;
        @BindView(R.id.contParentColor)
        LinearLayout contParentColor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}

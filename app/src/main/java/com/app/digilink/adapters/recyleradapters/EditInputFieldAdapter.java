package com.app.digilink.adapters.recyleradapters;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.google.gson.reflect.TypeToken;
import com.pakdev.homemenu.ItemTouchHelperCallback;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.models.InputModel;
import com.app.digilink.models.User.EditUserModel;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.widget.AnyEditTextView;
import com.app.digilink.widget.AnyTextView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class EditInputFieldAdapter extends RecyclerView.Adapter<EditInputFieldAdapter.ViewHolder> implements ItemTouchHelperCallback {


    private final OnItemClickListener onItemClick;
    private final UserModel currentUser;


    private Activity activity;
    private ArrayList<InputModel> arrayList;
    public EditUserModel editUserModelSending = new EditUserModel();

    public EditInputFieldAdapter(UserModel currentUser, Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.currentUser = currentUser;
        this.onItemClick = onItemClickListener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_edit_input_fields, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final InputModel model = arrayList.get(holder.getAdapterPosition());
        holder.txtTitle.setText(model.getName());
        holder.txtTitleOpend.setText(model.getName());
        holder.edtValue.setVisibility(View.GONE);
        setData(holder, model);

        holder.edtValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (model.getName().equalsIgnoreCase("Phone")) {
                    editUserModelSending.setPhone(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                } else if (model.getName().equalsIgnoreCase("address")) {
                    editUserModelSending.setAddress(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                } else if (model.getName().equalsIgnoreCase("occupation")) {
                    editUserModelSending.setOccupation(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                } else if (model.getName().equalsIgnoreCase("name")) {
                    editUserModelSending.setLast_name(charSequence.toString());
                    editUserModelSending.setFirst_name(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                } else if (model.getName().equalsIgnoreCase("email")) {
                    editUserModelSending.setEmail(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                }  else if (model.getName().equalsIgnoreCase("notes")) {
                    editUserModelSending.setNotes(charSequence.toString());
                    holder.txtTagSocialHandler.setVisibility(View.GONE);
                } else if (model.getName().equalsIgnoreCase("social icon")) {
                    holder.txtTagSocialHandler.setVisibility(View.VISIBLE);

                    editUserModelSending.setFacebook(charSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        setListener(holder, model);

    }


    private void setData(@NonNull ViewHolder holder, InputModel model) {
        if (model.getName().equalsIgnoreCase("Phone")) {
            holder.edtValue.setText(currentUser.getPhone());
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            editUserModelSending.setPhone(currentUser.getPhone());
            holder.imgMove.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);

        } else if (model.getName().equalsIgnoreCase("address")) {
            holder.imgMove.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.edtValue.setText(currentUser.getAddress());
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            editUserModelSending.setAddress(currentUser.getAddress());
        } else if (model.getName().equalsIgnoreCase("notes")) {
            holder.imgMove.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.edtValue.setText(currentUser.getNotes());
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            editUserModelSending.setNotes(currentUser.getNotes());
        }
        else if (model.getName().equalsIgnoreCase("occupation")) {
            holder.edtValue.setText(currentUser.getOccupation());
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            holder.imgEdit.setVisibility(View.VISIBLE);
            editUserModelSending.setOccupation(currentUser.getOccupation());
        } else if (model.getName().equalsIgnoreCase("name")) {
            holder.edtValue.setText(currentUser.getName());
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            editUserModelSending.setName(currentUser.getName());
        } else if (model.getName().equalsIgnoreCase("email")) {
            holder.imgMove.setVisibility(View.VISIBLE);
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.txtTagSocialHandler.setVisibility(View.GONE);
            holder.edtValue.setText(currentUser.getEmail());
            editUserModelSending.setEmail(currentUser.getEmail());
        } else if (model.getName().equalsIgnoreCase("social icon")) {
            holder.imgEdit.setVisibility(View.VISIBLE);
            holder.txtTagSocialHandler.setVisibility(View.VISIBLE);
            if (currentUser.getArrSocialLogin() != null && currentUser.getArrSocialLogin().size() > 0) {
                holder.edtValue.setText(currentUser.getArrSocialLogin().get(0).getLink());
                editUserModelSending.setFacebook(currentUser.getArrSocialLogin().get(0).getLink());
                editUserModelSending.setSocial_icon(currentUser.getArrSocialLogin().get(0).getLink());
            }
        }
    }

    private void setListener(ViewHolder holder, InputModel model) {
        holder.imgDelete.setVisibility(View.GONE);
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UIHelper.showToast(activity, "will be implemented");
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgMove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
                holder.edtValue.setVisibility(View.VISIBLE);
                holder.contOpened.setVisibility(View.VISIBLE);
                holder.contOpenedEditVale.setVisibility(View.VISIBLE);
                holder.contEdit.setVisibility(View.GONE);
            }
        });
        holder.imgTick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
                holder.contOpened.setVisibility(View.GONE);
                holder.edtValue.setVisibility(View.GONE);
                holder.contOpenedEditVale.setVisibility(View.GONE);
                holder.contEdit.setVisibility(View.VISIBLE);
            }
        });
        holder.imgcross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
                holder.contOpened.setVisibility(View.GONE);
                holder.edtValue.setVisibility(View.GONE);
                holder.contOpenedEditVale.setVisibility(View.GONE);

                holder.contEdit.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public InputModel getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (toPosition == 4 || toPosition == 5 || toPosition == 6 ||
                fromPosition == 4 || fromPosition == 5 || fromPosition == 6) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                   /* if (arrayList.get(i).getName().equalsIgnoreCase("name") ||
                            arrayList.get(i).getName().equalsIgnoreCase("social icon") ||
                            arrayList.get(i).getName().equalsIgnoreCase("occupation")) {
                        return;
                    }*/
                    Collections.swap(arrayList, i, i + 1);
                    notifyDataSetChanged();
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                  /*  if (arrayList.get(i).getName().equalsIgnoreCase("name") ||
                            arrayList.get(i).getName().equalsIgnoreCase("social icon") ||
                            arrayList.get(i).getName().equalsIgnoreCase("occupation")) {
                        return;
                    }*/
                    Collections.swap(arrayList, i, i - 1);
//                    swapdata(,arrayList);
                    notifyDataSetChanged();
                }
            }
//setData();
            if (arrayList.get(3).getName().equalsIgnoreCase("phone") &&
                    arrayList.get(4).getName().equalsIgnoreCase("email") &&
                    arrayList.get(5).getName().equalsIgnoreCase("address")) {
                editUserModelSending.setCard_order("1,2,3");
            } else if (arrayList.get(3).getName().equalsIgnoreCase("email") &&
                    arrayList.get(4).getName().equalsIgnoreCase("phone") &&
                    arrayList.get(5).getName().equalsIgnoreCase("address")) {
                editUserModelSending.setCard_order("2,1,3");
            } else if (arrayList.get(3).getName().equalsIgnoreCase("email") &&
                    arrayList.get(4).getName().equalsIgnoreCase("address") &&
                    arrayList.get(5).getName().equalsIgnoreCase("phone")) {
                editUserModelSending.setCard_order("2,3,1");
            } else if (arrayList.get(3).getName().equalsIgnoreCase("phone") &&
                    arrayList.get(4).getName().equalsIgnoreCase("address") &&
                    arrayList.get(5).getName().equalsIgnoreCase("email")) {
                editUserModelSending.setCard_order("1,3,2");
            } else if (arrayList.get(3).getName().equalsIgnoreCase("address") &&
                    arrayList.get(4).getName().equalsIgnoreCase("email") &&
                    arrayList.get(5).getName().equalsIgnoreCase("phone")) {
                editUserModelSending.setCard_order("3,2,1");
            } else if (arrayList.get(3).getName().equalsIgnoreCase("address") &&
                    arrayList.get(4).getName().equalsIgnoreCase("phone") &&
                    arrayList.get(5).getName().equalsIgnoreCase("email")) {
                editUserModelSending.setCard_order("3,1,2");
            }

            notifyItemMoved(fromPosition, toPosition);
        }
    }


    @Override
    public void onItemDismiss(int position) {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        AnyTextView txtTitle;
        @BindView(R.id.imgMove)
        ImageView imgMove;
        @BindView(R.id.imgEdit)
        ImageView imgEdit;
        @BindView(R.id.imgDelete)
        ImageView imgDelete;
        @BindView(R.id.contEdit)
        LinearLayout contEdit;
        @BindView(R.id.txtTitleOpend)
        AnyTextView txtTitleOpend;
        @BindView(R.id.imgTick)
        ImageView imgTick;
        @BindView(R.id.imgcross)
        ImageView imgcross;
        @BindView(R.id.imgDeleteOpened)
        ImageView imgDeleteOpened;
        @BindView(R.id.contOpened)
        LinearLayout contOpened;
        @BindView(R.id.txtTagSocialHandler)
        AnyTextView txtTagSocialHandler;
        @BindView(R.id.edtValue)
        AnyEditTextView edtValue;
        @BindView(R.id.contOpenedEditVale)
        LinearLayout contOpenedEditVale;
        @BindView(R.id.contParent)
        LinearLayout contParent;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}

package com.app.digilink.adapters.recyleradapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.jcminarro.roundkornerlayout.RoundKornerLinearLayout;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.app.digilink.models.CardTemplate;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class TemplateCardImagesAdapter extends RecyclerView.Adapter<TemplateCardImagesAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;
    private final boolean isShowCardOpt;


    private Activity activity;
    private ArrayList<CardTemplate> arrayList;

    public TemplateCardImagesAdapter(Activity activity, boolean isShowCardOpt, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.isShowCardOpt = isShowCardOpt;
        this.onItemClick = onItemClickListener;

    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_template_images, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final CardTemplate model = arrayList.get(holder.getAdapterPosition());

        holder.imgTemplate.setVisibility(View.VISIBLE);
        if (model.getCardImg() != null)
            ImageLoader.getInstance().displayImage(model.getCardImg(), holder.imgTemplate);
        holder.rkSelectedColor.setBackgroundColor(activity.getResources().getColor(R.color.transparent));
        setListener(holder, model);

        if (model.isSelected()) {
            holder.rkSelectedColor.setBackground(activity.getResources().getDrawable(R.drawable.rounded_box_holo));

        } else {
            holder.rkSelectedColor.setBackgroundColor(activity.getResources().getColor(R.color.transparent));

        }

    }

    private void setListener(ViewHolder holder, CardTemplate model) {
        holder.imgTemplate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.rkSelectedColor.setBackground(activity.getResources().getDrawable(R.drawable.rounded_box_holo));
                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public CardTemplate getItem(int position) {
        return arrayList.get(position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgTemplate)
        ImageView imgTemplate;
        @BindView(R.id.rkSelectedColor)
        RoundKornerLinearLayout rkSelectedColor;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}

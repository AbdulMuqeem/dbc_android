package com.app.digilink.adapters.recyleradapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;


import com.app.digilink.R;
import com.app.digilink.callbacks.OnItemClickListener;
import com.app.digilink.models.FontModel;
import com.app.digilink.models.User.EditUserModel;
import com.app.digilink.widget.AnyRadioButton;
import com.app.digilink.widget.AnyTextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;


public class EditFontFieldAdapter extends RecyclerView.Adapter<EditFontFieldAdapter.ViewHolder> {


    private final OnItemClickListener onItemClick;
    private final ArrayList<String> arrType;
    String spSize;

    private Activity activity;
    private ArrayList<FontModel> arrayList;

    public EditFontFieldAdapter(Activity activity, ArrayList arrayList, OnItemClickListener onItemClickListener) {
        this.activity = activity;
        this.arrayList = arrayList;
        this.onItemClick = onItemClickListener;
        arrType = new ArrayList<>();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(activity)
                .inflate(R.layout.item_edit_font, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final FontModel model = arrayList.get(holder.getAdapterPosition());


        holder.txtTitle.setText(model.getValue());
        holder.txtTitleOpend.setText(model.getValue());
        holder.contOpened.setVisibility(View.GONE);
        setListener(holder, model);

    }

    private void setListener(ViewHolder holder, FontModel model) {

        if (model.getValue().equalsIgnoreCase("FONT 2 - INFO")) {
            holder.radiogroup.setOnCheckedChangeListener((group, checkedId) -> {
                switch (checkedId) {
                    case R.id.font_regular_ArimaMadurai:
                        editUserModelSending.setFont("Arima Madurai");
                        editUserModelSending.setPhone_font("Arima Madurai");
                        editUserModelSending.setAddress_font("Arima Madurai");
                        editUserModelSending.setEmail_font("Arima Madurai");
                        break;
                    case R.id.font_regular_popin:
                        editUserModelSending.setFont("Popin");
                        editUserModelSending.setFont("Popin");
                        editUserModelSending.setPhone_font("Popin");
                        editUserModelSending.setAddress_font("Popin");
                        editUserModelSending.setEmail_font("Popin");
                        break;
                }
            });
        } else if (model.getValue().equalsIgnoreCase("FONT 1 - TITLE")) {
            holder.radiogroup.setOnCheckedChangeListener((group, checkedId) -> {
                switch (checkedId) {
                    case R.id.font_regular_ArimaMadurai:
                        // do operations specific to this selection
                        editUserModelSending.setFont("Arima Madurai");
                        editUserModelSending.setOccupation_font("Arima Madurai");
                        editUserModelSending.setName_font("Arima Madurai");
                        break;
                    case R.id.font_regular_popin:
                        // do operations specific to this selection
                        editUserModelSending.setFont("Popin");
                        editUserModelSending.setOccupation_font("Popin");
                        editUserModelSending.setName_font("Popin");
                        break;
                }
            });

        } else if (model.getValue().equalsIgnoreCase("FONT 3 - BANNER")) {
            holder.radiogroup.setOnCheckedChangeListener((group, checkedId) -> {
                switch (checkedId) {
                    case R.id.font_regular_ArimaMadurai:
                        // do operations specific to this selection
                        editUserModelSending.setFont("Arima Madurai");
                        editUserModelSending.setNotes_font("Arima Madurai");


                        break;
                    case R.id.font_regular_popin:
                        // do operations specific to this selection
                        editUserModelSending.setFont("Popin");
                        editUserModelSending.setNotes_font("Popin");
                        break;
                }
            });

        }

        holder.txtTitle.setOnClickListener(v -> onItemClick.onItemClick(holder.getAdapterPosition(), v, model));
        holder.imgEdit.setOnClickListener(v -> {
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            holder.contOpened.setVisibility(View.VISIBLE);
            holder.contEdit.setVisibility(View.GONE);
        });
        holder.imgTick.setOnClickListener(v -> {
            onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            holder.contOpened.setVisibility(View.GONE);
            holder.contEdit.setVisibility(View.VISIBLE);
        });
        holder.imgcross.setOnClickListener(v -> {
//                onItemClick.onItemClick(holder.getAdapterPosition(), v, model);
            holder.contOpened.setVisibility(View.GONE);
            holder.contEdit.setVisibility(View.VISIBLE);
        });

        holder.txtfontsize.setOnClickListener(v -> holder.spFontSize.performClick());
        holder.imgFontSize.setOnClickListener(v -> holder.spFontSize.performClick());

        arrType.clear();

        arrType.add("8px");
        arrType.add("10px");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity,
                R.layout.item_spinner1, arrType);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spFontSize.setAdapter(adapter);

        holder.spFontSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spSize = String.valueOf(adapterView.getSelectedItem());
                if (model.getValue().equalsIgnoreCase("FONT 2 - INFO")) {
                    editUserModelSending.setSize(spSize);
                    editUserModelSending.setPhone_size(spSize);
                    editUserModelSending.setAddress_size(spSize);
                    editUserModelSending.setEmail_size(spSize);
                } else if (model.getValue().equalsIgnoreCase("FONT 1 - TITLE")) {
                    // do operations specific to this selection
                    editUserModelSending.setSize(spSize);
                    editUserModelSending.setOccupation_size(spSize);
                    editUserModelSending.setName_size(spSize);

                } else if (model.getValue().equalsIgnoreCase("FONT 3 - BANNER")) {
                    editUserModelSending.setSize(spSize);
                    editUserModelSending.setNotes_size(spSize);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public FontModel getItem(int position) {
        return arrayList.get(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        AnyTextView txtTitle;
        @BindView(R.id.imgEdit)
        ImageView imgEdit;
        @BindView(R.id.contEdit)
        LinearLayout contEdit;
        @BindView(R.id.txtTitleOpend)
        AnyTextView txtTitleOpend;
        @BindView(R.id.spFontSize)
        Spinner spFontSize;
        @BindView(R.id.imgTick)
        ImageView imgTick;
        @BindView(R.id.imgcross)
        ImageView imgcross;
        @BindView(R.id.font_regular_popin)
        AnyRadioButton fontRegularPopin;
        @BindView(R.id.font_regular_ArimaMadurai)
        AnyRadioButton fontRegularArimaMadurai;
        @BindView(R.id.radiogroup)
        RadioGroup radiogroup;
        @BindView(R.id.contOpened)
        LinearLayout contOpened;
        @BindView(R.id.contParent)
        LinearLayout contParent;

        @BindView(R.id.txtfontsize)
        AnyTextView txtfontsize;
        @BindView(R.id.imgFontSize)
        ImageView imgFontSize;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public EditUserModel editUserModelSending = new EditUserModel();

    public void addRadioButtons(int number, ViewHolder holder, FontModel model) {

//        int selectedId = holder.radiogroup.getCheckedRadioButtonId();

        holder.radiogroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.font_regular_ArimaMadurai:
                    // do operations specific to this selection
                    editUserModelSending.setFont("Arima Madurai");
                    break;
                case R.id.font_regular_popin:
                    // do operations specific to this selection
                    editUserModelSending.setFont("Popin");
                    break;
            }
        });
    }

}

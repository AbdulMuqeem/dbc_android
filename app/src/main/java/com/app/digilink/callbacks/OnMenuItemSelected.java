package com.app.digilink.callbacks;

/**
 * Created by muhammadmuzammil on 5/30/2017.
 */

public interface OnMenuItemSelected
{
    void onMenuItemSelect();
}

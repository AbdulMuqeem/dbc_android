package com.app.digilink.callbacks;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(int position, View view, Object object);
}

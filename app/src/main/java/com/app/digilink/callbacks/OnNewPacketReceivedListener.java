package com.app.digilink.callbacks;

public interface OnNewPacketReceivedListener
{
    void onNewPacket(int event, Object data);
}

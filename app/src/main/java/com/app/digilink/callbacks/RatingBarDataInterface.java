package com.app.digilink.callbacks;

public interface RatingBarDataInterface {

    void getRatings(float ratingValue1, float ratingValue2);
}

package com.app.digilink.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;


import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.helperclasses.Helper;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.managers.retrofit.WebServiceFactory;
import com.app.digilink.managers.retrofit.WebServiceProxy;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.digilink.activities.HomeActivity;

import java.util.Random;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.digilink.constatnts.AppConstants.GCM_DATA_OBJECT;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        // // Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        super.onMessageReceived(remoteMessage);
        if (remoteMessage.getData().size() > 0) {
            // // Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            sendNotification(remoteMessage);
//            sendNotification(true,remoteMessage);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            // // Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(RemoteMessage messageBody) {

        Intent intent;
        GcmDataObject gcmDataObject = new GcmDataObject();
        if (SharedPreferenceManager.getInstance(this).getCurrentUser() == null) {
            intent = new Intent(this, HomeActivity.class);
            gcmDataObject.setUserExist(false);
            intent.putExtra(GCM_DATA_OBJECT, gcmDataObject);
        } else {
            intent = new Intent(this, HomeActivity.class);
            gcmDataObject.setMessage(messageBody.getData().get("message"));
            gcmDataObject.setScreenToRedirect(messageBody.getData().get("screenToRedirect"));
            gcmDataObject.setUserExist(true);
            intent.putExtra(GCM_DATA_OBJECT, gcmDataObject);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        Random random = new Random();
        int id = random.nextInt(9999);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, id /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody.getData().get("message"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody.getData().get("message")))
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        serviceCallToken(s);
    }

    private WebServiceProxy apiService;

    private void serviceCallToken(String newToken) {
        apiService = WebServiceFactory.getInstance("");
        Call<WebResponse<Object>> webResponseCall = null;
        webResponseCall = apiService.addToken(newToken);
        Log.d("FCM",newToken);

        try {
            if (Helper.isNetworkConnected(getApplicationContext(), true)) {
                webResponseCall.enqueue(new Callback<WebResponse<Object>>() {
                    @Override
                    public void onResponse(Call<WebResponse<Object>> call, Response<WebResponse<Object>> response) {

                        if (response.body() == null) {
                            return;
//                            callBack.onError("empty response");

                        }

                        if (response.isSuccessful() && response.body().isSuccess()) {
                            return;
//                            if (callBack != null)
//                                callBack.requestDataResponse(response.body());

                        } else if (response.body().noRecordFound()) {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
//                            callBack.onError(message);
                        } else {
                            String message = response.body().message != null ? response.body().message : response.errorBody().toString();
                            UIHelper.showToast(getApplicationContext(), message);
//                            callBack.onError(message);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<Object>> call, Throwable t) {
                        UIHelper.showShortToastInCenter(getApplicationContext(), "Something went wrong, Please check your internet connection.");
//                        dismissDialog();
//                        callBack.onError(null);
                    }
                });
            } else {
//                dismissDialog();
            }

        } catch (Exception e) {
//            dismissDialog();
            e.printStackTrace();

        }
    }


    public interface IRequestWebResponseAnyObjectCallBack {
        void requestDataResponse(WebResponse<Object> webResponse);

        void onError(Object object);
    }

}


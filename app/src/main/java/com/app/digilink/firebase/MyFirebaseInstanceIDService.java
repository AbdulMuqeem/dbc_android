package com.app.digilink.firebase;

import android.util.Log;

import com.app.digilink.managers.SharedPreferenceManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.app.digilink.constatnts.AppConstants;
import com.app.digilink.models.extramodels.InsertRegisteredDeviceModel;
import com.app.digilink.models.extramodels.RegisteredDeviceModel;

import ru.bullyboo.encoder.Encoder;
import ru.bullyboo.encoder.methods.AES;

import static com.app.digilink.constatnts.AppConstants.KEY_FIREBASE_TOKEN_UPDATED;
import static com.app.digilink.constatnts.AppConstants.KEY_REGISTERED_DEVICE;


public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    SharedPreferenceManager prefHelper;


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("NEW_TOKEN",s);


        prefHelper = SharedPreferenceManager.getInstance(getApplicationContext());
        sendRegistrationToServer(s);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        InsertRegisteredDeviceModel object = prefHelper.getObject(Encoder.BuilderAES()
                .method(AES.Method.AES_CBC_PKCS5PADDING)
                .message(AppConstants.KEY_INSERT_REGISTERED_DEVICE)
                .key("qetuoadgjlzcbm") // not necessary
                .keySize(AES.Key.SIZE_128) // not necessary
                .decrypt(), InsertRegisteredDeviceModel.class);
        RegisteredDeviceModel object2 = prefHelper.getObject(Encoder.BuilderAES()
                .method(AES.Method.AES_CBC_PKCS5PADDING)
                .message(KEY_REGISTERED_DEVICE)
                .key("qetuoadgjlzcbm") // not necessary
                .keySize(AES.Key.SIZE_128) // not necessary
                .decrypt(), RegisteredDeviceModel.class);


        if (object == null) {
            object = new InsertRegisteredDeviceModel();
            object.setDevicetoken(token);
            prefHelper.putObject(Encoder.BuilderAES()
                    .method(AES.Method.AES_CBC_PKCS5PADDING)
                    .message(AppConstants.KEY_INSERT_REGISTERED_DEVICE)
                    .key("qetuoadgjlzcbm") // not necessary
                    .keySize(AES.Key.SIZE_128) // not necessary
                    .decrypt(), object);
        } else {
            object.setDevicetoken(token);
            prefHelper.putObject(Encoder.BuilderAES()
                    .method(AES.Method.AES_CBC_PKCS5PADDING)
                    .message(AppConstants.KEY_INSERT_REGISTERED_DEVICE)
                    .key("qetuoadgjlzcbm") // not necessary
                    .keySize(AES.Key.SIZE_128) // not necessary
                    .decrypt(), object);
            prefHelper.putValue(Encoder.BuilderAES()
                    .method(AES.Method.AES_CBC_PKCS5PADDING)
                    .message(KEY_FIREBASE_TOKEN_UPDATED)
                    .key("qetuoadgjlzcbm") // not necessary
                    .keySize(AES.Key.SIZE_128) // not necessary
                    .decrypt(), true);
        }


        if (object2 == null) {
            object2 = new RegisteredDeviceModel();
            object2.setDevicetoken(token);
            prefHelper.putObject(Encoder.BuilderAES()
                    .method(AES.Method.AES_CBC_PKCS5PADDING)
                    .message(KEY_REGISTERED_DEVICE)
                    .key("qetuoadgjlzcbm") // not necessary
                    .keySize(AES.Key.SIZE_128) // not necessary
                    .decrypt(), object2);
        } else {
            object2.setDevicetoken(token);
            prefHelper.putObject(Encoder.BuilderAES()
                    .method(AES.Method.AES_CBC_PKCS5PADDING)
                    .message(KEY_REGISTERED_DEVICE)
                    .key("qetuoadgjlzcbm") // not necessary
                    .keySize(AES.Key.SIZE_128) // not necessary
                    .decrypt(), object);
        }


          Log.d(TAG, "sendRegistrationToServer: " + "---------" + token);
    }


}
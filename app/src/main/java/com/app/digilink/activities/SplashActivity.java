package com.app.digilink.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.app.digilink.R;
import com.app.digilink.constatnts.AppConstants;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.widget.AnyTextView;

import androidx.appcompat.app.AppCompatDelegate;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;
import static com.app.digilink.constatnts.AppConstants.FILENAME_LANG;
import static com.app.digilink.constatnts.AppConstants.KEY_DARK_THEME;

public class SplashActivity extends Activity {
    private final int ANIMATIONS_DELAY = 1000;

    private final int SPLASH_TIME_OUT = 500;
    @BindView(R.id.contParentLayout)
    LinearLayout contParentLayout;
    SharedPreferenceManager sharedPreferenceManager;
    boolean isUserLoggedIn = false;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.txtAppVersion)
    AnyTextView txtAppVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        isUserLoggedIn = sharedPreferenceManager.getCurrentUser() != null;
//
        if (sharedPreferenceManager.getString(KEY_DARK_THEME)!= null && sharedPreferenceManager.getString(KEY_DARK_THEME).equalsIgnoreCase("Dark")) {
            {
                AppCompatDelegate
                        .setDefaultNightMode(
                                AppCompatDelegate
                                        .MODE_NIGHT_YES);
            }
            this.setTheme(R.style.DarkTheme);
        } else {
            {
                AppCompatDelegate
                        .setDefaultNightMode(
                                AppCompatDelegate
                                        .MODE_NIGHT_NO);
            }
            this.setTheme(R.style.LightTheme);
        }
//
// fixing portrait mode problem for SDK 26 if using windowIsTranslucent = true
        if (Build.VERSION.SDK_INT == 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animateLayout(SPLASH_TIME_OUT);
            }
        }, ANIMATIONS_DELAY);


    }


    private void animateLayout(int SPLASH_TIME_OUT) {


        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1000);
        alphaAnimation.setRepeatCount(1);
//        alphaAnimation.setRepeatMode(Animation.REVERSE);
        imageView2.startAnimation(alphaAnimation);


        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //TODO: Run when animation start
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //TODO: Run when animation end

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        Intent i;

                        if (sharedPreferenceManager.getCurrentUser() != null && sharedPreferenceManager.getCurrentUser().getId() != 0) {
                            i = new Intent(SplashActivity.this, HomeActivity.class);
                            sharedPreferenceManager.setGuestUser(false);
                        } else {
//                            sharedPreferenceManager.putValue(FILENAME_LANG, "en");
                            sharedPreferenceManager.setGuestUser(true);
                            UserModel userModel = new UserModel();
                            userModel.setId(0);

                            sharedPreferenceManager.putObject(AppConstants.KEY_CURRENT_USER_MODEL, userModel);
                            i = new Intent(SplashActivity.this, HomeActivity.class);

                        }
//                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        i.addFlags(FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                        // close this activity
                        finish();
                    }
                }, 1000);


            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //TODO: Run when animation repeat
            }
        });
    }


}

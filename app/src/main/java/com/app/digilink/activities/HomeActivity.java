package com.app.digilink.activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.app.digilink.R;
import com.app.digilink.constatnts.WebServiceConstants;
import com.app.digilink.fragments.HomeFragment;
import com.app.digilink.fragments.LoginFragment;
import com.app.digilink.fragments.NotificationFragment;
import com.app.digilink.fragments.SpecificUserFragment;
import com.app.digilink.fragments.WalletCardsFragment;
import com.app.digilink.helperclasses.ui.helper.UIHelper;
import com.app.digilink.managers.SharedPreferenceManager;
import com.app.digilink.managers.retrofit.GsonFactory;
import com.app.digilink.managers.retrofit.WebServices;
import com.app.digilink.models.User.UserModel;
import com.app.digilink.models.sendingmodels.SearchModel;
import com.app.digilink.models.wrappers.WebResponse;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.installations.FirebaseInstallations;
import com.google.firebase.installations.InstallationTokenResult;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import io.reactivex.annotations.Nullable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

import static com.app.digilink.constatnts.AppConstants.EMAIL_ADDRESS;
import static com.app.digilink.constatnts.AppConstants.KEY_CURRENT_USER_MODEL;
import static com.app.digilink.constatnts.AppConstants.PCODE;


public class HomeActivity extends BaseActivity {


    RelativeLayout contParentActivityLayout;
    private UserModel userModel;
    private SharedPreferenceManager sharedPreferenceManager;
    private String newToken;
    private Uri data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contParentActivityLayout = findViewById(R.id.contParentActivityLayout);
        sharedPreferenceManager = SharedPreferenceManager.getInstance(this);
        userModel = sharedPreferenceManager.getCurrentUser();
//        setTheme(R.style.LightTheme);
//        PendingIntent pendingIntent = new NavDeepLinkBuilder(this)
//                .setGraph(R.navigation.nav_graph)
//                .setDestination(R.id.android)
//                .setArguments(args)
//                .createPendingIntent();

        Intent intent = getIntent();
        String action = intent.getAction();
        data = intent.getData();


    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

//        UserModel userModel = SharedPreferenceManager.getInstance(this).getObject(AppConstants.KEY_CURRENT_USER_MODEL, UserModel.class);
        initFragments(/*UserTypeEnum.fromCanonicalForm(userModel.getCssausermodel().getRole())*/);
    }

    public RelativeLayout getContParentActivityLayout() {
        return contParentActivityLayout;
    }

    @Override
    protected int getViewId() {
        return R.layout.activity_home;
    }

    @Override
    protected int getTitlebarLayoutId() {
        return R.id.titlebar;
    }

    @Override
    protected int getDockableFragmentId() {
        return R.id.contMain;
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    // FIXME: 26/07/2018 Use Enum UserType instead of String
    private void initFragments() {
//        FirebaseInstallations.getInstance().getToken(true).addOnCompleteListener(task -> newToken =  task.getResult().getToken())
        ;
//        serviceCall();
        if (data != null && Objects.requireNonNull(data.getPath()).contains("invite")) {
            addDockableFragment(NotificationFragment.newInstance(), false);
        } else
            addDockableFragment(HomeFragment.newInstance(), false);


    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("tag", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("tag", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("tag", "printHashKey()", e);
        }
    }


    @Override
    public void onBackPressed() {
        if (isInSelectingState) {
            if (genericClickableInterface != null) {
                genericClickableInterface.click();
                isInSelectingState = false;
            } else {
                UIHelper.showToast(getBaseContext(), "No call back selected.");
            }
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            closeApp();
        }
    }

//    private void serviceCall() {
//        SearchModel model = new SearchModel();
//        model.setEmail(sharedPreferenceManager.getString(EMAIL_ADDRESS));
//        model.setPassword(sharedPreferenceManager.getString(PCODE));
//        model.setDevicetoken(newToken);
//        new WebServices(this,
//                "", true)
//                .webServiceRequestAPIAnyObject(WebServiceConstants.METHOD_USER_LOGIN, model,
//                        new WebServices.IRequestWebResponseAnyObjectCallBack() {
//                            @Override
//                            public void requestDataResponse(WebResponse<Object> webResponse) {
//                                UserModel userModel = GsonFactory.getSimpleGson()
//                                        .fromJson(GsonFactory.getSimpleGson().toJson(webResponse.result), UserModel.class);
//
//                                sharedPreferenceManager.putObject(KEY_CURRENT_USER_MODEL, userModel);
//
//                            }
//
//                            @Override
//                            public void onError(Object object) {
//                            }
//                        });
//    }

}
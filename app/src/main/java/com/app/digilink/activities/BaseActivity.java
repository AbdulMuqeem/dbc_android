package com.app.digilink.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;


import android.util.Pair;
import android.view.View;

import com.app.digilink.BaseApplication;
import com.app.digilink.R;
import com.app.digilink.fragments.abstracts.BaseFragment;
import com.app.digilink.fragments.abstracts.GenericClickableInterface;
import com.app.digilink.helperclasses.ui.helper.NavigationBar;
import com.app.digilink.helperclasses.ui.helper.TitleBar;
import com.app.digilink.helperclasses.ui.helper.UIHelper;

import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public abstract class BaseActivity extends AppCompatActivity {


    protected TitleBar titleBar;
    public BaseFragment baseFragment;
    public GenericClickableInterface genericClickableInterface;
    public boolean isInSelectingState = false;
    protected NavigationBar navigationbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getViewId());
        setAndBindTitleBar();

//        setAndBindNavigationBar();
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    protected abstract int getViewId();

    protected abstract int getTitlebarLayoutId();

    //    protected abstract int getNavigationLayoutId();
    protected abstract int getDockableFragmentId();

//    public NavigationBar getNavigationbar() {
//        return this.navigationbar;
//    }

    private void setAndBindTitleBar() {
        titleBar = (TitleBar) findViewById(getTitlebarLayoutId());
        titleBar.setVisibility(View.GONE);
        titleBar.resetViews();
    }

    //    private void setAndBindNavigationBar() {
//        navigationbar = (NavigationBar) findViewById(getNavigationLayoutId());
////        navigationbar.setVisibility(View.GONE);
//        //   navigationBar.resetNvigationBar();
//    }
    @Override
    protected void onResume() {
        super.onResume();

//        if(DeviceUtils.isRooted(getApplicationContext())){
//        showAlertDialogAndExitApp("This application is not supported on rooted devices. Please restore or update your device");
//        }
    }



    public void showAlertDialogAndExitApp(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(BaseActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }

    public void closeApp() {
        UIHelper.showAlertDialog(getResources().getString(R.string.close_application), "Quit",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        dialog.dismiss();
                    }
                }
                , this);


    }

    public void addDockableFragment(Fragment fragment, boolean isTransition) {
        baseFragment = (BaseFragment) fragment;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (isTransition) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        }
        fragmentTransaction.replace(getDockableFragmentId(), fragment).addToBackStack(fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public void replaceDockableFragment(Fragment fragment, boolean isTransition) {
        baseFragment = (BaseFragment) fragment;
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        if (isTransition) {
//            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
//        }
//        fragmentTransaction.replace(getDockableFragmentId(), fragment).commitAllowingStateLoss();
//        Fragment currentFragment = getFragmentManager().findFragmentByTag("YourFragmentTag");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.detach(baseFragment);
        fragmentTransaction.attach(baseFragment);
        fragmentTransaction.commit();
    }


    public TitleBar getTitleBar() {
        return titleBar;
    }


    public void openActivity(Class<?> tClass) {
        Intent i = new Intent(this, tClass);
        startActivity(i);
    }


    public void openImagePreviewActivity(String url, String title) {
//        Intent i = new Intent(this, ImagePreviewActivity.class);
//        i.putExtra(IMAGE_PREVIEW_TITLE, title);
//        i.putExtra(IMAGE_PREVIEW_URL, url);
//        startActivity(i);
    }

//    public void openActivity(Class<?> tClass, UserTypeEnum userTypeEnum) {
//        Intent i = new Intent(this, tClass);
//        i.putExtra(KEY_JSON_STRING, userTypeEnum);
//        startActivity(i);
//    }
//

    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    public void clearAllActivitiesExceptThis(Class<?> cls) {
        Intent intents = new Intent(this, cls);
        intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intents);
        finish();
    }


    public void emptyBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm == null) return;
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popBackStack() {
        if (getSupportFragmentManager() == null) {
            return;
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    public void popStackTill(int stackNumber) {
        FragmentManager fm = getSupportFragmentManager();
        if (fm == null) return;
        for (int i = stackNumber; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack();
        }
    }

    public void popStackTill(String tag) {
        FragmentManager fm = getSupportFragmentManager();
        if (fm == null) {
            return;
        }

        int backStackEntryCount = fm.getBackStackEntryCount();
        for (int i = backStackEntryCount - 1; i > 0; i--) {
            if (fm.getBackStackEntryAt(i).getName().equalsIgnoreCase(tag)) {
                return;
            } else {
                fm.popBackStack();
            }
        }
    }

    public void notifyToAll(int event, Object data) {
        BaseApplication.getPublishSubject().onNext(new Pair<>(event, data));
    }

    public void refreshFragment(BaseFragment fragment) {
        popBackStack();
        addDockableFragment(fragment, false);

    }

    public void refreshFragment1(BaseFragment fragment) {
        addDockableFragment(fragment, false);

    }

    public void setGenericClickableInterface(GenericClickableInterface genericClickableInterface) {
        this.genericClickableInterface = genericClickableInterface;
    }
//    public void replacePermanentFramgment(Fragment fragment, boolean isTransition) {
//        baseFragment = (BaseFragment) fragment;
//        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//        if (isTransition) {
//            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
//        }
//        fragmentTransaction.replace(getPermanentViewId(), fragment).addToBackStack(fragment.getClass().getSimpleName())
//                .commit();
//    }


}
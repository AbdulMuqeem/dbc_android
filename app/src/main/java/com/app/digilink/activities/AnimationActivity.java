package com.app.digilink.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

import com.app.digilink.helperclasses.gif.GifWebView;

import java.io.IOException;
import java.io.InputStream;

@SuppressLint("Registered")
public class AnimationActivity extends Activity {
/** Called when the activity is first created. */
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    InputStream stream = null;
    try {
        stream = getAssets().open("load02.gif");
    } catch (IOException e) {
        e.printStackTrace();
    }
          GifWebView view = new GifWebView(this, "gif/Loader_0.2_sec.gif");

    setContentView(view);
}
}
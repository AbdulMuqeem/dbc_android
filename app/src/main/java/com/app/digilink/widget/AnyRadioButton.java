package com.app.digilink.widget;

import android.content.Context;
import android.util.AttributeSet;


import com.app.digilink.utility.Utils;

import androidx.appcompat.widget.AppCompatRadioButton;


public class AnyRadioButton extends AppCompatRadioButton {



    public AnyRadioButton(Context context) {
        super(context);
    }

    public AnyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        Utils.setTypefaceRadio(attrs, this);
    }

    public AnyRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Utils.setTypefaceRadio(attrs, this);
    }





}
